﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		public Int32 CalSc(int ppm, int ref_ppm)
		{
			Int32 val = 0;
			int offset = 0;

			offset = ppm - ref_ppm;

			if (offset > 0)
			{
				offset = Math.Abs(offset);
				if (offset > 3000)
				{
					val = -70000;
				}
				else if ((offset < 3000) && (offset >= 1000))
				{
					val = -40000;
				}
				else if ((offset < 1000) && (offset >= 500))
				{
					val = -20000;
				}
				else if ((offset < 500) && (offset >= 300))
				{
					val = -10000;
				}
				else if ((offset < 300) && (offset >= 100))
				{
					val = -3000;
				}
				else if (offset < 100)
				{
					val = -1000;
				}
				else
				{
					val = -100;
				}
				CAL_LOG("ppm " + ppm + ", ref " + ref_ppm + ", ret " + val + "\n");
				return val;
			}
			else
			{
                offset = Math.Abs(offset);        // louiey, 2018.03.15 ppm adjust시에 시간이 많이 걸리는 부분 살펴보다 빠진것 같아 추가함
                                                    // 이전으로 되돌리기 위해서 block
				if (offset > 3000)
				{
					val = 70000;
				}
				else if ((offset < 3000) && (offset >= 1000))
				{
					val = 40000;
				}
				else if ((offset < 1000) && (offset >= 500))
				{
					val = 20000;
				}
				else if ((offset < 500) && (offset >= 300))
				{
					val = 10000;
				}
				else if ((offset < 300) && (offset >= 100))
				{
					val = 3000;
				}
				else if (offset < 100)
				{
					val = 1000;
				}
				else
				{
					val = 100;
				}
				CAL_LOG("ppm " + ppm + ", ref " + ref_ppm + ", ret " + val + "\n");
				return val;
			}
		}

		public Int32 CalGain(int ppm, int ref_ppm)
		{
			Int32 val = 0;
			int offset = 0;

			//offset = ppm - ref_ppm;
			offset = ref_ppm - ppm;
#if false
			val = 16 * offset - 1558;	// CO2_CAL_JIG_V3.xlsx Gain Data sheet에서 추세선 참고
#else

#endif
			if(offset < 0)	// ref 가 작으면
			{
				offset = Math.Abs(offset);
				if (offset > 2000)
					val = -25000;
				else if ((offset < 2000) && (offset > 1000))
					val = -10000;
				else if ((offset < 1000) && (offset > 500))
					val = -6000;
				else if ((offset < 500) && (offset > 100))
					val = -1000;
			}
			else
			{
				if (offset > 2000)
					val = 25000;
				else if ((offset < 2000) && (offset > 1000))
					val = 10000;
				else if ((offset < 1000) && (offset > 500))
					val = 6000;
				else if ((offset < 500) && (offset > 100))
					val = 1000;
			}

			CAL_LOG("ppm " + ppm + ", ref " + ref_ppm + ", ret gain " + val + "\n");

			return val;
		}

		public void AllocQueBuffer()
		{
			int max_buffer = 10;

			if (Tps == null)
				Tps = new TpsHandler[MAX_MODULE_NUM];

			for (int i = 0; i < MAX_MODULE_NUM; i++)
			{
				Tps[i].dC_Q = new QueBuffer<UInt32>(max_buffer);
				Tps[i].dR_Q = new QueBuffer<UInt32>(max_buffer);
				Tps[i].sC_Q = new QueBuffer<UInt32>(max_buffer);
				Tps[i].sR_Q = new QueBuffer<UInt32>(max_buffer);
				Tps[i].TS_Q = new QueBuffer<byte>(max_buffer);
				Tps[i].Temp_Q = new QueBuffer<byte>(max_buffer);
				Tps[i].ppm_Q = new QueBuffer<UInt16>(max_buffer);
				Tps[i].flag_Q = new QueBuffer<int>(max_buffer);
			}
		}

		public void FreeQueBuffer()
		{
			Tps = null;
		}

		public UInt32 GetAvgDC(int channel)
		{
			UInt32 avg = 0;
			UInt32 num = (UInt32)Tps[channel].dC_Q.GetMax();
            UInt32 errorNum = 0, value = 0;

            for (int i = 0; i < num; i++)
			{
                value = Tps[channel].dC_Q.DeQue(i);
                if ((value == 0) || (value > 5000000))
                {
                    errorNum++;
                }
                else
                {
                    avg += value;
                }
                //avg += Tps[channel].dC_Q.DeQue(i);
                //CAL_LOG("GetAvgDc : " + Tps[channel].dC_Q.DeQue(i) + "\n");
            }

			if (avg == 0)
				return 0;

            num = num - errorNum;
            //CAL_LOG("dC Total " + avg + "\n");

            return (avg / num);
		}

		public UInt32 GetAvgDR(int channel)
		{
			UInt32 avg = 0;
			UInt32 num = (UInt32)Tps[channel].dR_Q.GetMax();
            UInt32 errorNum = 0, value = 0;

			for (int i = 0; i < num; i++)
			{
                value = Tps[channel].dR_Q.DeQue(i);
                //avg += Tps[channel].dR_Q.DeQue(i);
                if( (value == 0) || (value > 5000000))
                {
                    errorNum++;
                }
                else
                {
                    avg += value;
                }
				//CAL_LOG("GetAvgDR : " + Tps[channel].dR_Q.DeQue(i) + "\n");
			}

			if (avg == 0)
				return 0;

            num = num - errorNum;
			//CAL_LOG("dR Total " + avg + "\n");

			return (avg / num);
		}

		public void SetPpmCheckBox(byte channel)
		{
			switch(channel)
			{
				case 0:
					if (checkBoxM0.InvokeRequired)
					{
						checkBoxM0.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM0.Checked = true;
						}));
					}
					else
					{
						checkBoxM0.Checked = true;
					}
					
					break;

				case 1:
					if (checkBoxM1.InvokeRequired)
					{
						checkBoxM1.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM1.Checked = true;
						}));
					}
					else
					{
						checkBoxM1.Checked = true;
					}
					break;

				case 2:
					if (checkBoxM2.InvokeRequired)
					{
						checkBoxM2.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM2.Checked = true;
						}));
					}
					else
					{
						checkBoxM2.Checked = true;
					}
					break;

				case 3:
					if (checkBoxM3.InvokeRequired)
					{
						checkBoxM3.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM3.Checked = true;
						}));
					}
					else
					{
						checkBoxM3.Checked = true;
					}
					break;

				case 4:
					if (checkBoxM4.InvokeRequired)
					{
						checkBoxM4.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM4.Checked = true;
						}));
					}
					else
					{
						checkBoxM4.Checked = true;
					}
					break;


				case 5:
					if (checkBoxM5.InvokeRequired)
					{
						checkBoxM5.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM5.Checked = true;
						}));
					}
					else
					{
						checkBoxM5.Checked = true;
					}
					break;


				case 6:
					if (checkBoxM6.InvokeRequired)
					{
						checkBoxM6.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM6.Checked = true;
						}));
					}
					else
					{
						checkBoxM6.Checked = true;
					}
					break;


				case 7:
					if (checkBoxM7.InvokeRequired)
					{
						checkBoxM7.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM7.Checked = true;
						}));
					}
					else
					{
						checkBoxM7.Checked = true;
					}
					break;


				case 8:
					if (checkBoxM8.InvokeRequired)
					{
						checkBoxM8.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM8.Checked = true;
						}));
					}
					else
					{
						checkBoxM8.Checked = true;
					}
					break;


				case 9:
					if (checkBoxM9.InvokeRequired)
					{
						checkBoxM9.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM9.Checked = true;
						}));
					}
					else
					{
						checkBoxM9.Checked = true;
					}
					break;


				case 10:
					if (checkBoxM10.InvokeRequired)
					{
						checkBoxM10.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM10.Checked = true;
						}));
					}
					else
					{
						checkBoxM10.Checked = true;
					}
					break;


				case 11:
					if (checkBoxM11.InvokeRequired)
					{
						checkBoxM11.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM11.Checked = true;
						}));
					}
					else
					{
						checkBoxM11.Checked = true;
					}
					break;


				case 12:
					if (checkBoxM12.InvokeRequired)
					{
						checkBoxM12.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM12.Checked = true;
						}));
					}
					else
					{
						checkBoxM12.Checked = true;
					}
					break;


				case 13:
					if (checkBoxM13.InvokeRequired)
					{
						checkBoxM13.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM13.Checked = true;
						}));
					}
					else
					{
						checkBoxM13.Checked = true;
					}
					break;


				case 14:
					if (checkBoxM14.InvokeRequired)
					{
						checkBoxM14.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM14.Checked = true;
						}));
					}
					else
					{
						checkBoxM14.Checked = true;
					}
					break;


				case 15:
					if (checkBoxM15.InvokeRequired)
					{
						checkBoxM15.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM15.Checked = true;
						}));
					}
					else
					{
						checkBoxM15.Checked = true;
					}
					break;


				case 16:
					if (checkBoxM16.InvokeRequired)
					{
						checkBoxM16.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM16.Checked = true;
						}));
					}
					else
					{
						checkBoxM16.Checked = true;
					}
					break;


				case 17:
					if (checkBoxM17.InvokeRequired)
					{
						checkBoxM17.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM17.Checked = true;
						}));
					}
					else
					{
						checkBoxM17.Checked = true;
					}
					break;


				case 18:
					if (checkBoxM18.InvokeRequired)
					{
						checkBoxM18.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM18.Checked = true;
						}));
					}
					else
					{
						checkBoxM18.Checked = true;
					}
					break;


				case 19:
					if (checkBoxM19.InvokeRequired)
					{
						checkBoxM19.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxM19.Checked = true;
						}));
					}
					else
					{
						checkBoxM19.Checked = true;
					}
					break;
			}
		}

        public UInt16 GetAvailableModuleNum()
        {
            UInt16 num = 0;

            if (checkBoxTps0.Checked) { num++; }
            if (checkBoxTps1.Checked) { num++; }
            if (checkBoxTps2.Checked) { num++; }
            if (checkBoxTps3.Checked) { num++; }
            if (checkBoxTps4.Checked) { num++; }
            if (checkBoxTps5.Checked) { num++; }
            if (checkBoxTps6.Checked) { num++; }
            if (checkBoxTps7.Checked) { num++; }
            if (checkBoxTps8.Checked) { num++; }
            if (checkBoxTps9.Checked) { num++; }
            if (checkBoxTps10.Checked) { num++; }
            if (checkBoxTps11.Checked) { num++; }
            if (checkBoxTps12.Checked) { num++; }
            if (checkBoxTps13.Checked) { num++; }
            if (checkBoxTps14.Checked) { num++; }
            if (checkBoxTps15.Checked) { num++; }
            if (checkBoxTps16.Checked) { num++; }
            if (checkBoxTps17.Checked) { num++; }
            if (checkBoxTps18.Checked) { num++; }
            if (checkBoxTps19.Checked) { num++; }

            return num;
        }
	}
}
