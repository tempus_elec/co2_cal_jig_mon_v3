﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		TpsHandler[] Tps;
		UInt32 sC, sR;

		AutoResetEvent calEvent;
        AutoResetEvent calResEvent;

        const int CAL_EN_SET_DEFAULT = 0x20;
		const int CAL_EN_DC_DR_ADJ = 0x21;
		const int CAL_EN_TS_ADJ = 0x22;
		const int CAL_EN_PPM_ADJ = 0x23;
		const int CAL_EN_RATIO_ADJ = 0x24;
		const int CAL_EN_GAIN_ADJ = 0x25;
        const int CAL_EN_AUTO_CALIBRATION = 0x26;

        const int DC_MAX = 3000000;
		const int DC_MIN = 600000;
		const int DR_MAX = 3000000;
		const int DR_MIN = 600000;
		const int TS_MAX = 20;
		const int TS_MIN = 0;

		private object lockObject = null;

		bool calStopFlag = false;

        Thread tAutoCal = null;

        public void CalibrationStart(int mode)
		{
			Thread t1 = null;
			
			if (lockObject == null)
				lockObject = new object();

			switch (mode)
			{
				case CAL_EN_SET_DEFAULT:
					t1 = new Thread(new ThreadStart(SetToDefault));
					break;

				case CAL_EN_DC_DR_ADJ:
					t1 = new Thread(new ThreadStart(DcDrAdjusting));
					break;

				case CAL_EN_TS_ADJ:
					t1 = new Thread(new ThreadStart(TsAdjust));
					break;

				case CAL_EN_PPM_ADJ:
					t1 = new Thread(new ThreadStart(PpmAdjust));
					break;

				case CAL_EN_RATIO_ADJ:
					break;

				case CAL_EN_GAIN_ADJ:
					t1 = new Thread(new ThreadStart(GainAdjust));
					break;

                case CAL_EN_AUTO_CALIBRATION:
                    //t1 = new Thread(new ThreadStart(AutoCalibration));
                    break;

                default:
					break;
			}

			t1.Start();
		}
		
		public void DcDrAdjusting()
		{
			int count = 1;
			UInt16 CAL_READ_ALL_COUNT = 10;
			UInt16 delayTime = 5000;
			byte channel = 0, channel_max = 0;
            bool respMsg = false;

			if (lockObject == null)
				lockObject = new object();

			lock (lockObject)
			{
				if (calEvent == null)
					calEvent = new AutoResetEvent(false);

                if (calResEvent == null)
                    calResEvent = new AutoResetEvent(false);

                CAL_LOG("##########################\n");
				CAL_LOG("DcDrAdjusting is starting...\n");

				AllocQueBuffer();

				/* STEP 1 */
				{
					CAL_LOG("### STEP 1\n");
					CAL_LOG("Stopping Monitoring Flag...\n\n");
					CdcSendCmd(MSG_MON_FLAG, 2, 0, 0, 0, 0);
					Thread.Sleep(2000);
				}

				{
					if ( (checkBoxArrayPcb0.Checked == true) && (checkBoxArrayPcb1.Checked == true) )
					{
						channel = 0; channel_max = 20;
					}
					else if (checkBoxArrayPcb0.Checked == true)
					{
						channel = 0;
						channel_max = 10;
					}
					else if (checkBoxArrayPcb1.Checked == true)
					{
						channel = 10; channel_max = 20;
					}
				}

				/* STEP 2 */
				{
					CAL_LOG("STEP 2\n");
					CAL_LOG("Ref. Adjusting....\n");
					CAL_LOG(CAL_READ_ALL_COUNT + " times averaging with " + delayTime + " msec delay\n");
					for (UInt16 i = 0; i < CAL_READ_ALL_COUNT; i++)
					{
						CAL_LOG("Read Tempus all..." + count++ + " of " + CAL_READ_ALL_COUNT + "\n");
						CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
                        //Thread.Sleep(2500);
                        respMsg = calEvent.WaitOne(90000);
                        if(respMsg == false)
                        {
                            CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                            Thread.Sleep(2000);
                        }
                        Thread.Sleep(1000);
                    }

                    for (byte i = channel; i < channel_max; i++)
					{
#if true
						sC = GetAvgDC(i) - 25000;
						sR = GetAvgDR(i) - 25000;	// louiey, 2018.01.09. ppm 값을 높이기 위해서 15000을 빼줌
#else                                               // kiuoey, 2018.03.15. ppm fluctuation이 높아서수정해보았는데 잘 안맞는 듯...
                        sC = GetAvgDC(i) + 10000;
                        sR = GetAvgDR(i);
#endif
						CAL_LOG("CH " + i + " dC avg : " + sC + ", dR avg : " + sR + "\n");

						if (sC != 0)
						{
							if (!badList[i])
                            {
                                SetSc(sC, i);
                                respMsg = calResEvent.WaitOne(5000);
                                if (respMsg == false)
                                {
                                    CAL_LOG("SetSc timeout...\n");
                                    Thread.Sleep(2000);
                                }
                            }
                            Thread.Sleep(500);
						}

						if (sR != 0)
						{
							if (!badList[i])
                            {
                                SetSr(sR, i);
                                respMsg = calResEvent.WaitOne(5000);
                                if (respMsg == false)
                                {
                                    CAL_LOG("SetSr timeout...\n");
                                    Thread.Sleep(2000);
                                }
                            }
							Thread.Sleep(500);
						}

						if (calStopFlag)
						{
							FreeQueBuffer();
							calEvent = null;
                            calResEvent = null;
                            CAL_LOG(CallerName() + " Stopping Cal...\n");
							calStopFlag = false;
							return;
						}
					}
                    CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
                    respMsg = calEvent.WaitOne(90000);
                    if (respMsg == false)
                    {
                        CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                        Thread.Sleep(2000);
                    }
                }

				{
                    //Thread.Sleep(2500);
                    //CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
					FreeQueBuffer();
					calEvent = null;
                    calResEvent = null;
                }

				/* STEP 3 */
				{

				}
				CAL_LOG("DcDrAdjusting is ending.....\n");
				CAL_LOG("##########################\n\n");
			}
		}

		public void TsAdjust()
		{
			byte ts, temp;
			byte sub;

			lock (lockObject)
			{
				if (calEvent == null)
					calEvent = new AutoResetEvent(false);

                if (calResEvent == null)
                    calResEvent = new AutoResetEvent(false);

                AllocQueBuffer();

				CAL_LOG("#####################\n");
				CAL_LOG("TS Adjust Starting...\n");

				while (true)
				{
					CAL_LOG("Read Tempus All\n");
					CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
                    if (calEvent.WaitOne(90000) == false)
                    {
                        CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                        Thread.Sleep(2000);
                    }
                    Thread.Sleep(1000);

                    CAL_LOG("Check Temp/TS\n");

					for (byte channel = 0; channel < MAX_MODULE_NUM; channel++)
					{
						if (Tps[channel].flag_Q.DeQue(0) != 0x00)
						{
							temp = Tps[channel].Temp_Q.DeQue(0);
							ts = Tps[channel].TS_Q.DeQue(0);
							CAL_LOG("CH " + channel + " " + temp + " ℃ ts " + ts + "\n");

							sub = (byte)(temp - g_Amb);
							ts += sub;
							if (ts < 0) ts = 0;

							if (!badList[channel])
                            {
                                SetTs(ts, channel);
                                if (calResEvent.WaitOne(5000) == false)
                                {
                                    CAL_LOG("SetTs timeout...\n");
                                    Thread.Sleep(2000);
                                }
                            }
							CAL_LOG("CH " + channel + " ts " + ts + " sub " + sub + "\n");
							Thread.Sleep(1000);
						}

						if (calStopFlag)
						{
							FreeQueBuffer();
							calEvent = null;
                            calResEvent = null;
                            CAL_LOG(CallerName() + " Stopping Cal...\n");
							calStopFlag = false;
							return;
						}
					}
					
					CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
                    if (calEvent.WaitOne(90000) == false)
                    {
                        CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                        Thread.Sleep(2000);
                    }
                    Thread.Sleep(2500);

					break;
				}
				CAL_LOG("TS Adjust End...\n");
				CAL_LOG("################\n\n");
				FreeQueBuffer();
				calEvent = null;
                calResEvent = null;
            }
		}

		public void PpmAdjust()
		{
			const int CO2_OFFSET = 50;
			const int DELAY = 30000;

			bool loop = true;
			int count = 0;
			UInt16 ppm = 0;
			int ppm_offset = 0;
			UInt32 dc = 0;
			Int32 temp_dc = 0;
			int max_channel = MAX_MODULE_NUM;
			//int max_channel = 0;
			int reachedMax = 0;
			UInt16 availModule = 0;	// Convert.ToUInt16(textBoxModuleNum.Text);
			Module[] m = new Module[max_channel];
			byte channel = 0, channel_max = 0;
			int m_flag = 0;
            
            lock (lockObject)
			{
				if (calEvent == null)
					calEvent = new AutoResetEvent(false);

                if (calResEvent == null)
                    calResEvent = new AutoResetEvent(false);

                AllocQueBuffer();
				ClearModuleCheckBoxes();
				// check available module number
                if(checkBoxArrayPcb0.Checked && checkBoxArrayPcb1.Checked)
                {
                    channel = 0;
                    channel_max = 20;
                }
				else if (checkBoxArrayPcb0.Checked)
				{
					channel = 0;
					channel_max = 10;
				}
				else if (checkBoxArrayPcb1.Checked)
				{
					channel = 10;
					channel_max = 20;
				}
				//////////////////////////////////////////////

				// check available module
				for (int i = channel; i < channel_max; i++)
				{
					m[i].done = false;
				}
#if false
				// check whether text is empty
				if(!string.IsNullOrEmpty(textBoxModuleNum.Text))
				{
					availModule = Convert.ToUInt16(textBoxModuleNum.Text);
				}
				else
				{
					MessageBox.Show("Please input available module number");
					return;
				}
				////////////////////////////////////////////////////////////
#endif
				CAL_LOG("\n\n#####################\n");
				CAL_LOG("PPM Adjust Starting...\n");
								
				/* will try 0 module first */
				while (loop)
				{
                    //availModule = Convert.ToUInt16(textBoxModuleNum.Text);
                    availModule = GetAvailableModuleNum();
                    
                    if (textBoxModuleNum.InvokeRequired)
                    {
                        textBoxModuleNum.Invoke(new MethodInvoker(delegate () { textBoxModuleNum.Text = availModule.ToString(); }));
                    }
                    else
                    {
                        textBoxModuleNum.Text = availModule.ToString();
                    }

                    CAL_LOG("Read Tempus all..." + count++ + "\n");
					CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
                    if (calEvent.WaitOne(90000) == false)
                    {
                        CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                        Thread.Sleep(2000);
                    }
                    Thread.Sleep(1000);

                    // display modules status
                    CAL_LOG("Total : " + channel_max + ", done : " + reachedMax + "\n");
					for (byte i = channel; i < channel_max; i++)
					{
						if(m[i].done == false)
						{
							CAL_LOG("Ch " + i + " on going\n");
						}
						else
						{
							CAL_LOG("Ch " + i + " done\n");
						}
					}
					////////////////////////////////////////////////////////////////////

					for ( byte i = channel; i < channel_max; i++)
					{
						if (m[i].done == false)
						{
							m_flag = Tps[i].flag_Q.DeQue(0);
							//m_flag &= 0x80;
							//if ( (m_flag == 0x80) && ((m_flag & 0x08) != 0x08) )
							if ( ((m_flag & 0x80) == 0x80) && ((m_flag & 0x08) != 0x08) && ((m_flag & 0x10) != 0x10) )
							{
								ppm = Tps[i].ppm_Q.DeQue(Tps[i].ppm_Q.GetCount() - 1);
								ppm_offset = ppm - g_ref_ppm;
#if false
								ppm_offset = Math.Abs(ppm_offset);
								if (ppm_offset < CO2_OFFSET)
#else
								if( ( ppm_offset < CO2_OFFSET) && (ppm_offset > -10) )
#endif
								{
									CAL_LOG("TPS" + i + " reached expected..." + ppm + ", " + g_ref_ppm + "\n");
									m[i].done = true;
									reachedMax++;
									SetPpmCheckBox(i);
								}
								else
								{
									//if (Tps[i].flag_Q.DeQue(i) != 0x00)
									if (Tps[i].flag_Q.DeQue(0) != 0x00)
									{
										CAL_LOG("Tps" + i + " " + ppm + ", Ref " + g_ref_ppm + ", Offset = " + ppm_offset + "\n");
										temp_dc = CalSc(ppm, g_ref_ppm);

										dc = Tps[i].sC_Q.DeQue(Tps[i].ppm_Q.GetCount() - 1);
										dc = (UInt32)(dc + temp_dc);
										CAL_LOG(i + "...Request_sC_Update ==> " + dc + "\n\n");

										if (!badList[i])
                                        {
                                            SetSc(dc, i);
                                            if (calResEvent.WaitOne(5000) == false)
                                            {
                                                CAL_LOG("SetSc timeout...\n");
                                                Thread.Sleep(2000);
                                            }
                                        }
										Thread.Sleep(1000);
									}
									else
									{
										CAL_LOG("Channel " + i + " is not available\n");
									}
								}
#if false
								if (reachedMax == availModule)
								{
									CAL_LOG("PPM adjusted for all modules\n");
									loop = false;
									break;
								}
#endif
							}
							else
							{
								m[i].done = true;
								LOG("Error module " + i + ", skip to next...\n");
							}
						}
						else
						{

						}

						if (reachedMax >= availModule)
						{
							CAL_LOG("PPM adjusted for all modules\n");
							loop = false;
							break;
						}

						if (calStopFlag)
						{
							FreeQueBuffer();
							calEvent = null;
                            calResEvent = null;
                            CAL_LOG(CallerName() + " Stopping Cal...\n");
							calStopFlag = false;
							return;
						}

						Thread.Sleep(1000);
						//CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
						//calEvent.WaitOne();
					}
					Thread.Sleep(DELAY);
				}
                CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
                if (calEvent.WaitOne(90000) == false)
                {
                    CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                    Thread.Sleep(2000);
                }

                CAL_LOG("\n\n#####################\n");
				CAL_LOG("PPM Adjust End...\n");

				FreeQueBuffer();
				calEvent = null;
                calResEvent = null;
            }
		}

		public void GainAdjust()
		{
			const int CO2_OFFSET = 150;
			const int DELAY = 3000;

			bool loop = true;
			int count = 0;
			UInt16 ppm = 0;
			int ppm_offset = 0;

            const UInt32 GAIN_MAX_LIMIT = 100000;

			UInt32 gain = 0;
			Int32 temp_gain = 0;

			int max_channel = MAX_MODULE_NUM;
			//int max_channel = 0;
			int reachedMax = 0;
			UInt16 availModule = 0; // Convert.ToUInt16(textBoxModuleNum.Text);
			Module[] m = new Module[max_channel];
			byte channel = 0, channel_max = 0;
			int m_flag = 0;

			UInt32[] gainBuf = new UInt32[MAX_MODULE_NUM];
			lock (lockObject)
			{
				if (calEvent == null)
					calEvent = new AutoResetEvent(false);

                if (calResEvent == null)
                    calResEvent = new AutoResetEvent(false);

                AllocQueBuffer();
				ClearModuleCheckBoxes();
				// check available module number
                if (checkBoxArrayPcb0.Checked && checkBoxArrayPcb1.Checked)
                {
                    channel = 0;
                    channel_max = 20;
                }
                else if (checkBoxArrayPcb0.Checked)
                {
                    channel = 0;
                    channel_max = 10;
                }
                else if (checkBoxArrayPcb1.Checked)
                {
                    channel = 10;
                    channel_max = 20;
                }
                /*
				if (checkBoxArrayPcb0.Checked)
				{
					channel = 0;
					channel_max = 10;
				}

				if (checkBoxArrayPcb1.Checked)
				{
					channel = 10;
					channel_max = 20;
				}
                */
				//////////////////////////////////////////////

				// check available module
				for (int i = channel; i < channel_max; i++)
				{
					m[i].done = false;
					gainBuf[i] = 50000;	// default GAIN value
				}

				// check whether text is empty
				if (!string.IsNullOrEmpty(textBoxModuleNum.Text))
				{
					availModule = Convert.ToUInt16(textBoxModuleNum.Text);
				}
				else
				{
					MessageBox.Show("Please input available module number");
					return;
				}
				////////////////////////////////////////////////////////////

				CAL_LOG("\n\n#####################\n");
				CAL_LOG("GAIN Adjust Starting...\n");

				/* will try 0 module first */
				while (loop)
				{
					availModule = Convert.ToUInt16(textBoxModuleNum.Text);
					CAL_LOG("Read Tempus all..." + count++ + "\n");
					CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
                    if (calEvent.WaitOne(90000) == false)
                    {
                        CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                        Thread.Sleep(2000);
                    }
                    Thread.Sleep(1000);

					// display modules status
					CAL_LOG("Total : " + channel_max + ", done : " + reachedMax + "\n");
					for (byte i = channel; i < channel_max; i++)
					{
						if (m[i].done == false)
						{
							CAL_LOG("Ch " + i + " on going\n");
						}
						else
						{
							CAL_LOG("Ch " + i + " done\n");
						}
					}
					////////////////////////////////////////////////////////////////////

					for (byte i = channel; i < channel_max; i++)
					{
						if (m[i].done == false)
						{
							m_flag = Tps[i].flag_Q.DeQue(0);

							if (((m_flag & 0x80) == 0x80) && ((m_flag & 0x08) != 0x08) && ((m_flag & 0x10) != 0x10))
							{
								ppm = Tps[i].ppm_Q.DeQue(Tps[i].ppm_Q.GetCount() - 1);
								ppm_offset = ppm - g_ref_ppm;

								ppm_offset = Math.Abs(ppm_offset);
								if (ppm_offset < CO2_OFFSET)
								{
									CAL_LOG("TPS" + i + " reached expected..." + ppm + ", " + g_ref_ppm + "\n");
									m[i].done = true;
									reachedMax++;
									SetPpmCheckBox(i);
								}
								else
								{
									//if (Tps[i].flag_Q.DeQue(i) != 0x00)
									if (Tps[i].flag_Q.DeQue(0) != 0x00)
									{
										CAL_LOG("Tps" + i + " " + ppm + ", Ref " + g_ref_ppm + ", Offset = " + ppm_offset + "\n");
										temp_gain = CalGain(ppm, g_ref_ppm);

										gain = gainBuf[i];	// Tps[i].sC_Q.DeQue(Tps[i].ppm_Q.GetCount() - 1);
										gain = (UInt32)(temp_gain + gain);

										if (gain > GAIN_MAX_LIMIT)
                                        {
                                            gain = GAIN_MAX_LIMIT;
                                            CAL_LOG("Reached at MAX GAIN VALUE...ERROR!!!\n");
                                        }

										CAL_LOG(i + "...Request_Gain_Update ==> " + gain + "\n\n");
										gainBuf[i] = gain;

										if (!badList[i])
                                        {
                                            SetGain(gain, i);
                                            if (calResEvent.WaitOne(5000) == false)
                                            {
                                                CAL_LOG("SetGain timeout...\n");
                                                Thread.Sleep(2000);
                                            }
                                        }
										Thread.Sleep(1000);
									}
									else
									{
										CAL_LOG("Channel " + i + " is not available\n");
									}
								}
#if false
								if (reachedMax == availModule)
								{
									CAL_LOG("GAIN adjusted for all modules\n");
									loop = false;
									break;
								}
#endif
							}
							else
							{
								m[i].done = true;
								LOG("Error module " + i + ", skip to next...\n");
							}
						}
						else
						{

						}

						if (reachedMax >= availModule)
						{
							CAL_LOG("GAIN adjusted for all modules\n");
							loop = false;
							break;
						}

						if (calStopFlag)
						{
							FreeQueBuffer();
							calEvent = null;
                            calResEvent = null;
                            CAL_LOG(CallerName() + " Stopping Cal...\n");
							calStopFlag = false;
							return;
						}

						Thread.Sleep(1000);
					}
					Thread.Sleep(DELAY);
				}
                CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
                if (calEvent.WaitOne(90000) == false)
                {
                    CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                    Thread.Sleep(2000);
                }
                CAL_LOG("\n\n#####################\n");
				CAL_LOG("GAIN Adjust End...\n");

				FreeQueBuffer();
				calEvent = null;
                calResEvent = null;
            }
		}

		public void CalStop()
		{
			calStopFlag = true;
		}

		public void ClearModuleCheckBoxes()
		{
			try
			{
				// PCB ARRAY 0
				checkBoxM0.Checked = false;
				checkBoxM1.Checked = false;
				checkBoxM2.Checked = false;
				checkBoxM3.Checked = false;
				checkBoxM4.Checked = false;
				checkBoxM5.Checked = false;
				checkBoxM6.Checked = false;
				checkBoxM7.Checked = false;
				checkBoxM8.Checked = false;
				checkBoxM9.Checked = false;

				// PCB ARRAY 1
				checkBoxM10.Checked = false;
				checkBoxM11.Checked = false;
				checkBoxM12.Checked = false;
				checkBoxM13.Checked = false;
				checkBoxM14.Checked = false;
				checkBoxM15.Checked = false;
				checkBoxM16.Checked = false;
				checkBoxM17.Checked = false;
				checkBoxM18.Checked = false;
				checkBoxM19.Checked = false;
			}
			catch(Exception ex)
			{
				CAL_LOG(ex.Message);
			}
		}

        public void AutoCalibrationStart()
        {
            if (lockObject == null)
                lockObject = new object();

            tmr2Tick = 0;

            tAutoCal = new Thread(new ThreadStart(AutoCalibration));
            tAutoCal.Start();
            //tAutoCal = null;
        }

        public void AutoCalibration()
        {
            //lock (lockObject)
            {
#if true
                if (calEvent == null)
                    calEvent = new AutoResetEvent(false);

                if (calResEvent == null)
                    calResEvent = new AutoResetEvent(false);
#endif
                timer2.Start();
                AllocQueBuffer();

                CAL_LOG("#####################\n");
                CAL_LOG("Auto Calibration Starting...\n");
                
                CAL_LOG("########## Initialization starts....\n");
                CdcInit();

                if (calEvent.WaitOne(120000) == false)
                {
                    CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                    Thread.Sleep(2000);
                }
                Thread.Sleep(1000);

                CAL_LOG("########## Set To Default\n");
                SetToDefault();

                CAL_LOG("########## DcDr Adjust\n");
                DcDrAdjusting();
                //CalibrationStart(CAL_EN_DC_DR_ADJ);
                ChartClear();
                Chart2Clear();

                CAL_LOG("########## Temp Shift Adjust\n");
                TsAdjust();
                //CalibrationStart(CAL_EN_TS_ADJ);

                CAL_LOG("########## Low Ppm Adjust\n");
                PpmAdjust();
                //CalibrationStart(CAL_EN_PPM_ADJ);

                CAL_LOG("########## Please increase CO2 level and try manually\n");
                ChartClear();
                Chart2Clear();
#if false
                CAL_LOG("Monitor Start...\n");
                if (calEvent == null)
                    calEvent = new AutoResetEvent(false);

                if (calResEvent == null)
                    calResEvent = new AutoResetEvent(false);

                monBtnStatus = false;
                CdcMon();
                if (calEvent.WaitOne(120000) == false)
                {
                    CAL_LOG("CDC_MSG_CMD_GET_ALL timeout...\n");
                    Thread.Sleep(2000);
                }
#endif
                CAL_LOG("\nThe End Of AutoCalibration\n");
                CAL_LOG("################\n\n");
                CAL_LOG("################\n\n");

                FreeQueBuffer();
                calEvent = null;
                calResEvent = null;

                timer2.Stop();
            }
        }
    }
}
