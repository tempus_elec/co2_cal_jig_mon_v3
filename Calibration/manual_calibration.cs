﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		public void SetSc(UInt32 sc, byte channel)
		{
            CAL_LOG(CallerName() + " : " + sc + " , " + channel + "\n");
			if(channel < 10)
			{
				if (checkBoxArrayPcb0.Checked == true)
					CdcSendCmd(CDC_MSG_CMD_SET_SC, channel, (byte)(sc & 0x000000FF), (byte)((sc & 0x0000FF00) >> 8), (byte)((sc & 0x00FF0000) >> 16), (byte)((sc & 0xFF000000) >> 24));
				else
					ERR("ARRAY PCB 0 is unchecked\n");
			}
			else
			{
				if (checkBoxArrayPcb1.Checked == true)
					CdcSendCmd(CDC_MSG_CMD_SET_SC, channel, (byte)(sc & 0x000000FF), (byte)((sc & 0x0000FF00) >> 8), (byte)((sc & 0x00FF0000) >> 16), (byte)((sc & 0xFF000000) >> 24));
				else
					ERR("ARRAY PCB 1 is unchecked\n");
			}
		}

		public void SetSr(UInt32 sr, byte channel)
		{
            CAL_LOG(CallerName() + " : " + sr + " , " + channel + "\n");
            if (channel < 10)
			{
				if (checkBoxArrayPcb0.Checked == true)
					CdcSendCmd(CDC_MSG_CMD_SET_SR, channel, (byte)(sr & 0x000000FF), (byte)((sr & 0x0000FF00) >> 8), (byte)((sr & 0x00FF0000) >> 16), (byte)((sr & 0xFF000000) >> 24));
				else
					ERR("ARRAY PCB 0 is unchecked\n");
			}
			else
			{
				if (checkBoxArrayPcb1.Checked == true)
					CdcSendCmd(CDC_MSG_CMD_SET_SR, channel, (byte)(sr & 0x000000FF), (byte)((sr & 0x0000FF00) >> 8), (byte)((sr & 0x00FF0000) >> 16), (byte)((sr & 0xFF000000) >> 24));
				else
					ERR("ARRAY PCB 1 is unchecked\n");
			}
		}

		public void SetTs(UInt32 ts, byte channel)
		{
            CAL_LOG(CallerName() + " : " + ts + " , " + channel + "\n");
            if (channel < 10)
			{
				if (checkBoxArrayPcb0.Checked == true)
					CdcSendCmd(CDC_MSG_CMD_SET_TS, channel, (byte)(ts & 0x000000FF), (byte)((ts & 0x0000FF00) >> 8), (byte)((ts & 0x00FF0000) >> 16), (byte)((ts & 0xFF000000) >> 24));
				else
					ERR("ARRAY PCB 0 is unchecked\n");
			}
			else
			{
				if (checkBoxArrayPcb1.Checked == true)
					CdcSendCmd(CDC_MSG_CMD_SET_TS, channel, (byte)(ts & 0x000000FF), (byte)((ts & 0x0000FF00) >> 8), (byte)((ts & 0x00FF0000) >> 16), (byte)((ts & 0xFF000000) >> 24));
				else
					ERR("ARRAY PCB 1 is unchecked\n");
			}
		}

		public void SetGain(UInt32 gain, byte channel)
		{
            CAL_LOG(CallerName() + " : " + gain + " , " + channel + "\n");
            if (channel < 10)
			{
				if (checkBoxArrayPcb0.Checked == true)
					CdcSendCmd(CDC_MSG_CMD_SET_GAIN, channel, (byte)(gain & 0x000000FF), (byte)((gain & 0x0000FF00) >> 8), (byte)((gain & 0x00FF0000) >> 16), (byte)((gain & 0xFF000000) >> 24));
				else
					ERR("ARRAY PCB 0 is unchecked\n");
			}
			else
			{
				if (checkBoxArrayPcb1.Checked == true)
					CdcSendCmd(CDC_MSG_CMD_SET_GAIN, channel, (byte)(gain & 0x000000FF), (byte)((gain & 0x0000FF00) >> 8), (byte)((gain & 0x00FF0000) >> 16), (byte)((gain & 0xFF000000) >> 24));
				else
					ERR("ARRAY PCB 1 is unchecked\n");
			}
		}

		public void SetToDefault()
		{
			const int DEFAULT_SC = 1800000;
			const int DEFAULT_SR = 1500000;
			const int DEFAULT_TS = 8;
			const int DEFAULT_GAIN = 50000;
			int delay = 500;

			CAL_LOG("####################\n");
			CAL_LOG("Set To Default Value\n");

			CAL_LOG("####################\n");
			CAL_LOG("Set sC Value\n");
			if (checkBoxArrayPcb0.Checked == true)
			{
				for (byte i = 0; i < 10; i++)
				{
					if(!badList[i])
						SetSc(DEFAULT_SC, i);
					Thread.Sleep(delay);
				}
			}
			else
			{
				CAL_LOG("SET_SC : PCB 0 unchecked\n");
			}
			
			if (checkBoxArrayPcb1.Checked == true)
			{
				for (byte i = 10; i < 20; i++)
				{
					if (!badList[i])
						SetSc(DEFAULT_SC, i);
					Thread.Sleep(delay);
				}
			}
			else
			{
				CAL_LOG("SET_SC : PCB 1 unchecked\n");
			}

			CAL_LOG("####################\n");
			CAL_LOG("Set sR Value\n");
			if (checkBoxArrayPcb0.Checked == true)
			{
				for (byte i = 0; i < 10; i++)
				{
					if (!badList[i])
						SetSr(DEFAULT_SR, i);
					Thread.Sleep(delay);
				}
			}
			else
			{
				CAL_LOG("SET_SR : PCB 0 unchecked\n");
			}

			if (checkBoxArrayPcb1.Checked == true)
			{
				for (byte i = 10; i < 20; i++)
				{
					if (!badList[i])
						SetSr(DEFAULT_SR, i);
					Thread.Sleep(delay);
				}
			}
			else
			{
				CAL_LOG("SET_SR : PCB 1 unchecked\n");
			}

			CAL_LOG("####################\n");
			CAL_LOG("Set TS Value\n");
			if (checkBoxArrayPcb0.Checked == true)
			{
				for (byte i = 0; i < 10; i++)
				{
					if (!badList[i])
						SetTs(DEFAULT_TS, i);
					Thread.Sleep(delay);
				}
			}
			else
			{
				CAL_LOG("SET_TS : PCB 0 unchecked\n");
			}

			if (checkBoxArrayPcb1.Checked == true)
			{
				for (byte i = 0; i < 10; i++)
				{
					if (!badList[i])
						SetTs(DEFAULT_TS, i);
					Thread.Sleep(delay);
				}
			}
			else
			{
				CAL_LOG("SET_TS : PCB 0 unchecked\n");
			}

			CAL_LOG("####################\n");
			CAL_LOG("Set GAIN Value\n");
			if (checkBoxArrayPcb0.Checked == true)
			{
				for (byte i = 0; i < 10; i++)
				{
					if (!badList[i])
						SetGain(DEFAULT_GAIN, i);
					Thread.Sleep(delay);
				}
			}
			else
			{
				CAL_LOG("SET_GAIN : PCB 0 unchecked\n");
			}

			if (checkBoxArrayPcb1.Checked == true)
			{
				for (byte i = 10; i < 20; i++)
				{
					if (!badList[i])
						SetGain(DEFAULT_GAIN, i);
					Thread.Sleep(delay);
				}
			}
			else
			{
				CAL_LOG("SET_GAIN : PCB 1 unchecked\n");
			}

			Thread.Sleep(delay);
			CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
			CAL_LOG("####################\n");
			CAL_LOG("End of Set To Default\n");
		}

		public void SetToWriteAll()
		{

		}
	}
}
