﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		private void ChartInit()
		{
			//////////////////////////////////////////////////////////////
			//	PPM Chart Init
			//////////////////////////////////////////////////////////////
			Series chartSenseair = chartPpm.Series.Add("REF");
			Series chartTempus0 = chartPpm.Series.Add("TEMPUS0");
			Series chartTempus1 = chartPpm.Series.Add("TEMPUS1");
			Series chartTempus2 = chartPpm.Series.Add("TEMPUS2");
			Series chartTempus3 = chartPpm.Series.Add("TEMPUS3");
			Series chartTempus4 = chartPpm.Series.Add("TEMPUS4");
			Series chartTempus5 = chartPpm.Series.Add("TEMPUS5");
			Series chartTempus6 = chartPpm.Series.Add("TEMPUS6");
			Series chartTempus7 = chartPpm.Series.Add("TEMPUS7");
			Series chartTempus8 = chartPpm.Series.Add("TEMPUS8");
			Series chartTempus9 = chartPpm.Series.Add("TEMPUS9");

			Series chartSenseair2 = chartPpm2.Series.Add("REF");
			Series chartTempus10 = chartPpm2.Series.Add("TEMPUS10");
			Series chartTempus11 = chartPpm2.Series.Add("TEMPUS11");
			Series chartTempus12 = chartPpm2.Series.Add("TEMPUS12");
			Series chartTempus13 = chartPpm2.Series.Add("TEMPUS13");
			Series chartTempus14 = chartPpm2.Series.Add("TEMPUS14");
			Series chartTempus15 = chartPpm2.Series.Add("TEMPUS15");
			Series chartTempus16 = chartPpm2.Series.Add("TEMPUS16");
			Series chartTempus17 = chartPpm2.Series.Add("TEMPUS17");
			Series chartTempus18 = chartPpm2.Series.Add("TEMPUS18");
			Series chartTempus19 = chartPpm2.Series.Add("TEMPUS19");

			/*
			 * chart0 configure
			 */ 
			chartSenseair.ChartType = SeriesChartType.Line;
			chartPpm.Series["REF"].Color = System.Drawing.Color.Green;

			chartTempus0.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS0"].Color = System.Drawing.Color.Red;

			chartTempus1.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS1"].Color = System.Drawing.Color.Brown;

			chartTempus2.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS2"].Color = System.Drawing.Color.Blue;

			chartTempus3.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS3"].Color = System.Drawing.Color.Black;

			chartTempus4.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS4"].Color = System.Drawing.Color.Magenta;

			chartTempus5.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS5"].Color = System.Drawing.Color.Orange;

			chartTempus6.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS6"].Color = System.Drawing.Color.BlueViolet;

			chartTempus7.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS7"].Color = System.Drawing.Color.Tomato;

			chartTempus8.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS8"].Color = System.Drawing.Color.Cyan;

			chartTempus9.ChartType = SeriesChartType.Line;
			chartPpm.Series["TEMPUS9"].Color = System.Drawing.Color.Lime;

			/*
			 * char 2 configure
			 */
			chartSenseair2.ChartType = SeriesChartType.Line;
			chartPpm2.Series["REF"].Color = System.Drawing.Color.Green;

			chartTempus10.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS10"].Color = System.Drawing.Color.Red;

			chartTempus11.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS11"].Color = System.Drawing.Color.Brown;

			chartTempus12.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS12"].Color = System.Drawing.Color.Blue;

			chartTempus13.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS13"].Color = System.Drawing.Color.Black;

			chartTempus14.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS14"].Color = System.Drawing.Color.Magenta;

			chartTempus15.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS15"].Color = System.Drawing.Color.Orange;

			chartTempus16.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS16"].Color = System.Drawing.Color.BlueViolet;

			chartTempus17.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS17"].Color = System.Drawing.Color.Tomato;

			chartTempus18.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS18"].Color = System.Drawing.Color.Cyan;

			chartTempus19.ChartType = SeriesChartType.Line;
			chartPpm2.Series["TEMPUS19"].Color = System.Drawing.Color.Lime;
		}

		private void ChartPpmUpdate(UInt64 count, string ppm_ref, string tps0, string tps1, string tps2, string tps3, string tps4, string tps5, string tps6, string tps7, string tps8, string tps9)
		{
			try
			{
				if (checkBoxArrayPcb0.Checked == false)
					return;

				Double ref_ppm = Convert.ToDouble(ppm_ref);
				Double tps_value = 0;

				if (chartPpm.InvokeRequired)
				{
					chartPpm.Invoke(new MethodInvoker(delegate ()
					{
						try
						{
							if (checkBoxRef.Checked)
								chartPpm.Series["REF"].Points.AddXY(count, Convert.ToDouble(ppm_ref));

							if (checkBoxTps0.Checked)
							{
								tps_value = Convert.ToDouble(tps0);
								chartPpm.Series["TEMPUS0"].Points.AddXY(count, tps_value);
								textBoxDiff0.Text = Convert.ToString(ref_ppm - tps_value);
							}								
							else
							{
								textBoxDiff0.Text = "-";
							}

							if (checkBoxTps1.Checked)
							{
								tps_value = Convert.ToDouble(tps1);
								chartPpm.Series["TEMPUS1"].Points.AddXY(count, tps_value);
								textBoxDiff1.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff1.Text = "-";
							}

							if (checkBoxTps2.Checked)
							{
								tps_value = Convert.ToDouble(tps2);
								chartPpm.Series["TEMPUS2"].Points.AddXY(count, tps_value);
								textBoxDiff2.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff2.Text = "-";
							}

							if (checkBoxTps3.Checked)
							{
								tps_value = Convert.ToDouble(tps3);
								chartPpm.Series["TEMPUS3"].Points.AddXY(count, tps_value);
								textBoxDiff3.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff3.Text = "-";
							}

							if (checkBoxTps4.Checked)
							{
								tps_value = Convert.ToDouble(tps4);
								chartPpm.Series["TEMPUS4"].Points.AddXY(count, tps_value);
								textBoxDiff4.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff4.Text = "-";
							}

							if (checkBoxTps5.Checked)
							{
								tps_value = Convert.ToDouble(tps5);
								chartPpm.Series["TEMPUS5"].Points.AddXY(count, tps_value);
								textBoxDiff5.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff5.Text = "-";
							}

							if (checkBoxTps6.Checked)
							{
								tps_value = Convert.ToDouble(tps6);
								chartPpm.Series["TEMPUS6"].Points.AddXY(count, tps_value);
								textBoxDiff6.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff6.Text = "-";
							}

							if (checkBoxTps7.Checked)
							{
								tps_value = Convert.ToDouble(tps7);
								chartPpm.Series["TEMPUS7"].Points.AddXY(count, tps_value);
								textBoxDiff7.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff7.Text = "-";
							}

							if (checkBoxTps8.Checked)
							{
								tps_value = Convert.ToDouble(tps8);
								chartPpm.Series["TEMPUS8"].Points.AddXY(count, tps_value);
								textBoxDiff8.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff8.Text = "-";
							}

							if (checkBoxTps9.Checked)
							{
								tps_value = Convert.ToDouble(tps9);
								chartPpm.Series["TEMPUS9"].Points.AddXY(count, tps_value);
								textBoxDiff9.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff9.Text = "-";
							}

							chartPpm.Invalidate();
						}
						catch(Exception ex)
						{
							ERR(CallerName() + " : " + ex.Message + '\n');
						}
					}));
				}
				else
				{
					try
					{
						if (checkBoxRef.Checked)
							chartPpm.Series["REF"].Points.AddXY(count, Convert.ToDouble(ppm_ref));

						if (checkBoxTps0.Checked)
						{
							tps_value = Convert.ToDouble(tps0);
							chartPpm.Series["TEMPUS0"].Points.AddXY(count, tps_value);
							textBoxDiff0.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff0.Text = "-";
						}

						if (checkBoxTps1.Checked)
						{
							tps_value = Convert.ToDouble(tps1);
							chartPpm.Series["TEMPUS1"].Points.AddXY(count, tps_value);
							textBoxDiff1.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff1.Text = "-";
						}

						if (checkBoxTps2.Checked)
						{
							tps_value = Convert.ToDouble(tps2);
							chartPpm.Series["TEMPUS2"].Points.AddXY(count, tps_value);
							textBoxDiff2.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff2.Text = "-";
						}

						if (checkBoxTps3.Checked)
						{
							tps_value = Convert.ToDouble(tps3);
							chartPpm.Series["TEMPUS3"].Points.AddXY(count, tps_value);
							textBoxDiff3.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff3.Text = "-";
						}

						if (checkBoxTps4.Checked)
						{
							tps_value = Convert.ToDouble(tps4);
							chartPpm.Series["TEMPUS4"].Points.AddXY(count, tps_value);
							textBoxDiff4.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff4.Text = "-";
						}

						if (checkBoxTps5.Checked)
						{
							tps_value = Convert.ToDouble(tps5);
							chartPpm.Series["TEMPUS5"].Points.AddXY(count, tps_value);
							textBoxDiff5.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff5.Text = "-";
						}

						if (checkBoxTps6.Checked)
						{
							tps_value = Convert.ToDouble(tps6);
							chartPpm.Series["TEMPUS6"].Points.AddXY(count, tps_value);
							textBoxDiff6.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff6.Text = "-";
						}

						if (checkBoxTps7.Checked)
						{
							tps_value = Convert.ToDouble(tps7);
							chartPpm.Series["TEMPUS7"].Points.AddXY(count, tps_value);
							textBoxDiff7.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff7.Text = "-";
						}

						if (checkBoxTps8.Checked)
						{
							tps_value = Convert.ToDouble(tps8);
							chartPpm.Series["TEMPUS8"].Points.AddXY(count, tps_value);
							textBoxDiff8.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff8.Text = "-";
						}

						if (checkBoxTps9.Checked)
						{
							tps_value = Convert.ToDouble(tps9);
							chartPpm.Series["TEMPUS9"].Points.AddXY(count, tps_value);
							textBoxDiff9.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff9.Text = "-";
						}

						chartPpm.Invalidate();
					}
					catch (Exception ex)
					{
						ERR(CallerName() + " : " + ex.Message + '\n');
					}
				}
			}
			catch(Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		private void Chart2PpmUpdate(UInt64 count, string ppm_ref, string tps0, string tps1, string tps2, string tps3, string tps4, string tps5, string tps6, string tps7, string tps8, string tps9)
		{
			try
			{
				Double ref_ppm = Convert.ToDouble(ppm_ref);
				Double tps_value = 0;

				if (checkBoxArrayPcb1.Checked == false)
					return;

				if (chartPpm2.InvokeRequired)
				{
					chartPpm2.Invoke(new MethodInvoker(delegate ()
					{
						try
						{
							if (checkBoxRef2.Checked)
								chartPpm2.Series["REF"].Points.AddXY(count, Convert.ToDouble(ppm_ref));

							if (checkBoxTps10.Checked)
							{
								tps_value = Convert.ToDouble(tps0);
								chartPpm2.Series["TEMPUS10"].Points.AddXY(count, tps_value);
								textBoxDiff10.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff10.Text = "-";
							}

							if (checkBoxTps11.Checked)
							{
								tps_value = Convert.ToDouble(tps1);
								chartPpm2.Series["TEMPUS11"].Points.AddXY(count, tps_value);
								textBoxDiff11.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff11.Text = "-";
							}

							if (checkBoxTps12.Checked)
							{
								tps_value = Convert.ToDouble(tps2);
								chartPpm2.Series["TEMPUS12"].Points.AddXY(count, tps_value);
								textBoxDiff12.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff12.Text = "-";
							}

							if (checkBoxTps13.Checked)
							{
								tps_value = Convert.ToDouble(tps3);
								chartPpm2.Series["TEMPUS13"].Points.AddXY(count, tps_value);
								textBoxDiff13.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff13.Text = "-";
							}

							if (checkBoxTps14.Checked)
							{
								tps_value = Convert.ToDouble(tps4);
								chartPpm2.Series["TEMPUS14"].Points.AddXY(count, tps_value);
								textBoxDiff14.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff14.Text = "-";
							}

							if (checkBoxTps15.Checked)
							{
								tps_value = Convert.ToDouble(tps5);
								chartPpm2.Series["TEMPUS15"].Points.AddXY(count, tps_value);
								textBoxDiff15.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff15.Text = "-";
							}

							if (checkBoxTps16.Checked)
							{
								tps_value = Convert.ToDouble(tps6);
								chartPpm2.Series["TEMPUS16"].Points.AddXY(count, tps_value);
								textBoxDiff16.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff16.Text = "-";
							}

							if (checkBoxTps17.Checked)
							{
								tps_value = Convert.ToDouble(tps7);
								chartPpm2.Series["TEMPUS17"].Points.AddXY(count, tps_value);
								textBoxDiff17.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff17.Text = "-";
							}

							if (checkBoxTps18.Checked)
							{
								tps_value = Convert.ToDouble(tps8);
								chartPpm2.Series["TEMPUS18"].Points.AddXY(count, tps_value);
								textBoxDiff18.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff18.Text = "-";
							}

							if (checkBoxTps19.Checked)
							{
								tps_value = Convert.ToDouble(tps9);
								chartPpm2.Series["TEMPUS19"].Points.AddXY(count, tps_value);
								textBoxDiff19.Text = Convert.ToString(ref_ppm - tps_value);
							}
							else
							{
								textBoxDiff19.Text = "-";
							}

							chartPpm2.Invalidate();
						}
						catch(Exception ex)
						{
							ERR(CallerName() + " : " + ex.Message + '\n');
						}
					}));
				}
				else
				{
					try
					{
						if (checkBoxRef2.Checked)
							chartPpm2.Series["REF"].Points.AddXY(count, Convert.ToDouble(ppm_ref));

						if (checkBoxTps10.Checked)
						{
							tps_value = Convert.ToDouble(tps0);
							chartPpm2.Series["TEMPUS10"].Points.AddXY(count, tps_value);
							textBoxDiff10.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff10.Text = "-";
						}

						if (checkBoxTps11.Checked)
						{
							tps_value = Convert.ToDouble(tps1);
							chartPpm2.Series["TEMPUS11"].Points.AddXY(count, tps_value);
							textBoxDiff11.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff11.Text = "-";
						}

						if (checkBoxTps12.Checked)
						{
							tps_value = Convert.ToDouble(tps2);
							chartPpm2.Series["TEMPUS12"].Points.AddXY(count, tps_value);
							textBoxDiff12.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff12.Text = "-";
						}

						if (checkBoxTps13.Checked)
						{
							tps_value = Convert.ToDouble(tps3);
							chartPpm2.Series["TEMPUS13"].Points.AddXY(count, tps_value);
							textBoxDiff13.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff13.Text = "-";
						}

						if (checkBoxTps14.Checked)
						{
							tps_value = Convert.ToDouble(tps4);
							chartPpm2.Series["TEMPUS14"].Points.AddXY(count, tps_value);
							textBoxDiff14.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff14.Text = "-";
						}

						if (checkBoxTps15.Checked)
						{
							tps_value = Convert.ToDouble(tps5);
							chartPpm2.Series["TEMPUS15"].Points.AddXY(count, tps_value);
							textBoxDiff15.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff15.Text = "-";
						}

						if (checkBoxTps16.Checked)
						{
							tps_value = Convert.ToDouble(tps6);
							chartPpm2.Series["TEMPUS16"].Points.AddXY(count, tps_value);
							textBoxDiff16.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff16.Text = "-";
						}

						if (checkBoxTps17.Checked)
						{
							tps_value = Convert.ToDouble(tps7);
							chartPpm2.Series["TEMPUS17"].Points.AddXY(count, tps_value);
							textBoxDiff17.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff17.Text = "-";
						}

						if (checkBoxTps18.Checked)
						{
							tps_value = Convert.ToDouble(tps8);
							chartPpm2.Series["TEMPUS18"].Points.AddXY(count, tps_value);
							textBoxDiff18.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff18.Text = "-";
						}

						if (checkBoxTps19.Checked)
						{
							tps_value = Convert.ToDouble(tps9);
							chartPpm2.Series["TEMPUS19"].Points.AddXY(count, tps_value);
							textBoxDiff19.Text = Convert.ToString(ref_ppm - tps_value);
						}
						else
						{
							textBoxDiff19.Text = "-";
						}

						chartPpm2.Invalidate();
					}
					catch (Exception ex)
					{
						ERR(CallerName() + " : " + ex.Message + '\n');
					}
				}
			}
			catch(Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		public void ChartClear()
		{
            if(chartPpm.InvokeRequired)
            {
                chartPpm.Invoke(new MethodInvoker(delegate ()
                {
                    foreach (var series in chartPpm.Series)
                    {
                        series.Points.Clear();
                    }
                }));
            }
            else
            {
                foreach (var series in chartPpm.Series)
                {
                    series.Points.Clear();
                }
            }            
			chartCount = 0;
		}

		public void Chart2Clear()
		{
            if (chartPpm2.InvokeRequired)
            {
                chartPpm2.Invoke(new MethodInvoker(delegate ()
                {
                    foreach (var series in chartPpm2.Series)
                    {
                        series.Points.Clear();
                    }
                }));
            }
            else
            {
                foreach (var series in chartPpm2.Series)
                {
                    series.Points.Clear();
                }
            }
            chartCount = 0;
		}

		public void ChartIsValid(byte mod_offset, bool val)
		{
			switch (mod_offset)
			{
				case 0:
					if (checkBoxTps0.InvokeRequired)
					{
						checkBoxTps0.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps0.Checked = val;
						}));
					}
					else
					{
						checkBoxTps0.Checked = val;
					}
					break;

				case 1:
					if (checkBoxTps1.InvokeRequired)
					{
						checkBoxTps1.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps1.Checked = val;
						}));
					}
					else
					{
						checkBoxTps1.Checked = val;
					}
					break;

				case 2:
					if (checkBoxTps2.InvokeRequired)
					{
						checkBoxTps2.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps2.Checked = val;
						}));
					}
					else
					{
						checkBoxTps2.Checked = val;
					}
					break;

				case 3:
					if (checkBoxTps3.InvokeRequired)
					{
						checkBoxTps3.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps3.Checked = val;
						}));
					}
					else
					{
						checkBoxTps3.Checked = val;
					}
					break;

				case 4:
					if (checkBoxTps4.InvokeRequired)
					{
						checkBoxTps4.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps4.Checked = val;
						}));
					}
					else
					{
						checkBoxTps4.Checked = val;
					}
					break;

				case 5:
					if (checkBoxTps5.InvokeRequired)
					{
						checkBoxTps5.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps5.Checked = val;
						}));
					}
					else
					{
						checkBoxTps5.Checked = val;
					}
					break;

				case 6:
					if (checkBoxTps6.InvokeRequired)
					{
						checkBoxTps6.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps6.Checked = val;
						}));
					}
					else
					{
						checkBoxTps6.Checked = val;
					}
					break;

				case 7:
					if (checkBoxTps7.InvokeRequired)
					{
						checkBoxTps7.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps7.Checked = val;
						}));
					}
					else
					{
						checkBoxTps7.Checked = val;
					}
					break;

				case 8:
					if (checkBoxTps8.InvokeRequired)
					{
						checkBoxTps8.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps8.Checked = val;
						}));
					}
					else
					{
						checkBoxTps8.Checked = val;
					}
					break;

				case 9:
					if (checkBoxTps9.InvokeRequired)
					{
						checkBoxTps9.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps9.Checked = val;
						}));
					}
					else
					{
						checkBoxTps9.Checked = val;
					}
					break;

				case 10:
					if (checkBoxTps10.InvokeRequired)
					{
						checkBoxTps10.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps10.Checked = val;
						}));
					}
					else
					{
						checkBoxTps10.Checked = val;
					}
					break;

				case 11:
					if (checkBoxTps11.InvokeRequired)
					{
						checkBoxTps11.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps11.Checked = val;
						}));
					}
					else
					{
						checkBoxTps11.Checked = val;
					}
					break;

				case 12:
					if (checkBoxTps12.InvokeRequired)
					{
						checkBoxTps12.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps12.Checked = val;
						}));
					}
					else
					{
						checkBoxTps12.Checked = val;
					}
					break;

				case 13:
					if (checkBoxTps13.InvokeRequired)
					{
						checkBoxTps13.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps13.Checked = val;
						}));
					}
					else
					{
						checkBoxTps13.Checked = val;
					}
					break;

				case 14:
					if (checkBoxTps14.InvokeRequired)
					{
						checkBoxTps14.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps14.Checked = val;
						}));
					}
					else
					{
						checkBoxTps14.Checked = val;
					}
					break;

				case 15:
					if (checkBoxTps15.InvokeRequired)
					{
						checkBoxTps15.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps15.Checked = val;
						}));
					}
					else
					{
						checkBoxTps15.Checked = val;
					}
					break;

				case 16:
					if (checkBoxTps16.InvokeRequired)
					{
						checkBoxTps16.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps16.Checked = val;
						}));
					}
					else
					{
						checkBoxTps16.Checked = val;
					}
					break;

				case 17:
					if (checkBoxTps17.InvokeRequired)
					{
						checkBoxTps17.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps17.Checked = val;
						}));
					}
					else
					{
						checkBoxTps17.Checked = val;
					}
					break;

				case 18:
					if (checkBoxTps18.InvokeRequired)
					{
						checkBoxTps18.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps18.Checked = val;
						}));
					}
					else
					{
						checkBoxTps18.Checked = val;
					}
					break;

				case 19:
					if (checkBoxTps19.InvokeRequired)
					{
						checkBoxTps19.Invoke(new MethodInvoker(delegate ()
						{
							checkBoxTps19.Checked = val;
						}));
					}
					else
					{
						checkBoxTps19.Checked = val;
					}
					break;
			}
		}
	}
}
