﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class FormDataView : Form
	{
		int MAX_DG_COLUMN_NUMBER = 29;
		string[] column_name = {	"Time", "No", "Flag", "st0", "nt0", "st1", "nt1", "dC", "sC", "dR",
									"sR", "adc", "ratio", "ppm", "temp", "ts", "gain", "PWM", "Alarm", "I2C",
									"S/N", "Ver", "I/F", "Diff", "Ref", "Amb", "Rh", "I", "Rsv" };

		/*
         * SetupDataGridView()
         * DataGridView의 기본적인 설정을 합니다.
         */
		public void SetupDataGridView()
		{
			this.Controls.Add(dataGridView1);
			// DataGridView의 컬럼 갯수를 설정합니다.
			dataGridView1.ColumnCount = MAX_DG_COLUMN_NUMBER;

			// DataGridView에 컬럼을 추가합니다.
			for (int i = 0; i < MAX_DG_COLUMN_NUMBER; i++)
			{
				dataGridView1.Columns[i].Name = column_name[i];
				dataGridView1.Columns[column_name[i]].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			}

			for (int i = 0; i < MAX_DG_COLUMN_NUMBER; i++)
			{
				dataGridView1.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			}
			dataGridView1.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dataGridView1.RowPrePaint += new DataGridViewRowPrePaintEventHandler(this.dataGridView1_RowPrePaint);

			////////////////////////////////////////////////////////////////////////////////////
			this.Controls.Add(dataGridView2);
			// DataGridView의 컬럼 갯수를 설정합니다.
			dataGridView2.ColumnCount = MAX_DG_COLUMN_NUMBER;

			// DataGridView에 컬럼을 추가합니다.
			for (int i = 0; i < MAX_DG_COLUMN_NUMBER; i++)
			{
				dataGridView2.Columns[i].Name = column_name[i];
				dataGridView2.Columns[column_name[i]].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			}

			for (int i = 0; i < MAX_DG_COLUMN_NUMBER; i++)
			{
				dataGridView2.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
			}
			dataGridView2.RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
			this.dataGridView2.RowPrePaint += new DataGridViewRowPrePaintEventHandler(this.dataGridView2_RowPrePaint);
		}

		public void DataGridViewClear(bool gridNum)
		{
			if(gridNum)
				dataGridView1.Rows.Clear();
			else
				dataGridView2.Rows.Clear();
			//dataGridView2.Refresh();		// 화면이 깜빡거려서 눈이 아프다
		}

		public void PopulateDataGridView(string[][] str, bool gridNum)
		{
			try
			{
				if(gridNum)
				{
					if (dataGridView2.InvokeRequired)
					{
						dataGridView2.Invoke(new MethodInvoker(delegate ()
						{
							dataGridView2.Rows.Clear();
							
							for (int i = 10; i < frm1.MAX_MODULE_NUM; i++)
							{
								dataGridView2.Rows.Add(str[i]);
								CSV_SAVE(frm1.ConvertStringArrayToString(str[i]));
							}
						}));
					}
					else
					{
						dataGridView2.Rows.Clear();

						for (int i = 0; i < frm1.MAX_ARRAY_MOD_NUM; i++)
						{
							dataGridView2.Rows.Add(str[i]);
							CSV_SAVE(frm1.ConvertStringArrayToString(str[i]));
						}
					}
				}
				else
				{
					if (dataGridView1.InvokeRequired)
					{
						dataGridView1.Invoke(new MethodInvoker(delegate ()
						{
							dataGridView1.Rows.Clear();

							for (int i = 0; i < frm1.MAX_ARRAY_MOD_NUM; i++)
							{
								dataGridView1.Rows.Add(str[i]);
								CSV_SAVE(frm1.ConvertStringArrayToString(str[i]));
							}
						}));
					}
					else
					{
						dataGridView1.Rows.Clear();

						for (int i = 0; i < frm1.MAX_ARRAY_MOD_NUM; i++)
						{
							dataGridView1.Rows.Add(str[i]);
							CSV_SAVE(frm1.ConvertStringArrayToString(str[i]));
						}
					}
				}
			}
			catch (System.IndexOutOfRangeException ex)  // CS0168
			{
				frm1.ERR(frm1.CallerName() + " : " + ex.Message + '\n');
			}
		}

		private void dataGridView1_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
		{
			try
			{
				string str = (string)dataGridView1.Rows[e.RowIndex].Cells[2].Value;

				if (str == null)
					return;
#if false
				/***********************************************************************************/
				// 값이 '0'인 경우 에러라고 생각하여 특정색으로 표시를 한다
				// column_name의 Flag부터 체크하고 st0/1, nt0/1, dC/sC/dR/sR, ppm/gain을 체크하자
				// 그러면 2~12 까지 체크하면 됨
				/***********************************************************************************/
				for (int i = 3; i < 12; i++)
				{
					if (Convert.ToUInt32(dataGridView1.Rows[e.RowIndex].Cells[i].Value) == 0)
					{
						dataGridView1.Rows[e.RowIndex].Cells[i].Style.BackColor = Color.LightGray;
					}
				}

				// ppm check
				if (Convert.ToUInt32(dataGridView1.Rows[e.RowIndex].Cells[13].Value) == zero)
				{
					dataGridView1.Rows[e.RowIndex].Cells[14].Style.BackColor = Color.LightGray;
				}

				// gain check
				if (Convert.ToUInt32(dataGridView1.Rows[e.RowIndex].Cells[16].Value) == zero)
				{
					dataGridView1.Rows[e.RowIndex].Cells[17].Style.BackColor = Color.LightGray;
				}
				/***********************************************************************************/
#endif
				/***********************************************************************************/
				// flag를 check해서 0x00, 즉 version read error이면, 이면 회색으로 ROW를 칠한다
				// module 이 없다는 의미
				//if (Convert.ToByte(dataGridView2.Rows[e.RowIndex].Cells[2].Value) == val) 
#if true

				byte val = 0x50;
				byte flag = 0;
				str = str.Substring(2);
#if false
                flag = Convert.ToByte(str);
                val = 0x01;
                flag = (byte)(flag & val);     // Alarm Flag뜨면 노란색으로
                if (flag == val)
                {
                    dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightYellow;
                }
#endif
				flag = Convert.ToByte(str);
				val = 0x08;
				flag = (byte)(flag & 0x08);     // 동작되나 센서 죽은넘들은 회색으로
				if (flag == val)
				{
					dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
				}

				//flag = (byte)(flag & 0xFE);
				flag = Convert.ToByte(str);
				val = 0x50;
				flag = (byte)(flag & 0x50);     // 동작 안되는 모듈 빨간색으로
				if (flag != val)
				{
					dataGridView1.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
				}
				/***********************************************************************************/
#endif
			}
			catch (Exception ex)
			{
				frm1.ERR(frm1.CallerName() + " : " + ex.Message + "\n");
			}
		}

		private void dataGridView2_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
		{
			try
			{
				string str = (string)dataGridView2.Rows[e.RowIndex].Cells[2].Value;

				if (str == null)
					return;
#if false
				/***********************************************************************************/
				// 값이 '0'인 경우 에러라고 생각하여 특정색으로 표시를 한다
				// column_name의 Flag부터 체크하고 st0/1, nt0/1, dC/sC/dR/sR, ppm/gain을 체크하자
				// 그러면 2~12 까지 체크하면 됨
				/***********************************************************************************/
				for (int i = 3; i < 12; i++)
				{
					if (Convert.ToUInt32(dataGridView2.Rows[e.RowIndex].Cells[i].Value) == 0)
					{
						dataGridView2.Rows[e.RowIndex].Cells[i].Style.BackColor = Color.LightGray;
					}
				}

				// ppm check
				if (Convert.ToUInt32(dataGridView2.Rows[e.RowIndex].Cells[13].Value) == zero)
				{
					dataGridView2.Rows[e.RowIndex].Cells[14].Style.BackColor = Color.LightGray;
				}

				// gain check
				if (Convert.ToUInt32(dataGridView2.Rows[e.RowIndex].Cells[16].Value) == zero)
				{
					dataGridView2.Rows[e.RowIndex].Cells[17].Style.BackColor = Color.LightGray;
				}
				/***********************************************************************************/
#endif
				/***********************************************************************************/
				// flag를 check해서 0x00, 즉 version read error이면, 이면 회색으로 ROW를 칠한다
				// module 이 없다는 의미
				//if (Convert.ToByte(dataGridView2.Rows[e.RowIndex].Cells[2].Value) == val) 
#if true
				byte val = 0x50;
				byte flag = 0;
				str = str.Substring(2);
				flag = Convert.ToByte(str);
				
				val = 0x08;
				flag = (byte)(flag & 0x08);     // 동작되나 센서 죽은넘들은 회색으로
				if (flag == val)
				{
					dataGridView2.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.LightGray;
				}

				//flag = (byte)(flag & 0xFE);
				flag = Convert.ToByte(str);
				val = 0x50;
				flag = (byte)(flag & 0x50);     // 동작 안되는 모듈 빨간색으로
				if (flag != val)
				{
					dataGridView2.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Red;
				}
				/***********************************************************************************/
#endif
			}
			catch (Exception ex)
			{
				frm1.ERR(frm1.CallerName() + " : " + ex.Message + "\n");
			}
		}

		private void PopulateDataGridView()
		{
			// DataGridView에 삽입할 데이터를 설정합니다.
			string[] row0 = { "0", "29", "Revolution 9", "Beatles", "The Beatles [White Album]" };
			string[] row1 = { "1", "6", "Fools Rush In", "Frank Sinatra", "Nice 'N' Easy" };
			string[] row2 = { "2", "1", "One of These Days", "Pink Floyd", "Meddle" };
			string[] row3 = { "3", "7", "Where Is My Mind?", "Pixies", "Surfer Rosa" };
			string[] row4 = { "4", "9", "Can't Find My Mind", "Cramps", "Psychedelic Jungle" };
			string[] row5 = { "5", "13", "Scatterbrain. (As Dead As Leaves.)", "Radiohead", "Hail to the Thief" };
			string[] row6 = { "6", "3", "Dress", "P J Harvey", "Dry" };
			string[] row7 = { "7", "3", "Dress", "P J Harvey", "Dry" };
			string[] row8 = { "8", "3", "Dress", "P J Harvey", "Dry" };
			string[] row9 = { "9", "3", "Dress", "P J Harvey", "Dry" };
			string[] row10 = { "10", "3", "Dress", "P J Harvey", "Dry" };
			string[] row11 = { "11", "3", "Dress", "P J Harvey", "Dry" };
			string[] row12 = { "12", "3", "Dress", "P J Harvey", "Dry" };
			string[] row13 = { "13", "3", "Dress", "P J Harvey", "Dry" };
			string[] row14 = { "14", "3", "Dress", "P J Harvey", "Dry" };
			string[] row15 = { "15", "3", "Dress", "P J Harvey", "Dry" };
			string[] row16 = { "16", "3", "Dress", "P J Harvey", "Dry" };
			string[] row17 = { "17", "3", "Dress", "P J Harvey", "Dry" };
			string[] row18 = { "18", "3", "Dress", "P J Harvey", "Dry" };
			string[] row19 = { "19", "3", "Dress", "P J Harvey", "Dry" };
			

			// DataGridView에 한 줄씩 삽입합니다.
			for(int i = 0; i < frm1.MAX_MODULE_NUM; i++)
			{
				dataGridView2.Rows.Add(column_name);
			}
#if false
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(column_name);
			dataGridView2.Rows.Add(row14);
			dataGridView2.Rows.Add(row15);
			dataGridView2.Rows.Add(row16);
			dataGridView2.Rows.Add(row17);
			dataGridView2.Rows.Add(row18);
			dataGridView2.Rows.Add(row19);
#endif
		}
	}
}
