﻿namespace CO2_CAL_JIG_Mon_V3
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.richTextBoxCalLog = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonBadModule = new System.Windows.Forms.Button();
            this.buttonFw = new System.Windows.Forms.Button();
            this.buttonRead = new System.Windows.Forms.Button();
            this.buttonRef = new System.Windows.Forms.Button();
            this.buttonMonEnable = new System.Windows.Forms.Button();
            this.buttonAmb = new System.Windows.Forms.Button();
            this.buttonAdAll = new System.Windows.Forms.Button();
            this.buttonMuxIo = new System.Windows.Forms.Button();
            this.buttonAdRead = new System.Windows.Forms.Button();
            this.checkBoxOnOff = new System.Windows.Forms.CheckBox();
            this.buttonMuxEn = new System.Windows.Forms.Button();
            this.buttonMuxSel = new System.Windows.Forms.Button();
            this.buttonPower = new System.Windows.Forms.Button();
            this.buttonTpsSingle = new System.Windows.Forms.Button();
            this.comboBoxTpsSingle = new System.Windows.Forms.ComboBox();
            this.buttonMfc = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxRh = new System.Windows.Forms.TextBox();
            this.textBoxAmb = new System.Windows.Forms.TextBox();
            this.textBoxRef = new System.Windows.Forms.TextBox();
            this.buttonInit = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.buttonWriteAll = new System.Windows.Forms.Button();
            this.buttonDefault = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.textBoxSc10 = new System.Windows.Forms.TextBox();
            this.buttonGain10 = new System.Windows.Forms.Button();
            this.textBoxSr10 = new System.Windows.Forms.TextBox();
            this.buttonTs10 = new System.Windows.Forms.Button();
            this.textBoxGain10 = new System.Windows.Forms.TextBox();
            this.buttonSr10 = new System.Windows.Forms.Button();
            this.textBoxTs10 = new System.Windows.Forms.TextBox();
            this.buttonSc10 = new System.Windows.Forms.Button();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.textBoxSc15 = new System.Windows.Forms.TextBox();
            this.buttonGain15 = new System.Windows.Forms.Button();
            this.textBoxSr15 = new System.Windows.Forms.TextBox();
            this.buttonTs15 = new System.Windows.Forms.Button();
            this.textBoxGain15 = new System.Windows.Forms.TextBox();
            this.buttonSr15 = new System.Windows.Forms.Button();
            this.textBoxTs15 = new System.Windows.Forms.TextBox();
            this.buttonSc15 = new System.Windows.Forms.Button();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.textBoxSc19 = new System.Windows.Forms.TextBox();
            this.buttonGain19 = new System.Windows.Forms.Button();
            this.textBoxSr19 = new System.Windows.Forms.TextBox();
            this.buttonTs19 = new System.Windows.Forms.Button();
            this.textBoxGain19 = new System.Windows.Forms.TextBox();
            this.buttonSr19 = new System.Windows.Forms.Button();
            this.textBoxTs19 = new System.Windows.Forms.TextBox();
            this.buttonSc19 = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.textBoxSc11 = new System.Windows.Forms.TextBox();
            this.buttonGain11 = new System.Windows.Forms.Button();
            this.textBoxSr11 = new System.Windows.Forms.TextBox();
            this.buttonTs11 = new System.Windows.Forms.Button();
            this.textBoxGain11 = new System.Windows.Forms.TextBox();
            this.buttonSr11 = new System.Windows.Forms.Button();
            this.textBoxTs11 = new System.Windows.Forms.TextBox();
            this.buttonSc11 = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.textBoxSc14 = new System.Windows.Forms.TextBox();
            this.buttonGain14 = new System.Windows.Forms.Button();
            this.textBoxSr14 = new System.Windows.Forms.TextBox();
            this.buttonTs14 = new System.Windows.Forms.Button();
            this.textBoxGain14 = new System.Windows.Forms.TextBox();
            this.buttonSr14 = new System.Windows.Forms.Button();
            this.textBoxTs14 = new System.Windows.Forms.TextBox();
            this.buttonSc14 = new System.Windows.Forms.Button();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.textBoxSc16 = new System.Windows.Forms.TextBox();
            this.buttonGain16 = new System.Windows.Forms.Button();
            this.textBoxSr16 = new System.Windows.Forms.TextBox();
            this.buttonTs16 = new System.Windows.Forms.Button();
            this.textBoxGain16 = new System.Windows.Forms.TextBox();
            this.buttonSr16 = new System.Windows.Forms.Button();
            this.textBoxTs16 = new System.Windows.Forms.TextBox();
            this.buttonSc16 = new System.Windows.Forms.Button();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.textBoxSc18 = new System.Windows.Forms.TextBox();
            this.buttonGain18 = new System.Windows.Forms.Button();
            this.textBoxSr18 = new System.Windows.Forms.TextBox();
            this.buttonTs18 = new System.Windows.Forms.Button();
            this.textBoxGain18 = new System.Windows.Forms.TextBox();
            this.buttonSr18 = new System.Windows.Forms.Button();
            this.textBoxTs18 = new System.Windows.Forms.TextBox();
            this.buttonSc18 = new System.Windows.Forms.Button();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.textBoxSc12 = new System.Windows.Forms.TextBox();
            this.buttonGain12 = new System.Windows.Forms.Button();
            this.textBoxSr12 = new System.Windows.Forms.TextBox();
            this.buttonTs12 = new System.Windows.Forms.Button();
            this.textBoxGain12 = new System.Windows.Forms.TextBox();
            this.buttonSr12 = new System.Windows.Forms.Button();
            this.textBoxTs12 = new System.Windows.Forms.TextBox();
            this.buttonSc12 = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.textBoxSc13 = new System.Windows.Forms.TextBox();
            this.buttonGain13 = new System.Windows.Forms.Button();
            this.textBoxSr13 = new System.Windows.Forms.TextBox();
            this.buttonTs13 = new System.Windows.Forms.Button();
            this.textBoxGain13 = new System.Windows.Forms.TextBox();
            this.buttonSr13 = new System.Windows.Forms.Button();
            this.textBoxTs13 = new System.Windows.Forms.TextBox();
            this.buttonSc13 = new System.Windows.Forms.Button();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.textBoxSc17 = new System.Windows.Forms.TextBox();
            this.buttonGain17 = new System.Windows.Forms.Button();
            this.textBoxSr17 = new System.Windows.Forms.TextBox();
            this.buttonTs17 = new System.Windows.Forms.Button();
            this.textBoxGain17 = new System.Windows.Forms.TextBox();
            this.buttonSr17 = new System.Windows.Forms.Button();
            this.textBoxTs17 = new System.Windows.Forms.TextBox();
            this.buttonSc17 = new System.Windows.Forms.Button();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.textBoxSc5 = new System.Windows.Forms.TextBox();
            this.buttonGain5 = new System.Windows.Forms.Button();
            this.textBoxSr5 = new System.Windows.Forms.TextBox();
            this.buttonTs5 = new System.Windows.Forms.Button();
            this.textBoxGain5 = new System.Windows.Forms.TextBox();
            this.buttonSr5 = new System.Windows.Forms.Button();
            this.textBoxTs5 = new System.Windows.Forms.TextBox();
            this.buttonSc5 = new System.Windows.Forms.Button();
            this.groupBox0 = new System.Windows.Forms.GroupBox();
            this.textBoxSc0 = new System.Windows.Forms.TextBox();
            this.buttonGain0 = new System.Windows.Forms.Button();
            this.textBoxSr0 = new System.Windows.Forms.TextBox();
            this.buttonTs0 = new System.Windows.Forms.Button();
            this.textBoxGain0 = new System.Windows.Forms.TextBox();
            this.buttonSr0 = new System.Windows.Forms.Button();
            this.textBoxTs0 = new System.Windows.Forms.TextBox();
            this.buttonSc0 = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.textBoxSc6 = new System.Windows.Forms.TextBox();
            this.buttonGain6 = new System.Windows.Forms.Button();
            this.textBoxSr6 = new System.Windows.Forms.TextBox();
            this.buttonTs6 = new System.Windows.Forms.Button();
            this.textBoxGain6 = new System.Windows.Forms.TextBox();
            this.buttonSr6 = new System.Windows.Forms.Button();
            this.textBoxTs6 = new System.Windows.Forms.TextBox();
            this.buttonSc6 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.textBoxSc1 = new System.Windows.Forms.TextBox();
            this.buttonGain1 = new System.Windows.Forms.Button();
            this.textBoxSr1 = new System.Windows.Forms.TextBox();
            this.buttonTs1 = new System.Windows.Forms.Button();
            this.textBoxGain1 = new System.Windows.Forms.TextBox();
            this.buttonSr1 = new System.Windows.Forms.Button();
            this.textBoxTs1 = new System.Windows.Forms.TextBox();
            this.buttonSc1 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBoxSc7 = new System.Windows.Forms.TextBox();
            this.buttonGain7 = new System.Windows.Forms.Button();
            this.textBoxSr7 = new System.Windows.Forms.TextBox();
            this.buttonTs7 = new System.Windows.Forms.Button();
            this.textBoxGain7 = new System.Windows.Forms.TextBox();
            this.buttonSr7 = new System.Windows.Forms.Button();
            this.textBoxTs7 = new System.Windows.Forms.TextBox();
            this.buttonSc7 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxSc2 = new System.Windows.Forms.TextBox();
            this.buttonGain2 = new System.Windows.Forms.Button();
            this.textBoxSr2 = new System.Windows.Forms.TextBox();
            this.buttonTs2 = new System.Windows.Forms.Button();
            this.textBoxGain2 = new System.Windows.Forms.TextBox();
            this.buttonSr2 = new System.Windows.Forms.Button();
            this.textBoxTs2 = new System.Windows.Forms.TextBox();
            this.buttonSc2 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.textBoxSc8 = new System.Windows.Forms.TextBox();
            this.buttonGain8 = new System.Windows.Forms.Button();
            this.textBoxSr8 = new System.Windows.Forms.TextBox();
            this.buttonTs8 = new System.Windows.Forms.Button();
            this.textBoxGain8 = new System.Windows.Forms.TextBox();
            this.buttonSr8 = new System.Windows.Forms.Button();
            this.textBoxTs8 = new System.Windows.Forms.TextBox();
            this.buttonSc8 = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.textBoxSc3 = new System.Windows.Forms.TextBox();
            this.buttonGain3 = new System.Windows.Forms.Button();
            this.textBoxSr3 = new System.Windows.Forms.TextBox();
            this.buttonTs3 = new System.Windows.Forms.Button();
            this.textBoxGain3 = new System.Windows.Forms.TextBox();
            this.buttonSr3 = new System.Windows.Forms.Button();
            this.textBoxTs3 = new System.Windows.Forms.TextBox();
            this.buttonSc3 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.textBoxSc9 = new System.Windows.Forms.TextBox();
            this.buttonGain9 = new System.Windows.Forms.Button();
            this.textBoxSr9 = new System.Windows.Forms.TextBox();
            this.buttonTs9 = new System.Windows.Forms.Button();
            this.textBoxGain9 = new System.Windows.Forms.TextBox();
            this.buttonSr9 = new System.Windows.Forms.Button();
            this.textBoxTs9 = new System.Windows.Forms.TextBox();
            this.buttonSc9 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textBoxSc4 = new System.Windows.Forms.TextBox();
            this.buttonGain4 = new System.Windows.Forms.Button();
            this.textBoxSr4 = new System.Windows.Forms.TextBox();
            this.buttonTs4 = new System.Windows.Forms.Button();
            this.textBoxGain4 = new System.Windows.Forms.TextBox();
            this.buttonSr4 = new System.Windows.Forms.Button();
            this.textBoxTs4 = new System.Windows.Forms.TextBox();
            this.buttonSc4 = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.textBoxDiff19 = new System.Windows.Forms.TextBox();
            this.textBoxDiff18 = new System.Windows.Forms.TextBox();
            this.textBoxDiff17 = new System.Windows.Forms.TextBox();
            this.textBoxDiff16 = new System.Windows.Forms.TextBox();
            this.textBoxDiff15 = new System.Windows.Forms.TextBox();
            this.textBoxDiff14 = new System.Windows.Forms.TextBox();
            this.textBoxDiff13 = new System.Windows.Forms.TextBox();
            this.textBoxDiff12 = new System.Windows.Forms.TextBox();
            this.textBoxDiff11 = new System.Windows.Forms.TextBox();
            this.textBoxDiff10 = new System.Windows.Forms.TextBox();
            this.textBoxDiffRef2 = new System.Windows.Forms.TextBox();
            this.checkBoxSelectAll1 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps19 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps18 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps17 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps16 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps15 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps14 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps13 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps12 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps11 = new System.Windows.Forms.CheckBox();
            this.checkBoxRef2 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps10 = new System.Windows.Forms.CheckBox();
            this.buttonChartClear1 = new System.Windows.Forms.Button();
            this.chartPpm2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textBoxDiff9 = new System.Windows.Forms.TextBox();
            this.textBoxDiff8 = new System.Windows.Forms.TextBox();
            this.textBoxDiff7 = new System.Windows.Forms.TextBox();
            this.textBoxDiff6 = new System.Windows.Forms.TextBox();
            this.textBoxDiff5 = new System.Windows.Forms.TextBox();
            this.textBoxDiff4 = new System.Windows.Forms.TextBox();
            this.textBoxDiff3 = new System.Windows.Forms.TextBox();
            this.textBoxDiff2 = new System.Windows.Forms.TextBox();
            this.textBoxDiff1 = new System.Windows.Forms.TextBox();
            this.textBoxDiff0 = new System.Windows.Forms.TextBox();
            this.textBoxDiffRef = new System.Windows.Forms.TextBox();
            this.checkBoxSelectAll = new System.Windows.Forms.CheckBox();
            this.checkBoxTps9 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps8 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps7 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps6 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps5 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps4 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps3 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps2 = new System.Windows.Forms.CheckBox();
            this.checkBoxTps1 = new System.Windows.Forms.CheckBox();
            this.checkBoxRef = new System.Windows.Forms.CheckBox();
            this.checkBoxTps0 = new System.Windows.Forms.CheckBox();
            this.buttonChartClear0 = new System.Windows.Forms.Button();
            this.chartPpm = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBoxInit = new System.Windows.Forms.GroupBox();
            this.panel11 = new System.Windows.Forms.Panel();
            this.radioButtonRsv2 = new System.Windows.Forms.RadioButton();
            this.radioButtonRsv1 = new System.Windows.Forms.RadioButton();
            this.panel10 = new System.Windows.Forms.Panel();
            this.radioButtonUartIf = new System.Windows.Forms.RadioButton();
            this.radioButtonI2cIf = new System.Windows.Forms.RadioButton();
            this.checkBoxRsv2 = new System.Windows.Forms.CheckBox();
            this.checkBoxRsv1 = new System.Windows.Forms.CheckBox();
            this.checkBoxReadStatus = new System.Windows.Forms.CheckBox();
            this.checkBoxArrayPcb0 = new System.Windows.Forms.CheckBox();
            this.checkBoxArrayPcb1 = new System.Windows.Forms.CheckBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.checkBoxLogErr = new System.Windows.Forms.CheckBox();
            this.checkBoxLogMsg = new System.Windows.Forms.CheckBox();
            this.checkBoxLogLog = new System.Windows.Forms.CheckBox();
            this.label78 = new System.Windows.Forms.Label();
            this.panel7 = new System.Windows.Forms.Panel();
            this.checkBoxLog = new System.Windows.Forms.CheckBox();
            this.checkBoxErr = new System.Windows.Forms.CheckBox();
            this.checkBoxInfo = new System.Windows.Forms.CheckBox();
            this.label77 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label72 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.textBoxD3 = new System.Windows.Forms.TextBox();
            this.textBoxD2 = new System.Windows.Forms.TextBox();
            this.textBoxD1 = new System.Windows.Forms.TextBox();
            this.textBoxD0 = new System.Windows.Forms.TextBox();
            this.textBoxCh = new System.Windows.Forms.TextBox();
            this.textBoxCmd = new System.Windows.Forms.TextBox();
            this.buttonCdcSend = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.buttonCom = new System.Windows.Forms.Button();
            this.buttonComClose = new System.Windows.Forms.Button();
            this.comboBoxCom = new System.Windows.Forms.ComboBox();
            this.buttonComOpen = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.buttonI2cGen = new System.Windows.Forms.Button();
            this.buttonI2cGet = new System.Windows.Forms.Button();
            this.buttonI2cSet = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr = new System.Windows.Forms.TextBox();
            this.labelI2cAddr = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr0 = new System.Windows.Forms.TextBox();
            this.textBoxI2cAddr19 = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr9 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr10 = new System.Windows.Forms.TextBox();
            this.textBoxI2cAddr18 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr1 = new System.Windows.Forms.TextBox();
            this.textBoxI2cAddr8 = new System.Windows.Forms.TextBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr11 = new System.Windows.Forms.TextBox();
            this.textBoxI2cAddr17 = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr2 = new System.Windows.Forms.TextBox();
            this.textBoxI2cAddr7 = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr12 = new System.Windows.Forms.TextBox();
            this.textBoxI2cAddr16 = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr3 = new System.Windows.Forms.TextBox();
            this.textBoxI2cAddr6 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr13 = new System.Windows.Forms.TextBox();
            this.textBoxI2cAddr15 = new System.Windows.Forms.TextBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr4 = new System.Windows.Forms.TextBox();
            this.textBoxI2cAddr5 = new System.Windows.Forms.TextBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.textBoxI2cAddr14 = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.buttonVerGet = new System.Windows.Forms.Button();
            this.buttonAlarmGen = new System.Windows.Forms.Button();
            this.buttonAlarmGet = new System.Windows.Forms.Button();
            this.buttonAlarmSet = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxAlarmIn = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.textBoxAlarm0 = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBoxAlarm10 = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.textBoxAlarm1 = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.textBoxAlarm11 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.textBoxAlarm2 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.textBoxAlarm12 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.textBoxAlarm3 = new System.Windows.Forms.TextBox();
            this.label42 = new System.Windows.Forms.Label();
            this.textBoxAlarm13 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBoxAlarm4 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.textBoxAlarm14 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.textBoxAlarm5 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.textBoxAlarm15 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.textBoxAlarm6 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.textBoxAlarm16 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.textBoxAlarm7 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.textBoxAlarm17 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.textBoxAlarm8 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.textBoxAlarm18 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.textBoxAlarm9 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.textBoxAlarm19 = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.buttonSerialNumGet = new System.Windows.Forms.Button();
            this.buttonSerialNumSet = new System.Windows.Forms.Button();
            this.buttonSerialNumGenerate = new System.Windows.Forms.Button();
            this.textBoxSerialNumIn = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.textBoxSerialNum19 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum9 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum18 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum8 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum17 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum7 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum16 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum6 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum15 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum5 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum14 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum4 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum13 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum3 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum12 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum2 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum11 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum1 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum10 = new System.Windows.Forms.TextBox();
            this.textBoxSerialNum0 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.panel13 = new System.Windows.Forms.Panel();
            this.buttonMfcOff = new System.Windows.Forms.Button();
            this.textBoxTimer = new System.Windows.Forms.TextBox();
            this.buttonMfxStop = new System.Windows.Forms.Button();
            this.buttonPurge = new System.Windows.Forms.Button();
            this.richTextBoxSockLog = new System.Windows.Forms.RichTextBox();
            this.button2000ppm = new System.Windows.Forms.Button();
            this.button4500ppm = new System.Windows.Forms.Button();
            this.buttonSocketOn = new System.Windows.Forms.Button();
            this.button600ppm = new System.Windows.Forms.Button();
            this.buttonCalibrationStart = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.buttonToDef = new System.Windows.Forms.Button();
            this.buttonDcDrAdj = new System.Windows.Forms.Button();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.textBoxModuleNum = new System.Windows.Forms.TextBox();
            this.buttonCalStop = new System.Windows.Forms.Button();
            this.label79 = new System.Windows.Forms.Label();
            this.checkBoxM19 = new System.Windows.Forms.CheckBox();
            this.checkBoxM9 = new System.Windows.Forms.CheckBox();
            this.checkBoxM18 = new System.Windows.Forms.CheckBox();
            this.checkBoxM8 = new System.Windows.Forms.CheckBox();
            this.checkBoxM17 = new System.Windows.Forms.CheckBox();
            this.checkBoxM7 = new System.Windows.Forms.CheckBox();
            this.checkBoxM16 = new System.Windows.Forms.CheckBox();
            this.checkBoxM6 = new System.Windows.Forms.CheckBox();
            this.checkBoxM15 = new System.Windows.Forms.CheckBox();
            this.checkBoxM5 = new System.Windows.Forms.CheckBox();
            this.checkBoxM14 = new System.Windows.Forms.CheckBox();
            this.checkBoxM4 = new System.Windows.Forms.CheckBox();
            this.checkBoxM13 = new System.Windows.Forms.CheckBox();
            this.checkBoxM3 = new System.Windows.Forms.CheckBox();
            this.checkBoxM12 = new System.Windows.Forms.CheckBox();
            this.checkBoxM2 = new System.Windows.Forms.CheckBox();
            this.checkBoxM11 = new System.Windows.Forms.CheckBox();
            this.checkBoxM1 = new System.Windows.Forms.CheckBox();
            this.checkBoxM10 = new System.Windows.Forms.CheckBox();
            this.checkBoxM0 = new System.Windows.Forms.CheckBox();
            this.buttonTsAdj = new System.Windows.Forms.Button();
            this.buttonGainAdj = new System.Windows.Forms.Button();
            this.buttonPpmAdj = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.checkBoxPic = new System.Windows.Forms.CheckBox();
            this.labelM = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton20 = new System.Windows.Forms.RadioButton();
            this.radioButton11 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton19 = new System.Windows.Forms.RadioButton();
            this.radioButton12 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton18 = new System.Windows.Forms.RadioButton();
            this.radioButton13 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton17 = new System.Windows.Forms.RadioButton();
            this.radioButton14 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton16 = new System.Windows.Forms.RadioButton();
            this.radioButton15 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.buttonLogClear = new System.Windows.Forms.Button();
            this.buttonLogClear1 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.textBoxT2 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.panel9.SuspendLayout();
            this.groupBox22.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox20.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox0.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPpm2)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPpm)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBoxInit.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel12.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Location = new System.Drawing.Point(8, 595);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.Size = new System.Drawing.Size(465, 153);
            this.richTextBoxLog.TabIndex = 2;
            this.richTextBoxLog.Text = "";
            // 
            // richTextBoxCalLog
            // 
            this.richTextBoxCalLog.Location = new System.Drawing.Point(499, 595);
            this.richTextBoxCalLog.Name = "richTextBoxCalLog";
            this.richTextBoxCalLog.Size = new System.Drawing.Size(493, 153);
            this.richTextBoxCalLog.TabIndex = 2;
            this.richTextBoxCalLog.Text = "";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.buttonBadModule);
            this.panel1.Controls.Add(this.buttonFw);
            this.panel1.Controls.Add(this.buttonRead);
            this.panel1.Controls.Add(this.buttonRef);
            this.panel1.Controls.Add(this.buttonMonEnable);
            this.panel1.Controls.Add(this.buttonAmb);
            this.panel1.Controls.Add(this.buttonAdAll);
            this.panel1.Controls.Add(this.buttonMuxIo);
            this.panel1.Controls.Add(this.buttonAdRead);
            this.panel1.Controls.Add(this.checkBoxOnOff);
            this.panel1.Controls.Add(this.buttonMuxEn);
            this.panel1.Controls.Add(this.buttonMuxSel);
            this.panel1.Controls.Add(this.buttonPower);
            this.panel1.Controls.Add(this.buttonTpsSingle);
            this.panel1.Controls.Add(this.comboBoxTpsSingle);
            this.panel1.Controls.Add(this.buttonMfc);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBoxRh);
            this.panel1.Controls.Add(this.textBoxAmb);
            this.panel1.Controls.Add(this.textBoxRef);
            this.panel1.Location = new System.Drawing.Point(1023, 595);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(335, 155);
            this.panel1.TabIndex = 3;
            // 
            // buttonBadModule
            // 
            this.buttonBadModule.Location = new System.Drawing.Point(4, 127);
            this.buttonBadModule.Name = "buttonBadModule";
            this.buttonBadModule.Size = new System.Drawing.Size(73, 23);
            this.buttonBadModule.TabIndex = 18;
            this.buttonBadModule.Text = "Kill";
            this.buttonBadModule.UseVisualStyleBackColor = true;
            this.buttonBadModule.Click += new System.EventHandler(this.buttonBadModule_Click);
            // 
            // buttonFw
            // 
            this.buttonFw.Location = new System.Drawing.Point(110, 4);
            this.buttonFw.Name = "buttonFw";
            this.buttonFw.Size = new System.Drawing.Size(57, 23);
            this.buttonFw.TabIndex = 17;
            this.buttonFw.Text = "FW";
            this.buttonFw.UseVisualStyleBackColor = true;
            this.buttonFw.Click += new System.EventHandler(this.buttonFw_Click);
            // 
            // buttonRead
            // 
            this.buttonRead.Location = new System.Drawing.Point(255, 102);
            this.buttonRead.Name = "buttonRead";
            this.buttonRead.Size = new System.Drawing.Size(73, 23);
            this.buttonRead.TabIndex = 5;
            this.buttonRead.Text = "Read";
            this.buttonRead.UseVisualStyleBackColor = true;
            this.buttonRead.Click += new System.EventHandler(this.buttonRead_Click);
            // 
            // buttonRef
            // 
            this.buttonRef.Location = new System.Drawing.Point(4, 101);
            this.buttonRef.Name = "buttonRef";
            this.buttonRef.Size = new System.Drawing.Size(73, 23);
            this.buttonRef.TabIndex = 16;
            this.buttonRef.Text = "REF";
            this.buttonRef.UseVisualStyleBackColor = true;
            this.buttonRef.Click += new System.EventHandler(this.buttonRef_Click);
            // 
            // buttonMonEnable
            // 
            this.buttonMonEnable.Location = new System.Drawing.Point(255, 49);
            this.buttonMonEnable.Name = "buttonMonEnable";
            this.buttonMonEnable.Size = new System.Drawing.Size(73, 36);
            this.buttonMonEnable.TabIndex = 5;
            this.buttonMonEnable.Text = "Mon Enable";
            this.buttonMonEnable.UseVisualStyleBackColor = true;
            this.buttonMonEnable.Click += new System.EventHandler(this.buttonMonEnable_Click);
            // 
            // buttonAmb
            // 
            this.buttonAmb.Location = new System.Drawing.Point(173, 128);
            this.buttonAmb.Name = "buttonAmb";
            this.buttonAmb.Size = new System.Drawing.Size(69, 23);
            this.buttonAmb.TabIndex = 15;
            this.buttonAmb.Text = "AMB";
            this.buttonAmb.UseVisualStyleBackColor = true;
            this.buttonAmb.Click += new System.EventHandler(this.buttonAmb_Click);
            // 
            // buttonAdAll
            // 
            this.buttonAdAll.Location = new System.Drawing.Point(98, 127);
            this.buttonAdAll.Name = "buttonAdAll";
            this.buttonAdAll.Size = new System.Drawing.Size(69, 23);
            this.buttonAdAll.TabIndex = 14;
            this.buttonAdAll.Text = "AD All";
            this.buttonAdAll.UseVisualStyleBackColor = true;
            this.buttonAdAll.Click += new System.EventHandler(this.buttonAdAll_Click);
            // 
            // buttonMuxIo
            // 
            this.buttonMuxIo.Location = new System.Drawing.Point(110, 50);
            this.buttonMuxIo.Name = "buttonMuxIo";
            this.buttonMuxIo.Size = new System.Drawing.Size(57, 23);
            this.buttonMuxIo.TabIndex = 13;
            this.buttonMuxIo.Text = "MUX IO";
            this.buttonMuxIo.UseVisualStyleBackColor = true;
            this.buttonMuxIo.Click += new System.EventHandler(this.buttonMuxIo_Click);
            // 
            // buttonAdRead
            // 
            this.buttonAdRead.Location = new System.Drawing.Point(98, 102);
            this.buttonAdRead.Name = "buttonAdRead";
            this.buttonAdRead.Size = new System.Drawing.Size(69, 23);
            this.buttonAdRead.TabIndex = 12;
            this.buttonAdRead.Text = "AD Read";
            this.buttonAdRead.UseVisualStyleBackColor = true;
            this.buttonAdRead.Click += new System.EventHandler(this.buttonAdRead_Click);
            // 
            // checkBoxOnOff
            // 
            this.checkBoxOnOff.AutoSize = true;
            this.checkBoxOnOff.Location = new System.Drawing.Point(173, 28);
            this.checkBoxOnOff.Name = "checkBoxOnOff";
            this.checkBoxOnOff.Size = new System.Drawing.Size(61, 16);
            this.checkBoxOnOff.TabIndex = 11;
            this.checkBoxOnOff.Text = "On/Off";
            this.checkBoxOnOff.UseVisualStyleBackColor = true;
            // 
            // buttonMuxEn
            // 
            this.buttonMuxEn.Location = new System.Drawing.Point(98, 76);
            this.buttonMuxEn.Name = "buttonMuxEn";
            this.buttonMuxEn.Size = new System.Drawing.Size(69, 23);
            this.buttonMuxEn.TabIndex = 10;
            this.buttonMuxEn.Text = "MUX EN";
            this.buttonMuxEn.UseVisualStyleBackColor = true;
            this.buttonMuxEn.Click += new System.EventHandler(this.buttonMuxEn_Click);
            // 
            // buttonMuxSel
            // 
            this.buttonMuxSel.Location = new System.Drawing.Point(173, 102);
            this.buttonMuxSel.Name = "buttonMuxSel";
            this.buttonMuxSel.Size = new System.Drawing.Size(69, 23);
            this.buttonMuxSel.TabIndex = 9;
            this.buttonMuxSel.Text = "MUX Sel";
            this.buttonMuxSel.UseVisualStyleBackColor = true;
            this.buttonMuxSel.Click += new System.EventHandler(this.buttonMuxSel_Click);
            // 
            // buttonPower
            // 
            this.buttonPower.Location = new System.Drawing.Point(173, 77);
            this.buttonPower.Name = "buttonPower";
            this.buttonPower.Size = new System.Drawing.Size(69, 23);
            this.buttonPower.TabIndex = 8;
            this.buttonPower.Text = "Power";
            this.buttonPower.UseVisualStyleBackColor = true;
            this.buttonPower.Click += new System.EventHandler(this.buttonPower_Click);
            // 
            // buttonTpsSingle
            // 
            this.buttonTpsSingle.Location = new System.Drawing.Point(173, 50);
            this.buttonTpsSingle.Name = "buttonTpsSingle";
            this.buttonTpsSingle.Size = new System.Drawing.Size(69, 23);
            this.buttonTpsSingle.TabIndex = 6;
            this.buttonTpsSingle.Text = "Single";
            this.buttonTpsSingle.UseVisualStyleBackColor = true;
            this.buttonTpsSingle.Click += new System.EventHandler(this.buttonTpsSingle_Click);
            // 
            // comboBoxTpsSingle
            // 
            this.comboBoxTpsSingle.FormattingEnabled = true;
            this.comboBoxTpsSingle.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19"});
            this.comboBoxTpsSingle.Location = new System.Drawing.Point(173, 4);
            this.comboBoxTpsSingle.Name = "comboBoxTpsSingle";
            this.comboBoxTpsSingle.Size = new System.Drawing.Size(69, 20);
            this.comboBoxTpsSingle.TabIndex = 5;
            // 
            // buttonMfc
            // 
            this.buttonMfc.Location = new System.Drawing.Point(4, 75);
            this.buttonMfc.Name = "buttonMfc";
            this.buttonMfc.Size = new System.Drawing.Size(73, 23);
            this.buttonMfc.TabIndex = 4;
            this.buttonMfc.Text = "MFC";
            this.buttonMfc.UseVisualStyleBackColor = true;
            this.buttonMfc.Click += new System.EventHandler(this.buttonMfc_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(79, 54);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "RH";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(79, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "AMB";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(79, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "REF";
            // 
            // textBoxRh
            // 
            this.textBoxRh.Location = new System.Drawing.Point(4, 51);
            this.textBoxRh.Name = "textBoxRh";
            this.textBoxRh.Size = new System.Drawing.Size(73, 21);
            this.textBoxRh.TabIndex = 2;
            this.textBoxRh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxAmb
            // 
            this.textBoxAmb.Location = new System.Drawing.Point(4, 27);
            this.textBoxAmb.Name = "textBoxAmb";
            this.textBoxAmb.Size = new System.Drawing.Size(73, 21);
            this.textBoxAmb.TabIndex = 1;
            this.textBoxAmb.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxRef
            // 
            this.textBoxRef.Location = new System.Drawing.Point(4, 4);
            this.textBoxRef.Name = "textBoxRef";
            this.textBoxRef.Size = new System.Drawing.Size(73, 21);
            this.textBoxRef.TabIndex = 0;
            this.textBoxRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonInit
            // 
            this.buttonInit.Location = new System.Drawing.Point(6, 211);
            this.buttonInit.Name = "buttonInit";
            this.buttonInit.Size = new System.Drawing.Size(167, 23);
            this.buttonInit.TabIndex = 5;
            this.buttonInit.Text = "INIT";
            this.buttonInit.UseVisualStyleBackColor = true;
            this.buttonInit.Click += new System.EventHandler(this.buttonInit_Click);
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.buttonWriteAll);
            this.tabPage5.Controls.Add(this.buttonDefault);
            this.tabPage5.Controls.Add(this.panel9);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1352, 556);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Manual Calibration";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // buttonWriteAll
            // 
            this.buttonWriteAll.Location = new System.Drawing.Point(1003, 69);
            this.buttonWriteAll.Name = "buttonWriteAll";
            this.buttonWriteAll.Size = new System.Drawing.Size(75, 23);
            this.buttonWriteAll.TabIndex = 1;
            this.buttonWriteAll.Text = "Write All";
            this.buttonWriteAll.UseVisualStyleBackColor = true;
            this.buttonWriteAll.Click += new System.EventHandler(this.buttonWriteAll_Click);
            // 
            // buttonDefault
            // 
            this.buttonDefault.Location = new System.Drawing.Point(1003, 29);
            this.buttonDefault.Name = "buttonDefault";
            this.buttonDefault.Size = new System.Drawing.Size(116, 23);
            this.buttonDefault.TabIndex = 1;
            this.buttonDefault.Text = "Set To Default";
            this.buttonDefault.UseVisualStyleBackColor = true;
            this.buttonDefault.Click += new System.EventHandler(this.buttonDefault_Click);
            // 
            // panel9
            // 
            this.panel9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel9.Controls.Add(this.groupBox22);
            this.panel9.Controls.Add(this.groupBox21);
            this.panel9.Location = new System.Drawing.Point(4, 4);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(980, 549);
            this.panel9.TabIndex = 0;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.groupBox11);
            this.groupBox22.Controls.Add(this.groupBox16);
            this.groupBox22.Controls.Add(this.groupBox20);
            this.groupBox22.Controls.Add(this.groupBox12);
            this.groupBox22.Controls.Add(this.groupBox15);
            this.groupBox22.Controls.Add(this.groupBox17);
            this.groupBox22.Controls.Add(this.groupBox19);
            this.groupBox22.Controls.Add(this.groupBox13);
            this.groupBox22.Controls.Add(this.groupBox14);
            this.groupBox22.Controls.Add(this.groupBox18);
            this.groupBox22.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox22.Location = new System.Drawing.Point(484, 23);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(452, 485);
            this.groupBox22.TabIndex = 3;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "ARRAY 1";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.textBoxSc10);
            this.groupBox11.Controls.Add(this.buttonGain10);
            this.groupBox11.Controls.Add(this.textBoxSr10);
            this.groupBox11.Controls.Add(this.buttonTs10);
            this.groupBox11.Controls.Add(this.textBoxGain10);
            this.groupBox11.Controls.Add(this.buttonSr10);
            this.groupBox11.Controls.Add(this.textBoxTs10);
            this.groupBox11.Controls.Add(this.buttonSc10);
            this.groupBox11.Location = new System.Drawing.Point(18, 28);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(407, 38);
            this.groupBox11.TabIndex = 2;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "M10";
            // 
            // textBoxSc10
            // 
            this.textBoxSc10.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc10.Name = "textBoxSc10";
            this.textBoxSc10.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc10.TabIndex = 0;
            this.textBoxSc10.Text = "1800000";
            this.textBoxSc10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain10
            // 
            this.buttonGain10.Location = new System.Drawing.Point(368, 11);
            this.buttonGain10.Name = "buttonGain10";
            this.buttonGain10.Size = new System.Drawing.Size(30, 23);
            this.buttonGain10.TabIndex = 1;
            this.buttonGain10.Text = "Gain";
            this.buttonGain10.UseVisualStyleBackColor = true;
            this.buttonGain10.Click += new System.EventHandler(this.buttonGain10_Click);
            // 
            // textBoxSr10
            // 
            this.textBoxSr10.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr10.Name = "textBoxSr10";
            this.textBoxSr10.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr10.TabIndex = 0;
            this.textBoxSr10.Text = "1500000";
            this.textBoxSr10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs10
            // 
            this.buttonTs10.Location = new System.Drawing.Point(263, 11);
            this.buttonTs10.Name = "buttonTs10";
            this.buttonTs10.Size = new System.Drawing.Size(30, 23);
            this.buttonTs10.TabIndex = 1;
            this.buttonTs10.Text = "ts";
            this.buttonTs10.UseVisualStyleBackColor = true;
            this.buttonTs10.Click += new System.EventHandler(this.buttonTs10_Click);
            // 
            // textBoxGain10
            // 
            this.textBoxGain10.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain10.Name = "textBoxGain10";
            this.textBoxGain10.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain10.TabIndex = 0;
            this.textBoxGain10.Text = "50000";
            this.textBoxGain10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr10
            // 
            this.buttonSr10.Location = new System.Drawing.Point(181, 10);
            this.buttonSr10.Name = "buttonSr10";
            this.buttonSr10.Size = new System.Drawing.Size(30, 23);
            this.buttonSr10.TabIndex = 1;
            this.buttonSr10.Text = "sR";
            this.buttonSr10.UseVisualStyleBackColor = true;
            this.buttonSr10.Click += new System.EventHandler(this.buttonSr10_Click);
            // 
            // textBoxTs10
            // 
            this.textBoxTs10.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs10.Name = "textBoxTs10";
            this.textBoxTs10.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs10.TabIndex = 0;
            this.textBoxTs10.Text = "8";
            this.textBoxTs10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc10
            // 
            this.buttonSc10.Location = new System.Drawing.Point(70, 11);
            this.buttonSc10.Name = "buttonSc10";
            this.buttonSc10.Size = new System.Drawing.Size(30, 23);
            this.buttonSc10.TabIndex = 1;
            this.buttonSc10.Text = "sC";
            this.buttonSc10.UseVisualStyleBackColor = true;
            this.buttonSc10.Click += new System.EventHandler(this.buttonSc10_Click);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.textBoxSc15);
            this.groupBox16.Controls.Add(this.buttonGain15);
            this.groupBox16.Controls.Add(this.textBoxSr15);
            this.groupBox16.Controls.Add(this.buttonTs15);
            this.groupBox16.Controls.Add(this.textBoxGain15);
            this.groupBox16.Controls.Add(this.buttonSr15);
            this.groupBox16.Controls.Add(this.textBoxTs15);
            this.groupBox16.Controls.Add(this.buttonSc15);
            this.groupBox16.Location = new System.Drawing.Point(18, 248);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(407, 38);
            this.groupBox16.TabIndex = 2;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "M15";
            // 
            // textBoxSc15
            // 
            this.textBoxSc15.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc15.Name = "textBoxSc15";
            this.textBoxSc15.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc15.TabIndex = 0;
            this.textBoxSc15.Text = "1800000";
            this.textBoxSc15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain15
            // 
            this.buttonGain15.Location = new System.Drawing.Point(368, 11);
            this.buttonGain15.Name = "buttonGain15";
            this.buttonGain15.Size = new System.Drawing.Size(30, 23);
            this.buttonGain15.TabIndex = 1;
            this.buttonGain15.Text = "Gain";
            this.buttonGain15.UseVisualStyleBackColor = true;
            this.buttonGain15.Click += new System.EventHandler(this.buttonGain15_Click);
            // 
            // textBoxSr15
            // 
            this.textBoxSr15.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr15.Name = "textBoxSr15";
            this.textBoxSr15.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr15.TabIndex = 0;
            this.textBoxSr15.Text = "1500000";
            this.textBoxSr15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs15
            // 
            this.buttonTs15.Location = new System.Drawing.Point(263, 11);
            this.buttonTs15.Name = "buttonTs15";
            this.buttonTs15.Size = new System.Drawing.Size(30, 23);
            this.buttonTs15.TabIndex = 1;
            this.buttonTs15.Text = "ts";
            this.buttonTs15.UseVisualStyleBackColor = true;
            this.buttonTs15.Click += new System.EventHandler(this.buttonTs15_Click);
            // 
            // textBoxGain15
            // 
            this.textBoxGain15.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain15.Name = "textBoxGain15";
            this.textBoxGain15.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain15.TabIndex = 0;
            this.textBoxGain15.Text = "50000";
            this.textBoxGain15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr15
            // 
            this.buttonSr15.Location = new System.Drawing.Point(181, 10);
            this.buttonSr15.Name = "buttonSr15";
            this.buttonSr15.Size = new System.Drawing.Size(30, 23);
            this.buttonSr15.TabIndex = 1;
            this.buttonSr15.Text = "sR";
            this.buttonSr15.UseVisualStyleBackColor = true;
            this.buttonSr15.Click += new System.EventHandler(this.buttonSr15_Click);
            // 
            // textBoxTs15
            // 
            this.textBoxTs15.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs15.Name = "textBoxTs15";
            this.textBoxTs15.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs15.TabIndex = 0;
            this.textBoxTs15.Text = "8";
            this.textBoxTs15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc15
            // 
            this.buttonSc15.Location = new System.Drawing.Point(70, 11);
            this.buttonSc15.Name = "buttonSc15";
            this.buttonSc15.Size = new System.Drawing.Size(30, 23);
            this.buttonSc15.TabIndex = 1;
            this.buttonSc15.Text = "sC";
            this.buttonSc15.UseVisualStyleBackColor = true;
            this.buttonSc15.Click += new System.EventHandler(this.buttonSc15_Click);
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.textBoxSc19);
            this.groupBox20.Controls.Add(this.buttonGain19);
            this.groupBox20.Controls.Add(this.textBoxSr19);
            this.groupBox20.Controls.Add(this.buttonTs19);
            this.groupBox20.Controls.Add(this.textBoxGain19);
            this.groupBox20.Controls.Add(this.buttonSr19);
            this.groupBox20.Controls.Add(this.textBoxTs19);
            this.groupBox20.Controls.Add(this.buttonSc19);
            this.groupBox20.Location = new System.Drawing.Point(18, 424);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(407, 38);
            this.groupBox20.TabIndex = 2;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "M19";
            // 
            // textBoxSc19
            // 
            this.textBoxSc19.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc19.Name = "textBoxSc19";
            this.textBoxSc19.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc19.TabIndex = 0;
            this.textBoxSc19.Text = "1800000";
            this.textBoxSc19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain19
            // 
            this.buttonGain19.Location = new System.Drawing.Point(368, 11);
            this.buttonGain19.Name = "buttonGain19";
            this.buttonGain19.Size = new System.Drawing.Size(30, 23);
            this.buttonGain19.TabIndex = 1;
            this.buttonGain19.Text = "Gain";
            this.buttonGain19.UseVisualStyleBackColor = true;
            this.buttonGain19.Click += new System.EventHandler(this.buttonGain19_Click);
            // 
            // textBoxSr19
            // 
            this.textBoxSr19.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr19.Name = "textBoxSr19";
            this.textBoxSr19.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr19.TabIndex = 0;
            this.textBoxSr19.Text = "1500000";
            this.textBoxSr19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs19
            // 
            this.buttonTs19.Location = new System.Drawing.Point(263, 11);
            this.buttonTs19.Name = "buttonTs19";
            this.buttonTs19.Size = new System.Drawing.Size(30, 23);
            this.buttonTs19.TabIndex = 1;
            this.buttonTs19.Text = "ts";
            this.buttonTs19.UseVisualStyleBackColor = true;
            this.buttonTs19.Click += new System.EventHandler(this.buttonTs19_Click);
            // 
            // textBoxGain19
            // 
            this.textBoxGain19.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain19.Name = "textBoxGain19";
            this.textBoxGain19.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain19.TabIndex = 0;
            this.textBoxGain19.Text = "50000";
            this.textBoxGain19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr19
            // 
            this.buttonSr19.Location = new System.Drawing.Point(181, 10);
            this.buttonSr19.Name = "buttonSr19";
            this.buttonSr19.Size = new System.Drawing.Size(30, 23);
            this.buttonSr19.TabIndex = 1;
            this.buttonSr19.Text = "sR";
            this.buttonSr19.UseVisualStyleBackColor = true;
            this.buttonSr19.Click += new System.EventHandler(this.buttonSr19_Click);
            // 
            // textBoxTs19
            // 
            this.textBoxTs19.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs19.Name = "textBoxTs19";
            this.textBoxTs19.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs19.TabIndex = 0;
            this.textBoxTs19.Text = "8";
            this.textBoxTs19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc19
            // 
            this.buttonSc19.Location = new System.Drawing.Point(70, 11);
            this.buttonSc19.Name = "buttonSc19";
            this.buttonSc19.Size = new System.Drawing.Size(30, 23);
            this.buttonSc19.TabIndex = 1;
            this.buttonSc19.Text = "sC";
            this.buttonSc19.UseVisualStyleBackColor = true;
            this.buttonSc19.Click += new System.EventHandler(this.buttonSc19_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.textBoxSc11);
            this.groupBox12.Controls.Add(this.buttonGain11);
            this.groupBox12.Controls.Add(this.textBoxSr11);
            this.groupBox12.Controls.Add(this.buttonTs11);
            this.groupBox12.Controls.Add(this.textBoxGain11);
            this.groupBox12.Controls.Add(this.buttonSr11);
            this.groupBox12.Controls.Add(this.textBoxTs11);
            this.groupBox12.Controls.Add(this.buttonSc11);
            this.groupBox12.Location = new System.Drawing.Point(18, 72);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(407, 38);
            this.groupBox12.TabIndex = 2;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "M11";
            // 
            // textBoxSc11
            // 
            this.textBoxSc11.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc11.Name = "textBoxSc11";
            this.textBoxSc11.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc11.TabIndex = 0;
            this.textBoxSc11.Text = "1800000";
            this.textBoxSc11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain11
            // 
            this.buttonGain11.Location = new System.Drawing.Point(368, 11);
            this.buttonGain11.Name = "buttonGain11";
            this.buttonGain11.Size = new System.Drawing.Size(30, 23);
            this.buttonGain11.TabIndex = 1;
            this.buttonGain11.Text = "Gain";
            this.buttonGain11.UseVisualStyleBackColor = true;
            this.buttonGain11.Click += new System.EventHandler(this.buttonGain11_Click);
            // 
            // textBoxSr11
            // 
            this.textBoxSr11.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr11.Name = "textBoxSr11";
            this.textBoxSr11.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr11.TabIndex = 0;
            this.textBoxSr11.Text = "1500000";
            this.textBoxSr11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs11
            // 
            this.buttonTs11.Location = new System.Drawing.Point(263, 11);
            this.buttonTs11.Name = "buttonTs11";
            this.buttonTs11.Size = new System.Drawing.Size(30, 23);
            this.buttonTs11.TabIndex = 1;
            this.buttonTs11.Text = "ts";
            this.buttonTs11.UseVisualStyleBackColor = true;
            this.buttonTs11.Click += new System.EventHandler(this.buttonTs11_Click);
            // 
            // textBoxGain11
            // 
            this.textBoxGain11.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain11.Name = "textBoxGain11";
            this.textBoxGain11.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain11.TabIndex = 0;
            this.textBoxGain11.Text = "50000";
            this.textBoxGain11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr11
            // 
            this.buttonSr11.Location = new System.Drawing.Point(181, 10);
            this.buttonSr11.Name = "buttonSr11";
            this.buttonSr11.Size = new System.Drawing.Size(30, 23);
            this.buttonSr11.TabIndex = 1;
            this.buttonSr11.Text = "sR";
            this.buttonSr11.UseVisualStyleBackColor = true;
            this.buttonSr11.Click += new System.EventHandler(this.buttonSr11_Click);
            // 
            // textBoxTs11
            // 
            this.textBoxTs11.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs11.Name = "textBoxTs11";
            this.textBoxTs11.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs11.TabIndex = 0;
            this.textBoxTs11.Text = "8";
            this.textBoxTs11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc11
            // 
            this.buttonSc11.Location = new System.Drawing.Point(70, 11);
            this.buttonSc11.Name = "buttonSc11";
            this.buttonSc11.Size = new System.Drawing.Size(30, 23);
            this.buttonSc11.TabIndex = 1;
            this.buttonSc11.Text = "sC";
            this.buttonSc11.UseVisualStyleBackColor = true;
            this.buttonSc11.Click += new System.EventHandler(this.buttonSc11_Click);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.textBoxSc14);
            this.groupBox15.Controls.Add(this.buttonGain14);
            this.groupBox15.Controls.Add(this.textBoxSr14);
            this.groupBox15.Controls.Add(this.buttonTs14);
            this.groupBox15.Controls.Add(this.textBoxGain14);
            this.groupBox15.Controls.Add(this.buttonSr14);
            this.groupBox15.Controls.Add(this.textBoxTs14);
            this.groupBox15.Controls.Add(this.buttonSc14);
            this.groupBox15.Location = new System.Drawing.Point(18, 204);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(407, 38);
            this.groupBox15.TabIndex = 2;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "M14";
            // 
            // textBoxSc14
            // 
            this.textBoxSc14.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc14.Name = "textBoxSc14";
            this.textBoxSc14.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc14.TabIndex = 0;
            this.textBoxSc14.Text = "1800000";
            this.textBoxSc14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain14
            // 
            this.buttonGain14.Location = new System.Drawing.Point(368, 11);
            this.buttonGain14.Name = "buttonGain14";
            this.buttonGain14.Size = new System.Drawing.Size(30, 23);
            this.buttonGain14.TabIndex = 1;
            this.buttonGain14.Text = "Gain";
            this.buttonGain14.UseVisualStyleBackColor = true;
            this.buttonGain14.Click += new System.EventHandler(this.buttonGain14_Click);
            // 
            // textBoxSr14
            // 
            this.textBoxSr14.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr14.Name = "textBoxSr14";
            this.textBoxSr14.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr14.TabIndex = 0;
            this.textBoxSr14.Text = "1500000";
            this.textBoxSr14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs14
            // 
            this.buttonTs14.Location = new System.Drawing.Point(263, 11);
            this.buttonTs14.Name = "buttonTs14";
            this.buttonTs14.Size = new System.Drawing.Size(30, 23);
            this.buttonTs14.TabIndex = 1;
            this.buttonTs14.Text = "ts";
            this.buttonTs14.UseVisualStyleBackColor = true;
            this.buttonTs14.Click += new System.EventHandler(this.buttonTs14_Click);
            // 
            // textBoxGain14
            // 
            this.textBoxGain14.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain14.Name = "textBoxGain14";
            this.textBoxGain14.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain14.TabIndex = 0;
            this.textBoxGain14.Text = "50000";
            this.textBoxGain14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr14
            // 
            this.buttonSr14.Location = new System.Drawing.Point(181, 10);
            this.buttonSr14.Name = "buttonSr14";
            this.buttonSr14.Size = new System.Drawing.Size(30, 23);
            this.buttonSr14.TabIndex = 1;
            this.buttonSr14.Text = "sR";
            this.buttonSr14.UseVisualStyleBackColor = true;
            this.buttonSr14.Click += new System.EventHandler(this.buttonSr14_Click);
            // 
            // textBoxTs14
            // 
            this.textBoxTs14.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs14.Name = "textBoxTs14";
            this.textBoxTs14.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs14.TabIndex = 0;
            this.textBoxTs14.Text = "8";
            this.textBoxTs14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc14
            // 
            this.buttonSc14.Location = new System.Drawing.Point(70, 11);
            this.buttonSc14.Name = "buttonSc14";
            this.buttonSc14.Size = new System.Drawing.Size(30, 23);
            this.buttonSc14.TabIndex = 1;
            this.buttonSc14.Text = "sC";
            this.buttonSc14.UseVisualStyleBackColor = true;
            this.buttonSc14.Click += new System.EventHandler(this.buttonSc14_Click);
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.textBoxSc16);
            this.groupBox17.Controls.Add(this.buttonGain16);
            this.groupBox17.Controls.Add(this.textBoxSr16);
            this.groupBox17.Controls.Add(this.buttonTs16);
            this.groupBox17.Controls.Add(this.textBoxGain16);
            this.groupBox17.Controls.Add(this.buttonSr16);
            this.groupBox17.Controls.Add(this.textBoxTs16);
            this.groupBox17.Controls.Add(this.buttonSc16);
            this.groupBox17.Location = new System.Drawing.Point(18, 292);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(407, 38);
            this.groupBox17.TabIndex = 2;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "M16";
            // 
            // textBoxSc16
            // 
            this.textBoxSc16.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc16.Name = "textBoxSc16";
            this.textBoxSc16.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc16.TabIndex = 0;
            this.textBoxSc16.Text = "1800000";
            this.textBoxSc16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain16
            // 
            this.buttonGain16.Location = new System.Drawing.Point(368, 11);
            this.buttonGain16.Name = "buttonGain16";
            this.buttonGain16.Size = new System.Drawing.Size(30, 23);
            this.buttonGain16.TabIndex = 1;
            this.buttonGain16.Text = "Gain";
            this.buttonGain16.UseVisualStyleBackColor = true;
            this.buttonGain16.Click += new System.EventHandler(this.buttonGain16_Click);
            // 
            // textBoxSr16
            // 
            this.textBoxSr16.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr16.Name = "textBoxSr16";
            this.textBoxSr16.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr16.TabIndex = 0;
            this.textBoxSr16.Text = "1500000";
            this.textBoxSr16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs16
            // 
            this.buttonTs16.Location = new System.Drawing.Point(263, 11);
            this.buttonTs16.Name = "buttonTs16";
            this.buttonTs16.Size = new System.Drawing.Size(30, 23);
            this.buttonTs16.TabIndex = 1;
            this.buttonTs16.Text = "ts";
            this.buttonTs16.UseVisualStyleBackColor = true;
            this.buttonTs16.Click += new System.EventHandler(this.buttonTs16_Click);
            // 
            // textBoxGain16
            // 
            this.textBoxGain16.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain16.Name = "textBoxGain16";
            this.textBoxGain16.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain16.TabIndex = 0;
            this.textBoxGain16.Text = "50000";
            this.textBoxGain16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr16
            // 
            this.buttonSr16.Location = new System.Drawing.Point(181, 10);
            this.buttonSr16.Name = "buttonSr16";
            this.buttonSr16.Size = new System.Drawing.Size(30, 23);
            this.buttonSr16.TabIndex = 1;
            this.buttonSr16.Text = "sR";
            this.buttonSr16.UseVisualStyleBackColor = true;
            this.buttonSr16.Click += new System.EventHandler(this.buttonSr16_Click);
            // 
            // textBoxTs16
            // 
            this.textBoxTs16.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs16.Name = "textBoxTs16";
            this.textBoxTs16.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs16.TabIndex = 0;
            this.textBoxTs16.Text = "8";
            this.textBoxTs16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc16
            // 
            this.buttonSc16.Location = new System.Drawing.Point(70, 11);
            this.buttonSc16.Name = "buttonSc16";
            this.buttonSc16.Size = new System.Drawing.Size(30, 23);
            this.buttonSc16.TabIndex = 1;
            this.buttonSc16.Text = "sC";
            this.buttonSc16.UseVisualStyleBackColor = true;
            this.buttonSc16.Click += new System.EventHandler(this.buttonSc16_Click);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.textBoxSc18);
            this.groupBox19.Controls.Add(this.buttonGain18);
            this.groupBox19.Controls.Add(this.textBoxSr18);
            this.groupBox19.Controls.Add(this.buttonTs18);
            this.groupBox19.Controls.Add(this.textBoxGain18);
            this.groupBox19.Controls.Add(this.buttonSr18);
            this.groupBox19.Controls.Add(this.textBoxTs18);
            this.groupBox19.Controls.Add(this.buttonSc18);
            this.groupBox19.Location = new System.Drawing.Point(18, 380);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(407, 38);
            this.groupBox19.TabIndex = 2;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "M18";
            // 
            // textBoxSc18
            // 
            this.textBoxSc18.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc18.Name = "textBoxSc18";
            this.textBoxSc18.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc18.TabIndex = 0;
            this.textBoxSc18.Text = "1800000";
            this.textBoxSc18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain18
            // 
            this.buttonGain18.Location = new System.Drawing.Point(368, 11);
            this.buttonGain18.Name = "buttonGain18";
            this.buttonGain18.Size = new System.Drawing.Size(30, 23);
            this.buttonGain18.TabIndex = 1;
            this.buttonGain18.Text = "Gain";
            this.buttonGain18.UseVisualStyleBackColor = true;
            this.buttonGain18.Click += new System.EventHandler(this.buttonGain18_Click);
            // 
            // textBoxSr18
            // 
            this.textBoxSr18.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr18.Name = "textBoxSr18";
            this.textBoxSr18.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr18.TabIndex = 0;
            this.textBoxSr18.Text = "1500000";
            this.textBoxSr18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs18
            // 
            this.buttonTs18.Location = new System.Drawing.Point(263, 11);
            this.buttonTs18.Name = "buttonTs18";
            this.buttonTs18.Size = new System.Drawing.Size(30, 23);
            this.buttonTs18.TabIndex = 1;
            this.buttonTs18.Text = "ts";
            this.buttonTs18.UseVisualStyleBackColor = true;
            this.buttonTs18.Click += new System.EventHandler(this.buttonTs18_Click);
            // 
            // textBoxGain18
            // 
            this.textBoxGain18.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain18.Name = "textBoxGain18";
            this.textBoxGain18.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain18.TabIndex = 0;
            this.textBoxGain18.Text = "50000";
            this.textBoxGain18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr18
            // 
            this.buttonSr18.Location = new System.Drawing.Point(181, 10);
            this.buttonSr18.Name = "buttonSr18";
            this.buttonSr18.Size = new System.Drawing.Size(30, 23);
            this.buttonSr18.TabIndex = 1;
            this.buttonSr18.Text = "sR";
            this.buttonSr18.UseVisualStyleBackColor = true;
            this.buttonSr18.Click += new System.EventHandler(this.buttonSr18_Click);
            // 
            // textBoxTs18
            // 
            this.textBoxTs18.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs18.Name = "textBoxTs18";
            this.textBoxTs18.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs18.TabIndex = 0;
            this.textBoxTs18.Text = "8";
            this.textBoxTs18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc18
            // 
            this.buttonSc18.Location = new System.Drawing.Point(70, 11);
            this.buttonSc18.Name = "buttonSc18";
            this.buttonSc18.Size = new System.Drawing.Size(30, 23);
            this.buttonSc18.TabIndex = 1;
            this.buttonSc18.Text = "sC";
            this.buttonSc18.UseVisualStyleBackColor = true;
            this.buttonSc18.Click += new System.EventHandler(this.buttonSc18_Click);
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.textBoxSc12);
            this.groupBox13.Controls.Add(this.buttonGain12);
            this.groupBox13.Controls.Add(this.textBoxSr12);
            this.groupBox13.Controls.Add(this.buttonTs12);
            this.groupBox13.Controls.Add(this.textBoxGain12);
            this.groupBox13.Controls.Add(this.buttonSr12);
            this.groupBox13.Controls.Add(this.textBoxTs12);
            this.groupBox13.Controls.Add(this.buttonSc12);
            this.groupBox13.Location = new System.Drawing.Point(18, 116);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(407, 38);
            this.groupBox13.TabIndex = 2;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "M12";
            // 
            // textBoxSc12
            // 
            this.textBoxSc12.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc12.Name = "textBoxSc12";
            this.textBoxSc12.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc12.TabIndex = 0;
            this.textBoxSc12.Text = "1800000";
            this.textBoxSc12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain12
            // 
            this.buttonGain12.Location = new System.Drawing.Point(368, 11);
            this.buttonGain12.Name = "buttonGain12";
            this.buttonGain12.Size = new System.Drawing.Size(30, 23);
            this.buttonGain12.TabIndex = 1;
            this.buttonGain12.Text = "Gain";
            this.buttonGain12.UseVisualStyleBackColor = true;
            this.buttonGain12.Click += new System.EventHandler(this.buttonGain12_Click);
            // 
            // textBoxSr12
            // 
            this.textBoxSr12.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr12.Name = "textBoxSr12";
            this.textBoxSr12.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr12.TabIndex = 0;
            this.textBoxSr12.Text = "1500000";
            this.textBoxSr12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs12
            // 
            this.buttonTs12.Location = new System.Drawing.Point(263, 11);
            this.buttonTs12.Name = "buttonTs12";
            this.buttonTs12.Size = new System.Drawing.Size(30, 23);
            this.buttonTs12.TabIndex = 1;
            this.buttonTs12.Text = "ts";
            this.buttonTs12.UseVisualStyleBackColor = true;
            this.buttonTs12.Click += new System.EventHandler(this.buttonTs12_Click);
            // 
            // textBoxGain12
            // 
            this.textBoxGain12.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain12.Name = "textBoxGain12";
            this.textBoxGain12.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain12.TabIndex = 0;
            this.textBoxGain12.Text = "50000";
            this.textBoxGain12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr12
            // 
            this.buttonSr12.Location = new System.Drawing.Point(181, 10);
            this.buttonSr12.Name = "buttonSr12";
            this.buttonSr12.Size = new System.Drawing.Size(30, 23);
            this.buttonSr12.TabIndex = 1;
            this.buttonSr12.Text = "sR";
            this.buttonSr12.UseVisualStyleBackColor = true;
            this.buttonSr12.Click += new System.EventHandler(this.buttonSr12_Click);
            // 
            // textBoxTs12
            // 
            this.textBoxTs12.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs12.Name = "textBoxTs12";
            this.textBoxTs12.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs12.TabIndex = 0;
            this.textBoxTs12.Text = "8";
            this.textBoxTs12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc12
            // 
            this.buttonSc12.Location = new System.Drawing.Point(70, 11);
            this.buttonSc12.Name = "buttonSc12";
            this.buttonSc12.Size = new System.Drawing.Size(30, 23);
            this.buttonSc12.TabIndex = 1;
            this.buttonSc12.Text = "sC";
            this.buttonSc12.UseVisualStyleBackColor = true;
            this.buttonSc12.Click += new System.EventHandler(this.buttonSc12_Click);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.textBoxSc13);
            this.groupBox14.Controls.Add(this.buttonGain13);
            this.groupBox14.Controls.Add(this.textBoxSr13);
            this.groupBox14.Controls.Add(this.buttonTs13);
            this.groupBox14.Controls.Add(this.textBoxGain13);
            this.groupBox14.Controls.Add(this.buttonSr13);
            this.groupBox14.Controls.Add(this.textBoxTs13);
            this.groupBox14.Controls.Add(this.buttonSc13);
            this.groupBox14.Location = new System.Drawing.Point(18, 160);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(407, 38);
            this.groupBox14.TabIndex = 2;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "M13";
            // 
            // textBoxSc13
            // 
            this.textBoxSc13.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc13.Name = "textBoxSc13";
            this.textBoxSc13.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc13.TabIndex = 0;
            this.textBoxSc13.Text = "1800000";
            this.textBoxSc13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain13
            // 
            this.buttonGain13.Location = new System.Drawing.Point(368, 11);
            this.buttonGain13.Name = "buttonGain13";
            this.buttonGain13.Size = new System.Drawing.Size(30, 23);
            this.buttonGain13.TabIndex = 1;
            this.buttonGain13.Text = "Gain";
            this.buttonGain13.UseVisualStyleBackColor = true;
            this.buttonGain13.Click += new System.EventHandler(this.buttonGain13_Click);
            // 
            // textBoxSr13
            // 
            this.textBoxSr13.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr13.Name = "textBoxSr13";
            this.textBoxSr13.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr13.TabIndex = 0;
            this.textBoxSr13.Text = "1500000";
            this.textBoxSr13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs13
            // 
            this.buttonTs13.Location = new System.Drawing.Point(263, 11);
            this.buttonTs13.Name = "buttonTs13";
            this.buttonTs13.Size = new System.Drawing.Size(30, 23);
            this.buttonTs13.TabIndex = 1;
            this.buttonTs13.Text = "ts";
            this.buttonTs13.UseVisualStyleBackColor = true;
            this.buttonTs13.Click += new System.EventHandler(this.buttonTs13_Click);
            // 
            // textBoxGain13
            // 
            this.textBoxGain13.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain13.Name = "textBoxGain13";
            this.textBoxGain13.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain13.TabIndex = 0;
            this.textBoxGain13.Text = "50000";
            this.textBoxGain13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr13
            // 
            this.buttonSr13.Location = new System.Drawing.Point(181, 10);
            this.buttonSr13.Name = "buttonSr13";
            this.buttonSr13.Size = new System.Drawing.Size(30, 23);
            this.buttonSr13.TabIndex = 1;
            this.buttonSr13.Text = "sR";
            this.buttonSr13.UseVisualStyleBackColor = true;
            this.buttonSr13.Click += new System.EventHandler(this.buttonSr13_Click);
            // 
            // textBoxTs13
            // 
            this.textBoxTs13.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs13.Name = "textBoxTs13";
            this.textBoxTs13.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs13.TabIndex = 0;
            this.textBoxTs13.Text = "8";
            this.textBoxTs13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc13
            // 
            this.buttonSc13.Location = new System.Drawing.Point(70, 11);
            this.buttonSc13.Name = "buttonSc13";
            this.buttonSc13.Size = new System.Drawing.Size(30, 23);
            this.buttonSc13.TabIndex = 1;
            this.buttonSc13.Text = "sC";
            this.buttonSc13.UseVisualStyleBackColor = true;
            this.buttonSc13.Click += new System.EventHandler(this.buttonSc13_Click);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.textBoxSc17);
            this.groupBox18.Controls.Add(this.buttonGain17);
            this.groupBox18.Controls.Add(this.textBoxSr17);
            this.groupBox18.Controls.Add(this.buttonTs17);
            this.groupBox18.Controls.Add(this.textBoxGain17);
            this.groupBox18.Controls.Add(this.buttonSr17);
            this.groupBox18.Controls.Add(this.textBoxTs17);
            this.groupBox18.Controls.Add(this.buttonSc17);
            this.groupBox18.Location = new System.Drawing.Point(18, 336);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(407, 38);
            this.groupBox18.TabIndex = 2;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "M17";
            // 
            // textBoxSc17
            // 
            this.textBoxSc17.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc17.Name = "textBoxSc17";
            this.textBoxSc17.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc17.TabIndex = 0;
            this.textBoxSc17.Text = "1800000";
            this.textBoxSc17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain17
            // 
            this.buttonGain17.Location = new System.Drawing.Point(368, 11);
            this.buttonGain17.Name = "buttonGain17";
            this.buttonGain17.Size = new System.Drawing.Size(30, 23);
            this.buttonGain17.TabIndex = 1;
            this.buttonGain17.Text = "Gain";
            this.buttonGain17.UseVisualStyleBackColor = true;
            this.buttonGain17.Click += new System.EventHandler(this.buttonGain17_Click);
            // 
            // textBoxSr17
            // 
            this.textBoxSr17.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr17.Name = "textBoxSr17";
            this.textBoxSr17.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr17.TabIndex = 0;
            this.textBoxSr17.Text = "1500000";
            this.textBoxSr17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs17
            // 
            this.buttonTs17.Location = new System.Drawing.Point(263, 11);
            this.buttonTs17.Name = "buttonTs17";
            this.buttonTs17.Size = new System.Drawing.Size(30, 23);
            this.buttonTs17.TabIndex = 1;
            this.buttonTs17.Text = "ts";
            this.buttonTs17.UseVisualStyleBackColor = true;
            this.buttonTs17.Click += new System.EventHandler(this.buttonTs17_Click);
            // 
            // textBoxGain17
            // 
            this.textBoxGain17.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain17.Name = "textBoxGain17";
            this.textBoxGain17.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain17.TabIndex = 0;
            this.textBoxGain17.Text = "50000";
            this.textBoxGain17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr17
            // 
            this.buttonSr17.Location = new System.Drawing.Point(181, 10);
            this.buttonSr17.Name = "buttonSr17";
            this.buttonSr17.Size = new System.Drawing.Size(30, 23);
            this.buttonSr17.TabIndex = 1;
            this.buttonSr17.Text = "sR";
            this.buttonSr17.UseVisualStyleBackColor = true;
            this.buttonSr17.Click += new System.EventHandler(this.buttonSr17_Click);
            // 
            // textBoxTs17
            // 
            this.textBoxTs17.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs17.Name = "textBoxTs17";
            this.textBoxTs17.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs17.TabIndex = 0;
            this.textBoxTs17.Text = "8";
            this.textBoxTs17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc17
            // 
            this.buttonSc17.Location = new System.Drawing.Point(70, 11);
            this.buttonSc17.Name = "buttonSc17";
            this.buttonSc17.Size = new System.Drawing.Size(30, 23);
            this.buttonSc17.TabIndex = 1;
            this.buttonSc17.Text = "sC";
            this.buttonSc17.UseVisualStyleBackColor = true;
            this.buttonSc17.Click += new System.EventHandler(this.buttonSc17_Click);
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.groupBox6);
            this.groupBox21.Controls.Add(this.groupBox0);
            this.groupBox21.Controls.Add(this.groupBox7);
            this.groupBox21.Controls.Add(this.groupBox2);
            this.groupBox21.Controls.Add(this.groupBox8);
            this.groupBox21.Controls.Add(this.groupBox3);
            this.groupBox21.Controls.Add(this.groupBox9);
            this.groupBox21.Controls.Add(this.groupBox4);
            this.groupBox21.Controls.Add(this.groupBox10);
            this.groupBox21.Controls.Add(this.groupBox5);
            this.groupBox21.Location = new System.Drawing.Point(23, 23);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(445, 485);
            this.groupBox21.TabIndex = 1;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "ARRAY 0";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.textBoxSc5);
            this.groupBox6.Controls.Add(this.buttonGain5);
            this.groupBox6.Controls.Add(this.textBoxSr5);
            this.groupBox6.Controls.Add(this.buttonTs5);
            this.groupBox6.Controls.Add(this.textBoxGain5);
            this.groupBox6.Controls.Add(this.buttonSr5);
            this.groupBox6.Controls.Add(this.textBoxTs5);
            this.groupBox6.Controls.Add(this.buttonSc5);
            this.groupBox6.Location = new System.Drawing.Point(13, 243);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(407, 38);
            this.groupBox6.TabIndex = 2;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "M5";
            // 
            // textBoxSc5
            // 
            this.textBoxSc5.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc5.Name = "textBoxSc5";
            this.textBoxSc5.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc5.TabIndex = 0;
            this.textBoxSc5.Text = "1800000";
            this.textBoxSc5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain5
            // 
            this.buttonGain5.Location = new System.Drawing.Point(368, 11);
            this.buttonGain5.Name = "buttonGain5";
            this.buttonGain5.Size = new System.Drawing.Size(30, 23);
            this.buttonGain5.TabIndex = 1;
            this.buttonGain5.Text = "Gain";
            this.buttonGain5.UseVisualStyleBackColor = true;
            this.buttonGain5.Click += new System.EventHandler(this.buttonGain5_Click);
            // 
            // textBoxSr5
            // 
            this.textBoxSr5.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr5.Name = "textBoxSr5";
            this.textBoxSr5.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr5.TabIndex = 0;
            this.textBoxSr5.Text = "1500000";
            this.textBoxSr5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs5
            // 
            this.buttonTs5.Location = new System.Drawing.Point(263, 11);
            this.buttonTs5.Name = "buttonTs5";
            this.buttonTs5.Size = new System.Drawing.Size(30, 23);
            this.buttonTs5.TabIndex = 1;
            this.buttonTs5.Text = "ts";
            this.buttonTs5.UseVisualStyleBackColor = true;
            this.buttonTs5.Click += new System.EventHandler(this.buttonTs5_Click);
            // 
            // textBoxGain5
            // 
            this.textBoxGain5.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain5.Name = "textBoxGain5";
            this.textBoxGain5.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain5.TabIndex = 0;
            this.textBoxGain5.Text = "50000";
            this.textBoxGain5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr5
            // 
            this.buttonSr5.Location = new System.Drawing.Point(181, 10);
            this.buttonSr5.Name = "buttonSr5";
            this.buttonSr5.Size = new System.Drawing.Size(30, 23);
            this.buttonSr5.TabIndex = 1;
            this.buttonSr5.Text = "sR";
            this.buttonSr5.UseVisualStyleBackColor = true;
            this.buttonSr5.Click += new System.EventHandler(this.buttonSr5_Click);
            // 
            // textBoxTs5
            // 
            this.textBoxTs5.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs5.Name = "textBoxTs5";
            this.textBoxTs5.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs5.TabIndex = 0;
            this.textBoxTs5.Text = "8";
            this.textBoxTs5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc5
            // 
            this.buttonSc5.Location = new System.Drawing.Point(70, 11);
            this.buttonSc5.Name = "buttonSc5";
            this.buttonSc5.Size = new System.Drawing.Size(30, 23);
            this.buttonSc5.TabIndex = 1;
            this.buttonSc5.Text = "sC";
            this.buttonSc5.UseVisualStyleBackColor = true;
            this.buttonSc5.Click += new System.EventHandler(this.buttonSc5_Click);
            // 
            // groupBox0
            // 
            this.groupBox0.Controls.Add(this.textBoxSc0);
            this.groupBox0.Controls.Add(this.buttonGain0);
            this.groupBox0.Controls.Add(this.textBoxSr0);
            this.groupBox0.Controls.Add(this.buttonTs0);
            this.groupBox0.Controls.Add(this.textBoxGain0);
            this.groupBox0.Controls.Add(this.buttonSr0);
            this.groupBox0.Controls.Add(this.textBoxTs0);
            this.groupBox0.Controls.Add(this.buttonSc0);
            this.groupBox0.Location = new System.Drawing.Point(13, 23);
            this.groupBox0.Name = "groupBox0";
            this.groupBox0.Size = new System.Drawing.Size(407, 38);
            this.groupBox0.TabIndex = 2;
            this.groupBox0.TabStop = false;
            this.groupBox0.Text = "M0";
            // 
            // textBoxSc0
            // 
            this.textBoxSc0.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc0.Name = "textBoxSc0";
            this.textBoxSc0.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc0.TabIndex = 0;
            this.textBoxSc0.Text = "1800000";
            this.textBoxSc0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain0
            // 
            this.buttonGain0.Location = new System.Drawing.Point(368, 11);
            this.buttonGain0.Name = "buttonGain0";
            this.buttonGain0.Size = new System.Drawing.Size(30, 23);
            this.buttonGain0.TabIndex = 1;
            this.buttonGain0.Text = "Gain";
            this.buttonGain0.UseVisualStyleBackColor = true;
            this.buttonGain0.Click += new System.EventHandler(this.buttonGain0_Click);
            // 
            // textBoxSr0
            // 
            this.textBoxSr0.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr0.Name = "textBoxSr0";
            this.textBoxSr0.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr0.TabIndex = 0;
            this.textBoxSr0.Text = "1500000";
            this.textBoxSr0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs0
            // 
            this.buttonTs0.Location = new System.Drawing.Point(263, 11);
            this.buttonTs0.Name = "buttonTs0";
            this.buttonTs0.Size = new System.Drawing.Size(30, 23);
            this.buttonTs0.TabIndex = 1;
            this.buttonTs0.Text = "ts";
            this.buttonTs0.UseVisualStyleBackColor = true;
            this.buttonTs0.Click += new System.EventHandler(this.buttonTs0_Click);
            // 
            // textBoxGain0
            // 
            this.textBoxGain0.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain0.Name = "textBoxGain0";
            this.textBoxGain0.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain0.TabIndex = 0;
            this.textBoxGain0.Text = "50000";
            this.textBoxGain0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr0
            // 
            this.buttonSr0.Location = new System.Drawing.Point(181, 10);
            this.buttonSr0.Name = "buttonSr0";
            this.buttonSr0.Size = new System.Drawing.Size(30, 23);
            this.buttonSr0.TabIndex = 1;
            this.buttonSr0.Text = "sR";
            this.buttonSr0.UseVisualStyleBackColor = true;
            this.buttonSr0.Click += new System.EventHandler(this.buttonSr0_Click);
            // 
            // textBoxTs0
            // 
            this.textBoxTs0.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs0.Name = "textBoxTs0";
            this.textBoxTs0.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs0.TabIndex = 0;
            this.textBoxTs0.Text = "8";
            this.textBoxTs0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc0
            // 
            this.buttonSc0.Location = new System.Drawing.Point(70, 11);
            this.buttonSc0.Name = "buttonSc0";
            this.buttonSc0.Size = new System.Drawing.Size(30, 23);
            this.buttonSc0.TabIndex = 1;
            this.buttonSc0.Text = "sC";
            this.buttonSc0.UseVisualStyleBackColor = true;
            this.buttonSc0.Click += new System.EventHandler(this.buttonSc0_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.textBoxSc6);
            this.groupBox7.Controls.Add(this.buttonGain6);
            this.groupBox7.Controls.Add(this.textBoxSr6);
            this.groupBox7.Controls.Add(this.buttonTs6);
            this.groupBox7.Controls.Add(this.textBoxGain6);
            this.groupBox7.Controls.Add(this.buttonSr6);
            this.groupBox7.Controls.Add(this.textBoxTs6);
            this.groupBox7.Controls.Add(this.buttonSc6);
            this.groupBox7.Location = new System.Drawing.Point(13, 287);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(407, 38);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "M6";
            // 
            // textBoxSc6
            // 
            this.textBoxSc6.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc6.Name = "textBoxSc6";
            this.textBoxSc6.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc6.TabIndex = 0;
            this.textBoxSc6.Text = "1800000";
            this.textBoxSc6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain6
            // 
            this.buttonGain6.Location = new System.Drawing.Point(368, 11);
            this.buttonGain6.Name = "buttonGain6";
            this.buttonGain6.Size = new System.Drawing.Size(30, 23);
            this.buttonGain6.TabIndex = 1;
            this.buttonGain6.Text = "Gain";
            this.buttonGain6.UseVisualStyleBackColor = true;
            this.buttonGain6.Click += new System.EventHandler(this.buttonGain6_Click);
            // 
            // textBoxSr6
            // 
            this.textBoxSr6.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr6.Name = "textBoxSr6";
            this.textBoxSr6.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr6.TabIndex = 0;
            this.textBoxSr6.Text = "1500000";
            this.textBoxSr6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs6
            // 
            this.buttonTs6.Location = new System.Drawing.Point(263, 11);
            this.buttonTs6.Name = "buttonTs6";
            this.buttonTs6.Size = new System.Drawing.Size(30, 23);
            this.buttonTs6.TabIndex = 1;
            this.buttonTs6.Text = "ts";
            this.buttonTs6.UseVisualStyleBackColor = true;
            this.buttonTs6.Click += new System.EventHandler(this.buttonTs6_Click);
            // 
            // textBoxGain6
            // 
            this.textBoxGain6.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain6.Name = "textBoxGain6";
            this.textBoxGain6.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain6.TabIndex = 0;
            this.textBoxGain6.Text = "50000";
            this.textBoxGain6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr6
            // 
            this.buttonSr6.Location = new System.Drawing.Point(181, 10);
            this.buttonSr6.Name = "buttonSr6";
            this.buttonSr6.Size = new System.Drawing.Size(30, 23);
            this.buttonSr6.TabIndex = 1;
            this.buttonSr6.Text = "sR";
            this.buttonSr6.UseVisualStyleBackColor = true;
            this.buttonSr6.Click += new System.EventHandler(this.buttonSr6_Click);
            // 
            // textBoxTs6
            // 
            this.textBoxTs6.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs6.Name = "textBoxTs6";
            this.textBoxTs6.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs6.TabIndex = 0;
            this.textBoxTs6.Text = "8";
            this.textBoxTs6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc6
            // 
            this.buttonSc6.Location = new System.Drawing.Point(70, 11);
            this.buttonSc6.Name = "buttonSc6";
            this.buttonSc6.Size = new System.Drawing.Size(30, 23);
            this.buttonSc6.TabIndex = 1;
            this.buttonSc6.Text = "sC";
            this.buttonSc6.UseVisualStyleBackColor = true;
            this.buttonSc6.Click += new System.EventHandler(this.buttonSc6_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBoxSc1);
            this.groupBox2.Controls.Add(this.buttonGain1);
            this.groupBox2.Controls.Add(this.textBoxSr1);
            this.groupBox2.Controls.Add(this.buttonTs1);
            this.groupBox2.Controls.Add(this.textBoxGain1);
            this.groupBox2.Controls.Add(this.buttonSr1);
            this.groupBox2.Controls.Add(this.textBoxTs1);
            this.groupBox2.Controls.Add(this.buttonSc1);
            this.groupBox2.Location = new System.Drawing.Point(13, 67);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(407, 38);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "M1";
            // 
            // textBoxSc1
            // 
            this.textBoxSc1.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc1.Name = "textBoxSc1";
            this.textBoxSc1.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc1.TabIndex = 0;
            this.textBoxSc1.Text = "1800000";
            this.textBoxSc1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain1
            // 
            this.buttonGain1.Location = new System.Drawing.Point(368, 11);
            this.buttonGain1.Name = "buttonGain1";
            this.buttonGain1.Size = new System.Drawing.Size(30, 23);
            this.buttonGain1.TabIndex = 1;
            this.buttonGain1.Text = "Gain";
            this.buttonGain1.UseVisualStyleBackColor = true;
            this.buttonGain1.Click += new System.EventHandler(this.buttonGain1_Click);
            // 
            // textBoxSr1
            // 
            this.textBoxSr1.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr1.Name = "textBoxSr1";
            this.textBoxSr1.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr1.TabIndex = 0;
            this.textBoxSr1.Text = "1500000";
            this.textBoxSr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs1
            // 
            this.buttonTs1.Location = new System.Drawing.Point(263, 11);
            this.buttonTs1.Name = "buttonTs1";
            this.buttonTs1.Size = new System.Drawing.Size(30, 23);
            this.buttonTs1.TabIndex = 1;
            this.buttonTs1.Text = "ts";
            this.buttonTs1.UseVisualStyleBackColor = true;
            this.buttonTs1.Click += new System.EventHandler(this.buttonTs1_Click);
            // 
            // textBoxGain1
            // 
            this.textBoxGain1.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain1.Name = "textBoxGain1";
            this.textBoxGain1.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain1.TabIndex = 0;
            this.textBoxGain1.Text = "50000";
            this.textBoxGain1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr1
            // 
            this.buttonSr1.Location = new System.Drawing.Point(181, 10);
            this.buttonSr1.Name = "buttonSr1";
            this.buttonSr1.Size = new System.Drawing.Size(30, 23);
            this.buttonSr1.TabIndex = 1;
            this.buttonSr1.Text = "sR";
            this.buttonSr1.UseVisualStyleBackColor = true;
            this.buttonSr1.Click += new System.EventHandler(this.buttonSr1_Click);
            // 
            // textBoxTs1
            // 
            this.textBoxTs1.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs1.Name = "textBoxTs1";
            this.textBoxTs1.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs1.TabIndex = 0;
            this.textBoxTs1.Text = "8";
            this.textBoxTs1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc1
            // 
            this.buttonSc1.Location = new System.Drawing.Point(70, 11);
            this.buttonSc1.Name = "buttonSc1";
            this.buttonSc1.Size = new System.Drawing.Size(30, 23);
            this.buttonSc1.TabIndex = 1;
            this.buttonSc1.Text = "sC";
            this.buttonSc1.UseVisualStyleBackColor = true;
            this.buttonSc1.Click += new System.EventHandler(this.buttonSc1_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.textBoxSc7);
            this.groupBox8.Controls.Add(this.buttonGain7);
            this.groupBox8.Controls.Add(this.textBoxSr7);
            this.groupBox8.Controls.Add(this.buttonTs7);
            this.groupBox8.Controls.Add(this.textBoxGain7);
            this.groupBox8.Controls.Add(this.buttonSr7);
            this.groupBox8.Controls.Add(this.textBoxTs7);
            this.groupBox8.Controls.Add(this.buttonSc7);
            this.groupBox8.Location = new System.Drawing.Point(13, 331);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(407, 38);
            this.groupBox8.TabIndex = 2;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "M7";
            // 
            // textBoxSc7
            // 
            this.textBoxSc7.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc7.Name = "textBoxSc7";
            this.textBoxSc7.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc7.TabIndex = 0;
            this.textBoxSc7.Text = "1800000";
            this.textBoxSc7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain7
            // 
            this.buttonGain7.Location = new System.Drawing.Point(368, 11);
            this.buttonGain7.Name = "buttonGain7";
            this.buttonGain7.Size = new System.Drawing.Size(30, 23);
            this.buttonGain7.TabIndex = 1;
            this.buttonGain7.Text = "Gain";
            this.buttonGain7.UseVisualStyleBackColor = true;
            this.buttonGain7.Click += new System.EventHandler(this.buttonGain7_Click);
            // 
            // textBoxSr7
            // 
            this.textBoxSr7.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr7.Name = "textBoxSr7";
            this.textBoxSr7.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr7.TabIndex = 0;
            this.textBoxSr7.Text = "1500000";
            this.textBoxSr7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs7
            // 
            this.buttonTs7.Location = new System.Drawing.Point(263, 11);
            this.buttonTs7.Name = "buttonTs7";
            this.buttonTs7.Size = new System.Drawing.Size(30, 23);
            this.buttonTs7.TabIndex = 1;
            this.buttonTs7.Text = "ts";
            this.buttonTs7.UseVisualStyleBackColor = true;
            this.buttonTs7.Click += new System.EventHandler(this.buttonTs7_Click);
            // 
            // textBoxGain7
            // 
            this.textBoxGain7.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain7.Name = "textBoxGain7";
            this.textBoxGain7.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain7.TabIndex = 0;
            this.textBoxGain7.Text = "50000";
            this.textBoxGain7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr7
            // 
            this.buttonSr7.Location = new System.Drawing.Point(181, 10);
            this.buttonSr7.Name = "buttonSr7";
            this.buttonSr7.Size = new System.Drawing.Size(30, 23);
            this.buttonSr7.TabIndex = 1;
            this.buttonSr7.Text = "sR";
            this.buttonSr7.UseVisualStyleBackColor = true;
            this.buttonSr7.Click += new System.EventHandler(this.buttonSr7_Click);
            // 
            // textBoxTs7
            // 
            this.textBoxTs7.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs7.Name = "textBoxTs7";
            this.textBoxTs7.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs7.TabIndex = 0;
            this.textBoxTs7.Text = "8";
            this.textBoxTs7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc7
            // 
            this.buttonSc7.Location = new System.Drawing.Point(70, 11);
            this.buttonSc7.Name = "buttonSc7";
            this.buttonSc7.Size = new System.Drawing.Size(30, 23);
            this.buttonSc7.TabIndex = 1;
            this.buttonSc7.Text = "sC";
            this.buttonSc7.UseVisualStyleBackColor = true;
            this.buttonSc7.Click += new System.EventHandler(this.buttonSc7_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBoxSc2);
            this.groupBox3.Controls.Add(this.buttonGain2);
            this.groupBox3.Controls.Add(this.textBoxSr2);
            this.groupBox3.Controls.Add(this.buttonTs2);
            this.groupBox3.Controls.Add(this.textBoxGain2);
            this.groupBox3.Controls.Add(this.buttonSr2);
            this.groupBox3.Controls.Add(this.textBoxTs2);
            this.groupBox3.Controls.Add(this.buttonSc2);
            this.groupBox3.Location = new System.Drawing.Point(13, 111);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(407, 38);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "M2";
            // 
            // textBoxSc2
            // 
            this.textBoxSc2.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc2.Name = "textBoxSc2";
            this.textBoxSc2.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc2.TabIndex = 0;
            this.textBoxSc2.Text = "1800000";
            this.textBoxSc2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain2
            // 
            this.buttonGain2.Location = new System.Drawing.Point(368, 11);
            this.buttonGain2.Name = "buttonGain2";
            this.buttonGain2.Size = new System.Drawing.Size(30, 23);
            this.buttonGain2.TabIndex = 1;
            this.buttonGain2.Text = "Gain";
            this.buttonGain2.UseVisualStyleBackColor = true;
            this.buttonGain2.Click += new System.EventHandler(this.buttonGain2_Click);
            // 
            // textBoxSr2
            // 
            this.textBoxSr2.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr2.Name = "textBoxSr2";
            this.textBoxSr2.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr2.TabIndex = 0;
            this.textBoxSr2.Text = "1500000";
            this.textBoxSr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs2
            // 
            this.buttonTs2.Location = new System.Drawing.Point(263, 11);
            this.buttonTs2.Name = "buttonTs2";
            this.buttonTs2.Size = new System.Drawing.Size(30, 23);
            this.buttonTs2.TabIndex = 1;
            this.buttonTs2.Text = "ts";
            this.buttonTs2.UseVisualStyleBackColor = true;
            this.buttonTs2.Click += new System.EventHandler(this.buttonTs2_Click);
            // 
            // textBoxGain2
            // 
            this.textBoxGain2.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain2.Name = "textBoxGain2";
            this.textBoxGain2.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain2.TabIndex = 0;
            this.textBoxGain2.Text = "50000";
            this.textBoxGain2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr2
            // 
            this.buttonSr2.Location = new System.Drawing.Point(181, 10);
            this.buttonSr2.Name = "buttonSr2";
            this.buttonSr2.Size = new System.Drawing.Size(30, 23);
            this.buttonSr2.TabIndex = 1;
            this.buttonSr2.Text = "sR";
            this.buttonSr2.UseVisualStyleBackColor = true;
            this.buttonSr2.Click += new System.EventHandler(this.buttonSr2_Click);
            // 
            // textBoxTs2
            // 
            this.textBoxTs2.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs2.Name = "textBoxTs2";
            this.textBoxTs2.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs2.TabIndex = 0;
            this.textBoxTs2.Text = "8";
            this.textBoxTs2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc2
            // 
            this.buttonSc2.Location = new System.Drawing.Point(70, 11);
            this.buttonSc2.Name = "buttonSc2";
            this.buttonSc2.Size = new System.Drawing.Size(30, 23);
            this.buttonSc2.TabIndex = 1;
            this.buttonSc2.Text = "sC";
            this.buttonSc2.UseVisualStyleBackColor = true;
            this.buttonSc2.Click += new System.EventHandler(this.buttonSc2_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.textBoxSc8);
            this.groupBox9.Controls.Add(this.buttonGain8);
            this.groupBox9.Controls.Add(this.textBoxSr8);
            this.groupBox9.Controls.Add(this.buttonTs8);
            this.groupBox9.Controls.Add(this.textBoxGain8);
            this.groupBox9.Controls.Add(this.buttonSr8);
            this.groupBox9.Controls.Add(this.textBoxTs8);
            this.groupBox9.Controls.Add(this.buttonSc8);
            this.groupBox9.Location = new System.Drawing.Point(13, 375);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(407, 38);
            this.groupBox9.TabIndex = 2;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "M8";
            // 
            // textBoxSc8
            // 
            this.textBoxSc8.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc8.Name = "textBoxSc8";
            this.textBoxSc8.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc8.TabIndex = 0;
            this.textBoxSc8.Text = "1800000";
            this.textBoxSc8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain8
            // 
            this.buttonGain8.Location = new System.Drawing.Point(368, 11);
            this.buttonGain8.Name = "buttonGain8";
            this.buttonGain8.Size = new System.Drawing.Size(30, 23);
            this.buttonGain8.TabIndex = 1;
            this.buttonGain8.Text = "Gain";
            this.buttonGain8.UseVisualStyleBackColor = true;
            this.buttonGain8.Click += new System.EventHandler(this.buttonGain8_Click);
            // 
            // textBoxSr8
            // 
            this.textBoxSr8.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr8.Name = "textBoxSr8";
            this.textBoxSr8.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr8.TabIndex = 0;
            this.textBoxSr8.Text = "1500000";
            this.textBoxSr8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs8
            // 
            this.buttonTs8.Location = new System.Drawing.Point(263, 11);
            this.buttonTs8.Name = "buttonTs8";
            this.buttonTs8.Size = new System.Drawing.Size(30, 23);
            this.buttonTs8.TabIndex = 1;
            this.buttonTs8.Text = "ts";
            this.buttonTs8.UseVisualStyleBackColor = true;
            this.buttonTs8.Click += new System.EventHandler(this.buttonTs8_Click);
            // 
            // textBoxGain8
            // 
            this.textBoxGain8.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain8.Name = "textBoxGain8";
            this.textBoxGain8.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain8.TabIndex = 0;
            this.textBoxGain8.Text = "50000";
            this.textBoxGain8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr8
            // 
            this.buttonSr8.Location = new System.Drawing.Point(181, 10);
            this.buttonSr8.Name = "buttonSr8";
            this.buttonSr8.Size = new System.Drawing.Size(30, 23);
            this.buttonSr8.TabIndex = 1;
            this.buttonSr8.Text = "sR";
            this.buttonSr8.UseVisualStyleBackColor = true;
            this.buttonSr8.Click += new System.EventHandler(this.buttonSr8_Click);
            // 
            // textBoxTs8
            // 
            this.textBoxTs8.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs8.Name = "textBoxTs8";
            this.textBoxTs8.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs8.TabIndex = 0;
            this.textBoxTs8.Text = "8";
            this.textBoxTs8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc8
            // 
            this.buttonSc8.Location = new System.Drawing.Point(70, 11);
            this.buttonSc8.Name = "buttonSc8";
            this.buttonSc8.Size = new System.Drawing.Size(30, 23);
            this.buttonSc8.TabIndex = 1;
            this.buttonSc8.Text = "sC";
            this.buttonSc8.UseVisualStyleBackColor = true;
            this.buttonSc8.Click += new System.EventHandler(this.buttonSc8_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.textBoxSc3);
            this.groupBox4.Controls.Add(this.buttonGain3);
            this.groupBox4.Controls.Add(this.textBoxSr3);
            this.groupBox4.Controls.Add(this.buttonTs3);
            this.groupBox4.Controls.Add(this.textBoxGain3);
            this.groupBox4.Controls.Add(this.buttonSr3);
            this.groupBox4.Controls.Add(this.textBoxTs3);
            this.groupBox4.Controls.Add(this.buttonSc3);
            this.groupBox4.Location = new System.Drawing.Point(13, 155);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(407, 38);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "M3";
            // 
            // textBoxSc3
            // 
            this.textBoxSc3.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc3.Name = "textBoxSc3";
            this.textBoxSc3.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc3.TabIndex = 0;
            this.textBoxSc3.Text = "1800000";
            this.textBoxSc3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain3
            // 
            this.buttonGain3.Location = new System.Drawing.Point(368, 11);
            this.buttonGain3.Name = "buttonGain3";
            this.buttonGain3.Size = new System.Drawing.Size(30, 23);
            this.buttonGain3.TabIndex = 1;
            this.buttonGain3.Text = "Gain";
            this.buttonGain3.UseVisualStyleBackColor = true;
            this.buttonGain3.Click += new System.EventHandler(this.buttonGain3_Click);
            // 
            // textBoxSr3
            // 
            this.textBoxSr3.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr3.Name = "textBoxSr3";
            this.textBoxSr3.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr3.TabIndex = 0;
            this.textBoxSr3.Text = "1500000";
            this.textBoxSr3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs3
            // 
            this.buttonTs3.Location = new System.Drawing.Point(263, 11);
            this.buttonTs3.Name = "buttonTs3";
            this.buttonTs3.Size = new System.Drawing.Size(30, 23);
            this.buttonTs3.TabIndex = 1;
            this.buttonTs3.Text = "ts";
            this.buttonTs3.UseVisualStyleBackColor = true;
            this.buttonTs3.Click += new System.EventHandler(this.buttonTs3_Click);
            // 
            // textBoxGain3
            // 
            this.textBoxGain3.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain3.Name = "textBoxGain3";
            this.textBoxGain3.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain3.TabIndex = 0;
            this.textBoxGain3.Text = "50000";
            this.textBoxGain3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr3
            // 
            this.buttonSr3.Location = new System.Drawing.Point(181, 10);
            this.buttonSr3.Name = "buttonSr3";
            this.buttonSr3.Size = new System.Drawing.Size(30, 23);
            this.buttonSr3.TabIndex = 1;
            this.buttonSr3.Text = "sR";
            this.buttonSr3.UseVisualStyleBackColor = true;
            this.buttonSr3.Click += new System.EventHandler(this.buttonSr3_Click);
            // 
            // textBoxTs3
            // 
            this.textBoxTs3.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs3.Name = "textBoxTs3";
            this.textBoxTs3.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs3.TabIndex = 0;
            this.textBoxTs3.Text = "8";
            this.textBoxTs3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc3
            // 
            this.buttonSc3.Location = new System.Drawing.Point(70, 11);
            this.buttonSc3.Name = "buttonSc3";
            this.buttonSc3.Size = new System.Drawing.Size(30, 23);
            this.buttonSc3.TabIndex = 1;
            this.buttonSc3.Text = "sC";
            this.buttonSc3.UseVisualStyleBackColor = true;
            this.buttonSc3.Click += new System.EventHandler(this.buttonSc3_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.textBoxSc9);
            this.groupBox10.Controls.Add(this.buttonGain9);
            this.groupBox10.Controls.Add(this.textBoxSr9);
            this.groupBox10.Controls.Add(this.buttonTs9);
            this.groupBox10.Controls.Add(this.textBoxGain9);
            this.groupBox10.Controls.Add(this.buttonSr9);
            this.groupBox10.Controls.Add(this.textBoxTs9);
            this.groupBox10.Controls.Add(this.buttonSc9);
            this.groupBox10.Location = new System.Drawing.Point(13, 419);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(407, 38);
            this.groupBox10.TabIndex = 2;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "M9";
            // 
            // textBoxSc9
            // 
            this.textBoxSc9.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc9.Name = "textBoxSc9";
            this.textBoxSc9.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc9.TabIndex = 0;
            this.textBoxSc9.Text = "1800000";
            this.textBoxSc9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain9
            // 
            this.buttonGain9.Location = new System.Drawing.Point(368, 11);
            this.buttonGain9.Name = "buttonGain9";
            this.buttonGain9.Size = new System.Drawing.Size(30, 23);
            this.buttonGain9.TabIndex = 1;
            this.buttonGain9.Text = "Gain";
            this.buttonGain9.UseVisualStyleBackColor = true;
            this.buttonGain9.Click += new System.EventHandler(this.buttonGain9_Click);
            // 
            // textBoxSr9
            // 
            this.textBoxSr9.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr9.Name = "textBoxSr9";
            this.textBoxSr9.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr9.TabIndex = 0;
            this.textBoxSr9.Text = "1500000";
            this.textBoxSr9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs9
            // 
            this.buttonTs9.Location = new System.Drawing.Point(263, 11);
            this.buttonTs9.Name = "buttonTs9";
            this.buttonTs9.Size = new System.Drawing.Size(30, 23);
            this.buttonTs9.TabIndex = 1;
            this.buttonTs9.Text = "ts";
            this.buttonTs9.UseVisualStyleBackColor = true;
            this.buttonTs9.Click += new System.EventHandler(this.buttonTs9_Click);
            // 
            // textBoxGain9
            // 
            this.textBoxGain9.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain9.Name = "textBoxGain9";
            this.textBoxGain9.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain9.TabIndex = 0;
            this.textBoxGain9.Text = "50000";
            this.textBoxGain9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr9
            // 
            this.buttonSr9.Location = new System.Drawing.Point(181, 10);
            this.buttonSr9.Name = "buttonSr9";
            this.buttonSr9.Size = new System.Drawing.Size(30, 23);
            this.buttonSr9.TabIndex = 1;
            this.buttonSr9.Text = "sR";
            this.buttonSr9.UseVisualStyleBackColor = true;
            this.buttonSr9.Click += new System.EventHandler(this.buttonSr9_Click);
            // 
            // textBoxTs9
            // 
            this.textBoxTs9.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs9.Name = "textBoxTs9";
            this.textBoxTs9.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs9.TabIndex = 0;
            this.textBoxTs9.Text = "8";
            this.textBoxTs9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc9
            // 
            this.buttonSc9.Location = new System.Drawing.Point(70, 11);
            this.buttonSc9.Name = "buttonSc9";
            this.buttonSc9.Size = new System.Drawing.Size(30, 23);
            this.buttonSc9.TabIndex = 1;
            this.buttonSc9.Text = "sC";
            this.buttonSc9.UseVisualStyleBackColor = true;
            this.buttonSc9.Click += new System.EventHandler(this.buttonSc9_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textBoxSc4);
            this.groupBox5.Controls.Add(this.buttonGain4);
            this.groupBox5.Controls.Add(this.textBoxSr4);
            this.groupBox5.Controls.Add(this.buttonTs4);
            this.groupBox5.Controls.Add(this.textBoxGain4);
            this.groupBox5.Controls.Add(this.buttonSr4);
            this.groupBox5.Controls.Add(this.textBoxTs4);
            this.groupBox5.Controls.Add(this.buttonSc4);
            this.groupBox5.Location = new System.Drawing.Point(13, 199);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(407, 38);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "M4";
            // 
            // textBoxSc4
            // 
            this.textBoxSc4.Location = new System.Drawing.Point(6, 12);
            this.textBoxSc4.Name = "textBoxSc4";
            this.textBoxSc4.Size = new System.Drawing.Size(58, 21);
            this.textBoxSc4.TabIndex = 0;
            this.textBoxSc4.Text = "1800000";
            this.textBoxSc4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonGain4
            // 
            this.buttonGain4.Location = new System.Drawing.Point(368, 11);
            this.buttonGain4.Name = "buttonGain4";
            this.buttonGain4.Size = new System.Drawing.Size(30, 23);
            this.buttonGain4.TabIndex = 1;
            this.buttonGain4.Text = "Gain";
            this.buttonGain4.UseVisualStyleBackColor = true;
            this.buttonGain4.Click += new System.EventHandler(this.buttonGain4_Click);
            // 
            // textBoxSr4
            // 
            this.textBoxSr4.Location = new System.Drawing.Point(117, 12);
            this.textBoxSr4.Name = "textBoxSr4";
            this.textBoxSr4.Size = new System.Drawing.Size(58, 21);
            this.textBoxSr4.TabIndex = 0;
            this.textBoxSr4.Text = "1500000";
            this.textBoxSr4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonTs4
            // 
            this.buttonTs4.Location = new System.Drawing.Point(263, 11);
            this.buttonTs4.Name = "buttonTs4";
            this.buttonTs4.Size = new System.Drawing.Size(30, 23);
            this.buttonTs4.TabIndex = 1;
            this.buttonTs4.Text = "ts";
            this.buttonTs4.UseVisualStyleBackColor = true;
            this.buttonTs4.Click += new System.EventHandler(this.buttonTs4_Click);
            // 
            // textBoxGain4
            // 
            this.textBoxGain4.Location = new System.Drawing.Point(312, 12);
            this.textBoxGain4.Name = "textBoxGain4";
            this.textBoxGain4.Size = new System.Drawing.Size(50, 21);
            this.textBoxGain4.TabIndex = 0;
            this.textBoxGain4.Text = "50000";
            this.textBoxGain4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSr4
            // 
            this.buttonSr4.Location = new System.Drawing.Point(181, 10);
            this.buttonSr4.Name = "buttonSr4";
            this.buttonSr4.Size = new System.Drawing.Size(30, 23);
            this.buttonSr4.TabIndex = 1;
            this.buttonSr4.Text = "sR";
            this.buttonSr4.UseVisualStyleBackColor = true;
            this.buttonSr4.Click += new System.EventHandler(this.buttonSr4_Click);
            // 
            // textBoxTs4
            // 
            this.textBoxTs4.Location = new System.Drawing.Point(229, 12);
            this.textBoxTs4.Name = "textBoxTs4";
            this.textBoxTs4.Size = new System.Drawing.Size(28, 21);
            this.textBoxTs4.TabIndex = 0;
            this.textBoxTs4.Text = "8";
            this.textBoxTs4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonSc4
            // 
            this.buttonSc4.Location = new System.Drawing.Point(70, 11);
            this.buttonSc4.Name = "buttonSc4";
            this.buttonSc4.Size = new System.Drawing.Size(30, 23);
            this.buttonSc4.TabIndex = 1;
            this.buttonSc4.Text = "sC";
            this.buttonSc4.UseVisualStyleBackColor = true;
            this.buttonSc4.Click += new System.EventHandler(this.buttonSc4_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.textBoxDiff19);
            this.tabPage4.Controls.Add(this.textBoxDiff18);
            this.tabPage4.Controls.Add(this.textBoxDiff17);
            this.tabPage4.Controls.Add(this.textBoxDiff16);
            this.tabPage4.Controls.Add(this.textBoxDiff15);
            this.tabPage4.Controls.Add(this.textBoxDiff14);
            this.tabPage4.Controls.Add(this.textBoxDiff13);
            this.tabPage4.Controls.Add(this.textBoxDiff12);
            this.tabPage4.Controls.Add(this.textBoxDiff11);
            this.tabPage4.Controls.Add(this.textBoxDiff10);
            this.tabPage4.Controls.Add(this.textBoxDiffRef2);
            this.tabPage4.Controls.Add(this.checkBoxSelectAll1);
            this.tabPage4.Controls.Add(this.checkBoxTps19);
            this.tabPage4.Controls.Add(this.checkBoxTps18);
            this.tabPage4.Controls.Add(this.checkBoxTps17);
            this.tabPage4.Controls.Add(this.checkBoxTps16);
            this.tabPage4.Controls.Add(this.checkBoxTps15);
            this.tabPage4.Controls.Add(this.checkBoxTps14);
            this.tabPage4.Controls.Add(this.checkBoxTps13);
            this.tabPage4.Controls.Add(this.checkBoxTps12);
            this.tabPage4.Controls.Add(this.checkBoxTps11);
            this.tabPage4.Controls.Add(this.checkBoxRef2);
            this.tabPage4.Controls.Add(this.checkBoxTps10);
            this.tabPage4.Controls.Add(this.buttonChartClear1);
            this.tabPage4.Controls.Add(this.chartPpm2);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(1352, 556);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "10 ~ 19 Graph";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // textBoxDiff19
            // 
            this.textBoxDiff19.Location = new System.Drawing.Point(1279, 521);
            this.textBoxDiff19.Name = "textBoxDiff19";
            this.textBoxDiff19.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff19.TabIndex = 16;
            this.textBoxDiff19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff18
            // 
            this.textBoxDiff18.Location = new System.Drawing.Point(1279, 499);
            this.textBoxDiff18.Name = "textBoxDiff18";
            this.textBoxDiff18.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff18.TabIndex = 16;
            this.textBoxDiff18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff17
            // 
            this.textBoxDiff17.Location = new System.Drawing.Point(1279, 477);
            this.textBoxDiff17.Name = "textBoxDiff17";
            this.textBoxDiff17.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff17.TabIndex = 16;
            this.textBoxDiff17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff16
            // 
            this.textBoxDiff16.Location = new System.Drawing.Point(1279, 455);
            this.textBoxDiff16.Name = "textBoxDiff16";
            this.textBoxDiff16.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff16.TabIndex = 16;
            this.textBoxDiff16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff15
            // 
            this.textBoxDiff15.Location = new System.Drawing.Point(1279, 433);
            this.textBoxDiff15.Name = "textBoxDiff15";
            this.textBoxDiff15.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff15.TabIndex = 16;
            this.textBoxDiff15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff14
            // 
            this.textBoxDiff14.Location = new System.Drawing.Point(1279, 411);
            this.textBoxDiff14.Name = "textBoxDiff14";
            this.textBoxDiff14.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff14.TabIndex = 16;
            this.textBoxDiff14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff13
            // 
            this.textBoxDiff13.Location = new System.Drawing.Point(1279, 389);
            this.textBoxDiff13.Name = "textBoxDiff13";
            this.textBoxDiff13.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff13.TabIndex = 16;
            this.textBoxDiff13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff12
            // 
            this.textBoxDiff12.Location = new System.Drawing.Point(1279, 367);
            this.textBoxDiff12.Name = "textBoxDiff12";
            this.textBoxDiff12.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff12.TabIndex = 16;
            this.textBoxDiff12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff11
            // 
            this.textBoxDiff11.Location = new System.Drawing.Point(1279, 345);
            this.textBoxDiff11.Name = "textBoxDiff11";
            this.textBoxDiff11.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff11.TabIndex = 16;
            this.textBoxDiff11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff10
            // 
            this.textBoxDiff10.Location = new System.Drawing.Point(1279, 323);
            this.textBoxDiff10.Name = "textBoxDiff10";
            this.textBoxDiff10.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff10.TabIndex = 16;
            this.textBoxDiff10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiffRef2
            // 
            this.textBoxDiffRef2.Location = new System.Drawing.Point(1279, 301);
            this.textBoxDiffRef2.Name = "textBoxDiffRef2";
            this.textBoxDiffRef2.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiffRef2.TabIndex = 16;
            this.textBoxDiffRef2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxSelectAll1
            // 
            this.checkBoxSelectAll1.AutoSize = true;
            this.checkBoxSelectAll1.Location = new System.Drawing.Point(1196, 283);
            this.checkBoxSelectAll1.Name = "checkBoxSelectAll1";
            this.checkBoxSelectAll1.Size = new System.Drawing.Size(77, 16);
            this.checkBoxSelectAll1.TabIndex = 15;
            this.checkBoxSelectAll1.Text = "Select All";
            this.checkBoxSelectAll1.UseVisualStyleBackColor = true;
            this.checkBoxSelectAll1.CheckedChanged += new System.EventHandler(this.checkBoxSelectAll1_CheckedChanged);
            // 
            // checkBoxTps19
            // 
            this.checkBoxTps19.AutoSize = true;
            this.checkBoxTps19.ForeColor = System.Drawing.Color.Lime;
            this.checkBoxTps19.Location = new System.Drawing.Point(1196, 526);
            this.checkBoxTps19.Name = "checkBoxTps19";
            this.checkBoxTps19.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps19.TabIndex = 4;
            this.checkBoxTps19.Text = "Tps19";
            this.checkBoxTps19.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps18
            // 
            this.checkBoxTps18.AutoSize = true;
            this.checkBoxTps18.ForeColor = System.Drawing.Color.Cyan;
            this.checkBoxTps18.Location = new System.Drawing.Point(1196, 504);
            this.checkBoxTps18.Name = "checkBoxTps18";
            this.checkBoxTps18.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps18.TabIndex = 5;
            this.checkBoxTps18.Text = "Tps18";
            this.checkBoxTps18.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps17
            // 
            this.checkBoxTps17.AutoSize = true;
            this.checkBoxTps17.ForeColor = System.Drawing.Color.Tomato;
            this.checkBoxTps17.Location = new System.Drawing.Point(1196, 482);
            this.checkBoxTps17.Name = "checkBoxTps17";
            this.checkBoxTps17.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps17.TabIndex = 6;
            this.checkBoxTps17.Text = "Tps17";
            this.checkBoxTps17.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps16
            // 
            this.checkBoxTps16.AutoSize = true;
            this.checkBoxTps16.ForeColor = System.Drawing.Color.BlueViolet;
            this.checkBoxTps16.Location = new System.Drawing.Point(1196, 460);
            this.checkBoxTps16.Name = "checkBoxTps16";
            this.checkBoxTps16.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps16.TabIndex = 7;
            this.checkBoxTps16.Text = "Tps16";
            this.checkBoxTps16.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps15
            // 
            this.checkBoxTps15.AutoSize = true;
            this.checkBoxTps15.ForeColor = System.Drawing.Color.Orange;
            this.checkBoxTps15.Location = new System.Drawing.Point(1196, 438);
            this.checkBoxTps15.Name = "checkBoxTps15";
            this.checkBoxTps15.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps15.TabIndex = 8;
            this.checkBoxTps15.Text = "Tps15";
            this.checkBoxTps15.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps14
            // 
            this.checkBoxTps14.AutoSize = true;
            this.checkBoxTps14.ForeColor = System.Drawing.Color.Magenta;
            this.checkBoxTps14.Location = new System.Drawing.Point(1196, 416);
            this.checkBoxTps14.Name = "checkBoxTps14";
            this.checkBoxTps14.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps14.TabIndex = 9;
            this.checkBoxTps14.Text = "Tps14";
            this.checkBoxTps14.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps13
            // 
            this.checkBoxTps13.AutoSize = true;
            this.checkBoxTps13.Location = new System.Drawing.Point(1196, 394);
            this.checkBoxTps13.Name = "checkBoxTps13";
            this.checkBoxTps13.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps13.TabIndex = 10;
            this.checkBoxTps13.Text = "Tps13";
            this.checkBoxTps13.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps12
            // 
            this.checkBoxTps12.AutoSize = true;
            this.checkBoxTps12.ForeColor = System.Drawing.Color.Blue;
            this.checkBoxTps12.Location = new System.Drawing.Point(1196, 372);
            this.checkBoxTps12.Name = "checkBoxTps12";
            this.checkBoxTps12.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps12.TabIndex = 11;
            this.checkBoxTps12.Text = "Tps12";
            this.checkBoxTps12.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps11
            // 
            this.checkBoxTps11.AutoSize = true;
            this.checkBoxTps11.ForeColor = System.Drawing.Color.Brown;
            this.checkBoxTps11.Location = new System.Drawing.Point(1196, 350);
            this.checkBoxTps11.Name = "checkBoxTps11";
            this.checkBoxTps11.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps11.TabIndex = 12;
            this.checkBoxTps11.Text = "Tps11";
            this.checkBoxTps11.UseVisualStyleBackColor = true;
            // 
            // checkBoxRef2
            // 
            this.checkBoxRef2.AutoSize = true;
            this.checkBoxRef2.Checked = true;
            this.checkBoxRef2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRef2.ForeColor = System.Drawing.Color.Green;
            this.checkBoxRef2.Location = new System.Drawing.Point(1197, 306);
            this.checkBoxRef2.Name = "checkBoxRef2";
            this.checkBoxRef2.Size = new System.Drawing.Size(42, 16);
            this.checkBoxRef2.TabIndex = 13;
            this.checkBoxRef2.Text = "Ref";
            this.checkBoxRef2.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps10
            // 
            this.checkBoxTps10.AutoSize = true;
            this.checkBoxTps10.ForeColor = System.Drawing.Color.Red;
            this.checkBoxTps10.Location = new System.Drawing.Point(1197, 328);
            this.checkBoxTps10.Name = "checkBoxTps10";
            this.checkBoxTps10.Size = new System.Drawing.Size(58, 16);
            this.checkBoxTps10.TabIndex = 14;
            this.checkBoxTps10.Text = "Tps10";
            this.checkBoxTps10.UseVisualStyleBackColor = true;
            // 
            // buttonChartClear1
            // 
            this.buttonChartClear1.Location = new System.Drawing.Point(1279, 276);
            this.buttonChartClear1.Name = "buttonChartClear1";
            this.buttonChartClear1.Size = new System.Drawing.Size(61, 23);
            this.buttonChartClear1.TabIndex = 2;
            this.buttonChartClear1.Text = "Clear";
            this.buttonChartClear1.UseVisualStyleBackColor = true;
            this.buttonChartClear1.Click += new System.EventHandler(this.buttonChartClear1_Click);
            // 
            // chartPpm2
            // 
            chartArea1.AxisY.IsStartedFromZero = false;
            chartArea1.Name = "ChartArea1";
            this.chartPpm2.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartPpm2.Legends.Add(legend1);
            this.chartPpm2.Location = new System.Drawing.Point(4, 4);
            this.chartPpm2.Name = "chartPpm2";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartPpm2.Series.Add(series1);
            this.chartPpm2.Size = new System.Drawing.Size(1345, 549);
            this.chartPpm2.TabIndex = 0;
            this.chartPpm2.Text = "chart2";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textBoxDiff9);
            this.tabPage3.Controls.Add(this.textBoxDiff8);
            this.tabPage3.Controls.Add(this.textBoxDiff7);
            this.tabPage3.Controls.Add(this.textBoxDiff6);
            this.tabPage3.Controls.Add(this.textBoxDiff5);
            this.tabPage3.Controls.Add(this.textBoxDiff4);
            this.tabPage3.Controls.Add(this.textBoxDiff3);
            this.tabPage3.Controls.Add(this.textBoxDiff2);
            this.tabPage3.Controls.Add(this.textBoxDiff1);
            this.tabPage3.Controls.Add(this.textBoxDiff0);
            this.tabPage3.Controls.Add(this.textBoxDiffRef);
            this.tabPage3.Controls.Add(this.checkBoxSelectAll);
            this.tabPage3.Controls.Add(this.checkBoxTps9);
            this.tabPage3.Controls.Add(this.checkBoxTps8);
            this.tabPage3.Controls.Add(this.checkBoxTps7);
            this.tabPage3.Controls.Add(this.checkBoxTps6);
            this.tabPage3.Controls.Add(this.checkBoxTps5);
            this.tabPage3.Controls.Add(this.checkBoxTps4);
            this.tabPage3.Controls.Add(this.checkBoxTps3);
            this.tabPage3.Controls.Add(this.checkBoxTps2);
            this.tabPage3.Controls.Add(this.checkBoxTps1);
            this.tabPage3.Controls.Add(this.checkBoxRef);
            this.tabPage3.Controls.Add(this.checkBoxTps0);
            this.tabPage3.Controls.Add(this.buttonChartClear0);
            this.tabPage3.Controls.Add(this.chartPpm);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1352, 556);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "0~9 Graph";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textBoxDiff9
            // 
            this.textBoxDiff9.Location = new System.Drawing.Point(1285, 522);
            this.textBoxDiff9.Name = "textBoxDiff9";
            this.textBoxDiff9.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff9.TabIndex = 5;
            this.textBoxDiff9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff8
            // 
            this.textBoxDiff8.Location = new System.Drawing.Point(1285, 500);
            this.textBoxDiff8.Name = "textBoxDiff8";
            this.textBoxDiff8.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff8.TabIndex = 5;
            this.textBoxDiff8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff7
            // 
            this.textBoxDiff7.Location = new System.Drawing.Point(1285, 478);
            this.textBoxDiff7.Name = "textBoxDiff7";
            this.textBoxDiff7.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff7.TabIndex = 5;
            this.textBoxDiff7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff6
            // 
            this.textBoxDiff6.Location = new System.Drawing.Point(1285, 456);
            this.textBoxDiff6.Name = "textBoxDiff6";
            this.textBoxDiff6.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff6.TabIndex = 5;
            this.textBoxDiff6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff5
            // 
            this.textBoxDiff5.Location = new System.Drawing.Point(1285, 434);
            this.textBoxDiff5.Name = "textBoxDiff5";
            this.textBoxDiff5.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff5.TabIndex = 5;
            this.textBoxDiff5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff4
            // 
            this.textBoxDiff4.Location = new System.Drawing.Point(1285, 412);
            this.textBoxDiff4.Name = "textBoxDiff4";
            this.textBoxDiff4.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff4.TabIndex = 5;
            this.textBoxDiff4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff3
            // 
            this.textBoxDiff3.Location = new System.Drawing.Point(1285, 390);
            this.textBoxDiff3.Name = "textBoxDiff3";
            this.textBoxDiff3.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff3.TabIndex = 5;
            this.textBoxDiff3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff2
            // 
            this.textBoxDiff2.Location = new System.Drawing.Point(1285, 368);
            this.textBoxDiff2.Name = "textBoxDiff2";
            this.textBoxDiff2.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff2.TabIndex = 5;
            this.textBoxDiff2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff1
            // 
            this.textBoxDiff1.Location = new System.Drawing.Point(1285, 346);
            this.textBoxDiff1.Name = "textBoxDiff1";
            this.textBoxDiff1.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff1.TabIndex = 5;
            this.textBoxDiff1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiff0
            // 
            this.textBoxDiff0.Location = new System.Drawing.Point(1285, 324);
            this.textBoxDiff0.Name = "textBoxDiff0";
            this.textBoxDiff0.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiff0.TabIndex = 5;
            this.textBoxDiff0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDiffRef
            // 
            this.textBoxDiffRef.Location = new System.Drawing.Point(1285, 302);
            this.textBoxDiffRef.Name = "textBoxDiffRef";
            this.textBoxDiffRef.Size = new System.Drawing.Size(61, 21);
            this.textBoxDiffRef.TabIndex = 5;
            this.textBoxDiffRef.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // checkBoxSelectAll
            // 
            this.checkBoxSelectAll.AutoSize = true;
            this.checkBoxSelectAll.Location = new System.Drawing.Point(1196, 283);
            this.checkBoxSelectAll.Name = "checkBoxSelectAll";
            this.checkBoxSelectAll.Size = new System.Drawing.Size(77, 16);
            this.checkBoxSelectAll.TabIndex = 4;
            this.checkBoxSelectAll.Text = "Select All";
            this.checkBoxSelectAll.UseVisualStyleBackColor = true;
            this.checkBoxSelectAll.CheckedChanged += new System.EventHandler(this.checkBoxSelectAll_CheckedChanged);
            // 
            // checkBoxTps9
            // 
            this.checkBoxTps9.AutoSize = true;
            this.checkBoxTps9.ForeColor = System.Drawing.Color.Lime;
            this.checkBoxTps9.Location = new System.Drawing.Point(1195, 527);
            this.checkBoxTps9.Name = "checkBoxTps9";
            this.checkBoxTps9.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps9.TabIndex = 3;
            this.checkBoxTps9.Text = "Tps9";
            this.checkBoxTps9.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps8
            // 
            this.checkBoxTps8.AutoSize = true;
            this.checkBoxTps8.ForeColor = System.Drawing.Color.Cyan;
            this.checkBoxTps8.Location = new System.Drawing.Point(1195, 505);
            this.checkBoxTps8.Name = "checkBoxTps8";
            this.checkBoxTps8.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps8.TabIndex = 3;
            this.checkBoxTps8.Text = "Tps8";
            this.checkBoxTps8.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps7
            // 
            this.checkBoxTps7.AutoSize = true;
            this.checkBoxTps7.ForeColor = System.Drawing.Color.Tomato;
            this.checkBoxTps7.Location = new System.Drawing.Point(1195, 483);
            this.checkBoxTps7.Name = "checkBoxTps7";
            this.checkBoxTps7.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps7.TabIndex = 3;
            this.checkBoxTps7.Text = "Tps7";
            this.checkBoxTps7.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps6
            // 
            this.checkBoxTps6.AutoSize = true;
            this.checkBoxTps6.ForeColor = System.Drawing.Color.BlueViolet;
            this.checkBoxTps6.Location = new System.Drawing.Point(1195, 461);
            this.checkBoxTps6.Name = "checkBoxTps6";
            this.checkBoxTps6.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps6.TabIndex = 3;
            this.checkBoxTps6.Text = "Tps6";
            this.checkBoxTps6.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps5
            // 
            this.checkBoxTps5.AutoSize = true;
            this.checkBoxTps5.ForeColor = System.Drawing.Color.Orange;
            this.checkBoxTps5.Location = new System.Drawing.Point(1195, 439);
            this.checkBoxTps5.Name = "checkBoxTps5";
            this.checkBoxTps5.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps5.TabIndex = 3;
            this.checkBoxTps5.Text = "Tps5";
            this.checkBoxTps5.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps4
            // 
            this.checkBoxTps4.AutoSize = true;
            this.checkBoxTps4.ForeColor = System.Drawing.Color.Magenta;
            this.checkBoxTps4.Location = new System.Drawing.Point(1195, 417);
            this.checkBoxTps4.Name = "checkBoxTps4";
            this.checkBoxTps4.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps4.TabIndex = 3;
            this.checkBoxTps4.Text = "Tps4";
            this.checkBoxTps4.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps3
            // 
            this.checkBoxTps3.AutoSize = true;
            this.checkBoxTps3.Location = new System.Drawing.Point(1195, 395);
            this.checkBoxTps3.Name = "checkBoxTps3";
            this.checkBoxTps3.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps3.TabIndex = 3;
            this.checkBoxTps3.Text = "Tps3";
            this.checkBoxTps3.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps2
            // 
            this.checkBoxTps2.AutoSize = true;
            this.checkBoxTps2.ForeColor = System.Drawing.Color.Blue;
            this.checkBoxTps2.Location = new System.Drawing.Point(1195, 373);
            this.checkBoxTps2.Name = "checkBoxTps2";
            this.checkBoxTps2.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps2.TabIndex = 3;
            this.checkBoxTps2.Text = "Tps2";
            this.checkBoxTps2.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps1
            // 
            this.checkBoxTps1.AutoSize = true;
            this.checkBoxTps1.ForeColor = System.Drawing.Color.Brown;
            this.checkBoxTps1.Location = new System.Drawing.Point(1195, 351);
            this.checkBoxTps1.Name = "checkBoxTps1";
            this.checkBoxTps1.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps1.TabIndex = 3;
            this.checkBoxTps1.Text = "Tps1";
            this.checkBoxTps1.UseVisualStyleBackColor = true;
            // 
            // checkBoxRef
            // 
            this.checkBoxRef.AutoSize = true;
            this.checkBoxRef.Checked = true;
            this.checkBoxRef.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxRef.ForeColor = System.Drawing.Color.Green;
            this.checkBoxRef.Location = new System.Drawing.Point(1196, 307);
            this.checkBoxRef.Name = "checkBoxRef";
            this.checkBoxRef.Size = new System.Drawing.Size(42, 16);
            this.checkBoxRef.TabIndex = 3;
            this.checkBoxRef.Text = "Ref";
            this.checkBoxRef.UseVisualStyleBackColor = true;
            // 
            // checkBoxTps0
            // 
            this.checkBoxTps0.AutoSize = true;
            this.checkBoxTps0.ForeColor = System.Drawing.Color.Red;
            this.checkBoxTps0.Location = new System.Drawing.Point(1196, 329);
            this.checkBoxTps0.Name = "checkBoxTps0";
            this.checkBoxTps0.Size = new System.Drawing.Size(52, 16);
            this.checkBoxTps0.TabIndex = 3;
            this.checkBoxTps0.Text = "Tps0";
            this.checkBoxTps0.UseVisualStyleBackColor = true;
            // 
            // buttonChartClear0
            // 
            this.buttonChartClear0.Location = new System.Drawing.Point(1285, 276);
            this.buttonChartClear0.Name = "buttonChartClear0";
            this.buttonChartClear0.Size = new System.Drawing.Size(61, 23);
            this.buttonChartClear0.TabIndex = 2;
            this.buttonChartClear0.Text = "Clear";
            this.buttonChartClear0.UseVisualStyleBackColor = true;
            this.buttonChartClear0.Click += new System.EventHandler(this.buttonChartClear0_Click);
            // 
            // chartPpm
            // 
            chartArea2.AxisY.IsStartedFromZero = false;
            chartArea2.Name = "ChartArea1";
            this.chartPpm.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chartPpm.Legends.Add(legend2);
            this.chartPpm.Location = new System.Drawing.Point(4, 4);
            this.chartPpm.Name = "chartPpm";
            series2.ChartArea = "ChartArea1";
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.chartPpm.Series.Add(series2);
            this.chartPpm.Size = new System.Drawing.Size(1345, 549);
            this.chartPpm.TabIndex = 0;
            this.chartPpm.Text = "chart1";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBoxInit);
            this.tabPage1.Controls.Add(this.panel8);
            this.tabPage1.Controls.Add(this.panel7);
            this.tabPage1.Controls.Add(this.panel6);
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel5);
            this.tabPage1.Controls.Add(this.panel4);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1352, 556);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Configuration";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBoxInit
            // 
            this.groupBoxInit.Controls.Add(this.panel11);
            this.groupBoxInit.Controls.Add(this.panel10);
            this.groupBoxInit.Controls.Add(this.checkBoxRsv2);
            this.groupBoxInit.Controls.Add(this.checkBoxRsv1);
            this.groupBoxInit.Controls.Add(this.checkBoxReadStatus);
            this.groupBoxInit.Controls.Add(this.checkBoxArrayPcb0);
            this.groupBoxInit.Controls.Add(this.checkBoxArrayPcb1);
            this.groupBoxInit.Controls.Add(this.buttonInit);
            this.groupBoxInit.Location = new System.Drawing.Point(1147, 271);
            this.groupBoxInit.Name = "groupBoxInit";
            this.groupBoxInit.Size = new System.Drawing.Size(179, 279);
            this.groupBoxInit.TabIndex = 11;
            this.groupBoxInit.TabStop = false;
            this.groupBoxInit.Text = "Initialize";
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.radioButtonRsv2);
            this.panel11.Controls.Add(this.radioButtonRsv1);
            this.panel11.Location = new System.Drawing.Point(7, 160);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(153, 25);
            this.panel11.TabIndex = 12;
            // 
            // radioButtonRsv2
            // 
            this.radioButtonRsv2.AutoSize = true;
            this.radioButtonRsv2.Location = new System.Drawing.Point(98, 6);
            this.radioButtonRsv2.Name = "radioButtonRsv2";
            this.radioButtonRsv2.Size = new System.Drawing.Size(44, 16);
            this.radioButtonRsv2.TabIndex = 12;
            this.radioButtonRsv2.TabStop = true;
            this.radioButtonRsv2.Text = "Rsv";
            this.radioButtonRsv2.UseVisualStyleBackColor = true;
            // 
            // radioButtonRsv1
            // 
            this.radioButtonRsv1.AutoSize = true;
            this.radioButtonRsv1.Location = new System.Drawing.Point(3, 6);
            this.radioButtonRsv1.Name = "radioButtonRsv1";
            this.radioButtonRsv1.Size = new System.Drawing.Size(44, 16);
            this.radioButtonRsv1.TabIndex = 11;
            this.radioButtonRsv1.TabStop = true;
            this.radioButtonRsv1.Text = "Rsv";
            this.radioButtonRsv1.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.radioButtonUartIf);
            this.panel10.Controls.Add(this.radioButtonI2cIf);
            this.panel10.Location = new System.Drawing.Point(6, 86);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(154, 23);
            this.panel10.TabIndex = 12;
            // 
            // radioButtonUartIf
            // 
            this.radioButtonUartIf.AutoSize = true;
            this.radioButtonUartIf.Checked = true;
            this.radioButtonUartIf.Location = new System.Drawing.Point(3, 4);
            this.radioButtonUartIf.Name = "radioButtonUartIf";
            this.radioButtonUartIf.Size = new System.Drawing.Size(69, 16);
            this.radioButtonUartIf.TabIndex = 8;
            this.radioButtonUartIf.TabStop = true;
            this.radioButtonUartIf.Text = "UART IF";
            this.radioButtonUartIf.UseVisualStyleBackColor = true;
            // 
            // radioButtonI2cIf
            // 
            this.radioButtonI2cIf.AutoSize = true;
            this.radioButtonI2cIf.Location = new System.Drawing.Point(98, 4);
            this.radioButtonI2cIf.Name = "radioButtonI2cIf";
            this.radioButtonI2cIf.Size = new System.Drawing.Size(55, 16);
            this.radioButtonI2cIf.TabIndex = 9;
            this.radioButtonI2cIf.Text = "I2C IF";
            this.radioButtonI2cIf.UseVisualStyleBackColor = true;
            // 
            // checkBoxRsv2
            // 
            this.checkBoxRsv2.AutoSize = true;
            this.checkBoxRsv2.Location = new System.Drawing.Point(7, 138);
            this.checkBoxRsv2.Name = "checkBoxRsv2";
            this.checkBoxRsv2.Size = new System.Drawing.Size(77, 16);
            this.checkBoxRsv2.TabIndex = 10;
            this.checkBoxRsv2.Text = "Reserved";
            this.checkBoxRsv2.UseVisualStyleBackColor = true;
            // 
            // checkBoxRsv1
            // 
            this.checkBoxRsv1.AutoSize = true;
            this.checkBoxRsv1.Location = new System.Drawing.Point(7, 116);
            this.checkBoxRsv1.Name = "checkBoxRsv1";
            this.checkBoxRsv1.Size = new System.Drawing.Size(77, 16);
            this.checkBoxRsv1.TabIndex = 10;
            this.checkBoxRsv1.Text = "Reserved";
            this.checkBoxRsv1.UseVisualStyleBackColor = true;
            // 
            // checkBoxReadStatus
            // 
            this.checkBoxReadStatus.AutoSize = true;
            this.checkBoxReadStatus.Checked = true;
            this.checkBoxReadStatus.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxReadStatus.Location = new System.Drawing.Point(7, 64);
            this.checkBoxReadStatus.Name = "checkBoxReadStatus";
            this.checkBoxReadStatus.Size = new System.Drawing.Size(92, 16);
            this.checkBoxReadStatus.TabIndex = 7;
            this.checkBoxReadStatus.Text = "Read Status";
            this.checkBoxReadStatus.UseVisualStyleBackColor = true;
            // 
            // checkBoxArrayPcb0
            // 
            this.checkBoxArrayPcb0.AutoSize = true;
            this.checkBoxArrayPcb0.Checked = true;
            this.checkBoxArrayPcb0.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxArrayPcb0.Location = new System.Drawing.Point(7, 20);
            this.checkBoxArrayPcb0.Name = "checkBoxArrayPcb0";
            this.checkBoxArrayPcb0.Size = new System.Drawing.Size(93, 16);
            this.checkBoxArrayPcb0.TabIndex = 6;
            this.checkBoxArrayPcb0.Text = "Array PCB 0";
            this.checkBoxArrayPcb0.UseVisualStyleBackColor = true;
            // 
            // checkBoxArrayPcb1
            // 
            this.checkBoxArrayPcb1.AutoSize = true;
            this.checkBoxArrayPcb1.Location = new System.Drawing.Point(7, 42);
            this.checkBoxArrayPcb1.Name = "checkBoxArrayPcb1";
            this.checkBoxArrayPcb1.Size = new System.Drawing.Size(93, 16);
            this.checkBoxArrayPcb1.TabIndex = 6;
            this.checkBoxArrayPcb1.Text = "Array PCB 1";
            this.checkBoxArrayPcb1.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel8.Controls.Add(this.checkBoxLogErr);
            this.panel8.Controls.Add(this.checkBoxLogMsg);
            this.panel8.Controls.Add(this.checkBoxLogLog);
            this.panel8.Controls.Add(this.label78);
            this.panel8.Location = new System.Drawing.Point(1147, 160);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(73, 97);
            this.panel8.TabIndex = 10;
            // 
            // checkBoxLogErr
            // 
            this.checkBoxLogErr.AutoSize = true;
            this.checkBoxLogErr.Checked = true;
            this.checkBoxLogErr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLogErr.Location = new System.Drawing.Point(8, 70);
            this.checkBoxLogErr.Name = "checkBoxLogErr";
            this.checkBoxLogErr.Size = new System.Drawing.Size(48, 16);
            this.checkBoxLogErr.TabIndex = 0;
            this.checkBoxLogErr.Text = "ERR";
            this.checkBoxLogErr.UseVisualStyleBackColor = true;
            // 
            // checkBoxLogMsg
            // 
            this.checkBoxLogMsg.AutoSize = true;
            this.checkBoxLogMsg.Location = new System.Drawing.Point(8, 48);
            this.checkBoxLogMsg.Name = "checkBoxLogMsg";
            this.checkBoxLogMsg.Size = new System.Drawing.Size(52, 16);
            this.checkBoxLogMsg.TabIndex = 0;
            this.checkBoxLogMsg.Text = "MSG";
            this.checkBoxLogMsg.UseVisualStyleBackColor = true;
            // 
            // checkBoxLogLog
            // 
            this.checkBoxLogLog.AutoSize = true;
            this.checkBoxLogLog.Checked = true;
            this.checkBoxLogLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLogLog.Location = new System.Drawing.Point(8, 26);
            this.checkBoxLogLog.Name = "checkBoxLogLog";
            this.checkBoxLogLog.Size = new System.Drawing.Size(49, 16);
            this.checkBoxLogLog.TabIndex = 0;
            this.checkBoxLogLog.Text = "LOG";
            this.checkBoxLogLog.UseVisualStyleBackColor = true;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(6, 4);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(64, 12);
            this.label78.TabIndex = 0;
            this.label78.Text = "LOG Level";
            // 
            // panel7
            // 
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel7.Controls.Add(this.checkBoxLog);
            this.panel7.Controls.Add(this.checkBoxErr);
            this.panel7.Controls.Add(this.checkBoxInfo);
            this.panel7.Controls.Add(this.label77);
            this.panel7.Location = new System.Drawing.Point(1147, 68);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(73, 85);
            this.panel7.TabIndex = 9;
            // 
            // checkBoxLog
            // 
            this.checkBoxLog.AutoSize = true;
            this.checkBoxLog.Checked = true;
            this.checkBoxLog.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxLog.Location = new System.Drawing.Point(6, 24);
            this.checkBoxLog.Name = "checkBoxLog";
            this.checkBoxLog.Size = new System.Drawing.Size(49, 16);
            this.checkBoxLog.TabIndex = 8;
            this.checkBoxLog.Text = "LOG";
            this.checkBoxLog.UseVisualStyleBackColor = true;
            // 
            // checkBoxErr
            // 
            this.checkBoxErr.AutoSize = true;
            this.checkBoxErr.Checked = true;
            this.checkBoxErr.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxErr.Location = new System.Drawing.Point(6, 59);
            this.checkBoxErr.Name = "checkBoxErr";
            this.checkBoxErr.Size = new System.Drawing.Size(48, 16);
            this.checkBoxErr.TabIndex = 8;
            this.checkBoxErr.Text = "ERR";
            this.checkBoxErr.UseVisualStyleBackColor = true;
            // 
            // checkBoxInfo
            // 
            this.checkBoxInfo.AutoSize = true;
            this.checkBoxInfo.Checked = true;
            this.checkBoxInfo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxInfo.Location = new System.Drawing.Point(6, 42);
            this.checkBoxInfo.Name = "checkBoxInfo";
            this.checkBoxInfo.Size = new System.Drawing.Size(52, 16);
            this.checkBoxInfo.TabIndex = 8;
            this.checkBoxInfo.Text = "INFO";
            this.checkBoxInfo.UseVisualStyleBackColor = true;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(3, 3);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(30, 12);
            this.label77.TabIndex = 0;
            this.label77.Text = "LOG";
            // 
            // panel6
            // 
            this.panel6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel6.Controls.Add(this.label72);
            this.panel6.Controls.Add(this.label76);
            this.panel6.Controls.Add(this.label75);
            this.panel6.Controls.Add(this.label74);
            this.panel6.Controls.Add(this.label73);
            this.panel6.Controls.Add(this.label71);
            this.panel6.Controls.Add(this.textBoxD3);
            this.panel6.Controls.Add(this.textBoxD2);
            this.panel6.Controls.Add(this.textBoxD1);
            this.panel6.Controls.Add(this.textBoxD0);
            this.panel6.Controls.Add(this.textBoxCh);
            this.panel6.Controls.Add(this.textBoxCmd);
            this.panel6.Controls.Add(this.buttonCdcSend);
            this.panel6.Location = new System.Drawing.Point(777, 6);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(430, 56);
            this.panel6.TabIndex = 7;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(60, 34);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(50, 12);
            this.label72.TabIndex = 2;
            this.label72.Text = "channel";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(272, 34);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(18, 12);
            this.label76.TabIndex = 2;
            this.label76.Text = "d3";
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(224, 34);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(18, 12);
            this.label75.TabIndex = 2;
            this.label75.Text = "d2";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(175, 34);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(18, 12);
            this.label74.TabIndex = 2;
            this.label74.Text = "d1";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(126, 34);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(18, 12);
            this.label73.TabIndex = 2;
            this.label73.Text = "d0";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(21, 34);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(30, 12);
            this.label71.TabIndex = 2;
            this.label71.Text = "cmd";
            // 
            // textBoxD3
            // 
            this.textBoxD3.Location = new System.Drawing.Point(259, 5);
            this.textBoxD3.Name = "textBoxD3";
            this.textBoxD3.Size = new System.Drawing.Size(43, 21);
            this.textBoxD3.TabIndex = 1;
            this.textBoxD3.Text = "0";
            this.textBoxD3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxD2
            // 
            this.textBoxD2.Location = new System.Drawing.Point(210, 5);
            this.textBoxD2.Name = "textBoxD2";
            this.textBoxD2.Size = new System.Drawing.Size(43, 21);
            this.textBoxD2.TabIndex = 1;
            this.textBoxD2.Text = "0";
            this.textBoxD2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxD1
            // 
            this.textBoxD1.Location = new System.Drawing.Point(161, 5);
            this.textBoxD1.Name = "textBoxD1";
            this.textBoxD1.Size = new System.Drawing.Size(43, 21);
            this.textBoxD1.TabIndex = 1;
            this.textBoxD1.Text = "0";
            this.textBoxD1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxD0
            // 
            this.textBoxD0.Location = new System.Drawing.Point(112, 6);
            this.textBoxD0.Name = "textBoxD0";
            this.textBoxD0.Size = new System.Drawing.Size(43, 21);
            this.textBoxD0.TabIndex = 1;
            this.textBoxD0.Text = "1";
            this.textBoxD0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxCh
            // 
            this.textBoxCh.Location = new System.Drawing.Point(63, 6);
            this.textBoxCh.Name = "textBoxCh";
            this.textBoxCh.Size = new System.Drawing.Size(43, 21);
            this.textBoxCh.TabIndex = 1;
            this.textBoxCh.Text = "1";
            this.textBoxCh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxCmd
            // 
            this.textBoxCmd.Location = new System.Drawing.Point(14, 6);
            this.textBoxCmd.Name = "textBoxCmd";
            this.textBoxCmd.Size = new System.Drawing.Size(43, 21);
            this.textBoxCmd.TabIndex = 1;
            this.textBoxCmd.Text = "36";
            this.textBoxCmd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonCdcSend
            // 
            this.buttonCdcSend.Location = new System.Drawing.Point(338, 5);
            this.buttonCdcSend.Name = "buttonCdcSend";
            this.buttonCdcSend.Size = new System.Drawing.Size(75, 23);
            this.buttonCdcSend.TabIndex = 0;
            this.buttonCdcSend.Text = "CdcSend";
            this.buttonCdcSend.UseVisualStyleBackColor = true;
            this.buttonCdcSend.Click += new System.EventHandler(this.buttonCdcSend_Click);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.buttonCom);
            this.panel2.Controls.Add(this.buttonComClose);
            this.panel2.Controls.Add(this.comboBoxCom);
            this.panel2.Controls.Add(this.buttonComOpen);
            this.panel2.Location = new System.Drawing.Point(7, 6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(360, 56);
            this.panel2.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(57, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "CDC Port";
            // 
            // buttonCom
            // 
            this.buttonCom.Location = new System.Drawing.Point(101, 22);
            this.buttonCom.Name = "buttonCom";
            this.buttonCom.Size = new System.Drawing.Size(53, 23);
            this.buttonCom.TabIndex = 1;
            this.buttonCom.Text = "COM";
            this.buttonCom.UseVisualStyleBackColor = true;
            this.buttonCom.Click += new System.EventHandler(this.buttonCom_Click);
            // 
            // buttonComClose
            // 
            this.buttonComClose.Location = new System.Drawing.Point(239, 22);
            this.buttonComClose.Name = "buttonComClose";
            this.buttonComClose.Size = new System.Drawing.Size(52, 23);
            this.buttonComClose.TabIndex = 3;
            this.buttonComClose.Text = "Close";
            this.buttonComClose.UseVisualStyleBackColor = true;
            this.buttonComClose.Click += new System.EventHandler(this.buttonComClose_Click);
            // 
            // comboBoxCom
            // 
            this.comboBoxCom.FormattingEnabled = true;
            this.comboBoxCom.Location = new System.Drawing.Point(12, 24);
            this.comboBoxCom.Name = "comboBoxCom";
            this.comboBoxCom.Size = new System.Drawing.Size(83, 20);
            this.comboBoxCom.TabIndex = 0;
            // 
            // buttonComOpen
            // 
            this.buttonComOpen.Location = new System.Drawing.Point(178, 22);
            this.buttonComOpen.Name = "buttonComOpen";
            this.buttonComOpen.Size = new System.Drawing.Size(55, 23);
            this.buttonComOpen.TabIndex = 2;
            this.buttonComOpen.Text = "Open";
            this.buttonComOpen.UseVisualStyleBackColor = true;
            this.buttonComOpen.Click += new System.EventHandler(this.buttonComOpen_Click);
            // 
            // panel5
            // 
            this.panel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel5.Controls.Add(this.buttonI2cGen);
            this.panel5.Controls.Add(this.buttonI2cGet);
            this.panel5.Controls.Add(this.buttonI2cSet);
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.textBoxI2cAddr);
            this.panel5.Controls.Add(this.labelI2cAddr);
            this.panel5.Controls.Add(this.label70);
            this.panel5.Controls.Add(this.textBoxI2cAddr0);
            this.panel5.Controls.Add(this.textBoxI2cAddr19);
            this.panel5.Controls.Add(this.label69);
            this.panel5.Controls.Add(this.label50);
            this.panel5.Controls.Add(this.textBoxI2cAddr9);
            this.panel5.Controls.Add(this.label68);
            this.panel5.Controls.Add(this.label51);
            this.panel5.Controls.Add(this.textBoxI2cAddr10);
            this.panel5.Controls.Add(this.textBoxI2cAddr18);
            this.panel5.Controls.Add(this.label67);
            this.panel5.Controls.Add(this.label52);
            this.panel5.Controls.Add(this.textBoxI2cAddr1);
            this.panel5.Controls.Add(this.textBoxI2cAddr8);
            this.panel5.Controls.Add(this.label66);
            this.panel5.Controls.Add(this.label53);
            this.panel5.Controls.Add(this.textBoxI2cAddr11);
            this.panel5.Controls.Add(this.textBoxI2cAddr17);
            this.panel5.Controls.Add(this.label65);
            this.panel5.Controls.Add(this.label54);
            this.panel5.Controls.Add(this.textBoxI2cAddr2);
            this.panel5.Controls.Add(this.textBoxI2cAddr7);
            this.panel5.Controls.Add(this.label64);
            this.panel5.Controls.Add(this.label55);
            this.panel5.Controls.Add(this.textBoxI2cAddr12);
            this.panel5.Controls.Add(this.textBoxI2cAddr16);
            this.panel5.Controls.Add(this.label63);
            this.panel5.Controls.Add(this.label56);
            this.panel5.Controls.Add(this.textBoxI2cAddr3);
            this.panel5.Controls.Add(this.textBoxI2cAddr6);
            this.panel5.Controls.Add(this.label62);
            this.panel5.Controls.Add(this.label57);
            this.panel5.Controls.Add(this.textBoxI2cAddr13);
            this.panel5.Controls.Add(this.textBoxI2cAddr15);
            this.panel5.Controls.Add(this.label61);
            this.panel5.Controls.Add(this.label58);
            this.panel5.Controls.Add(this.textBoxI2cAddr4);
            this.panel5.Controls.Add(this.textBoxI2cAddr5);
            this.panel5.Controls.Add(this.label60);
            this.panel5.Controls.Add(this.label59);
            this.panel5.Controls.Add(this.textBoxI2cAddr14);
            this.panel5.Location = new System.Drawing.Point(777, 68);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(360, 482);
            this.panel5.TabIndex = 5;
            // 
            // buttonI2cGen
            // 
            this.buttonI2cGen.Location = new System.Drawing.Point(261, 37);
            this.buttonI2cGen.Name = "buttonI2cGen";
            this.buttonI2cGen.Size = new System.Drawing.Size(75, 23);
            this.buttonI2cGen.TabIndex = 4;
            this.buttonI2cGen.Text = "Generate";
            this.buttonI2cGen.UseVisualStyleBackColor = true;
            this.buttonI2cGen.Click += new System.EventHandler(this.buttonI2cGen_Click);
            // 
            // buttonI2cGet
            // 
            this.buttonI2cGet.Location = new System.Drawing.Point(222, 389);
            this.buttonI2cGet.Name = "buttonI2cGet";
            this.buttonI2cGet.Size = new System.Drawing.Size(75, 23);
            this.buttonI2cGet.TabIndex = 2;
            this.buttonI2cGet.Text = "I2C Get";
            this.buttonI2cGet.UseVisualStyleBackColor = true;
            this.buttonI2cGet.Click += new System.EventHandler(this.buttonI2cGet_Click);
            // 
            // buttonI2cSet
            // 
            this.buttonI2cSet.Location = new System.Drawing.Point(85, 389);
            this.buttonI2cSet.Name = "buttonI2cSet";
            this.buttonI2cSet.Size = new System.Drawing.Size(75, 23);
            this.buttonI2cSet.TabIndex = 1;
            this.buttonI2cSet.Text = "I2C Set";
            this.buttonI2cSet.UseVisualStyleBackColor = true;
            this.buttonI2cSet.Click += new System.EventHandler(this.buttonI2cSet_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(47, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 12);
            this.label7.TabIndex = 0;
            this.label7.Text = "I2C Addr Set";
            // 
            // textBoxI2cAddr
            // 
            this.textBoxI2cAddr.Location = new System.Drawing.Point(152, 38);
            this.textBoxI2cAddr.Name = "textBoxI2cAddr";
            this.textBoxI2cAddr.Size = new System.Drawing.Size(75, 21);
            this.textBoxI2cAddr.TabIndex = 3;
            this.textBoxI2cAddr.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelI2cAddr
            // 
            this.labelI2cAddr.AutoSize = true;
            this.labelI2cAddr.Location = new System.Drawing.Point(153, 62);
            this.labelI2cAddr.Name = "labelI2cAddr";
            this.labelI2cAddr.Size = new System.Drawing.Size(18, 12);
            this.labelI2cAddr.TabIndex = 0;
            this.labelI2cAddr.Text = "0x";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(47, 42);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(74, 12);
            this.label70.TabIndex = 0;
            this.label70.Text = "I2C Address";
            // 
            // textBoxI2cAddr0
            // 
            this.textBoxI2cAddr0.Location = new System.Drawing.Point(85, 92);
            this.textBoxI2cAddr0.Name = "textBoxI2cAddr0";
            this.textBoxI2cAddr0.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr0.TabIndex = 1;
            this.textBoxI2cAddr0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxI2cAddr19
            // 
            this.textBoxI2cAddr19.Location = new System.Drawing.Point(261, 335);
            this.textBoxI2cAddr19.Name = "textBoxI2cAddr19";
            this.textBoxI2cAddr19.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr19.TabIndex = 1;
            this.textBoxI2cAddr19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(220, 338);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(32, 12);
            this.label69.TabIndex = 2;
            this.label69.Text = "M 19";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(44, 95);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(26, 12);
            this.label50.TabIndex = 2;
            this.label50.Text = "M 0";
            // 
            // textBoxI2cAddr9
            // 
            this.textBoxI2cAddr9.Location = new System.Drawing.Point(85, 335);
            this.textBoxI2cAddr9.Name = "textBoxI2cAddr9";
            this.textBoxI2cAddr9.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr9.TabIndex = 1;
            this.textBoxI2cAddr9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(44, 338);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(26, 12);
            this.label68.TabIndex = 2;
            this.label68.Text = "M 9";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(220, 95);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(32, 12);
            this.label51.TabIndex = 2;
            this.label51.Text = "M 10";
            // 
            // textBoxI2cAddr10
            // 
            this.textBoxI2cAddr10.Location = new System.Drawing.Point(261, 92);
            this.textBoxI2cAddr10.Name = "textBoxI2cAddr10";
            this.textBoxI2cAddr10.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr10.TabIndex = 1;
            this.textBoxI2cAddr10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxI2cAddr18
            // 
            this.textBoxI2cAddr18.Location = new System.Drawing.Point(261, 308);
            this.textBoxI2cAddr18.Name = "textBoxI2cAddr18";
            this.textBoxI2cAddr18.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr18.TabIndex = 1;
            this.textBoxI2cAddr18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(220, 311);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(32, 12);
            this.label67.TabIndex = 2;
            this.label67.Text = "M 18";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(44, 122);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(26, 12);
            this.label52.TabIndex = 2;
            this.label52.Text = "M 1";
            // 
            // textBoxI2cAddr1
            // 
            this.textBoxI2cAddr1.Location = new System.Drawing.Point(85, 119);
            this.textBoxI2cAddr1.Name = "textBoxI2cAddr1";
            this.textBoxI2cAddr1.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr1.TabIndex = 1;
            this.textBoxI2cAddr1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxI2cAddr8
            // 
            this.textBoxI2cAddr8.Location = new System.Drawing.Point(85, 308);
            this.textBoxI2cAddr8.Name = "textBoxI2cAddr8";
            this.textBoxI2cAddr8.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr8.TabIndex = 1;
            this.textBoxI2cAddr8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(44, 311);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(26, 12);
            this.label66.TabIndex = 2;
            this.label66.Text = "M 8";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(220, 122);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(32, 12);
            this.label53.TabIndex = 2;
            this.label53.Text = "M 11";
            // 
            // textBoxI2cAddr11
            // 
            this.textBoxI2cAddr11.Location = new System.Drawing.Point(261, 119);
            this.textBoxI2cAddr11.Name = "textBoxI2cAddr11";
            this.textBoxI2cAddr11.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr11.TabIndex = 1;
            this.textBoxI2cAddr11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxI2cAddr17
            // 
            this.textBoxI2cAddr17.Location = new System.Drawing.Point(261, 281);
            this.textBoxI2cAddr17.Name = "textBoxI2cAddr17";
            this.textBoxI2cAddr17.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr17.TabIndex = 1;
            this.textBoxI2cAddr17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(220, 284);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(32, 12);
            this.label65.TabIndex = 2;
            this.label65.Text = "M 17";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(44, 149);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(26, 12);
            this.label54.TabIndex = 2;
            this.label54.Text = "M 2";
            // 
            // textBoxI2cAddr2
            // 
            this.textBoxI2cAddr2.Location = new System.Drawing.Point(85, 146);
            this.textBoxI2cAddr2.Name = "textBoxI2cAddr2";
            this.textBoxI2cAddr2.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr2.TabIndex = 1;
            this.textBoxI2cAddr2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxI2cAddr7
            // 
            this.textBoxI2cAddr7.Location = new System.Drawing.Point(85, 281);
            this.textBoxI2cAddr7.Name = "textBoxI2cAddr7";
            this.textBoxI2cAddr7.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr7.TabIndex = 1;
            this.textBoxI2cAddr7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(44, 284);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(26, 12);
            this.label64.TabIndex = 2;
            this.label64.Text = "M 7";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(220, 149);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(32, 12);
            this.label55.TabIndex = 2;
            this.label55.Text = "M 12";
            // 
            // textBoxI2cAddr12
            // 
            this.textBoxI2cAddr12.Location = new System.Drawing.Point(261, 146);
            this.textBoxI2cAddr12.Name = "textBoxI2cAddr12";
            this.textBoxI2cAddr12.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr12.TabIndex = 1;
            this.textBoxI2cAddr12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxI2cAddr16
            // 
            this.textBoxI2cAddr16.Location = new System.Drawing.Point(261, 254);
            this.textBoxI2cAddr16.Name = "textBoxI2cAddr16";
            this.textBoxI2cAddr16.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr16.TabIndex = 1;
            this.textBoxI2cAddr16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(220, 257);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(32, 12);
            this.label63.TabIndex = 2;
            this.label63.Text = "M 16";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(44, 176);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(26, 12);
            this.label56.TabIndex = 2;
            this.label56.Text = "M 3";
            // 
            // textBoxI2cAddr3
            // 
            this.textBoxI2cAddr3.Location = new System.Drawing.Point(85, 173);
            this.textBoxI2cAddr3.Name = "textBoxI2cAddr3";
            this.textBoxI2cAddr3.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr3.TabIndex = 1;
            this.textBoxI2cAddr3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxI2cAddr6
            // 
            this.textBoxI2cAddr6.Location = new System.Drawing.Point(85, 254);
            this.textBoxI2cAddr6.Name = "textBoxI2cAddr6";
            this.textBoxI2cAddr6.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr6.TabIndex = 1;
            this.textBoxI2cAddr6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(44, 257);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(26, 12);
            this.label62.TabIndex = 2;
            this.label62.Text = "M 6";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(220, 176);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(32, 12);
            this.label57.TabIndex = 2;
            this.label57.Text = "M 13";
            // 
            // textBoxI2cAddr13
            // 
            this.textBoxI2cAddr13.Location = new System.Drawing.Point(261, 173);
            this.textBoxI2cAddr13.Name = "textBoxI2cAddr13";
            this.textBoxI2cAddr13.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr13.TabIndex = 1;
            this.textBoxI2cAddr13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxI2cAddr15
            // 
            this.textBoxI2cAddr15.Location = new System.Drawing.Point(261, 227);
            this.textBoxI2cAddr15.Name = "textBoxI2cAddr15";
            this.textBoxI2cAddr15.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr15.TabIndex = 1;
            this.textBoxI2cAddr15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(220, 230);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(32, 12);
            this.label61.TabIndex = 2;
            this.label61.Text = "M 15";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(44, 203);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(26, 12);
            this.label58.TabIndex = 2;
            this.label58.Text = "M 4";
            // 
            // textBoxI2cAddr4
            // 
            this.textBoxI2cAddr4.Location = new System.Drawing.Point(85, 200);
            this.textBoxI2cAddr4.Name = "textBoxI2cAddr4";
            this.textBoxI2cAddr4.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr4.TabIndex = 1;
            this.textBoxI2cAddr4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxI2cAddr5
            // 
            this.textBoxI2cAddr5.Location = new System.Drawing.Point(85, 227);
            this.textBoxI2cAddr5.Name = "textBoxI2cAddr5";
            this.textBoxI2cAddr5.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr5.TabIndex = 1;
            this.textBoxI2cAddr5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(44, 230);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(26, 12);
            this.label60.TabIndex = 2;
            this.label60.Text = "M 5";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(220, 203);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(32, 12);
            this.label59.TabIndex = 2;
            this.label59.Text = "M 14";
            // 
            // textBoxI2cAddr14
            // 
            this.textBoxI2cAddr14.Location = new System.Drawing.Point(261, 200);
            this.textBoxI2cAddr14.Name = "textBoxI2cAddr14";
            this.textBoxI2cAddr14.Size = new System.Drawing.Size(63, 21);
            this.textBoxI2cAddr14.TabIndex = 1;
            this.textBoxI2cAddr14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Controls.Add(this.buttonVerGet);
            this.panel4.Controls.Add(this.buttonAlarmGen);
            this.panel4.Controls.Add(this.buttonAlarmGet);
            this.panel4.Controls.Add(this.buttonAlarmSet);
            this.panel4.Controls.Add(this.label6);
            this.panel4.Controls.Add(this.textBoxAlarmIn);
            this.panel4.Controls.Add(this.label29);
            this.panel4.Controls.Add(this.label49);
            this.panel4.Controls.Add(this.textBoxAlarm0);
            this.panel4.Controls.Add(this.label48);
            this.panel4.Controls.Add(this.textBoxAlarm10);
            this.panel4.Controls.Add(this.label47);
            this.panel4.Controls.Add(this.textBoxAlarm1);
            this.panel4.Controls.Add(this.label46);
            this.panel4.Controls.Add(this.textBoxAlarm11);
            this.panel4.Controls.Add(this.label45);
            this.panel4.Controls.Add(this.textBoxAlarm2);
            this.panel4.Controls.Add(this.label44);
            this.panel4.Controls.Add(this.textBoxAlarm12);
            this.panel4.Controls.Add(this.label43);
            this.panel4.Controls.Add(this.textBoxAlarm3);
            this.panel4.Controls.Add(this.label42);
            this.panel4.Controls.Add(this.textBoxAlarm13);
            this.panel4.Controls.Add(this.label41);
            this.panel4.Controls.Add(this.textBoxAlarm4);
            this.panel4.Controls.Add(this.label40);
            this.panel4.Controls.Add(this.textBoxAlarm14);
            this.panel4.Controls.Add(this.label39);
            this.panel4.Controls.Add(this.textBoxAlarm5);
            this.panel4.Controls.Add(this.label38);
            this.panel4.Controls.Add(this.textBoxAlarm15);
            this.panel4.Controls.Add(this.label37);
            this.panel4.Controls.Add(this.textBoxAlarm6);
            this.panel4.Controls.Add(this.label36);
            this.panel4.Controls.Add(this.textBoxAlarm16);
            this.panel4.Controls.Add(this.label35);
            this.panel4.Controls.Add(this.textBoxAlarm7);
            this.panel4.Controls.Add(this.label34);
            this.panel4.Controls.Add(this.textBoxAlarm17);
            this.panel4.Controls.Add(this.label33);
            this.panel4.Controls.Add(this.textBoxAlarm8);
            this.panel4.Controls.Add(this.label32);
            this.panel4.Controls.Add(this.textBoxAlarm18);
            this.panel4.Controls.Add(this.label31);
            this.panel4.Controls.Add(this.textBoxAlarm9);
            this.panel4.Controls.Add(this.label30);
            this.panel4.Controls.Add(this.textBoxAlarm19);
            this.panel4.Location = new System.Drawing.Point(392, 68);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(360, 482);
            this.panel4.TabIndex = 5;
            // 
            // buttonVerGet
            // 
            this.buttonVerGet.Location = new System.Drawing.Point(213, 439);
            this.buttonVerGet.Name = "buttonVerGet";
            this.buttonVerGet.Size = new System.Drawing.Size(75, 23);
            this.buttonVerGet.TabIndex = 8;
            this.buttonVerGet.Text = "Ver Get";
            this.buttonVerGet.UseVisualStyleBackColor = true;
            this.buttonVerGet.Click += new System.EventHandler(this.buttonVerGet_Click);
            // 
            // buttonAlarmGen
            // 
            this.buttonAlarmGen.Location = new System.Drawing.Point(252, 36);
            this.buttonAlarmGen.Name = "buttonAlarmGen";
            this.buttonAlarmGen.Size = new System.Drawing.Size(75, 23);
            this.buttonAlarmGen.TabIndex = 4;
            this.buttonAlarmGen.Text = "Generate";
            this.buttonAlarmGen.UseVisualStyleBackColor = true;
            this.buttonAlarmGen.Click += new System.EventHandler(this.buttonAlarmGen_Click);
            // 
            // buttonAlarmGet
            // 
            this.buttonAlarmGet.Location = new System.Drawing.Point(213, 389);
            this.buttonAlarmGet.Name = "buttonAlarmGet";
            this.buttonAlarmGet.Size = new System.Drawing.Size(75, 23);
            this.buttonAlarmGet.TabIndex = 2;
            this.buttonAlarmGet.Text = "Alarm Get";
            this.buttonAlarmGet.UseVisualStyleBackColor = true;
            this.buttonAlarmGet.Click += new System.EventHandler(this.buttonAlarmGet_Click);
            // 
            // buttonAlarmSet
            // 
            this.buttonAlarmSet.Location = new System.Drawing.Point(76, 389);
            this.buttonAlarmSet.Name = "buttonAlarmSet";
            this.buttonAlarmSet.Size = new System.Drawing.Size(75, 23);
            this.buttonAlarmSet.TabIndex = 1;
            this.buttonAlarmSet.Text = "Alarm Set";
            this.buttonAlarmSet.UseVisualStyleBackColor = true;
            this.buttonAlarmSet.Click += new System.EventHandler(this.buttonAlarmSet_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 12);
            this.label6.TabIndex = 0;
            this.label6.Text = "Alarm Set";
            // 
            // textBoxAlarmIn
            // 
            this.textBoxAlarmIn.Location = new System.Drawing.Point(143, 37);
            this.textBoxAlarmIn.Name = "textBoxAlarmIn";
            this.textBoxAlarmIn.Size = new System.Drawing.Size(100, 21);
            this.textBoxAlarmIn.TabIndex = 3;
            this.textBoxAlarmIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(38, 41);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(74, 12);
            this.label29.TabIndex = 0;
            this.label29.Text = "Alarm Value";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(211, 338);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(32, 12);
            this.label49.TabIndex = 2;
            this.label49.Text = "M 19";
            // 
            // textBoxAlarm0
            // 
            this.textBoxAlarm0.Location = new System.Drawing.Point(76, 92);
            this.textBoxAlarm0.Name = "textBoxAlarm0";
            this.textBoxAlarm0.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm0.TabIndex = 1;
            this.textBoxAlarm0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(35, 338);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(26, 12);
            this.label48.TabIndex = 2;
            this.label48.Text = "M 9";
            // 
            // textBoxAlarm10
            // 
            this.textBoxAlarm10.Location = new System.Drawing.Point(252, 92);
            this.textBoxAlarm10.Name = "textBoxAlarm10";
            this.textBoxAlarm10.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm10.TabIndex = 1;
            this.textBoxAlarm10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(211, 311);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(32, 12);
            this.label47.TabIndex = 2;
            this.label47.Text = "M 18";
            // 
            // textBoxAlarm1
            // 
            this.textBoxAlarm1.Location = new System.Drawing.Point(76, 119);
            this.textBoxAlarm1.Name = "textBoxAlarm1";
            this.textBoxAlarm1.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm1.TabIndex = 1;
            this.textBoxAlarm1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(35, 311);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(26, 12);
            this.label46.TabIndex = 2;
            this.label46.Text = "M 8";
            // 
            // textBoxAlarm11
            // 
            this.textBoxAlarm11.Location = new System.Drawing.Point(252, 119);
            this.textBoxAlarm11.Name = "textBoxAlarm11";
            this.textBoxAlarm11.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm11.TabIndex = 1;
            this.textBoxAlarm11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(211, 284);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(32, 12);
            this.label45.TabIndex = 2;
            this.label45.Text = "M 17";
            // 
            // textBoxAlarm2
            // 
            this.textBoxAlarm2.Location = new System.Drawing.Point(76, 146);
            this.textBoxAlarm2.Name = "textBoxAlarm2";
            this.textBoxAlarm2.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm2.TabIndex = 1;
            this.textBoxAlarm2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(35, 284);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(26, 12);
            this.label44.TabIndex = 2;
            this.label44.Text = "M 7";
            // 
            // textBoxAlarm12
            // 
            this.textBoxAlarm12.Location = new System.Drawing.Point(252, 146);
            this.textBoxAlarm12.Name = "textBoxAlarm12";
            this.textBoxAlarm12.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm12.TabIndex = 1;
            this.textBoxAlarm12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(211, 257);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(32, 12);
            this.label43.TabIndex = 2;
            this.label43.Text = "M 16";
            // 
            // textBoxAlarm3
            // 
            this.textBoxAlarm3.Location = new System.Drawing.Point(76, 173);
            this.textBoxAlarm3.Name = "textBoxAlarm3";
            this.textBoxAlarm3.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm3.TabIndex = 1;
            this.textBoxAlarm3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(35, 257);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(26, 12);
            this.label42.TabIndex = 2;
            this.label42.Text = "M 6";
            // 
            // textBoxAlarm13
            // 
            this.textBoxAlarm13.Location = new System.Drawing.Point(252, 173);
            this.textBoxAlarm13.Name = "textBoxAlarm13";
            this.textBoxAlarm13.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm13.TabIndex = 1;
            this.textBoxAlarm13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(211, 230);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(32, 12);
            this.label41.TabIndex = 2;
            this.label41.Text = "M 15";
            // 
            // textBoxAlarm4
            // 
            this.textBoxAlarm4.Location = new System.Drawing.Point(76, 200);
            this.textBoxAlarm4.Name = "textBoxAlarm4";
            this.textBoxAlarm4.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm4.TabIndex = 1;
            this.textBoxAlarm4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(35, 230);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(26, 12);
            this.label40.TabIndex = 2;
            this.label40.Text = "M 5";
            // 
            // textBoxAlarm14
            // 
            this.textBoxAlarm14.Location = new System.Drawing.Point(252, 200);
            this.textBoxAlarm14.Name = "textBoxAlarm14";
            this.textBoxAlarm14.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm14.TabIndex = 1;
            this.textBoxAlarm14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(211, 203);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(32, 12);
            this.label39.TabIndex = 2;
            this.label39.Text = "M 14";
            // 
            // textBoxAlarm5
            // 
            this.textBoxAlarm5.Location = new System.Drawing.Point(76, 227);
            this.textBoxAlarm5.Name = "textBoxAlarm5";
            this.textBoxAlarm5.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm5.TabIndex = 1;
            this.textBoxAlarm5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(35, 203);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(26, 12);
            this.label38.TabIndex = 2;
            this.label38.Text = "M 4";
            // 
            // textBoxAlarm15
            // 
            this.textBoxAlarm15.Location = new System.Drawing.Point(252, 227);
            this.textBoxAlarm15.Name = "textBoxAlarm15";
            this.textBoxAlarm15.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm15.TabIndex = 1;
            this.textBoxAlarm15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(211, 176);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(32, 12);
            this.label37.TabIndex = 2;
            this.label37.Text = "M 13";
            // 
            // textBoxAlarm6
            // 
            this.textBoxAlarm6.Location = new System.Drawing.Point(76, 254);
            this.textBoxAlarm6.Name = "textBoxAlarm6";
            this.textBoxAlarm6.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm6.TabIndex = 1;
            this.textBoxAlarm6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(35, 176);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(26, 12);
            this.label36.TabIndex = 2;
            this.label36.Text = "M 3";
            // 
            // textBoxAlarm16
            // 
            this.textBoxAlarm16.Location = new System.Drawing.Point(252, 254);
            this.textBoxAlarm16.Name = "textBoxAlarm16";
            this.textBoxAlarm16.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm16.TabIndex = 1;
            this.textBoxAlarm16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(211, 149);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(32, 12);
            this.label35.TabIndex = 2;
            this.label35.Text = "M 12";
            // 
            // textBoxAlarm7
            // 
            this.textBoxAlarm7.Location = new System.Drawing.Point(76, 281);
            this.textBoxAlarm7.Name = "textBoxAlarm7";
            this.textBoxAlarm7.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm7.TabIndex = 1;
            this.textBoxAlarm7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(35, 149);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(26, 12);
            this.label34.TabIndex = 2;
            this.label34.Text = "M 2";
            // 
            // textBoxAlarm17
            // 
            this.textBoxAlarm17.Location = new System.Drawing.Point(252, 281);
            this.textBoxAlarm17.Name = "textBoxAlarm17";
            this.textBoxAlarm17.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm17.TabIndex = 1;
            this.textBoxAlarm17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(211, 122);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(32, 12);
            this.label33.TabIndex = 2;
            this.label33.Text = "M 11";
            // 
            // textBoxAlarm8
            // 
            this.textBoxAlarm8.Location = new System.Drawing.Point(76, 308);
            this.textBoxAlarm8.Name = "textBoxAlarm8";
            this.textBoxAlarm8.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm8.TabIndex = 1;
            this.textBoxAlarm8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(35, 122);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(26, 12);
            this.label32.TabIndex = 2;
            this.label32.Text = "M 1";
            // 
            // textBoxAlarm18
            // 
            this.textBoxAlarm18.Location = new System.Drawing.Point(252, 308);
            this.textBoxAlarm18.Name = "textBoxAlarm18";
            this.textBoxAlarm18.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm18.TabIndex = 1;
            this.textBoxAlarm18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(211, 95);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(32, 12);
            this.label31.TabIndex = 2;
            this.label31.Text = "M 10";
            // 
            // textBoxAlarm9
            // 
            this.textBoxAlarm9.Location = new System.Drawing.Point(76, 335);
            this.textBoxAlarm9.Name = "textBoxAlarm9";
            this.textBoxAlarm9.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm9.TabIndex = 1;
            this.textBoxAlarm9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(35, 95);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(26, 12);
            this.label30.TabIndex = 2;
            this.label30.Text = "M 0";
            // 
            // textBoxAlarm19
            // 
            this.textBoxAlarm19.Location = new System.Drawing.Point(252, 335);
            this.textBoxAlarm19.Name = "textBoxAlarm19";
            this.textBoxAlarm19.Size = new System.Drawing.Size(63, 21);
            this.textBoxAlarm19.TabIndex = 1;
            this.textBoxAlarm19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.buttonSerialNumGet);
            this.panel3.Controls.Add(this.buttonSerialNumSet);
            this.panel3.Controls.Add(this.buttonSerialNumGenerate);
            this.panel3.Controls.Add(this.textBoxSerialNumIn);
            this.panel3.Controls.Add(this.label27);
            this.panel3.Controls.Add(this.label17);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.label16);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.label15);
            this.panel3.Controls.Add(this.label24);
            this.panel3.Controls.Add(this.label14);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.label22);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.label20);
            this.panel3.Controls.Add(this.label10);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.label9);
            this.panel3.Controls.Add(this.label18);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Controls.Add(this.textBoxSerialNum19);
            this.panel3.Controls.Add(this.textBoxSerialNum9);
            this.panel3.Controls.Add(this.textBoxSerialNum18);
            this.panel3.Controls.Add(this.textBoxSerialNum8);
            this.panel3.Controls.Add(this.textBoxSerialNum17);
            this.panel3.Controls.Add(this.textBoxSerialNum7);
            this.panel3.Controls.Add(this.textBoxSerialNum16);
            this.panel3.Controls.Add(this.textBoxSerialNum6);
            this.panel3.Controls.Add(this.textBoxSerialNum15);
            this.panel3.Controls.Add(this.textBoxSerialNum5);
            this.panel3.Controls.Add(this.textBoxSerialNum14);
            this.panel3.Controls.Add(this.textBoxSerialNum4);
            this.panel3.Controls.Add(this.textBoxSerialNum13);
            this.panel3.Controls.Add(this.textBoxSerialNum3);
            this.panel3.Controls.Add(this.textBoxSerialNum12);
            this.panel3.Controls.Add(this.textBoxSerialNum2);
            this.panel3.Controls.Add(this.textBoxSerialNum11);
            this.panel3.Controls.Add(this.textBoxSerialNum1);
            this.panel3.Controls.Add(this.textBoxSerialNum10);
            this.panel3.Controls.Add(this.textBoxSerialNum0);
            this.panel3.Controls.Add(this.label28);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Location = new System.Drawing.Point(7, 68);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(360, 482);
            this.panel3.TabIndex = 5;
            // 
            // buttonSerialNumGet
            // 
            this.buttonSerialNumGet.Location = new System.Drawing.Point(203, 389);
            this.buttonSerialNumGet.Name = "buttonSerialNumGet";
            this.buttonSerialNumGet.Size = new System.Drawing.Size(75, 23);
            this.buttonSerialNumGet.TabIndex = 6;
            this.buttonSerialNumGet.Text = "SN Get";
            this.buttonSerialNumGet.UseVisualStyleBackColor = true;
            this.buttonSerialNumGet.Click += new System.EventHandler(this.buttonSerialNumGet_Click);
            // 
            // buttonSerialNumSet
            // 
            this.buttonSerialNumSet.Location = new System.Drawing.Point(64, 389);
            this.buttonSerialNumSet.Name = "buttonSerialNumSet";
            this.buttonSerialNumSet.Size = new System.Drawing.Size(75, 23);
            this.buttonSerialNumSet.TabIndex = 5;
            this.buttonSerialNumSet.Text = "SN Set";
            this.buttonSerialNumSet.UseVisualStyleBackColor = true;
            this.buttonSerialNumSet.Click += new System.EventHandler(this.buttonSerialNumSet_Click);
            // 
            // buttonSerialNumGenerate
            // 
            this.buttonSerialNumGenerate.Location = new System.Drawing.Point(242, 35);
            this.buttonSerialNumGenerate.Name = "buttonSerialNumGenerate";
            this.buttonSerialNumGenerate.Size = new System.Drawing.Size(75, 23);
            this.buttonSerialNumGenerate.TabIndex = 4;
            this.buttonSerialNumGenerate.Text = "Generate";
            this.buttonSerialNumGenerate.UseVisualStyleBackColor = true;
            this.buttonSerialNumGenerate.Click += new System.EventHandler(this.buttonSerialNumGenerate_Click);
            // 
            // textBoxSerialNumIn
            // 
            this.textBoxSerialNumIn.Location = new System.Drawing.Point(98, 37);
            this.textBoxSerialNumIn.Name = "textBoxSerialNumIn";
            this.textBoxSerialNumIn.Size = new System.Drawing.Size(100, 21);
            this.textBoxSerialNumIn.TabIndex = 3;
            this.textBoxSerialNumIn.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(201, 338);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 12);
            this.label27.TabIndex = 2;
            this.label27.Text = "M 19";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(23, 338);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(26, 12);
            this.label17.TabIndex = 2;
            this.label17.Text = "M 9";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(201, 311);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(32, 12);
            this.label26.TabIndex = 2;
            this.label26.Text = "M 18";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 311);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(26, 12);
            this.label16.TabIndex = 2;
            this.label16.Text = "M 8";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(201, 284);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(32, 12);
            this.label25.TabIndex = 2;
            this.label25.Text = "M 17";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 284);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 12);
            this.label15.TabIndex = 2;
            this.label15.Text = "M 7";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(201, 257);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(32, 12);
            this.label24.TabIndex = 2;
            this.label24.Text = "M 16";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 257);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 12);
            this.label14.TabIndex = 2;
            this.label14.Text = "M 6";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(201, 230);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(32, 12);
            this.label23.TabIndex = 2;
            this.label23.Text = "M 15";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 230);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 12);
            this.label13.TabIndex = 2;
            this.label13.Text = "M 5";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(201, 203);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(32, 12);
            this.label22.TabIndex = 2;
            this.label22.Text = "M 14";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(23, 203);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(26, 12);
            this.label12.TabIndex = 2;
            this.label12.Text = "M 4";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(201, 176);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(32, 12);
            this.label21.TabIndex = 2;
            this.label21.Text = "M 13";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 176);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(26, 12);
            this.label11.TabIndex = 2;
            this.label11.Text = "M 3";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(201, 149);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 12);
            this.label20.TabIndex = 2;
            this.label20.Text = "M 12";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 149);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(26, 12);
            this.label10.TabIndex = 2;
            this.label10.Text = "M 2";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(201, 122);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(32, 12);
            this.label19.TabIndex = 2;
            this.label19.Text = "M 11";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(23, 122);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(26, 12);
            this.label9.TabIndex = 2;
            this.label9.Text = "M 1";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(201, 95);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(32, 12);
            this.label18.TabIndex = 2;
            this.label18.Text = "M 10";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(26, 12);
            this.label8.TabIndex = 2;
            this.label8.Text = "M 0";
            // 
            // textBoxSerialNum19
            // 
            this.textBoxSerialNum19.Location = new System.Drawing.Point(242, 335);
            this.textBoxSerialNum19.Name = "textBoxSerialNum19";
            this.textBoxSerialNum19.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum19.TabIndex = 1;
            this.textBoxSerialNum19.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum9
            // 
            this.textBoxSerialNum9.Location = new System.Drawing.Point(64, 335);
            this.textBoxSerialNum9.Name = "textBoxSerialNum9";
            this.textBoxSerialNum9.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum9.TabIndex = 1;
            this.textBoxSerialNum9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum18
            // 
            this.textBoxSerialNum18.Location = new System.Drawing.Point(242, 308);
            this.textBoxSerialNum18.Name = "textBoxSerialNum18";
            this.textBoxSerialNum18.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum18.TabIndex = 1;
            this.textBoxSerialNum18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum8
            // 
            this.textBoxSerialNum8.Location = new System.Drawing.Point(64, 308);
            this.textBoxSerialNum8.Name = "textBoxSerialNum8";
            this.textBoxSerialNum8.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum8.TabIndex = 1;
            this.textBoxSerialNum8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum17
            // 
            this.textBoxSerialNum17.Location = new System.Drawing.Point(242, 281);
            this.textBoxSerialNum17.Name = "textBoxSerialNum17";
            this.textBoxSerialNum17.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum17.TabIndex = 1;
            this.textBoxSerialNum17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum7
            // 
            this.textBoxSerialNum7.Location = new System.Drawing.Point(64, 281);
            this.textBoxSerialNum7.Name = "textBoxSerialNum7";
            this.textBoxSerialNum7.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum7.TabIndex = 1;
            this.textBoxSerialNum7.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum16
            // 
            this.textBoxSerialNum16.Location = new System.Drawing.Point(242, 254);
            this.textBoxSerialNum16.Name = "textBoxSerialNum16";
            this.textBoxSerialNum16.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum16.TabIndex = 1;
            this.textBoxSerialNum16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum6
            // 
            this.textBoxSerialNum6.Location = new System.Drawing.Point(64, 254);
            this.textBoxSerialNum6.Name = "textBoxSerialNum6";
            this.textBoxSerialNum6.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum6.TabIndex = 1;
            this.textBoxSerialNum6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum15
            // 
            this.textBoxSerialNum15.Location = new System.Drawing.Point(242, 227);
            this.textBoxSerialNum15.Name = "textBoxSerialNum15";
            this.textBoxSerialNum15.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum15.TabIndex = 1;
            this.textBoxSerialNum15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum5
            // 
            this.textBoxSerialNum5.Location = new System.Drawing.Point(64, 227);
            this.textBoxSerialNum5.Name = "textBoxSerialNum5";
            this.textBoxSerialNum5.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum5.TabIndex = 1;
            this.textBoxSerialNum5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum14
            // 
            this.textBoxSerialNum14.Location = new System.Drawing.Point(242, 200);
            this.textBoxSerialNum14.Name = "textBoxSerialNum14";
            this.textBoxSerialNum14.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum14.TabIndex = 1;
            this.textBoxSerialNum14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum4
            // 
            this.textBoxSerialNum4.Location = new System.Drawing.Point(64, 200);
            this.textBoxSerialNum4.Name = "textBoxSerialNum4";
            this.textBoxSerialNum4.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum4.TabIndex = 1;
            this.textBoxSerialNum4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum13
            // 
            this.textBoxSerialNum13.Location = new System.Drawing.Point(242, 173);
            this.textBoxSerialNum13.Name = "textBoxSerialNum13";
            this.textBoxSerialNum13.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum13.TabIndex = 1;
            this.textBoxSerialNum13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum3
            // 
            this.textBoxSerialNum3.Location = new System.Drawing.Point(64, 173);
            this.textBoxSerialNum3.Name = "textBoxSerialNum3";
            this.textBoxSerialNum3.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum3.TabIndex = 1;
            this.textBoxSerialNum3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum12
            // 
            this.textBoxSerialNum12.Location = new System.Drawing.Point(242, 146);
            this.textBoxSerialNum12.Name = "textBoxSerialNum12";
            this.textBoxSerialNum12.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum12.TabIndex = 1;
            this.textBoxSerialNum12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum2
            // 
            this.textBoxSerialNum2.Location = new System.Drawing.Point(64, 146);
            this.textBoxSerialNum2.Name = "textBoxSerialNum2";
            this.textBoxSerialNum2.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum2.TabIndex = 1;
            this.textBoxSerialNum2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum11
            // 
            this.textBoxSerialNum11.Location = new System.Drawing.Point(242, 119);
            this.textBoxSerialNum11.Name = "textBoxSerialNum11";
            this.textBoxSerialNum11.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum11.TabIndex = 1;
            this.textBoxSerialNum11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum1
            // 
            this.textBoxSerialNum1.Location = new System.Drawing.Point(64, 119);
            this.textBoxSerialNum1.Name = "textBoxSerialNum1";
            this.textBoxSerialNum1.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum1.TabIndex = 1;
            this.textBoxSerialNum1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum10
            // 
            this.textBoxSerialNum10.Location = new System.Drawing.Point(242, 92);
            this.textBoxSerialNum10.Name = "textBoxSerialNum10";
            this.textBoxSerialNum10.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum10.TabIndex = 1;
            this.textBoxSerialNum10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxSerialNum0
            // 
            this.textBoxSerialNum0.Location = new System.Drawing.Point(64, 92);
            this.textBoxSerialNum0.Name = "textBoxSerialNum0";
            this.textBoxSerialNum0.Size = new System.Drawing.Size(63, 21);
            this.textBoxSerialNum0.TabIndex = 1;
            this.textBoxSerialNum0.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(23, 40);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(67, 12);
            this.label28.TabIndex = 0;
            this.label28.Text = "Start Of SN";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(23, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 12);
            this.label5.TabIndex = 0;
            this.label5.Text = "Serial Number";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage6);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(8, 7);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1360, 582);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.panel13);
            this.tabPage6.Controls.Add(this.panel12);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(1352, 556);
            this.tabPage6.TabIndex = 6;
            this.tabPage6.Text = "Auto Calibration";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.textBoxT2);
            this.panel13.Controls.Add(this.buttonMfcOff);
            this.panel13.Controls.Add(this.textBoxTimer);
            this.panel13.Controls.Add(this.buttonMfxStop);
            this.panel13.Controls.Add(this.buttonPurge);
            this.panel13.Controls.Add(this.richTextBoxSockLog);
            this.panel13.Controls.Add(this.button2000ppm);
            this.panel13.Controls.Add(this.button4500ppm);
            this.panel13.Controls.Add(this.buttonSocketOn);
            this.panel13.Controls.Add(this.button600ppm);
            this.panel13.Controls.Add(this.buttonCalibrationStart);
            this.panel13.Location = new System.Drawing.Point(417, 11);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(924, 533);
            this.panel13.TabIndex = 4;
            // 
            // buttonMfcOff
            // 
            this.buttonMfcOff.Location = new System.Drawing.Point(188, 461);
            this.buttonMfcOff.Name = "buttonMfcOff";
            this.buttonMfcOff.Size = new System.Drawing.Size(75, 23);
            this.buttonMfcOff.TabIndex = 5;
            this.buttonMfcOff.Text = "MFC OFF";
            this.buttonMfcOff.UseVisualStyleBackColor = true;
            this.buttonMfcOff.Click += new System.EventHandler(this.buttonMfcOff_Click);
            // 
            // textBoxTimer
            // 
            this.textBoxTimer.Location = new System.Drawing.Point(188, 207);
            this.textBoxTimer.Name = "textBoxTimer";
            this.textBoxTimer.Size = new System.Drawing.Size(76, 21);
            this.textBoxTimer.TabIndex = 4;
            this.textBoxTimer.Text = "0 sec";
            this.textBoxTimer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonMfxStop
            // 
            this.buttonMfxStop.Location = new System.Drawing.Point(31, 266);
            this.buttonMfxStop.Name = "buttonMfxStop";
            this.buttonMfxStop.Size = new System.Drawing.Size(101, 23);
            this.buttonMfxStop.TabIndex = 3;
            this.buttonMfxStop.Text = "Stop";
            this.buttonMfxStop.UseVisualStyleBackColor = true;
            this.buttonMfxStop.Click += new System.EventHandler(this.buttonMfxStop_Click);
            // 
            // buttonPurge
            // 
            this.buttonPurge.Location = new System.Drawing.Point(31, 431);
            this.buttonPurge.Name = "buttonPurge";
            this.buttonPurge.Size = new System.Drawing.Size(101, 23);
            this.buttonPurge.TabIndex = 3;
            this.buttonPurge.Text = "Purge";
            this.buttonPurge.UseVisualStyleBackColor = true;
            this.buttonPurge.Click += new System.EventHandler(this.buttonPurge_Click);
            // 
            // richTextBoxSockLog
            // 
            this.richTextBoxSockLog.Location = new System.Drawing.Point(188, 229);
            this.richTextBoxSockLog.Name = "richTextBoxSockLog";
            this.richTextBoxSockLog.Size = new System.Drawing.Size(591, 225);
            this.richTextBoxSockLog.TabIndex = 2;
            this.richTextBoxSockLog.Text = "";
            // 
            // button2000ppm
            // 
            this.button2000ppm.Location = new System.Drawing.Point(31, 389);
            this.button2000ppm.Name = "button2000ppm";
            this.button2000ppm.Size = new System.Drawing.Size(101, 23);
            this.button2000ppm.TabIndex = 1;
            this.button2000ppm.Text = "2000 ppm";
            this.button2000ppm.UseVisualStyleBackColor = true;
            this.button2000ppm.Click += new System.EventHandler(this.button2000ppm_Click);
            // 
            // button4500ppm
            // 
            this.button4500ppm.Location = new System.Drawing.Point(31, 348);
            this.button4500ppm.Name = "button4500ppm";
            this.button4500ppm.Size = new System.Drawing.Size(101, 23);
            this.button4500ppm.TabIndex = 1;
            this.button4500ppm.Text = "4500 ppm";
            this.button4500ppm.UseVisualStyleBackColor = true;
            this.button4500ppm.Click += new System.EventHandler(this.button4500ppm_Click);
            // 
            // buttonSocketOn
            // 
            this.buttonSocketOn.Location = new System.Drawing.Point(31, 229);
            this.buttonSocketOn.Name = "buttonSocketOn";
            this.buttonSocketOn.Size = new System.Drawing.Size(101, 23);
            this.buttonSocketOn.TabIndex = 1;
            this.buttonSocketOn.Text = "Socket On";
            this.buttonSocketOn.UseVisualStyleBackColor = true;
            this.buttonSocketOn.Click += new System.EventHandler(this.buttonSocketOn_Click);
            // 
            // button600ppm
            // 
            this.button600ppm.Location = new System.Drawing.Point(31, 304);
            this.button600ppm.Name = "button600ppm";
            this.button600ppm.Size = new System.Drawing.Size(101, 23);
            this.button600ppm.TabIndex = 1;
            this.button600ppm.Text = "600 ppm";
            this.button600ppm.UseVisualStyleBackColor = true;
            this.button600ppm.Click += new System.EventHandler(this.button600ppm_Click);
            // 
            // buttonCalibrationStart
            // 
            this.buttonCalibrationStart.Font = new System.Drawing.Font("Gulim", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.buttonCalibrationStart.ForeColor = System.Drawing.Color.Red;
            this.buttonCalibrationStart.Location = new System.Drawing.Point(31, 25);
            this.buttonCalibrationStart.Name = "buttonCalibrationStart";
            this.buttonCalibrationStart.Size = new System.Drawing.Size(345, 101);
            this.buttonCalibrationStart.TabIndex = 0;
            this.buttonCalibrationStart.Text = "Calibration Start";
            this.buttonCalibrationStart.UseVisualStyleBackColor = true;
            this.buttonCalibrationStart.Click += new System.EventHandler(this.buttonCalibrationStart_Click);
            // 
            // panel12
            // 
            this.panel12.Controls.Add(this.buttonToDef);
            this.panel12.Controls.Add(this.buttonDcDrAdj);
            this.panel12.Controls.Add(this.groupBox23);
            this.panel12.Controls.Add(this.buttonTsAdj);
            this.panel12.Controls.Add(this.buttonGainAdj);
            this.panel12.Controls.Add(this.buttonPpmAdj);
            this.panel12.Location = new System.Drawing.Point(13, 11);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(373, 533);
            this.panel12.TabIndex = 3;
            // 
            // buttonToDef
            // 
            this.buttonToDef.Location = new System.Drawing.Point(16, 14);
            this.buttonToDef.Name = "buttonToDef";
            this.buttonToDef.Size = new System.Drawing.Size(113, 23);
            this.buttonToDef.TabIndex = 0;
            this.buttonToDef.Text = "Set To Default";
            this.buttonToDef.UseVisualStyleBackColor = true;
            this.buttonToDef.Click += new System.EventHandler(this.buttonToDef_Click);
            // 
            // buttonDcDrAdj
            // 
            this.buttonDcDrAdj.Location = new System.Drawing.Point(16, 58);
            this.buttonDcDrAdj.Name = "buttonDcDrAdj";
            this.buttonDcDrAdj.Size = new System.Drawing.Size(113, 23);
            this.buttonDcDrAdj.TabIndex = 0;
            this.buttonDcDrAdj.Text = "dC/dR Adjust";
            this.buttonDcDrAdj.UseVisualStyleBackColor = true;
            this.buttonDcDrAdj.Click += new System.EventHandler(this.buttonDcDrAdj_Click);
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.textBoxModuleNum);
            this.groupBox23.Controls.Add(this.buttonCalStop);
            this.groupBox23.Controls.Add(this.label79);
            this.groupBox23.Controls.Add(this.checkBoxM19);
            this.groupBox23.Controls.Add(this.checkBoxM9);
            this.groupBox23.Controls.Add(this.checkBoxM18);
            this.groupBox23.Controls.Add(this.checkBoxM8);
            this.groupBox23.Controls.Add(this.checkBoxM17);
            this.groupBox23.Controls.Add(this.checkBoxM7);
            this.groupBox23.Controls.Add(this.checkBoxM16);
            this.groupBox23.Controls.Add(this.checkBoxM6);
            this.groupBox23.Controls.Add(this.checkBoxM15);
            this.groupBox23.Controls.Add(this.checkBoxM5);
            this.groupBox23.Controls.Add(this.checkBoxM14);
            this.groupBox23.Controls.Add(this.checkBoxM4);
            this.groupBox23.Controls.Add(this.checkBoxM13);
            this.groupBox23.Controls.Add(this.checkBoxM3);
            this.groupBox23.Controls.Add(this.checkBoxM12);
            this.groupBox23.Controls.Add(this.checkBoxM2);
            this.groupBox23.Controls.Add(this.checkBoxM11);
            this.groupBox23.Controls.Add(this.checkBoxM1);
            this.groupBox23.Controls.Add(this.checkBoxM10);
            this.groupBox23.Controls.Add(this.checkBoxM0);
            this.groupBox23.Location = new System.Drawing.Point(188, 14);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(162, 344);
            this.groupBox23.TabIndex = 1;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "PPM Adjust";
            // 
            // textBoxModuleNum
            // 
            this.textBoxModuleNum.Location = new System.Drawing.Point(17, 316);
            this.textBoxModuleNum.Name = "textBoxModuleNum";
            this.textBoxModuleNum.Size = new System.Drawing.Size(68, 21);
            this.textBoxModuleNum.TabIndex = 4;
            // 
            // buttonCalStop
            // 
            this.buttonCalStop.Location = new System.Drawing.Point(18, 20);
            this.buttonCalStop.Name = "buttonCalStop";
            this.buttonCalStop.Size = new System.Drawing.Size(75, 23);
            this.buttonCalStop.TabIndex = 2;
            this.buttonCalStop.Text = "Cal Stop";
            this.buttonCalStop.UseVisualStyleBackColor = true;
            this.buttonCalStop.Click += new System.EventHandler(this.buttonCalStop_Click);
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Location = new System.Drawing.Point(6, 301);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(151, 12);
            this.label79.TabIndex = 3;
            this.label79.Text = "Available Module Number";
            // 
            // checkBoxM19
            // 
            this.checkBoxM19.AutoSize = true;
            this.checkBoxM19.Location = new System.Drawing.Point(88, 259);
            this.checkBoxM19.Name = "checkBoxM19";
            this.checkBoxM19.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM19.TabIndex = 2;
            this.checkBoxM19.Text = "M19";
            this.checkBoxM19.UseVisualStyleBackColor = true;
            // 
            // checkBoxM9
            // 
            this.checkBoxM9.AutoSize = true;
            this.checkBoxM9.Location = new System.Drawing.Point(17, 259);
            this.checkBoxM9.Name = "checkBoxM9";
            this.checkBoxM9.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM9.TabIndex = 2;
            this.checkBoxM9.Text = "M9";
            this.checkBoxM9.UseVisualStyleBackColor = true;
            // 
            // checkBoxM18
            // 
            this.checkBoxM18.AutoSize = true;
            this.checkBoxM18.Location = new System.Drawing.Point(88, 237);
            this.checkBoxM18.Name = "checkBoxM18";
            this.checkBoxM18.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM18.TabIndex = 2;
            this.checkBoxM18.Text = "M18";
            this.checkBoxM18.UseVisualStyleBackColor = true;
            // 
            // checkBoxM8
            // 
            this.checkBoxM8.AutoSize = true;
            this.checkBoxM8.Location = new System.Drawing.Point(17, 237);
            this.checkBoxM8.Name = "checkBoxM8";
            this.checkBoxM8.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM8.TabIndex = 2;
            this.checkBoxM8.Text = "M8";
            this.checkBoxM8.UseVisualStyleBackColor = true;
            // 
            // checkBoxM17
            // 
            this.checkBoxM17.AutoSize = true;
            this.checkBoxM17.Location = new System.Drawing.Point(88, 215);
            this.checkBoxM17.Name = "checkBoxM17";
            this.checkBoxM17.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM17.TabIndex = 2;
            this.checkBoxM17.Text = "M17";
            this.checkBoxM17.UseVisualStyleBackColor = true;
            // 
            // checkBoxM7
            // 
            this.checkBoxM7.AutoSize = true;
            this.checkBoxM7.Location = new System.Drawing.Point(17, 215);
            this.checkBoxM7.Name = "checkBoxM7";
            this.checkBoxM7.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM7.TabIndex = 2;
            this.checkBoxM7.Text = "M7";
            this.checkBoxM7.UseVisualStyleBackColor = true;
            // 
            // checkBoxM16
            // 
            this.checkBoxM16.AutoSize = true;
            this.checkBoxM16.Location = new System.Drawing.Point(88, 193);
            this.checkBoxM16.Name = "checkBoxM16";
            this.checkBoxM16.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM16.TabIndex = 2;
            this.checkBoxM16.Text = "M16";
            this.checkBoxM16.UseVisualStyleBackColor = true;
            // 
            // checkBoxM6
            // 
            this.checkBoxM6.AutoSize = true;
            this.checkBoxM6.Location = new System.Drawing.Point(17, 193);
            this.checkBoxM6.Name = "checkBoxM6";
            this.checkBoxM6.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM6.TabIndex = 2;
            this.checkBoxM6.Text = "M6";
            this.checkBoxM6.UseVisualStyleBackColor = true;
            // 
            // checkBoxM15
            // 
            this.checkBoxM15.AutoSize = true;
            this.checkBoxM15.Location = new System.Drawing.Point(88, 171);
            this.checkBoxM15.Name = "checkBoxM15";
            this.checkBoxM15.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM15.TabIndex = 2;
            this.checkBoxM15.Text = "M15";
            this.checkBoxM15.UseVisualStyleBackColor = true;
            // 
            // checkBoxM5
            // 
            this.checkBoxM5.AutoSize = true;
            this.checkBoxM5.Location = new System.Drawing.Point(17, 171);
            this.checkBoxM5.Name = "checkBoxM5";
            this.checkBoxM5.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM5.TabIndex = 2;
            this.checkBoxM5.Text = "M5";
            this.checkBoxM5.UseVisualStyleBackColor = true;
            // 
            // checkBoxM14
            // 
            this.checkBoxM14.AutoSize = true;
            this.checkBoxM14.Location = new System.Drawing.Point(88, 149);
            this.checkBoxM14.Name = "checkBoxM14";
            this.checkBoxM14.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM14.TabIndex = 2;
            this.checkBoxM14.Text = "M14";
            this.checkBoxM14.UseVisualStyleBackColor = true;
            // 
            // checkBoxM4
            // 
            this.checkBoxM4.AutoSize = true;
            this.checkBoxM4.Location = new System.Drawing.Point(17, 149);
            this.checkBoxM4.Name = "checkBoxM4";
            this.checkBoxM4.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM4.TabIndex = 2;
            this.checkBoxM4.Text = "M4";
            this.checkBoxM4.UseVisualStyleBackColor = true;
            // 
            // checkBoxM13
            // 
            this.checkBoxM13.AutoSize = true;
            this.checkBoxM13.Location = new System.Drawing.Point(88, 127);
            this.checkBoxM13.Name = "checkBoxM13";
            this.checkBoxM13.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM13.TabIndex = 2;
            this.checkBoxM13.Text = "M13";
            this.checkBoxM13.UseVisualStyleBackColor = true;
            // 
            // checkBoxM3
            // 
            this.checkBoxM3.AutoSize = true;
            this.checkBoxM3.Location = new System.Drawing.Point(17, 127);
            this.checkBoxM3.Name = "checkBoxM3";
            this.checkBoxM3.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM3.TabIndex = 2;
            this.checkBoxM3.Text = "M3";
            this.checkBoxM3.UseVisualStyleBackColor = true;
            // 
            // checkBoxM12
            // 
            this.checkBoxM12.AutoSize = true;
            this.checkBoxM12.Location = new System.Drawing.Point(88, 105);
            this.checkBoxM12.Name = "checkBoxM12";
            this.checkBoxM12.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM12.TabIndex = 2;
            this.checkBoxM12.Text = "M12";
            this.checkBoxM12.UseVisualStyleBackColor = true;
            // 
            // checkBoxM2
            // 
            this.checkBoxM2.AutoSize = true;
            this.checkBoxM2.Location = new System.Drawing.Point(17, 105);
            this.checkBoxM2.Name = "checkBoxM2";
            this.checkBoxM2.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM2.TabIndex = 2;
            this.checkBoxM2.Text = "M2";
            this.checkBoxM2.UseVisualStyleBackColor = true;
            // 
            // checkBoxM11
            // 
            this.checkBoxM11.AutoSize = true;
            this.checkBoxM11.Location = new System.Drawing.Point(89, 83);
            this.checkBoxM11.Name = "checkBoxM11";
            this.checkBoxM11.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM11.TabIndex = 1;
            this.checkBoxM11.Text = "M11";
            this.checkBoxM11.UseVisualStyleBackColor = true;
            // 
            // checkBoxM1
            // 
            this.checkBoxM1.AutoSize = true;
            this.checkBoxM1.Location = new System.Drawing.Point(18, 83);
            this.checkBoxM1.Name = "checkBoxM1";
            this.checkBoxM1.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM1.TabIndex = 1;
            this.checkBoxM1.Text = "M1";
            this.checkBoxM1.UseVisualStyleBackColor = true;
            // 
            // checkBoxM10
            // 
            this.checkBoxM10.AutoSize = true;
            this.checkBoxM10.Location = new System.Drawing.Point(89, 64);
            this.checkBoxM10.Name = "checkBoxM10";
            this.checkBoxM10.Size = new System.Drawing.Size(47, 16);
            this.checkBoxM10.TabIndex = 1;
            this.checkBoxM10.Text = "M10";
            this.checkBoxM10.UseVisualStyleBackColor = true;
            // 
            // checkBoxM0
            // 
            this.checkBoxM0.AutoSize = true;
            this.checkBoxM0.Location = new System.Drawing.Point(18, 64);
            this.checkBoxM0.Name = "checkBoxM0";
            this.checkBoxM0.Size = new System.Drawing.Size(41, 16);
            this.checkBoxM0.TabIndex = 1;
            this.checkBoxM0.Text = "M0";
            this.checkBoxM0.UseVisualStyleBackColor = true;
            // 
            // buttonTsAdj
            // 
            this.buttonTsAdj.Location = new System.Drawing.Point(16, 103);
            this.buttonTsAdj.Name = "buttonTsAdj";
            this.buttonTsAdj.Size = new System.Drawing.Size(113, 23);
            this.buttonTsAdj.TabIndex = 0;
            this.buttonTsAdj.Text = "TS Adjust";
            this.buttonTsAdj.UseVisualStyleBackColor = true;
            this.buttonTsAdj.Click += new System.EventHandler(this.buttonTsAdj_Click);
            // 
            // buttonGainAdj
            // 
            this.buttonGainAdj.Location = new System.Drawing.Point(16, 199);
            this.buttonGainAdj.Name = "buttonGainAdj";
            this.buttonGainAdj.Size = new System.Drawing.Size(113, 23);
            this.buttonGainAdj.TabIndex = 0;
            this.buttonGainAdj.Text = "Gain Adjust";
            this.buttonGainAdj.UseVisualStyleBackColor = true;
            this.buttonGainAdj.Click += new System.EventHandler(this.buttonGainAdj_Click);
            // 
            // buttonPpmAdj
            // 
            this.buttonPpmAdj.Location = new System.Drawing.Point(16, 156);
            this.buttonPpmAdj.Name = "buttonPpmAdj";
            this.buttonPpmAdj.Size = new System.Drawing.Size(113, 23);
            this.buttonPpmAdj.TabIndex = 0;
            this.buttonPpmAdj.Text = "PPM Adjust";
            this.buttonPpmAdj.UseVisualStyleBackColor = true;
            this.buttonPpmAdj.Click += new System.EventHandler(this.buttonPpmAdj_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.checkBoxPic);
            this.tabPage2.Controls.Add(this.labelM);
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(1352, 556);
            this.tabPage2.TabIndex = 5;
            this.tabPage2.Text = "PIC Kit";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // checkBoxPic
            // 
            this.checkBoxPic.AutoSize = true;
            this.checkBoxPic.Location = new System.Drawing.Point(315, 36);
            this.checkBoxPic.Name = "checkBoxPic";
            this.checkBoxPic.Size = new System.Drawing.Size(185, 16);
            this.checkBoxPic.TabIndex = 3;
            this.checkBoxPic.Text = "PIC Program Mode Disabled";
            this.checkBoxPic.UseVisualStyleBackColor = true;
            this.checkBoxPic.CheckedChanged += new System.EventHandler(this.checkBoxPic_CheckedChanged);
            // 
            // labelM
            // 
            this.labelM.AutoSize = true;
            this.labelM.Font = new System.Drawing.Font("Gulim", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.labelM.Location = new System.Drawing.Point(24, 298);
            this.labelM.Name = "labelM";
            this.labelM.Size = new System.Drawing.Size(223, 16);
            this.labelM.TabIndex = 2;
            this.labelM.Text = "Please select module number";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Controls.Add(this.radioButton20);
            this.groupBox1.Controls.Add(this.radioButton11);
            this.groupBox1.Controls.Add(this.radioButton10);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton19);
            this.groupBox1.Controls.Add(this.radioButton12);
            this.groupBox1.Controls.Add(this.radioButton9);
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton18);
            this.groupBox1.Controls.Add(this.radioButton13);
            this.groupBox1.Controls.Add(this.radioButton8);
            this.groupBox1.Controls.Add(this.radioButton4);
            this.groupBox1.Controls.Add(this.radioButton17);
            this.groupBox1.Controls.Add(this.radioButton14);
            this.groupBox1.Controls.Add(this.radioButton7);
            this.groupBox1.Controls.Add(this.radioButton5);
            this.groupBox1.Controls.Add(this.radioButton16);
            this.groupBox1.Controls.Add(this.radioButton15);
            this.groupBox1.Controls.Add(this.radioButton6);
            this.groupBox1.Location = new System.Drawing.Point(15, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(243, 272);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Module Select";
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(29, 37);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(40, 16);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.Text = "M0";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton20
            // 
            this.radioButton20.AutoSize = true;
            this.radioButton20.Location = new System.Drawing.Point(157, 235);
            this.radioButton20.Name = "radioButton20";
            this.radioButton20.Size = new System.Drawing.Size(46, 16);
            this.radioButton20.TabIndex = 0;
            this.radioButton20.Text = "M19";
            this.radioButton20.UseVisualStyleBackColor = true;
            this.radioButton20.CheckedChanged += new System.EventHandler(this.radioButton20_CheckedChanged);
            // 
            // radioButton11
            // 
            this.radioButton11.AutoSize = true;
            this.radioButton11.Location = new System.Drawing.Point(157, 37);
            this.radioButton11.Name = "radioButton11";
            this.radioButton11.Size = new System.Drawing.Size(46, 16);
            this.radioButton11.TabIndex = 0;
            this.radioButton11.Text = "M10";
            this.radioButton11.UseVisualStyleBackColor = true;
            this.radioButton11.CheckedChanged += new System.EventHandler(this.radioButton11_CheckedChanged);
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Location = new System.Drawing.Point(29, 235);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(40, 16);
            this.radioButton10.TabIndex = 0;
            this.radioButton10.Text = "M9";
            this.radioButton10.UseVisualStyleBackColor = true;
            this.radioButton10.CheckedChanged += new System.EventHandler(this.radioButton10_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(29, 59);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(40, 16);
            this.radioButton2.TabIndex = 0;
            this.radioButton2.Text = "M1";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton19
            // 
            this.radioButton19.AutoSize = true;
            this.radioButton19.Location = new System.Drawing.Point(157, 213);
            this.radioButton19.Name = "radioButton19";
            this.radioButton19.Size = new System.Drawing.Size(46, 16);
            this.radioButton19.TabIndex = 0;
            this.radioButton19.Text = "M18";
            this.radioButton19.UseVisualStyleBackColor = true;
            this.radioButton19.CheckedChanged += new System.EventHandler(this.radioButton19_CheckedChanged);
            // 
            // radioButton12
            // 
            this.radioButton12.AutoSize = true;
            this.radioButton12.Location = new System.Drawing.Point(157, 59);
            this.radioButton12.Name = "radioButton12";
            this.radioButton12.Size = new System.Drawing.Size(46, 16);
            this.radioButton12.TabIndex = 0;
            this.radioButton12.Text = "M11";
            this.radioButton12.UseVisualStyleBackColor = true;
            this.radioButton12.CheckedChanged += new System.EventHandler(this.radioButton12_CheckedChanged);
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Location = new System.Drawing.Point(29, 213);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(40, 16);
            this.radioButton9.TabIndex = 0;
            this.radioButton9.Text = "M8";
            this.radioButton9.UseVisualStyleBackColor = true;
            this.radioButton9.CheckedChanged += new System.EventHandler(this.radioButton9_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(29, 81);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(40, 16);
            this.radioButton3.TabIndex = 0;
            this.radioButton3.Text = "M2";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton18
            // 
            this.radioButton18.AutoSize = true;
            this.radioButton18.Location = new System.Drawing.Point(157, 191);
            this.radioButton18.Name = "radioButton18";
            this.radioButton18.Size = new System.Drawing.Size(46, 16);
            this.radioButton18.TabIndex = 0;
            this.radioButton18.Text = "M17";
            this.radioButton18.UseVisualStyleBackColor = true;
            this.radioButton18.CheckedChanged += new System.EventHandler(this.radioButton18_CheckedChanged);
            // 
            // radioButton13
            // 
            this.radioButton13.AutoSize = true;
            this.radioButton13.Location = new System.Drawing.Point(157, 81);
            this.radioButton13.Name = "radioButton13";
            this.radioButton13.Size = new System.Drawing.Size(46, 16);
            this.radioButton13.TabIndex = 0;
            this.radioButton13.Text = "M12";
            this.radioButton13.UseVisualStyleBackColor = true;
            this.radioButton13.CheckedChanged += new System.EventHandler(this.radioButton13_CheckedChanged);
            // 
            // radioButton8
            // 
            this.radioButton8.AutoSize = true;
            this.radioButton8.Location = new System.Drawing.Point(29, 191);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(40, 16);
            this.radioButton8.TabIndex = 0;
            this.radioButton8.Text = "M7";
            this.radioButton8.UseVisualStyleBackColor = true;
            this.radioButton8.CheckedChanged += new System.EventHandler(this.radioButton8_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(29, 103);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(40, 16);
            this.radioButton4.TabIndex = 0;
            this.radioButton4.Text = "M3";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton17
            // 
            this.radioButton17.AutoSize = true;
            this.radioButton17.Location = new System.Drawing.Point(157, 169);
            this.radioButton17.Name = "radioButton17";
            this.radioButton17.Size = new System.Drawing.Size(46, 16);
            this.radioButton17.TabIndex = 0;
            this.radioButton17.Text = "M16";
            this.radioButton17.UseVisualStyleBackColor = true;
            this.radioButton17.CheckedChanged += new System.EventHandler(this.radioButton17_CheckedChanged);
            // 
            // radioButton14
            // 
            this.radioButton14.AutoSize = true;
            this.radioButton14.Location = new System.Drawing.Point(157, 103);
            this.radioButton14.Name = "radioButton14";
            this.radioButton14.Size = new System.Drawing.Size(46, 16);
            this.radioButton14.TabIndex = 0;
            this.radioButton14.Text = "M13";
            this.radioButton14.UseVisualStyleBackColor = true;
            this.radioButton14.CheckedChanged += new System.EventHandler(this.radioButton14_CheckedChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(29, 169);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(40, 16);
            this.radioButton7.TabIndex = 0;
            this.radioButton7.Text = "M6";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton7_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(29, 125);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(40, 16);
            this.radioButton5.TabIndex = 0;
            this.radioButton5.Text = "M4";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // radioButton16
            // 
            this.radioButton16.AutoSize = true;
            this.radioButton16.Location = new System.Drawing.Point(157, 147);
            this.radioButton16.Name = "radioButton16";
            this.radioButton16.Size = new System.Drawing.Size(46, 16);
            this.radioButton16.TabIndex = 0;
            this.radioButton16.Text = "M15";
            this.radioButton16.UseVisualStyleBackColor = true;
            this.radioButton16.CheckedChanged += new System.EventHandler(this.radioButton16_CheckedChanged);
            // 
            // radioButton15
            // 
            this.radioButton15.AutoSize = true;
            this.radioButton15.Location = new System.Drawing.Point(157, 125);
            this.radioButton15.Name = "radioButton15";
            this.radioButton15.Size = new System.Drawing.Size(46, 16);
            this.radioButton15.TabIndex = 0;
            this.radioButton15.Text = "M14";
            this.radioButton15.UseVisualStyleBackColor = true;
            this.radioButton15.CheckedChanged += new System.EventHandler(this.radioButton15_CheckedChanged);
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Location = new System.Drawing.Point(29, 147);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(40, 16);
            this.radioButton6.TabIndex = 0;
            this.radioButton6.Text = "M5";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton6_CheckedChanged);
            // 
            // buttonLogClear
            // 
            this.buttonLogClear.Location = new System.Drawing.Point(470, 722);
            this.buttonLogClear.Name = "buttonLogClear";
            this.buttonLogClear.Size = new System.Drawing.Size(23, 23);
            this.buttonLogClear.TabIndex = 4;
            this.buttonLogClear.Text = "C";
            this.buttonLogClear.UseVisualStyleBackColor = true;
            this.buttonLogClear.Click += new System.EventHandler(this.buttonLogClear_Click);
            // 
            // buttonLogClear1
            // 
            this.buttonLogClear1.Location = new System.Drawing.Point(994, 724);
            this.buttonLogClear1.Name = "buttonLogClear1";
            this.buttonLogClear1.Size = new System.Drawing.Size(23, 23);
            this.buttonLogClear1.TabIndex = 4;
            this.buttonLogClear1.Text = "C";
            this.buttonLogClear1.UseVisualStyleBackColor = true;
            this.buttonLogClear1.Click += new System.EventHandler(this.buttonLogClear1_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(297, 21);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(52, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "Default";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // textBoxT2
            // 
            this.textBoxT2.Location = new System.Drawing.Point(382, 25);
            this.textBoxT2.Name = "textBoxT2";
            this.textBoxT2.Size = new System.Drawing.Size(71, 21);
            this.textBoxT2.TabIndex = 6;
            this.textBoxT2.Text = "0";
            this.textBoxT2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.buttonLogClear1);
            this.Controls.Add(this.buttonLogClear);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.richTextBoxCalLog);
            this.Controls.Add(this.richTextBoxLog);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "CO2_CAL_JIG_V3.1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.groupBox22.ResumeLayout(false);
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox0.ResumeLayout(false);
            this.groupBox0.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPpm2)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartPpm)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBoxInit.ResumeLayout(false);
            this.groupBoxInit.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion
		private System.Windows.Forms.RichTextBox richTextBoxLog;
		private System.Windows.Forms.RichTextBox richTextBoxCalLog;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textBoxRh;
		private System.Windows.Forms.TextBox textBoxAmb;
		private System.Windows.Forms.TextBox textBoxRef;
		private System.IO.Ports.SerialPort serialPort1;
		private System.Windows.Forms.Button buttonMonEnable;
		private System.Windows.Forms.Button buttonRead;
		private System.Windows.Forms.Button buttonInit;
		private System.Windows.Forms.TabPage tabPage5;
		private System.Windows.Forms.TabPage tabPage4;
		private System.Windows.Forms.CheckBox checkBoxTps19;
		private System.Windows.Forms.CheckBox checkBoxTps18;
		private System.Windows.Forms.CheckBox checkBoxTps17;
		private System.Windows.Forms.CheckBox checkBoxTps16;
		private System.Windows.Forms.CheckBox checkBoxTps15;
		private System.Windows.Forms.CheckBox checkBoxTps14;
		private System.Windows.Forms.CheckBox checkBoxTps13;
		private System.Windows.Forms.CheckBox checkBoxTps12;
		private System.Windows.Forms.CheckBox checkBoxTps11;
		private System.Windows.Forms.CheckBox checkBoxRef2;
		private System.Windows.Forms.CheckBox checkBoxTps10;
		private System.Windows.Forms.Button buttonChartClear1;
		private System.Windows.Forms.DataVisualization.Charting.Chart chartPpm2;
		private System.Windows.Forms.TabPage tabPage3;
		private System.Windows.Forms.CheckBox checkBoxTps9;
		private System.Windows.Forms.CheckBox checkBoxTps8;
		private System.Windows.Forms.CheckBox checkBoxTps7;
		private System.Windows.Forms.CheckBox checkBoxTps6;
		private System.Windows.Forms.CheckBox checkBoxTps5;
		private System.Windows.Forms.CheckBox checkBoxTps4;
		private System.Windows.Forms.CheckBox checkBoxTps3;
		private System.Windows.Forms.CheckBox checkBoxTps2;
		private System.Windows.Forms.CheckBox checkBoxTps1;
		private System.Windows.Forms.CheckBox checkBoxRef;
		private System.Windows.Forms.CheckBox checkBoxTps0;
		private System.Windows.Forms.Button buttonChartClear0;
		private System.Windows.Forms.DataVisualization.Charting.Chart chartPpm;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.CheckBox checkBoxArrayPcb1;
		private System.Windows.Forms.CheckBox checkBoxArrayPcb0;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button buttonCom;
		private System.Windows.Forms.Button buttonComClose;
		private System.Windows.Forms.ComboBox comboBoxCom;
		private System.Windows.Forms.Button buttonComOpen;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Button buttonI2cGen;
		private System.Windows.Forms.Button buttonI2cGet;
		private System.Windows.Forms.Button buttonI2cSet;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox textBoxI2cAddr;
		private System.Windows.Forms.Label labelI2cAddr;
		private System.Windows.Forms.Label label70;
		private System.Windows.Forms.TextBox textBoxI2cAddr0;
		private System.Windows.Forms.TextBox textBoxI2cAddr19;
		private System.Windows.Forms.Label label69;
		private System.Windows.Forms.Label label50;
		private System.Windows.Forms.TextBox textBoxI2cAddr9;
		private System.Windows.Forms.Label label68;
		private System.Windows.Forms.Label label51;
		private System.Windows.Forms.TextBox textBoxI2cAddr10;
		private System.Windows.Forms.TextBox textBoxI2cAddr18;
		private System.Windows.Forms.Label label67;
		private System.Windows.Forms.Label label52;
		private System.Windows.Forms.TextBox textBoxI2cAddr1;
		private System.Windows.Forms.TextBox textBoxI2cAddr8;
		private System.Windows.Forms.Label label66;
		private System.Windows.Forms.Label label53;
		private System.Windows.Forms.TextBox textBoxI2cAddr11;
		private System.Windows.Forms.TextBox textBoxI2cAddr17;
		private System.Windows.Forms.Label label65;
		private System.Windows.Forms.Label label54;
		private System.Windows.Forms.TextBox textBoxI2cAddr2;
		private System.Windows.Forms.TextBox textBoxI2cAddr7;
		private System.Windows.Forms.Label label64;
		private System.Windows.Forms.Label label55;
		private System.Windows.Forms.TextBox textBoxI2cAddr12;
		private System.Windows.Forms.TextBox textBoxI2cAddr16;
		private System.Windows.Forms.Label label63;
		private System.Windows.Forms.Label label56;
		private System.Windows.Forms.TextBox textBoxI2cAddr3;
		private System.Windows.Forms.TextBox textBoxI2cAddr6;
		private System.Windows.Forms.Label label62;
		private System.Windows.Forms.Label label57;
		private System.Windows.Forms.TextBox textBoxI2cAddr13;
		private System.Windows.Forms.TextBox textBoxI2cAddr15;
		private System.Windows.Forms.Label label61;
		private System.Windows.Forms.Label label58;
		private System.Windows.Forms.TextBox textBoxI2cAddr4;
		private System.Windows.Forms.TextBox textBoxI2cAddr5;
		private System.Windows.Forms.Label label60;
		private System.Windows.Forms.Label label59;
		private System.Windows.Forms.TextBox textBoxI2cAddr14;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Button buttonAlarmGen;
		private System.Windows.Forms.Button buttonAlarmGet;
		private System.Windows.Forms.Button buttonAlarmSet;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox textBoxAlarmIn;
		private System.Windows.Forms.Label label29;
		private System.Windows.Forms.Label label49;
		private System.Windows.Forms.TextBox textBoxAlarm0;
		private System.Windows.Forms.Label label48;
		private System.Windows.Forms.TextBox textBoxAlarm10;
		private System.Windows.Forms.Label label47;
		private System.Windows.Forms.TextBox textBoxAlarm1;
		private System.Windows.Forms.Label label46;
		private System.Windows.Forms.TextBox textBoxAlarm11;
		private System.Windows.Forms.Label label45;
		private System.Windows.Forms.TextBox textBoxAlarm2;
		private System.Windows.Forms.Label label44;
		private System.Windows.Forms.TextBox textBoxAlarm12;
		private System.Windows.Forms.Label label43;
		private System.Windows.Forms.TextBox textBoxAlarm3;
		private System.Windows.Forms.Label label42;
		private System.Windows.Forms.TextBox textBoxAlarm13;
		private System.Windows.Forms.Label label41;
		private System.Windows.Forms.TextBox textBoxAlarm4;
		private System.Windows.Forms.Label label40;
		private System.Windows.Forms.TextBox textBoxAlarm14;
		private System.Windows.Forms.Label label39;
		private System.Windows.Forms.TextBox textBoxAlarm5;
		private System.Windows.Forms.Label label38;
		private System.Windows.Forms.TextBox textBoxAlarm15;
		private System.Windows.Forms.Label label37;
		private System.Windows.Forms.TextBox textBoxAlarm6;
		private System.Windows.Forms.Label label36;
		private System.Windows.Forms.TextBox textBoxAlarm16;
		private System.Windows.Forms.Label label35;
		private System.Windows.Forms.TextBox textBoxAlarm7;
		private System.Windows.Forms.Label label34;
		private System.Windows.Forms.TextBox textBoxAlarm17;
		private System.Windows.Forms.Label label33;
		private System.Windows.Forms.TextBox textBoxAlarm8;
		private System.Windows.Forms.Label label32;
		private System.Windows.Forms.TextBox textBoxAlarm18;
		private System.Windows.Forms.Label label31;
		private System.Windows.Forms.TextBox textBoxAlarm9;
		private System.Windows.Forms.Label label30;
		private System.Windows.Forms.TextBox textBoxAlarm19;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.Button buttonSerialNumGet;
		private System.Windows.Forms.Button buttonSerialNumSet;
		private System.Windows.Forms.Button buttonSerialNumGenerate;
		private System.Windows.Forms.TextBox textBoxSerialNumIn;
		private System.Windows.Forms.Label label27;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label26;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label25;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label24;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label23;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label22;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label21;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label20;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label19;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label18;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox textBoxSerialNum19;
		private System.Windows.Forms.TextBox textBoxSerialNum9;
		private System.Windows.Forms.TextBox textBoxSerialNum18;
		private System.Windows.Forms.TextBox textBoxSerialNum8;
		private System.Windows.Forms.TextBox textBoxSerialNum17;
		private System.Windows.Forms.TextBox textBoxSerialNum7;
		private System.Windows.Forms.TextBox textBoxSerialNum16;
		private System.Windows.Forms.TextBox textBoxSerialNum6;
		private System.Windows.Forms.TextBox textBoxSerialNum15;
		private System.Windows.Forms.TextBox textBoxSerialNum5;
		private System.Windows.Forms.TextBox textBoxSerialNum14;
		private System.Windows.Forms.TextBox textBoxSerialNum4;
		private System.Windows.Forms.TextBox textBoxSerialNum13;
		private System.Windows.Forms.TextBox textBoxSerialNum3;
		private System.Windows.Forms.TextBox textBoxSerialNum12;
		private System.Windows.Forms.TextBox textBoxSerialNum2;
		private System.Windows.Forms.TextBox textBoxSerialNum11;
		private System.Windows.Forms.TextBox textBoxSerialNum1;
		private System.Windows.Forms.TextBox textBoxSerialNum10;
		private System.Windows.Forms.TextBox textBoxSerialNum0;
		private System.Windows.Forms.Label label28;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.Button buttonLogClear;
		private System.Windows.Forms.Button buttonLogClear1;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Label label72;
		private System.Windows.Forms.Label label76;
		private System.Windows.Forms.Label label75;
		private System.Windows.Forms.Label label74;
		private System.Windows.Forms.Label label73;
		private System.Windows.Forms.Label label71;
		private System.Windows.Forms.TextBox textBoxD3;
		private System.Windows.Forms.TextBox textBoxD2;
		private System.Windows.Forms.TextBox textBoxD1;
		private System.Windows.Forms.TextBox textBoxD0;
		private System.Windows.Forms.TextBox textBoxCh;
		private System.Windows.Forms.TextBox textBoxCmd;
		private System.Windows.Forms.Button buttonCdcSend;
		private System.Windows.Forms.CheckBox checkBoxSelectAll1;
		private System.Windows.Forms.CheckBox checkBoxSelectAll;
		private System.Windows.Forms.Button buttonVerGet;
		private System.Windows.Forms.Panel panel7;
		private System.Windows.Forms.CheckBox checkBoxLog;
		private System.Windows.Forms.CheckBox checkBoxErr;
		private System.Windows.Forms.CheckBox checkBoxInfo;
		private System.Windows.Forms.Label label77;
		private System.Windows.Forms.Panel panel8;
		private System.Windows.Forms.CheckBox checkBoxLogErr;
		private System.Windows.Forms.CheckBox checkBoxLogMsg;
		private System.Windows.Forms.CheckBox checkBoxLogLog;
		private System.Windows.Forms.Label label78;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton20;
		private System.Windows.Forms.RadioButton radioButton11;
		private System.Windows.Forms.RadioButton radioButton10;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.RadioButton radioButton19;
		private System.Windows.Forms.RadioButton radioButton12;
		private System.Windows.Forms.RadioButton radioButton9;
		private System.Windows.Forms.RadioButton radioButton3;
		private System.Windows.Forms.RadioButton radioButton18;
		private System.Windows.Forms.RadioButton radioButton13;
		private System.Windows.Forms.RadioButton radioButton8;
		private System.Windows.Forms.RadioButton radioButton4;
		private System.Windows.Forms.RadioButton radioButton17;
		private System.Windows.Forms.RadioButton radioButton14;
		private System.Windows.Forms.RadioButton radioButton7;
		private System.Windows.Forms.RadioButton radioButton5;
		private System.Windows.Forms.RadioButton radioButton16;
		private System.Windows.Forms.RadioButton radioButton15;
		private System.Windows.Forms.RadioButton radioButton6;
		private System.Windows.Forms.Label labelM;
		private System.Windows.Forms.CheckBox checkBoxPic;
		private System.Windows.Forms.Panel panel9;
		private System.Windows.Forms.GroupBox groupBox22;
		private System.Windows.Forms.GroupBox groupBox11;
		private System.Windows.Forms.TextBox textBoxSc10;
		private System.Windows.Forms.Button buttonGain10;
		private System.Windows.Forms.TextBox textBoxSr10;
		private System.Windows.Forms.Button buttonTs10;
		private System.Windows.Forms.TextBox textBoxGain10;
		private System.Windows.Forms.Button buttonSr10;
		private System.Windows.Forms.TextBox textBoxTs10;
		private System.Windows.Forms.Button buttonSc10;
		private System.Windows.Forms.GroupBox groupBox16;
		private System.Windows.Forms.TextBox textBoxSc15;
		private System.Windows.Forms.Button buttonGain15;
		private System.Windows.Forms.TextBox textBoxSr15;
		private System.Windows.Forms.Button buttonTs15;
		private System.Windows.Forms.TextBox textBoxGain15;
		private System.Windows.Forms.Button buttonSr15;
		private System.Windows.Forms.TextBox textBoxTs15;
		private System.Windows.Forms.Button buttonSc15;
		private System.Windows.Forms.GroupBox groupBox20;
		private System.Windows.Forms.TextBox textBoxSc19;
		private System.Windows.Forms.Button buttonGain19;
		private System.Windows.Forms.TextBox textBoxSr19;
		private System.Windows.Forms.Button buttonTs19;
		private System.Windows.Forms.TextBox textBoxGain19;
		private System.Windows.Forms.Button buttonSr19;
		private System.Windows.Forms.TextBox textBoxTs19;
		private System.Windows.Forms.Button buttonSc19;
		private System.Windows.Forms.GroupBox groupBox12;
		private System.Windows.Forms.TextBox textBoxSc11;
		private System.Windows.Forms.Button buttonGain11;
		private System.Windows.Forms.TextBox textBoxSr11;
		private System.Windows.Forms.Button buttonTs11;
		private System.Windows.Forms.TextBox textBoxGain11;
		private System.Windows.Forms.Button buttonSr11;
		private System.Windows.Forms.TextBox textBoxTs11;
		private System.Windows.Forms.Button buttonSc11;
		private System.Windows.Forms.GroupBox groupBox15;
		private System.Windows.Forms.TextBox textBoxSc14;
		private System.Windows.Forms.Button buttonGain14;
		private System.Windows.Forms.TextBox textBoxSr14;
		private System.Windows.Forms.Button buttonTs14;
		private System.Windows.Forms.TextBox textBoxGain14;
		private System.Windows.Forms.Button buttonSr14;
		private System.Windows.Forms.TextBox textBoxTs14;
		private System.Windows.Forms.Button buttonSc14;
		private System.Windows.Forms.GroupBox groupBox17;
		private System.Windows.Forms.TextBox textBoxSc16;
		private System.Windows.Forms.Button buttonGain16;
		private System.Windows.Forms.TextBox textBoxSr16;
		private System.Windows.Forms.Button buttonTs16;
		private System.Windows.Forms.TextBox textBoxGain16;
		private System.Windows.Forms.Button buttonSr16;
		private System.Windows.Forms.TextBox textBoxTs16;
		private System.Windows.Forms.Button buttonSc16;
		private System.Windows.Forms.GroupBox groupBox19;
		private System.Windows.Forms.TextBox textBoxSc18;
		private System.Windows.Forms.Button buttonGain18;
		private System.Windows.Forms.TextBox textBoxSr18;
		private System.Windows.Forms.Button buttonTs18;
		private System.Windows.Forms.TextBox textBoxGain18;
		private System.Windows.Forms.Button buttonSr18;
		private System.Windows.Forms.TextBox textBoxTs18;
		private System.Windows.Forms.Button buttonSc18;
		private System.Windows.Forms.GroupBox groupBox13;
		private System.Windows.Forms.TextBox textBoxSc12;
		private System.Windows.Forms.Button buttonGain12;
		private System.Windows.Forms.TextBox textBoxSr12;
		private System.Windows.Forms.Button buttonTs12;
		private System.Windows.Forms.TextBox textBoxGain12;
		private System.Windows.Forms.Button buttonSr12;
		private System.Windows.Forms.TextBox textBoxTs12;
		private System.Windows.Forms.Button buttonSc12;
		private System.Windows.Forms.GroupBox groupBox14;
		private System.Windows.Forms.TextBox textBoxSc13;
		private System.Windows.Forms.Button buttonGain13;
		private System.Windows.Forms.TextBox textBoxSr13;
		private System.Windows.Forms.Button buttonTs13;
		private System.Windows.Forms.TextBox textBoxGain13;
		private System.Windows.Forms.Button buttonSr13;
		private System.Windows.Forms.TextBox textBoxTs13;
		private System.Windows.Forms.Button buttonSc13;
		private System.Windows.Forms.GroupBox groupBox18;
		private System.Windows.Forms.TextBox textBoxSc17;
		private System.Windows.Forms.Button buttonGain17;
		private System.Windows.Forms.TextBox textBoxSr17;
		private System.Windows.Forms.Button buttonTs17;
		private System.Windows.Forms.TextBox textBoxGain17;
		private System.Windows.Forms.Button buttonSr17;
		private System.Windows.Forms.TextBox textBoxTs17;
		private System.Windows.Forms.Button buttonSc17;
		private System.Windows.Forms.GroupBox groupBox21;
		private System.Windows.Forms.GroupBox groupBox6;
		private System.Windows.Forms.TextBox textBoxSc5;
		private System.Windows.Forms.Button buttonGain5;
		private System.Windows.Forms.TextBox textBoxSr5;
		private System.Windows.Forms.Button buttonTs5;
		private System.Windows.Forms.TextBox textBoxGain5;
		private System.Windows.Forms.Button buttonSr5;
		private System.Windows.Forms.TextBox textBoxTs5;
		private System.Windows.Forms.Button buttonSc5;
		private System.Windows.Forms.GroupBox groupBox0;
		private System.Windows.Forms.TextBox textBoxSc0;
		private System.Windows.Forms.Button buttonGain0;
		private System.Windows.Forms.TextBox textBoxSr0;
		private System.Windows.Forms.Button buttonTs0;
		private System.Windows.Forms.TextBox textBoxGain0;
		private System.Windows.Forms.Button buttonSr0;
		private System.Windows.Forms.TextBox textBoxTs0;
		private System.Windows.Forms.Button buttonSc0;
		private System.Windows.Forms.GroupBox groupBox7;
		private System.Windows.Forms.TextBox textBoxSc6;
		private System.Windows.Forms.Button buttonGain6;
		private System.Windows.Forms.TextBox textBoxSr6;
		private System.Windows.Forms.Button buttonTs6;
		private System.Windows.Forms.TextBox textBoxGain6;
		private System.Windows.Forms.Button buttonSr6;
		private System.Windows.Forms.TextBox textBoxTs6;
		private System.Windows.Forms.Button buttonSc6;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox textBoxSc1;
		private System.Windows.Forms.Button buttonGain1;
		private System.Windows.Forms.TextBox textBoxSr1;
		private System.Windows.Forms.Button buttonTs1;
		private System.Windows.Forms.TextBox textBoxGain1;
		private System.Windows.Forms.Button buttonSr1;
		private System.Windows.Forms.TextBox textBoxTs1;
		private System.Windows.Forms.Button buttonSc1;
		private System.Windows.Forms.GroupBox groupBox8;
		private System.Windows.Forms.TextBox textBoxSc7;
		private System.Windows.Forms.Button buttonGain7;
		private System.Windows.Forms.TextBox textBoxSr7;
		private System.Windows.Forms.Button buttonTs7;
		private System.Windows.Forms.TextBox textBoxGain7;
		private System.Windows.Forms.Button buttonSr7;
		private System.Windows.Forms.TextBox textBoxTs7;
		private System.Windows.Forms.Button buttonSc7;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.TextBox textBoxSc2;
		private System.Windows.Forms.Button buttonGain2;
		private System.Windows.Forms.TextBox textBoxSr2;
		private System.Windows.Forms.Button buttonTs2;
		private System.Windows.Forms.TextBox textBoxGain2;
		private System.Windows.Forms.Button buttonSr2;
		private System.Windows.Forms.TextBox textBoxTs2;
		private System.Windows.Forms.Button buttonSc2;
		private System.Windows.Forms.GroupBox groupBox9;
		private System.Windows.Forms.TextBox textBoxSc8;
		private System.Windows.Forms.Button buttonGain8;
		private System.Windows.Forms.TextBox textBoxSr8;
		private System.Windows.Forms.Button buttonTs8;
		private System.Windows.Forms.TextBox textBoxGain8;
		private System.Windows.Forms.Button buttonSr8;
		private System.Windows.Forms.TextBox textBoxTs8;
		private System.Windows.Forms.Button buttonSc8;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.TextBox textBoxSc3;
		private System.Windows.Forms.Button buttonGain3;
		private System.Windows.Forms.TextBox textBoxSr3;
		private System.Windows.Forms.Button buttonTs3;
		private System.Windows.Forms.TextBox textBoxGain3;
		private System.Windows.Forms.Button buttonSr3;
		private System.Windows.Forms.TextBox textBoxTs3;
		private System.Windows.Forms.Button buttonSc3;
		private System.Windows.Forms.GroupBox groupBox10;
		private System.Windows.Forms.TextBox textBoxSc9;
		private System.Windows.Forms.Button buttonGain9;
		private System.Windows.Forms.TextBox textBoxSr9;
		private System.Windows.Forms.Button buttonTs9;
		private System.Windows.Forms.TextBox textBoxGain9;
		private System.Windows.Forms.Button buttonSr9;
		private System.Windows.Forms.TextBox textBoxTs9;
		private System.Windows.Forms.Button buttonSc9;
		private System.Windows.Forms.GroupBox groupBox5;
		private System.Windows.Forms.TextBox textBoxSc4;
		private System.Windows.Forms.Button buttonGain4;
		private System.Windows.Forms.TextBox textBoxSr4;
		private System.Windows.Forms.Button buttonTs4;
		private System.Windows.Forms.TextBox textBoxGain4;
		private System.Windows.Forms.Button buttonSr4;
		private System.Windows.Forms.TextBox textBoxTs4;
		private System.Windows.Forms.Button buttonSc4;
		private System.Windows.Forms.TabPage tabPage6;
		private System.Windows.Forms.Button buttonWriteAll;
		private System.Windows.Forms.Button buttonDefault;
		private System.Windows.Forms.Button buttonGainAdj;
		private System.Windows.Forms.Button buttonTsAdj;
		private System.Windows.Forms.Button buttonPpmAdj;
		private System.Windows.Forms.Button buttonDcDrAdj;
		private System.Windows.Forms.Button buttonToDef;
		private System.Windows.Forms.GroupBox groupBox23;
		private System.Windows.Forms.TextBox textBoxModuleNum;
		private System.Windows.Forms.Label label79;
		private System.Windows.Forms.CheckBox checkBoxM19;
		private System.Windows.Forms.CheckBox checkBoxM9;
		private System.Windows.Forms.CheckBox checkBoxM18;
		private System.Windows.Forms.CheckBox checkBoxM8;
		private System.Windows.Forms.CheckBox checkBoxM17;
		private System.Windows.Forms.CheckBox checkBoxM7;
		private System.Windows.Forms.CheckBox checkBoxM16;
		private System.Windows.Forms.CheckBox checkBoxM6;
		private System.Windows.Forms.CheckBox checkBoxM15;
		private System.Windows.Forms.CheckBox checkBoxM5;
		private System.Windows.Forms.CheckBox checkBoxM14;
		private System.Windows.Forms.CheckBox checkBoxM4;
		private System.Windows.Forms.CheckBox checkBoxM13;
		private System.Windows.Forms.CheckBox checkBoxM3;
		private System.Windows.Forms.CheckBox checkBoxM12;
		private System.Windows.Forms.CheckBox checkBoxM2;
		private System.Windows.Forms.CheckBox checkBoxM11;
		private System.Windows.Forms.CheckBox checkBoxM1;
		private System.Windows.Forms.CheckBox checkBoxM10;
		private System.Windows.Forms.CheckBox checkBoxM0;
		private System.Windows.Forms.Button buttonMfc;
		private System.Windows.Forms.GroupBox groupBoxInit;
		private System.Windows.Forms.RadioButton radioButtonI2cIf;
		private System.Windows.Forms.RadioButton radioButtonUartIf;
		private System.Windows.Forms.CheckBox checkBoxReadStatus;
		private System.Windows.Forms.RadioButton radioButtonRsv2;
		private System.Windows.Forms.RadioButton radioButtonRsv1;
		private System.Windows.Forms.CheckBox checkBoxRsv2;
		private System.Windows.Forms.CheckBox checkBoxRsv1;
		private System.Windows.Forms.Panel panel11;
		private System.Windows.Forms.Panel panel10;
		private System.Windows.Forms.Button buttonTpsSingle;
		private System.Windows.Forms.ComboBox comboBoxTpsSingle;
		private System.Windows.Forms.Button buttonMuxEn;
		private System.Windows.Forms.Button buttonMuxSel;
		private System.Windows.Forms.Button buttonPower;
		private System.Windows.Forms.CheckBox checkBoxOnOff;
		private System.Windows.Forms.Button buttonAdRead;
		private System.Windows.Forms.Button buttonMuxIo;
		private System.Windows.Forms.Button buttonAdAll;
		private System.Windows.Forms.Button buttonAmb;
		private System.Windows.Forms.Button buttonRef;
		private System.Windows.Forms.Button buttonFw;
		private System.Windows.Forms.Button buttonCalStop;
		private System.Windows.Forms.TextBox textBoxDiff19;
		private System.Windows.Forms.TextBox textBoxDiff18;
		private System.Windows.Forms.TextBox textBoxDiff17;
		private System.Windows.Forms.TextBox textBoxDiff16;
		private System.Windows.Forms.TextBox textBoxDiff15;
		private System.Windows.Forms.TextBox textBoxDiff14;
		private System.Windows.Forms.TextBox textBoxDiff13;
		private System.Windows.Forms.TextBox textBoxDiff12;
		private System.Windows.Forms.TextBox textBoxDiff11;
		private System.Windows.Forms.TextBox textBoxDiff10;
		private System.Windows.Forms.TextBox textBoxDiffRef2;
		private System.Windows.Forms.TextBox textBoxDiff9;
		private System.Windows.Forms.TextBox textBoxDiff8;
		private System.Windows.Forms.TextBox textBoxDiff7;
		private System.Windows.Forms.TextBox textBoxDiff6;
		private System.Windows.Forms.TextBox textBoxDiff5;
		private System.Windows.Forms.TextBox textBoxDiff4;
		private System.Windows.Forms.TextBox textBoxDiff3;
		private System.Windows.Forms.TextBox textBoxDiff2;
		private System.Windows.Forms.TextBox textBoxDiff1;
		private System.Windows.Forms.TextBox textBoxDiff0;
		private System.Windows.Forms.TextBox textBoxDiffRef;
		private System.Windows.Forms.Button buttonBadModule;
		private System.Windows.Forms.Panel panel13;
		private System.Windows.Forms.Button buttonCalibrationStart;
		private System.Windows.Forms.Panel panel12;
		private System.Windows.Forms.RichTextBox richTextBoxSockLog;
		private System.Windows.Forms.Button button2000ppm;
		private System.Windows.Forms.Button button4500ppm;
		private System.Windows.Forms.Button buttonSocketOn;
		private System.Windows.Forms.Button button600ppm;
		private System.Windows.Forms.Button buttonMfxStop;
		private System.Windows.Forms.Button buttonPurge;
		private System.Windows.Forms.TextBox textBoxTimer;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button buttonMfcOff;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.TextBox textBoxT2;
    }
}

