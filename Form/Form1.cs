﻿#define MFC_MON_ENABLE
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		FormDataView FrmDv = null;
		FormMfc FrmMfc = null;
		Form_MFCMini FrmMfcMini = null;
		UInt32 tmrTick = 0;
        UInt32 tmr2Tick = 0;

		public Form1()
		{
			InitializeComponent();

			LogInit();
			ChartInit();

			FrmDv = new FormDataView(this);
			FrmDv.Show();
			//ConfigFileOpen();

			FormClosing += new FormClosingEventHandler(Form1_FormClosing);
		}

		private void Form1_FormClosing(Object sender, FormClosingEventArgs e)
		{
#if false
			UartClose();
			CloseLogFiles();
			ConfigFileSave(cdcPort);
			FrmDv.Close();
#endif
		}
		
		private void buttonCom_Click(object sender, EventArgs e)
		{
			UartCom();
		}

		private void buttonComOpen_Click(object sender, EventArgs e)
		{
			UartOpen();
		}

		private void buttonComClose_Click(object sender, EventArgs e)
		{
			UartClose();
		}

		private void buttonSerialNumSet_Click(object sender, EventArgs e)
		{
			SerialNumberSet();
		}

		private void buttonSerialNumGenerate_Click(object sender, EventArgs e)
		{
			SerialNumberGenerate();
		}

		private void buttonSerialNumGet_Click(object sender, EventArgs e)
		{
			SerialNumberGet();
		}

		private void buttonAlarmGen_Click(object sender, EventArgs e)
		{
			AlarmGenerate();
		}

		private void buttonAlarmSet_Click(object sender, EventArgs e)
		{
			AlarmSet();
		}

		private void buttonAlarmGet_Click(object sender, EventArgs e)
		{
			AlarmGet();
		}

		private void buttonI2cGen_Click(object sender, EventArgs e)
		{
			I2cAddrGenerate();
		}

		private void buttonI2cSet_Click(object sender, EventArgs e)
		{
			I2cAddrSet();
		}

		private void buttonI2cGet_Click(object sender, EventArgs e)
		{
			I2cAddrGet();
		}

		private void buttonChartClear1_Click(object sender, EventArgs e)
		{
			Chart2Clear();
		}

		private void buttonChartClear0_Click(object sender, EventArgs e)
		{
			ChartClear();
		}

		private void buttonManualCal_Click(object sender, EventArgs e)
		{
			
		}

		private void buttonInit_Click(object sender, EventArgs e)
		{
			CdcInit();
		}

		private void buttonMonEnable_Click(object sender, EventArgs e)
		{
			CdcMon();
		}

		private void buttonRead_Click(object sender, EventArgs e)
		{
			CdcRead();
		}

		private void buttonLogClear_Click(object sender, EventArgs e)
		{
			LOG_Clear();
		}

		private void buttonLogClear1_Click(object sender, EventArgs e)
		{
			CAL_LOG_Clear();
		}

		private void buttonCdcSend_Click(object sender, EventArgs e)
		{
			CdcSendCmd(	Convert.ToByte(textBoxCmd.Text), Convert.ToByte(textBoxCh.Text), Convert.ToByte(textBoxD0.Text),
						Convert.ToByte(textBoxD1.Text), Convert.ToByte(textBoxD2.Text), Convert.ToByte(textBoxD3.Text));
		}

		private void checkBoxSelectAll_CheckedChanged(object sender, EventArgs e)
		{
			if(checkBoxSelectAll.Checked == true)
			{
				checkBoxRef.Checked = true;
				checkBoxTps0.Checked = true;
				checkBoxTps1.Checked = true;
				checkBoxTps2.Checked = true;
				checkBoxTps3.Checked = true;
				checkBoxTps4.Checked = true;
				checkBoxTps5.Checked = true;
				checkBoxTps6.Checked = true;
				checkBoxTps7.Checked = true;
				checkBoxTps8.Checked = true;
				checkBoxTps9.Checked = true;
			}
			else
			{
				checkBoxRef.Checked = false;
				checkBoxTps0.Checked = false;
				checkBoxTps1.Checked = false;
				checkBoxTps2.Checked = false;
				checkBoxTps3.Checked = false;
				checkBoxTps4.Checked = false;
				checkBoxTps5.Checked = false;
				checkBoxTps6.Checked = false;
				checkBoxTps7.Checked = false;
				checkBoxTps8.Checked = false;
				checkBoxTps9.Checked = false;
			}
		}

		private void checkBoxSelectAll1_CheckedChanged(object sender, EventArgs e)
		{
			if (checkBoxSelectAll1.Checked == true)
			{
				checkBoxRef2.Checked = true;
				checkBoxTps10.Checked = true;
				checkBoxTps11.Checked = true;
				checkBoxTps12.Checked = true;
				checkBoxTps13.Checked = true;
				checkBoxTps14.Checked = true;
				checkBoxTps15.Checked = true;
				checkBoxTps16.Checked = true;
				checkBoxTps17.Checked = true;
				checkBoxTps18.Checked = true;
				checkBoxTps19.Checked = true;
			}
			else
			{
				checkBoxRef2.Checked = false;
				checkBoxTps10.Checked = false;
				checkBoxTps11.Checked = false;
				checkBoxTps12.Checked = false;
				checkBoxTps13.Checked = false;
				checkBoxTps14.Checked = false;
				checkBoxTps15.Checked = false;
				checkBoxTps16.Checked = false;
				checkBoxTps17.Checked = false;
				checkBoxTps18.Checked = false;
				checkBoxTps19.Checked = false;
			}
		}

		private void buttonVerGet_Click(object sender, EventArgs e)
		{
			VerGet();
		}

		private void radioButton1_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(0);
		}

		private void radioButton2_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(1);
		}

		private void radioButton3_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(2);
		}

		private void radioButton4_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(3);
		}

		private void radioButton5_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(4);
		}

		private void radioButton6_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(5);
		}

		private void radioButton7_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(6);
		}

		private void radioButton8_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(7);
		}

		private void radioButton9_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(8);
		}

		private void radioButton10_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(9);
		}

		private void radioButton11_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(10);
		}

		private void radioButton12_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(11);
		}

		private void radioButton13_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(12);
		}

		private void radioButton14_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(13);
		}

		private void radioButton15_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(14);
		}

		private void radioButton16_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(15);
		}

		private void radioButton17_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(16);
		}

		private void radioButton18_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(17);
		}

		private void radioButton19_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(18);
		}

		private void radioButton20_CheckedChanged(object sender, EventArgs e)
		{
			PicModuleSelect(19);
		}
		
		private void checkBoxPic_CheckedChanged(object sender, EventArgs e)
		{
			if (checkBoxPic.Checked == true)
				checkBoxPic.Text = "PIC Program Mode Enabled";
			else
				checkBoxPic.Text = "PIC Program Mode Disabled";
		}

		private void buttonSc0_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc0.Text), 0);
		}

		private void buttonSc1_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc1.Text), 1);
		}

		private void buttonSc2_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc2.Text), 2);
		}

		private void buttonSc3_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc3.Text), 3);
		}

		private void buttonSc4_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc4.Text), 4);
		}

		private void buttonSc5_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc5.Text), 5);
		}

		private void buttonSc6_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc6.Text), 6);
		}

		private void buttonSc7_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc7.Text), 7);
		}

		private void buttonSc8_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc8.Text), 8);
		}

		private void buttonSc9_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc9.Text), 9);
		}

		private void buttonSc10_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc10.Text), 10);
		}

		private void buttonSc11_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc11.Text), 11);
		}

		private void buttonSc12_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc12.Text), 12);
		}

		private void buttonSc13_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc13.Text), 13);
		}

		private void buttonSc14_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc14.Text), 14);
		}

		private void buttonSc15_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc15.Text), 15);
		}

		private void buttonSc16_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc16.Text), 16);
		}

		private void buttonSc17_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc17.Text), 17);
		}

		private void buttonSc18_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc18.Text), 18);
		}

		private void buttonSc19_Click(object sender, EventArgs e)
		{
			SetSc(Convert.ToUInt32(textBoxSc19.Text), 19);
		}

		private void buttonSr0_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr0.Text), 0);
		}

		private void buttonSr1_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr1.Text), 1);
		}

		private void buttonSr2_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr2.Text), 2);
		}

		private void buttonSr3_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr3.Text), 3);
		}

		private void buttonSr4_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr4.Text), 4);
		}

		private void buttonSr5_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr5.Text), 5);
		}

		private void buttonSr6_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr6.Text), 6);
		}

		private void buttonSr7_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr7.Text), 7);
		}

		private void buttonSr8_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr8.Text), 8);
		}

		private void buttonSr9_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr9.Text), 9);
		}

		private void buttonSr10_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr10.Text), 10);
		}

		private void buttonSr11_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr11.Text), 11);
		}

		private void buttonSr12_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr12.Text), 12);
		}

		private void buttonSr13_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr13.Text), 13);
		}

		private void buttonSr14_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr14.Text), 14);
		}

		private void buttonSr15_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr15.Text), 15);
		}

		private void buttonSr16_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr16.Text), 16);
		}

		private void buttonSr17_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr17.Text), 17);
		}

		private void buttonSr18_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr18.Text), 18);
		}

		private void buttonSr19_Click(object sender, EventArgs e)
		{
			SetSr(Convert.ToUInt32(textBoxSr19.Text), 19);
		}

		private void buttonTs0_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs0.Text), 0);
		}

		private void buttonTs1_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs1.Text), 1);
		}

		private void buttonTs2_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs2.Text), 2);
		}

		private void buttonTs3_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs3.Text), 3);
		}

		private void buttonTs4_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs4.Text), 4);
		}

		private void buttonTs5_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs5.Text), 5);
		}

		private void buttonTs6_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs6.Text), 6);
		}

		private void buttonTs7_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs7.Text), 7);
		}

		private void buttonTs8_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs8.Text), 8);
		}

		private void buttonTs9_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs9.Text), 9);
		}

		private void buttonTs10_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs10.Text), 10);
		}

		private void buttonTs11_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs11.Text), 11);
		}

		private void buttonTs12_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs12.Text), 12);
		}

		private void buttonTs13_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs13.Text), 13);
		}

		private void buttonTs14_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs14.Text), 14);
		}

		private void buttonTs15_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs15.Text), 15);
		}

		private void buttonTs16_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs16.Text), 16);
		}

		private void buttonTs17_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs17.Text), 17);
		}

		private void buttonTs18_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs18.Text), 18);
		}

		private void buttonTs19_Click(object sender, EventArgs e)
		{
			SetTs(Convert.ToUInt32(textBoxTs19.Text), 19);
		}

		private void buttonGain0_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain0.Text), 0);
		}

		private void buttonGain1_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain1.Text), 1);
		}

		private void buttonGain2_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain2.Text), 2);
		}

		private void buttonGain3_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain3.Text), 3);
		}

		private void buttonGain4_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain4.Text), 4);
		}

		private void buttonGain5_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain5.Text), 5);
		}

		private void buttonGain6_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain6.Text), 6);
		}

		private void buttonGain7_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain7.Text), 7);
		}

		private void buttonGain8_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain8.Text), 8);
		}

		private void buttonGain9_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain9.Text), 9);
		}

		private void buttonGain10_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain10.Text), 10);
		}

		private void buttonGain11_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain11.Text), 11);
		}

		private void buttonGain12_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain12.Text), 12);
		}

		private void buttonGain13_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain13.Text), 13);
		}

		private void buttonGain14_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain14.Text), 14);
		}

		private void buttonGain15_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain15.Text), 15);
		}

		private void buttonGain16_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain16.Text), 16);
		}

		private void buttonGain17_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain17.Text), 17);
		}

		private void buttonGain18_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain18.Text), 18);
		}

		private void buttonGain19_Click(object sender, EventArgs e)
		{
			SetGain(Convert.ToUInt32(textBoxGain19.Text), 19);
		}

		private void buttonDefault_Click(object sender, EventArgs e)
		{
			SetToDefault();
		}

		private void buttonWriteAll_Click(object sender, EventArgs e)
		{
			SetToWriteAll();
		}

		private void buttonToDef_Click(object sender, EventArgs e)
		{
			SetToDefault();
		}

		private void buttonDcDrAdj_Click(object sender, EventArgs e)
		{
			CalibrationStart(CAL_EN_DC_DR_ADJ);
		}

		private void buttonPpmAdj_Click(object sender, EventArgs e)
		{
			CalibrationStart(CAL_EN_PPM_ADJ);
		}

		private void buttonTsAdj_Click(object sender, EventArgs e)
		{
			CalibrationStart(CAL_EN_TS_ADJ);
		}

		private void buttonMfc_Click(object sender, EventArgs e)
		{
			FrmMfcMini = new Form_MFCMini(this);
			//FrmMfc.ShowDialog();
			FrmMfcMini.Show();
		}

		private void buttonTpsSingle_Click(object sender, EventArgs e)
		{
			CdcSingleRead(Convert.ToByte(comboBoxTpsSingle.SelectedItem));
		}

		private void buttonPower_Click(object sender, EventArgs e)
		{
			if(checkBoxOnOff.Checked)
				CdcSingleWrite(CDC_MSG_CMD_SET_POWER, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
			else
				CdcSingleWrite(CDC_MSG_CMD_SET_POWER, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 0);
		}

		private void buttonMuxSel_Click(object sender, EventArgs e)
		{
			if (checkBoxOnOff.Checked)
				CdcSingleWrite(CDC_MSG_CMD_SET_MUX_SEL, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
			else
				CdcSingleWrite(CDC_MSG_CMD_SET_MUX_SEL, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 0);
		}

		private void buttonMuxEn_Click(object sender, EventArgs e)
		{
			if (checkBoxOnOff.Checked)
				CdcSingleWrite(CDC_MSG_CMD_SET_MUX_EN, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
			else
				CdcSingleWrite(CDC_MSG_CMD_SET_MUX_EN, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 0);
		}

		private void buttonAdRead_Click(object sender, EventArgs e)
		{
			CdcSingleWrite(CDC_MSG_CMD_AD_READ, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
		}

		private void buttonMuxIo_Click(object sender, EventArgs e)
		{
			CdcSingleWrite(CDC_MSG_CMD_SET_MUX_CH, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
		}

		private void buttonAdAll_Click(object sender, EventArgs e)
		{
			CdcSingleWrite(CDC_MSG_CMD_AD_READ_ALL, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
		}

		private void buttonAmb_Click(object sender, EventArgs e)
		{
			CdcSingleWrite(CDC_MSG_CMD_GET_AMB_RH, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
		}

		private void buttonRef_Click(object sender, EventArgs e)
		{
			CdcSingleWrite(CDC_MSG_CMD_GET_REF, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
		}

		private void buttonFw_Click(object sender, EventArgs e)
		{
			CdcSingleWrite(CDC_MSG_CMD_GET_VER, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
		}

		private void buttonCalStop_Click(object sender, EventArgs e)
		{
			CalStop();
		}

		public void request_ref()
		{
			CdcSingleWrite(CDC_MSG_CMD_GET_REF, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
		}

		private void buttonBadModule_Click(object sender, EventArgs e)
		{
			if (checkBoxOnOff.Checked)
				CdcSingleWrite(CDC_MSG_CMD_SET_BAD_MODULE, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
			else
				CdcSingleWrite(CDC_MSG_CMD_SET_BAD_MODULE, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 0);
		}

		private void buttonGainAdj_Click(object sender, EventArgs e)
		{
			CalibrationStart(CAL_EN_GAIN_ADJ);
		}

		private void button600ppm_Click(object sender, EventArgs e)
		{
			SocketSend("SET,2000,40,0,0,10111");
#if MFC_MON_ENABLE
            monBtnStatus = false;
            CdcMon();
#else
			if (monBtnStatus == false)
			{
				monBtnStatus = true;
				CdcMon();
			}
			tmrTick = 0;
			timer1.Start();
#endif
        }

		private void button4500ppm_Click(object sender, EventArgs e)
		{
			SocketSend("SET,1000,180,0,0,10111");
#if MFC_MON_ENABLE
            monBtnStatus = false;
            CdcMon();
#else
			if (monBtnStatus == false)
			{
				monBtnStatus = true;
				CdcMon();
			}
			tmrTick = 0;
			timer1.Start();
#endif
        }

		private void button2000ppm_Click(object sender, EventArgs e)
		{
			SocketSend("SET,500,32,0,0,10111");

#if  MFC_MON_ENABLE
            monBtnStatus = false;
            CdcMon();
#else
			if (monBtnStatus == false)
			{
				monBtnStatus = true;
				CdcMon();
			}
			tmrTick = 0;
			timer1.Start();
#endif
        }

		private void buttonMfxStop_Click(object sender, EventArgs e)
		{
			SocketSend("SET,0,0,0,0,10001");
#if MFC_MON_ENABLE
            
#else
            timer1.Stop();
#endif
        }

		private void buttonPurge_Click(object sender, EventArgs e)
		{
			SocketSend("SET,0,0,0,0,11001");
		}

		bool sockStatus = false;
		private void buttonSocketOn_Click(object sender, EventArgs e)
		{
			if(sockStatus)
			{
				sockStatus = false;
				SocketStop();
				buttonSocketOn.Text = "Socket On";
			}
			else
			{
				sockStatus = true;
				SocketStart();
				buttonSocketOn.Text = "Socket Off";
			}
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			textBoxTimer.Text = tmrTick.ToString() + " sec";
			CdcSingleWrite(CDC_MSG_CMD_GET_REF, Convert.ToByte(comboBoxTpsSingle.SelectedItem), 1);
			tmrTick++;
		}

		private void buttonMfcOff_Click(object sender, EventArgs e)
		{
			SocketSend("CLR");
#if MFC_MON_ENABLE
            
#else
			timer1.Stop();
#endif
        }

        private void buttonCalibrationStart_Click(object sender, EventArgs e)
        {
            AutoCalibrationStart();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ConfigFileOpen();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if(timer2.Enabled)
            {
                tmr2Tick++;
                if (textBoxT2.InvokeRequired)
                {
                    textBoxT2.Invoke(new MethodInvoker(delegate ()
                    {
                        textBoxT2.Text = tmr2Tick.ToString(); ;
                    }));
                }
                else
                {
                    textBoxT2.Text = tmr2Tick.ToString(); ;
                }
            }
        }
    }
}
