﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		public void AlarmGenerate()
		{
			UInt32 alarm = 0;

			try
			{
				if (checkBoxArrayPcb0.Checked)
				{
					if (string.IsNullOrWhiteSpace(textBoxAlarmIn.Text))
					{
						ERR("Alarm value is empty\n");
					}
					else
					{
						alarm = Convert.ToUInt32(textBoxAlarmIn.Text);

						textBoxAlarm0.Text = alarm.ToString();
						textBoxAlarm1.Text = alarm.ToString();
						textBoxAlarm2.Text = alarm.ToString();
						textBoxAlarm3.Text = alarm.ToString();
						textBoxAlarm4.Text = alarm.ToString();
						textBoxAlarm5.Text = alarm.ToString();
						textBoxAlarm6.Text = alarm.ToString();
						textBoxAlarm7.Text = alarm.ToString();
						textBoxAlarm8.Text = alarm.ToString();
						textBoxAlarm9.Text = alarm.ToString();
					}
				}
				else
				{
					textBoxAlarm0.Text = "";
					textBoxAlarm1.Text = "";
					textBoxAlarm2.Text = "";
					textBoxAlarm3.Text = "";
					textBoxAlarm4.Text = "";
					textBoxAlarm5.Text = "";
					textBoxAlarm6.Text = "";
					textBoxAlarm7.Text = "";
					textBoxAlarm8.Text = "";
					textBoxAlarm9.Text = "";

					ERR("Array PCB 0 is empty\n");
				}

				if (checkBoxArrayPcb1.Checked)
				{
					if (string.IsNullOrWhiteSpace(textBoxAlarmIn.Text))
					{
						ERR("Alarm value is empty\n");
					}
					else
					{
						alarm = Convert.ToUInt32(textBoxAlarmIn.Text);
						
						textBoxAlarm10.Text = alarm.ToString();
						textBoxAlarm11.Text = alarm.ToString();
						textBoxAlarm12.Text = alarm.ToString();
						textBoxAlarm13.Text = alarm.ToString();
						textBoxAlarm14.Text = alarm.ToString();
						textBoxAlarm15.Text = alarm.ToString();
						textBoxAlarm16.Text = alarm.ToString();
						textBoxAlarm17.Text = alarm.ToString();
						textBoxAlarm18.Text = alarm.ToString();
						textBoxAlarm19.Text = alarm.ToString();
					}
				}
				else
				{
					textBoxAlarm10.Text = "";
					textBoxAlarm11.Text = "";
					textBoxAlarm12.Text = "";
					textBoxAlarm13.Text = "";
					textBoxAlarm14.Text = "";
					textBoxAlarm15.Text = "";
					textBoxAlarm16.Text = "";
					textBoxAlarm17.Text = "";
					textBoxAlarm18.Text = "";
					textBoxAlarm19.Text = "";

					ERR("Array PCB 1 is empty\n");
				}
			}
			catch(Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		public void AlarmSet()
		{
			UInt16 alarm = 0;
			byte i = 0;
			byte alarm_h;
			byte alarm_l;
			int timeout = 500;

			if (checkBoxArrayPcb0.Checked)
			{
				alarm = Convert.ToUInt16(textBoxAlarm0.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);

				alarm = Convert.ToUInt16(textBoxAlarm1.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);

				alarm = Convert.ToUInt16(textBoxAlarm2.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);

				alarm = Convert.ToUInt16(textBoxAlarm3.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);

				alarm = Convert.ToUInt16(textBoxAlarm4.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);

				alarm = Convert.ToUInt16(textBoxAlarm5.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);

				alarm = Convert.ToUInt16(textBoxAlarm6.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);

				alarm = Convert.ToUInt16(textBoxAlarm7.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);

				alarm = Convert.ToUInt16(textBoxAlarm8.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);

				alarm = Convert.ToUInt16(textBoxAlarm9.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
				Thread.Sleep(timeout);
			}
			else
			{
				ERR("Array PCB 0 is empty\n");
			}

			if (checkBoxArrayPcb1.Checked)
			{
				alarm = Convert.ToUInt16(textBoxAlarm10.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);

				alarm = Convert.ToUInt16(textBoxAlarm11.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);

				alarm = Convert.ToUInt16(textBoxAlarm12.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);

				alarm = Convert.ToUInt16(textBoxAlarm13.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);

				alarm = Convert.ToUInt16(textBoxAlarm14.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);

				alarm = Convert.ToUInt16(textBoxAlarm15.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);

				alarm = Convert.ToUInt16(textBoxAlarm16.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);

				alarm = Convert.ToUInt16(textBoxAlarm17.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);

				alarm = Convert.ToUInt16(textBoxAlarm18.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);

				alarm = Convert.ToUInt16(textBoxAlarm19.Text);
				alarm_h = (byte)((alarm & 0xFF00) >> 8);
				alarm_l = (byte)(alarm & 0x00FF);
				CdcSendCmd(CDC_MSG_CMD_SET_ALARM, i++, alarm_h, alarm_l, 0, 0);
			}
			else
			{
				ERR("Array PCB 1 is empty\n");
			}
		}

		public void AlarmGet()
		{
			CdcSendCmd(CDC_MSG_CMD_GET_ALARM, 0, 0, 0, 0, 0);
		}

		public void VerGet()
		{
			CdcSendCmd(CDC_MSG_CMD_GET_VER, 0, 0, 0, 0, 0);
		}
	}
}
