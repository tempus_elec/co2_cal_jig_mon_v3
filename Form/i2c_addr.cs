﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		public void I2cAddrGenerate()
		{
			byte i2c_addr = 0;
			string i2c_addr_str = null;

			try
			{
				if (checkBoxArrayPcb0.Checked)
				{
					if (string.IsNullOrWhiteSpace(textBoxI2cAddr.Text))
					{
						ERR("Alarm value is empty\n");
					}
					else
					{
						i2c_addr = Convert.ToByte(textBoxI2cAddr.Text);
						i2c_addr_str = string.Format("0x{0:X2}", i2c_addr);
						
						labelI2cAddr.Text = i2c_addr_str;

						i2c_addr_str = i2c_addr.ToString();

						textBoxI2cAddr0.Text = i2c_addr_str;
						textBoxI2cAddr1.Text = i2c_addr_str;
						textBoxI2cAddr2.Text = i2c_addr_str;
						textBoxI2cAddr3.Text = i2c_addr_str;
						textBoxI2cAddr4.Text = i2c_addr_str;
						textBoxI2cAddr5.Text = i2c_addr_str;
						textBoxI2cAddr6.Text = i2c_addr_str;
						textBoxI2cAddr7.Text = i2c_addr_str;
						textBoxI2cAddr8.Text = i2c_addr_str;
						textBoxI2cAddr9.Text = i2c_addr_str;
					}
				}
				else
				{
					i2c_addr_str = "";

					textBoxI2cAddr0.Text = i2c_addr_str;
					textBoxI2cAddr1.Text = i2c_addr_str;
					textBoxI2cAddr2.Text = i2c_addr_str;
					textBoxI2cAddr3.Text = i2c_addr_str;
					textBoxI2cAddr4.Text = i2c_addr_str;
					textBoxI2cAddr5.Text = i2c_addr_str;
					textBoxI2cAddr6.Text = i2c_addr_str;
					textBoxI2cAddr7.Text = i2c_addr_str;
					textBoxI2cAddr8.Text = i2c_addr_str;
					textBoxI2cAddr9.Text = i2c_addr_str;

					ERR("Array PCB 0 is empty\n");
				}

				if (checkBoxArrayPcb1.Checked)
				{
					if (string.IsNullOrWhiteSpace(textBoxI2cAddr.Text))
					{
						ERR("Alarm value is empty\n");
					}
					else
					{
						i2c_addr = Convert.ToByte(textBoxI2cAddr.Text);
						i2c_addr_str = string.Format("0x{0:X2}", i2c_addr);

						labelI2cAddr.Text = i2c_addr_str;

						i2c_addr_str = i2c_addr.ToString();

						textBoxI2cAddr10.Text = i2c_addr_str;
						textBoxI2cAddr11.Text = i2c_addr_str;
						textBoxI2cAddr12.Text = i2c_addr_str;
						textBoxI2cAddr13.Text = i2c_addr_str;
						textBoxI2cAddr14.Text = i2c_addr_str;
						textBoxI2cAddr15.Text = i2c_addr_str;
						textBoxI2cAddr16.Text = i2c_addr_str;
						textBoxI2cAddr17.Text = i2c_addr_str;
						textBoxI2cAddr18.Text = i2c_addr_str;
						textBoxI2cAddr19.Text = i2c_addr_str;
					}
				}
				else
				{
					i2c_addr_str = "";

					textBoxI2cAddr10.Text = i2c_addr_str;
					textBoxI2cAddr11.Text = i2c_addr_str;
					textBoxI2cAddr12.Text = i2c_addr_str;
					textBoxI2cAddr13.Text = i2c_addr_str;
					textBoxI2cAddr14.Text = i2c_addr_str;
					textBoxI2cAddr15.Text = i2c_addr_str;
					textBoxI2cAddr16.Text = i2c_addr_str;
					textBoxI2cAddr17.Text = i2c_addr_str;
					textBoxI2cAddr18.Text = i2c_addr_str;
					textBoxI2cAddr19.Text = i2c_addr_str;

					ERR("Array PCB 1 is empty\n");
				}
			}
			catch(Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		public void I2cAddrSet()
		{
			byte i = 0;
			int timeout = 500;

			if (checkBoxArrayPcb0.Checked)
			{
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr0.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr1.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr2.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr3.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr4.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr5.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr6.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr7.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr8.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr9.Text), 0, 0, 0);
				Thread.Sleep(timeout);
			}
			else
			{
				ERR("Array PCB 0 is empty\n");
			}

			if (checkBoxArrayPcb1.Checked)
			{
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr10.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr11.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr12.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr13.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr14.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr15.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr16.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr17.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr18.Text), 0, 0, 0);
				Thread.Sleep(timeout);
				CdcSendCmd(CDC_MSG_CMD_SET_I2C_ADDR, i++, Convert.ToByte(textBoxI2cAddr19.Text), 0, 0, 0);
				Thread.Sleep(timeout);
			}
			else
			{
				ERR("Array PCB 1 is empty\n");
			}
		}

		public void I2cAddrGet()
		{
			CdcSendCmd(CDC_MSG_CMD_GET_I2C_ADDR, 0, 0, 0, 0, 0);
		}
	}
}
