﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		public void PicModuleSelect(byte m)
		{
			try
			{
				if (checkBoxPic.Checked == true)
				{
					labelM.Text = "Module " + Convert.ToString(m) + "selected";

					CdcSendCmd(CDC_MSG_CMD_SET_PIC_NUM, m, 0, 0, 0, 0);
					Thread.Sleep(50);
				}
				else
				{
					MessageBox.Show("PIC Program Mode is not enabled");
				}
			}
			catch(Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message + '\n');
			}			
		}
	}
}
