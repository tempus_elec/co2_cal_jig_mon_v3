﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		public void SerialNumberGenerate()
		{
			UInt32 serNum = 0;

			try
			{
				if (checkBoxArrayPcb0.Checked)
				{
					if (string.IsNullOrWhiteSpace(textBoxSerialNumIn.Text))
					{
						ERR("Serial Number is empty\n");
					}
					else
					{
						serNum = Convert.ToUInt32(textBoxSerialNumIn.Text);

						textBoxSerialNum0.Text = Convert.ToString(serNum++);
						textBoxSerialNum1.Text = Convert.ToString(serNum++);
						textBoxSerialNum2.Text = Convert.ToString(serNum++);
						textBoxSerialNum3.Text = Convert.ToString(serNum++);
						textBoxSerialNum4.Text = Convert.ToString(serNum++);
						textBoxSerialNum5.Text = Convert.ToString(serNum++);
						textBoxSerialNum6.Text = Convert.ToString(serNum++);
						textBoxSerialNum7.Text = Convert.ToString(serNum++);
						textBoxSerialNum8.Text = Convert.ToString(serNum++);
						textBoxSerialNum9.Text = Convert.ToString(serNum++);
					}
				}
				else
				{
					textBoxSerialNum0.Text = "";
					textBoxSerialNum1.Text = "";
					textBoxSerialNum2.Text = "";
					textBoxSerialNum3.Text = "";
					textBoxSerialNum4.Text = "";
					textBoxSerialNum5.Text = "";
					textBoxSerialNum6.Text = "";
					textBoxSerialNum7.Text = "";
					textBoxSerialNum8.Text = "";
					textBoxSerialNum9.Text = "";

					ERR("Array PCB 0 is empty\n");
				}

				if (checkBoxArrayPcb1.Checked)
				{
					if (string.IsNullOrWhiteSpace(textBoxSerialNumIn.Text))
					{
						ERR("Serial Number is empty\n");
					}
					else
					{
						serNum = Convert.ToUInt32(textBoxSerialNumIn.Text) + 10;
						
						textBoxSerialNum10.Text = Convert.ToString(serNum++);
						textBoxSerialNum11.Text = Convert.ToString(serNum++);
						textBoxSerialNum12.Text = Convert.ToString(serNum++);
						textBoxSerialNum13.Text = Convert.ToString(serNum++);
						textBoxSerialNum14.Text = Convert.ToString(serNum++);
						textBoxSerialNum15.Text = Convert.ToString(serNum++);
						textBoxSerialNum16.Text = Convert.ToString(serNum++);
						textBoxSerialNum17.Text = Convert.ToString(serNum++);
						textBoxSerialNum18.Text = Convert.ToString(serNum++);
						textBoxSerialNum19.Text = Convert.ToString(serNum++);
					}
				}
				else
				{
					textBoxSerialNum10.Text = "";
					textBoxSerialNum11.Text = "";
					textBoxSerialNum12.Text = "";
					textBoxSerialNum13.Text = "";
					textBoxSerialNum14.Text = "";
					textBoxSerialNum15.Text = "";
					textBoxSerialNum16.Text = "";
					textBoxSerialNum17.Text = "";
					textBoxSerialNum18.Text = "";
					textBoxSerialNum19.Text = "";

					ERR("Array PCB 1 is empty\n");
				}
			}
			catch(Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}

		public void SerialNumberSet()
		{
			UInt32 sn;
			int i = 0;
			int timeout = 500*2;

			if (checkBoxArrayPcb0.Checked)
			{
				sn = Convert.ToUInt32(textBoxSerialNum0.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum1.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum2.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum3.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum4.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum5.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum6.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum7.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum8.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum9.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);
			}
			else
			{
				ERR("Array PCB 0 is empty\n");
			}

			if (checkBoxArrayPcb1.Checked)
			{
				sn = Convert.ToUInt32(textBoxSerialNum10.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum11.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum12.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum13.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum14.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum15.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum16.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum17.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum18.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);

				sn = Convert.ToUInt32(textBoxSerialNum19.Text);
				CdcSendCmd(CDC_MSG_CMD_SET_SERIAL, (byte)i++, (byte)(sn & 0x000000FF), (byte)((sn & 0x0000FF00) >> 8), (byte)((sn & 0x00FF0000) >> 16), (byte)((sn & 0xFF000000) >> 24));
				Thread.Sleep(timeout);
			}
			else
			{
				ERR("Array PCB 1 is empty\n");
			}
		}

		public void SerialNumberGet()
		{
			CdcSendCmd(CDC_MSG_CMD_GET_SERIAL, 0, 0, 0, 0, 0);
		}
	}
}
