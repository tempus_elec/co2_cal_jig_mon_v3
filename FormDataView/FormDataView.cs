﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class FormDataView : Form
	{
		Form1 frm1;

		FileStream streamCsv;
		StreamWriter psWriterCsv;

		public FormDataView()
		{
			InitializeComponent();
		}

		public FormDataView(Form1 _form)
		{
			InitializeComponent();
			frm1 = _form;

			SetupDataGridView();
			CsvOpen();
		}

		private void FormDataView_FormClosing(Object sender, FormClosingEventArgs e)
		{
			CsvClose();
		}

		private void CsvOpen()
		{
			string path;

			try
			{
				if (psWriterCsv == null)
				{
					// folder에 log file 열기
					path = frm1.desktop_path + "/module_cal" + ".csv";
					streamCsv = new FileStream(path, FileMode.Create, FileAccess.Write);
					psWriterCsv = new StreamWriter(streamCsv, System.Text.Encoding.Default);

					for (int i = 0; i < dataGridView2.Columns.Count; i++)
					{
						psWriterCsv.Write(dataGridView2.Columns[i].Name);
						if (i != dataGridView2.Columns.Count)
						{
							psWriterCsv.Write(",");
						}
					}
					psWriterCsv.Write("\n");
					frm1.LOG("CSV Opened\n");
				}
				else
				{
					frm1.ERR("CSV file creation has problem...\n");
				}
			}
			catch(Exception ex)
			{
				frm1.ERR(frm1.CallerName() + " : " + ex.Message);
			}
		}

		private void CsvClose()
		{
			try
			{
				if (psWriterCsv == null)
				{
					frm1.ERR("psWriterCsv CSV file not opened\n");
				}
				else
				{
					psWriterCsv.Flush();
					psWriterCsv.Close();
					psWriterCsv = null;
				}
			}
			catch(Exception ex)
			{
				frm1.ERR(frm1.CallerName() + " : " + ex.Message);
			}
		}

		private void CSV_SAVE(string str)
		{
			string[] spData = str.Split(' ');

			try
			{
				for (int i = 0; i < spData.Length; i++)
				{
					psWriterCsv.Write(spData[i] + ",");
				}
				psWriterCsv.Write("\n");
			}
			catch(Exception ex)
			{
				frm1.ERR(frm1.CallerName() + " : " + ex.Message);
			}
		}
	}
}
