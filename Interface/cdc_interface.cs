﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		/*
		 * CDC Message
		 */ 
		const int CDC_MSG_FLAG_LED_GREEN = 0x10;
		const int CDC_MSG_FLAG_LED_RED = 0x11;
		const int CDC_MSG_FLAG_LED_BLUE = 0x12;

		const int CDC_MSG_MON_REF_SENSOR = 0x20;
		const int CDC_MSG_MON_TEMPUS = 0x21;
		const int CDC_MSG_MON_RAW = 0x22;
		const int CDC_MSG_MON_STR = 0x23;
		const int CDC_MSG_MON_LOG_TPS_TASK = 0x24;
		const int CDC_MSG_MON_AMB_RH = 0x25;
		const int CDC_MSG_MON_ALARM_SIG = 0x26;
		const int MSG_MON_FLAG = 0x27;

		const int CDC_MSG_CMD_GET_TEMPUS = 0x30;
		const int CDC_MSG_CMD_GET_REF = 0x31;
		const int CDC_MSG_CMD_GET_AMB_RH = 0x32;
		const int CDC_MSG_CMD_GET_ALL = 0x33;
		const int CDC_MSG_CMD_INIT = 0x34;
		const int CDC_MSG_CMD_GET_VER = 0x35;
		const int CDC_MSG_CMD_GET_SERIAL = 0x36;
		const int CDC_MSG_CMD_GET_ALARM = 0x37;
		const int CDC_MSG_CMD_GET_PPM = 0x38;
		const int CDC_MSG_CMD_GET_I2C_ADDR = 0x39;

		const int CDC_MSG_CMD_SET_SERIAL = 0x3A;
		const int CDC_MSG_CMD_SET_ALARM = 0x3B;
		const int CDC_MSG_CMD_SET_PIC_NUM = 0x3C;
		const int CDC_MSG_CMD_GET_TEMPUS_SINGLE = 0x3D;
		const int CDC_MSG_CMD_SET_POWER = 0x3E;
		const int CDC_MSG_CMD_SET_MUX_SEL = 0x3F;
		const int CDC_MSG_CMD_SET_MUX_EN = 0x40;
		const int CDC_MSG_CMD_AD_READ = 0x41;
		const int CDC_MSG_CMD_SET_MUX_CH = 0x42;
		const int CDC_MSG_CMD_AD_READ_ALL = 0x43;

		const int CDC_MSG_CMD_SET_SC = 0x60;
		const int CDC_MSG_CMD_SET_SR = 0x61;
		const int CDC_MSG_CMD_SET_GAIN = 0x62;
		const int CDC_MSG_CMD_SET_TS = 0x63;
		const int CDC_MSG_CMD_SET_DBG = 0x64;
		const int CDC_MSG_CMD_SET_CURV = 0x65;
		const int CDC_MSG_CMD_SET_I2C_ADDR = 0x66;

		const int CDC_MSG_CMD_SET_BAD_MODULE = 0x9F;
		/**************************************************/


		/*
		 *	CDC Response Message
		 *	These are same as FW message response
		 */
		const int BSP_CMD_GET_VER = 0x10;
		const int BSP_RES_GET_VER = 0x11;
		const int BSP_CMD_GET_SER = 0x12;
		const int BSP_RES_GET_SER = 0x13;
		const int BSP_CMD_GET_PPM = 0x14;
		const int BSP_RES_GET_PPM = 0x15;
		const int BSP_CMD_GET_ALARM = 0x16;
		const int BSP_RES_GET_ALARM = 0x17;
		const int BSP_CMD_SET_ALARM = 0x18;
		const int BSP_RES_SET_ALARM = 0x19;
		const int BSP_CMD_GET_REF = 0x1A;
		const int BSP_RES_GET_REF = 0x1B;
		const int BSP_CMD_DBG_GET_INFO = 0x80;
		const int BSP_RES_DBG_GET_INFO = 0x81;
		const int BSP_CMD_DBG_GET_I2C_ADDR = 0x82;
		const int BSP_RES_DBG_GET_I2C_ADDR = 0x83;
		const int BSP_CMD_DBG_SET_SER = 0x90;
		const int BSP_RES_DBG_SET_SER = 0x91;
		const int BSP_CMD_DBG_SET_SC = 0x92;
		const int BSP_RES_DBG_SET_SC = 0x93;
		const int BSP_CMD_DBG_SET_SR = 0x94;
		const int BSP_RES_DBG_SET_SR = 0x95;
		const int BSP_CMD_DBG_SET_GAIN = 0x96;
		const int BSP_RES_DBG_SET_GAIN = 0x97;
		const int BSP_CMD_DBG_SET_I2C_ADDR = 0x98;
		const int BSP_RES_DBG_SET_I2C_ADDR = 0x99;
		const int BSP_CMD_DBG_SET_TS = 0x9A;
		const int BSP_RES_DBG_SET_TS = 0x9B;
		const int BSP_CMD_DBG_SET_DBG = 0x9C;
		const int BSP_RES_DBG_SET_DBG = 0x9D;
		const int BSP_CMD_DBG_SET_CURV = 0x9E;
		const int BSP_RES_DBG_SET_CURV = 0x9F;
		const int BSP_CMD_SET_CALIBRATION_MODE = 0xA0;
		const int BSP_RES_SET_CALIBRATION_MODE = 0xA1;
		/**************************************************/

		/*
		 * CDC variables
		 */ 
		bool monBtnStatus = false;

		public void CdcSendCmd(byte cmd, byte channel, byte p0, byte p1, byte p2, byte p3)
		{
			byte[] msg = { 0xaa, 0x55, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
			msg[2] = cmd;
			msg[3] = channel;
			msg[4] = p0;
			msg[5] = p1;
			msg[6] = p2;
			msg[7] = p3;

			if ( (sPort_Tempus == null) || (sPort_Tempus.IsOpen == false))
			{
				ERR("Please open COM port first!!!!\n");
			}
			else
			{
				sPort_Tempus.Write(msg, 0, msg.Length);
				LOG("PC CMD " + msg[2].ToString() + ", channel " + msg[3].ToString() + ", " + msg[4].ToString() + ", " + msg[5].ToString() + ", " + msg[6].ToString() + ", " + msg[7].ToString() + "\n");
			}
			Delay(50);
		}

		public void CdcInit()
		{
			byte pcb = 0;
			byte status_if = 0;
			byte cbRsv = 0;
			byte rbRsv = 0;

			// check array pcb 0/1
			if (checkBoxArrayPcb0.Checked)
				pcb |= 0x80;
			if (checkBoxArrayPcb1.Checked)
				pcb |= 0x08;

			// check read status
			if (checkBoxReadStatus.Checked)
				status_if |= 0x80;
			// check interface selection
			if (radioButtonUartIf.Checked)
				status_if |= 0x00;
			else
				status_if |= 0x08;

			// check checkbox reserved
			if (checkBoxRsv1.Checked)
				cbRsv |= 0x80;
			if (checkBoxRsv2.Checked)
				cbRsv |= 0x08;

			// check radiobutton reserved
			if (radioButtonRsv1.Checked)
				rbRsv |= 0x80;
			if (radioButtonRsv2.Checked)
				rbRsv |= 0x08;

			CdcSendCmd(CDC_MSG_CMD_INIT, 0, pcb, status_if, cbRsv, rbRsv);
		}

		public void CdcMon()
		{
            /*
			 *	MSG_MON_FLAG
			 */
            if (monBtnStatus == false)
			{
                if (buttonMonEnable.InvokeRequired)
                {
                    buttonMonEnable.Invoke(new MethodInvoker(delegate () { buttonMonEnable.Text = "Mon Disable"; }));
                }
                else
                {
                    buttonMonEnable.Text = "Mon Disable";
                }
				CdcSendCmd(MSG_MON_FLAG, 2, 0, 1, 0, 0);
				monBtnStatus = true;
			}
			else
			{
                if (buttonMonEnable.InvokeRequired)
                {
                    buttonMonEnable.Invoke(new MethodInvoker(delegate () { buttonMonEnable.Text = "Mon Enable"; }));
                }
                else
                {
                    buttonMonEnable.Text = "Mon Enable";
                }
				CdcSendCmd(MSG_MON_FLAG, 2, 0, 0, 0, 0);
				monBtnStatus = false;
			}
		}

		public void CdcRead()
		{
			CdcSendCmd(CDC_MSG_CMD_GET_ALL, 0, 0, 0, 0, 0);
		}

		public void CdcSingleRead(byte channel)
		{
			CdcSendCmd(CDC_MSG_CMD_GET_TEMPUS_SINGLE, channel, 0, 0, 0, 0);
		}

		public void CdcSingleWrite(byte cmd, byte channel, byte onoff)
		{
			CdcSendCmd(cmd, channel, onoff, 0, 0, 0);
		}
	}
}
