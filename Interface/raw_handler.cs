﻿//#define ERROR_LOG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		int g_ref_ppm = 0;
		byte g_Amb = 0;

		public struct RawMsg_ST
		{
			public Boolean syncFound;
			public UInt16 syncCount;
			public UInt16 len;
			public int curOffset;

			public RawMsg_ST(Boolean a, UInt16 b, UInt16 c, UInt16 d)
			{
				syncFound = a;
				syncCount = b;
				len = c;
				curOffset = d;
			}

			public void Clear()
			{
				syncFound = false;
				syncCount = 0;
				len = 0;
				curOffset = 0;
			}
		}

		RawMsg_ST raw = new RawMsg_ST(false, 0, 0, 0);
        
        public void HandleRawData()
		{
			string[][] modMsg = new string[][] {    new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},

													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""},
													new string[] {"", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""}
												};
			DateTime now;
			int offset = 0;
			int len;

			int ref_ppm;
			float amb, rh;
			UInt16 amb_raw, rh_raw;
			UInt16 currI = 0;
			byte arrayNumber = 0;
			UInt32 ser;
			int pwm;
			int alarm;

			int no, flag, st0, nt0, st1, nt1;
			UInt32 dC, sC, dR, sR, gain;
			UInt32 ratioH, ratioL;
			float ratio;
			byte ts, temp;
			UInt16 adc, ppm;
			int i2c_addr;
			string ver;
			string userInterface;

			int ppm_offset = 13;

			byte m_off = 0;     // louiey, to distinguish bad module
			byte badModuleNum = 0;

            int i, i_max;

			now = DateTime.Now;

			// parse header data
			len = rcvBuf[offset] | (rcvBuf[offset + 1] << 8);
			offset += 2;

			ref_ppm = rcvBuf[offset] | (rcvBuf[offset + 1] << 8);
			g_ref_ppm = ref_ppm;
			offset += 2;

			amb_raw = (UInt16)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8));
			amb = (float)(((amb_raw / 65536.0) * 165.0) - 40.0);
			g_Amb = (byte)amb;
			offset += 2;

			rh_raw = (UInt16)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8));
			rh = (float)((rh_raw / 65536.0) * 100.0);
			offset += 2;

			arrayNumber = rcvBuf[offset++];
			offset++;                                       // due to reserved

            if (arrayNumber == 1)
            {
                i = 10;
                i_max = 20;
                m_off = 10;
            }
            else
            {
                i = 0;
                i_max = 10;
                m_off = 0;
            }

			for ( ; i < i_max; i++)
			{
				int j = 0;
				// parsing module values
				no = rcvBuf[offset++];

				//
				flag = rcvBuf[offset++];
				if ((flag & 0x01) == 0x01)
				{
					userInterface = "I2C";
				}
				else
				{
					userInterface = "UART";
				}
				// check bad modules
				if( ((flag & 0x80) != 0x80) || ((flag & 0x08) == 0x08) || ((flag & 0x10) == 0x10) )
				{
					// bad module
					ChartIsValid(m_off, false);     // louiey, 2018.01.08
					badList[m_off++] = true;
				}
				else
				{
					// good module
					ChartIsValid(m_off, true);
					badList[m_off++] = false;
					badModuleNum++;
				}
#if false
                if (textBoxModuleNum.InvokeRequired)
				{
					textBoxModuleNum.Invoke(new MethodInvoker(delegate ()
					{
						textBoxModuleNum.Text = badModuleNum.ToString();
					}));
				}
				else
				{
					textBoxModuleNum.Text = badModuleNum.ToString();
				}
#endif
				///////////////////////////////////////
				ver = rcvBuf[offset++].ToString() + "." + rcvBuf[offset++].ToString() + "." + rcvBuf[offset++].ToString();
				ser = (UInt32)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8) | (rcvBuf[offset + 2] << 16) | (rcvBuf[offset + 3] << 24));
				offset += 4;
				pwm = rcvBuf[offset] | (rcvBuf[offset + 1] << 8);
				offset += 2;
				alarm = rcvBuf[offset] | (rcvBuf[offset + 1] << 8);
				offset += 2;
				i2c_addr = rcvBuf[offset++];

				st0 = rcvBuf[offset++];
				nt0 = rcvBuf[offset++];
				st1 = rcvBuf[offset++];
				nt1 = rcvBuf[offset++];

				dC = (UInt32)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8) | (rcvBuf[offset + 2] << 16) | (rcvBuf[offset + 3] << 24));
				offset += 4;

				sC = (UInt32)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8) | (rcvBuf[offset + 2] << 16) | (rcvBuf[offset + 3] << 24));
				offset += 4;

				dR = (UInt32)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8) | (rcvBuf[offset + 2] << 16) | (rcvBuf[offset + 3] << 24));
				offset += 4;

				sR = (UInt32)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8) | (rcvBuf[offset + 2] << 16) | (rcvBuf[offset + 3] << 24));
				offset += 4;

				adc = (UInt16)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8));
				offset += 2;

				ratio = (float)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8) | (rcvBuf[offset + 2] << 16) | (rcvBuf[offset + 3] << 24));
				ratioH = rcvBuf[offset + 3];
				ratioL = (UInt32)((rcvBuf[offset]) | (rcvBuf[offset + 1] << 8) | (rcvBuf[offset + 2] << 16));
				offset += 4;

				ppm = (UInt16)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8));
				offset += 2;

				temp = (byte)rcvBuf[offset];
				offset += 1;

				ts = (byte)rcvBuf[offset];
				offset += 1;

				gain = (UInt32)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8) | (rcvBuf[offset + 2] << 16) | (rcvBuf[offset + 3] << 24));
				offset += 4;

				currI = (UInt16)(rcvBuf[offset] | (rcvBuf[offset + 1] << 8));
				offset += 2;

				offset += 2;			// due to reserved

				if (textBoxRef.InvokeRequired)
				{
					textBoxRef.Invoke(new MethodInvoker(delegate ()
					{
						textBoxRef.Text = Convert.ToString(ref_ppm);
					}));
				}
				else
				{
					textBoxRef.Text = Convert.ToString(ref_ppm);
				}

				if (textBoxAmb.InvokeRequired)
				{
					textBoxAmb.Invoke(new MethodInvoker(delegate ()
					{
						textBoxAmb.Text = amb.ToString("#.#") + " ℃";
					}));
				}
				else
				{
					textBoxAmb.Text = amb.ToString("#.#") + " ℃";
				}

				if (textBoxRh.InvokeRequired)
				{
					textBoxRh.Invoke(new MethodInvoker(delegate ()
					{
						textBoxRh.Text = rh.ToString("#.#") + " %";
					}));
				}
				else
				{
					textBoxRh.Text = rh.ToString("#.#") + " %";
				}
				/*
				textBoxRef.Text = Convert.ToString(ref_ppm);
				textBoxAmb.Text = amb.ToString("#.#") + " ℃";
				textBoxRh.Text = rh.ToString("#.#") + " %";
				*/
				// converts to string so can display at datagrid
				//modMsg[i][j++] = now.ToString("yy/MM/dd-HH:mm:ss");
				modMsg[i][j++] = now.ToString("HH:mm:ss");
				modMsg[i][j++] = Convert.ToString(no);
				modMsg[i][j++] = string.Format("0x{0:X2}", flag);
				modMsg[i][j++] = Convert.ToString(st0);
				modMsg[i][j++] = Convert.ToString(nt0);
				modMsg[i][j++] = Convert.ToString(st1);
				modMsg[i][j++] = Convert.ToString(nt1);
				modMsg[i][j++] = Convert.ToString(dC);
				modMsg[i][j++] = Convert.ToString(sC);
				modMsg[i][j++] = Convert.ToString(dR);
				modMsg[i][j++] = Convert.ToString(sR);
				modMsg[i][j++] = Convert.ToString(adc);
				//modMsg[i][j++] = Convert.ToString(ratioH) + '.' + Convert.ToString(ratioL);
				modMsg[i][j++] = Convert.ToString(ratioH) + '.' + String.Format("{0:0000}", ratioL);
				modMsg[i][j++] = Convert.ToString(ppm);
				//modMsg[i][j++] = Convert.ToString(temp) + " ℃";
				modMsg[i][j++] = Convert.ToString(temp);
				modMsg[i][j++] = Convert.ToString(ts);
				modMsg[i][j++] = Convert.ToString(gain);
				modMsg[i][j++] = Convert.ToString(pwm);
				modMsg[i][j++] = Convert.ToString(alarm);
				//modMsg[i][j++] = Convert.ToString(i2c_addr);
				modMsg[i][j++] = string.Format("0x{0:X2}", i2c_addr);
				modMsg[i][j++] = Convert.ToString(ser);
				modMsg[i][j++] = ver;
				modMsg[i][j++] = userInterface;
				modMsg[i][j++] = Convert.ToString(ppm - ref_ppm);
				modMsg[i][j++] = Convert.ToString(g_ref_ppm);
				modMsg[i][j++] = Convert.ToString(g_Amb);
				modMsg[i][j++] = rh.ToString("#.##");//Convert.ToString(rh);
				modMsg[i][j++] = currI.ToString();
				modMsg[i][j++] = ":)";
				/* Que */
				if (Tps != null)
				{
					if ((dC != 0) && (dC < DC_MAX) && (dC > DC_MIN))
					{
						Tps[i].dC_Q.EnQue(dC);
					}
					else
					{
#if ERROR_LOG
						if (flag != 0)
							CAL_LOG("ERROR Ch " + no + " : dC value - " + dC + "\n");
#endif
					}

					if ((dR != 0) && (dR < DR_MAX) && (dR > DR_MIN))
					{
						Tps[i].dR_Q.EnQue(dR);
					}
					else
					{
#if ERROR_LOG
						if (flag != 0)
							CAL_LOG("ERROR Ch " + no + " : dR value - " + dR + "\n");
#endif
					}

					if ((sC != 0) && (sC < DC_MAX) && (sC > DC_MIN))
					{
						Tps[i].sC_Q.EnQue(sC);
					}
					else
					{
#if ERROR_LOG
						if (flag != 0)
							CAL_LOG("ERROR Ch " + no + " : sC value - " + sC + "\n");
#endif
					}

					if ((sR != 0) && (sR < DR_MAX) && (sR > DR_MIN))
					{
						Tps[i].sR_Q.EnQue(sR);
					}
					else
					{
#if ERROR_LOG
						if (flag != 0)
							CAL_LOG("ERROR Ch " + no + " : sR value - " + sR + "\n");
#endif
					}

					Tps[i].TS_Q.EnQue(ts);

					Tps[i].Temp_Q.EnQue(temp);
					Tps[i].ppm_Q.EnQue(ppm);
					Tps[i].flag_Q.EnQue(flag);
				}
				/*********************/
			}

			if (calEvent != null)
			{
                if(checkBoxArrayPcb0.Checked && checkBoxArrayPcb1.Checked)
                {
                    if(arrayNumber == 1)
                    {
                        LOG("RAW Event Set\n");
                        calEvent.Set();
                    }
                }
                else if(checkBoxArrayPcb0.Checked)
                {
                    if (arrayNumber == 0)
                    {
                        LOG("RAW Event0 Set\n");
                        calEvent.Set();
                    }
                }
                else if (checkBoxArrayPcb1.Checked)
                {
                    if (arrayNumber == 1)
                    {
                        LOG("RAW Event1 Set\n");
                        calEvent.Set();
                    }
                }
                else
                {
                    ERR(CallerName() + " Event Set Error\n");
                }
            }

			if(arrayNumber == 0)
            {
				FrmDv.PopulateDataGridView(modMsg, false);
                // update chart
                ChartPpmUpdate(chartCount++, Convert.ToString(ref_ppm), modMsg[0][ppm_offset], modMsg[1][ppm_offset], modMsg[2][ppm_offset],
                                                modMsg[3][ppm_offset], modMsg[4][ppm_offset], modMsg[5][ppm_offset], modMsg[6][ppm_offset],
                                                modMsg[7][ppm_offset], modMsg[8][ppm_offset], modMsg[9][ppm_offset]);
            }				
			else
            {
				FrmDv.PopulateDataGridView(modMsg, true);
			// update chart
                Chart2PpmUpdate(chartCount++, Convert.ToString(ref_ppm), modMsg[10][ppm_offset], modMsg[11][ppm_offset], modMsg[12][ppm_offset],
                                                modMsg[13][ppm_offset], modMsg[14][ppm_offset], modMsg[15][ppm_offset], modMsg[16][ppm_offset],
                                                modMsg[17][ppm_offset], modMsg[18][ppm_offset], modMsg[19][ppm_offset]);
            }			
		}
	}
}
