﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		SerialPort sPort_Tempus = null;

		UInt32 chartCount = 0;

		Mutex mtx = new Mutex();

		void SerialPort_DataReceivedPackets(object sender, SerialDataReceivedEventArgs e)
		{
			try
			{
				mtx.WaitOne();
				int intRecSize = sPort_Tempus.BytesToRead;
				string strRecData;
				string[] spData;
				int dataLen = 0;
				byte[] buff;
#if true
				if ((intRecSize == 0) || (intRecSize > (RAW_MAX_LENGTH + 4)))
				{
					buff = new byte[intRecSize];
					sPort_Tempus.Read(buff, 0, intRecSize);
					//ERR("CDC got wrong packet length - " + intRecSize + "\n");
					mtx.ReleaseMutex();
					return;
				}
#endif
				buff = new byte[intRecSize];
				dataLen = intRecSize - 4;

				sPort_Tempus.Read(buff, 0, intRecSize);
				LOG("PC Read Size : " + intRecSize + '\n');

				strRecData = System.Text.Encoding.UTF8.GetString(buff);
				spData = strRecData.Split(' ');

				if (spData[0] == "MSG")
				{
					byte[] payload = new byte[dataLen];
					StringBuilder resHex = new StringBuilder(1 * 2);
					resHex.AppendFormat("{0:x2}", buff[4]);

					INFO("Data : " + spData[0] + ", RES : 0x" + resHex.ToString() + ", CH : " + buff[5] + ", Len : " + dataLen + "\n");
					Array.Copy(buff, 4, payload, 0, dataLen);
					ResponseParser(payload, (UInt16)dataLen);
				}
				else if (spData[0] == "RAW")
				{
					LOG("RAW : " + spData[0] + "\n");
					Buffer.BlockCopy(buff, 4, rcvBuf, 0, dataLen);
					HandleRawData();
				}
				else if (spData[0] == "LOG")
				{
					if(checkBoxLogLog.Checked == true)
						_L(strRecData.Substring(4));
				}
				else if (spData[0] == "ERR")
				{
					if (checkBoxLogErr.Checked == true)
						ERR(strRecData.Substring(4));
				}
				else
				{
					LOG(strRecData);
				}
				mtx.ReleaseMutex();
			}
			catch (System.IndexOutOfRangeException ex)  // CS0168
			{
				mtx.ReleaseMutex();
				ERR(CallerName() + " : " + ex.Message + '\n');
			}
		}

		/*
		 *	Resp | Channel | Payload data
		 * 
		 */
		public void ResponseParser(byte[] msg, UInt16 len)
		{
			byte response = msg[0];
			UInt32 dat;
			byte channel = msg[1];

			try
			{
				switch (response)
				{
					case BSP_RES_GET_REF:
						UInt16 ref_ppm_val = (UInt16)(msg[2] | (msg[3]<<8));
						if (textBoxRef.InvokeRequired)
						{
							textBoxRef.Invoke(new MethodInvoker(delegate ()
							{
								textBoxRef.Text = ref_ppm_val.ToString();
								ChartPpmUpdate(chartCount, ref_ppm_val.ToString(), "400", "400", "400", "400", "400", "400", "400", "400", "400", "400");
								Chart2PpmUpdate(chartCount++, ref_ppm_val.ToString(), "400", "400", "400", "400", "400", "400", "400", "400", "400", "400");
							}));
						}
						else
						{
							textBoxRef.Text = ref_ppm_val.ToString();
							ChartPpmUpdate(chartCount, ref_ppm_val.ToString(), "400", "400", "400", "400", "400", "400", "400", "400", "400", "400");
							Chart2PpmUpdate(chartCount++, ref_ppm_val.ToString(), "400", "400", "400", "400", "400", "400", "400", "400", "400", "400");
						}
						break;

					case BSP_RES_GET_VER:
						LOG("VER -> " + msg[2] + "." + msg[3] + "." + msg[4] + "\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_GET_SER:
						dat = (UInt32)(msg[2] | (msg[3] << 8) | (msg[4] << 16) | (msg[5] << 24));
						LOG("SN -> " + dat + "\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_GET_PPM:
						dat = (UInt32)(msg[2] | (msg[3] << 8));
						LOG("PPM -> " + dat + "\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_GET_ALARM:
						dat = (UInt32)(msg[2] | (msg[3] << 8));
						LOG("ALARM -> " + dat + "\n");
                        LOG("GET_ALARM Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_SET_ALARM:
                        LOG("SET_ALARM Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_DBG_GET_INFO:
                        LOG("GET_INFO Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_DBG_GET_I2C_ADDR:
						dat = (UInt32)(msg[2]);
						LOG("I2C ADDR -> " + dat + "\n");
                        LOG("GET_I2C_ADDR Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_DBG_SET_SER:
                        LOG("SET_SER Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_DBG_SET_SC:
                        LOG("SET_SC Res\n");
                        if(calResEvent != null)
                            calResEvent.Set();
						break;

					case BSP_RES_DBG_SET_SR:
                        LOG("SET_SR Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_DBG_SET_GAIN:
                        LOG("SET_GAIN Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_DBG_SET_I2C_ADDR:
                        LOG("SET_I2C_ADDR Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_DBG_SET_TS:
						//dat = (UInt32)(msg[2]);
						//LOG("Temp Shift -> " + dat + "\n");
                        LOG("SET_TS Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_DBG_SET_DBG:
                        LOG("SET_DBG Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_DBG_SET_CURV:
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;

					case BSP_RES_SET_CALIBRATION_MODE:
                        LOG("SET_CALIBRATION_MODE Res\n");
                        if (calResEvent != null)
                            calResEvent.Set();
                        break;
				}
			}
			catch (Exception e)
			{
				ERR(CallerName() + " : " + e.Message);
			}
		}

		public void UartCom()
		{
			UInt16 isPortFound = 0;

			LOG("COM port finding...\n");
			buttonComOpen.Enabled = false;
			buttonComClose.Enabled = false;
			
			comboBoxCom.Items.Clear();
			comboBoxCom.BeginUpdate();
			
			foreach (string comport in SerialPort.GetPortNames())
			{
				comboBoxCom.Items.Add(comport);
				LOG("Found " + comport + "\n");

				isPortFound++;
			}
			comboBoxCom.EndUpdate();

			CheckForIllegalCrossThreadCalls = false;

			if (isPortFound != 0)
			{
				buttonComOpen.Enabled = true;
				buttonComClose.Enabled = true;
			}
			else
			{
				LOG("Couldn't found available COM port.\n");

				buttonComOpen.Enabled = false;
				buttonComClose.Enabled = false;
			}
		}

		private void UartOpen()
		{
			try
			{
				if (null == sPort_Tempus)
				{
					if (comboBoxCom.SelectedItem == null)
					{
						LOG("시리얼 포트를 선택해 주세요!!!");
					}
					else
					{
						sPort_Tempus = new SerialPort();
						sPort_Tempus.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceivedPackets);
						//sPort_Tempus.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceivedBytes);
						sPort_Tempus.PortName = comboBoxCom.SelectedItem.ToString();
						sPort_Tempus.BaudRate = 115200;     // 230400;
						sPort_Tempus.DataBits = (int)8;
						sPort_Tempus.Parity = Parity.None;
						sPort_Tempus.StopBits = StopBits.One;
						//sPort_Tempus.ReadTimeout = (int)5000*2;
						sPort_Tempus.ReadTimeout = 0;
						sPort_Tempus.WriteTimeout = (int)5000;
						sPort_Tempus.NewLine = "\n";
                        try
                        {
                            sPort_Tempus.Open();
                        }
                        catch (Exception ex)
                        {
                            UartClose();
                            ERR(CallerName() + " : " + ex.Message);
                        }

                        cdcPort = sPort_Tempus.PortName;
					}

				}

				if (sPort_Tempus.IsOpen)
				{
					buttonComOpen.Enabled = false;
					buttonComClose.Enabled = true;
					LOG("시리얼 포트를 연결했습니다... : )\n");
				}
				else
				{
					if (comboBoxCom.SelectedItem != null)
						LOG("시리얼 포트가 사용중입니다\n");

					buttonComOpen.Enabled = false;
					buttonComClose.Enabled = true;
				}
			}
			catch (System.Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
                UartClose();
			}
		}

		private void UartOpen(string comName)
		{
			try
			{
				if (null == sPort_Tempus)
				{
					if (comName == null)
					{
						LOG("시리얼 포트를 선택해 주세요!!!");
					}
					else
					{
						sPort_Tempus = new SerialPort();
						sPort_Tempus.DataReceived += new SerialDataReceivedEventHandler(SerialPort_DataReceivedPackets);
						sPort_Tempus.PortName = comName;
						sPort_Tempus.BaudRate = 115200;     // 230400;
						sPort_Tempus.DataBits = (int)8;
						sPort_Tempus.Parity = Parity.None;
						sPort_Tempus.StopBits = StopBits.One;
						sPort_Tempus.ReadTimeout = 0;
						sPort_Tempus.WriteTimeout = (int)5000;
						sPort_Tempus.NewLine = "\n";
                        try
                        {
                            sPort_Tempus.Open();
                        }
                        catch (Exception ex)
                        {
                            UartClose();
                            ERR(CallerName() + " : " + ex.Message);
                        }
                    }
				}

				if (sPort_Tempus.IsOpen)
				{
					buttonComOpen.Enabled = false;
					buttonComClose.Enabled = true;
					LOG("시리얼 포트를 연결했습니다... : )\n");
				}
				else
				{
					if (comboBoxCom.SelectedItem != null)
						LOG("시리얼 포트가 사용중입니다\n");

					buttonComOpen.Enabled = false;
					buttonComClose.Enabled = true;
				}
			}
			catch (System.Exception ex)
			{
				ERR(CallerName() + " : " + ex.Message);
                UartClose();
			}
		}

		private void UartClose()
		{
			try
			{
				if (null != sPort_Tempus)
				{
					if (sPort_Tempus.IsOpen)
					{
						sPort_Tempus.Close();
						sPort_Tempus.Dispose();
						sPort_Tempus = null;
						buttonComOpen.Enabled = true;
						buttonComClose.Enabled = false;
						LOG("시리얼 포트를 닫았습니다\n");
					}
					else
					{
						LOG("시리얼 포트가 닫혀있습니다\n");
					}
                    sPort_Tempus = null;
                }
			}
			catch (Exception ex)  // CS0168
			{
				ERR(CallerName() + " : " + ex.Message);
			}
		}
	}
}
