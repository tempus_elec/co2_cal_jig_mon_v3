﻿namespace CO2_CAL_JIG_Mon_V3
{
	partial class FormMfc
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMfc));
			this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
			this.textBoxSend = new System.Windows.Forms.TextBox();
			this.timerUp = new System.Windows.Forms.Timer(this.components);
			this.buttonSend = new System.Windows.Forms.Button();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.label8 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.textBoxV = new System.Windows.Forms.TextBox();
			this.textBoxP = new System.Windows.Forms.TextBox();
			this.textBoxM3 = new System.Windows.Forms.TextBox();
			this.textBoxM2 = new System.Windows.Forms.TextBox();
			this.textBoxM1 = new System.Windows.Forms.TextBox();
			this.textBoxV1 = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.button1 = new System.Windows.Forms.Button();
			this.buttonValveCheckboxSend = new System.Windows.Forms.Button();
			this.checkBoxValve5 = new System.Windows.Forms.CheckBox();
			this.checkBoxValve4 = new System.Windows.Forms.CheckBox();
			this.label17 = new System.Windows.Forms.Label();
			this.label16 = new System.Windows.Forms.Label();
			this.label15 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.checkBoxValve3 = new System.Windows.Forms.CheckBox();
			this.checkBoxValve2 = new System.Windows.Forms.CheckBox();
			this.checkBoxValve1 = new System.Windows.Forms.CheckBox();
			this.checkBoxOnOff = new System.Windows.Forms.CheckBox();
			this.comboBoxValve = new System.Windows.Forms.ComboBox();
			this.buttonValveSend = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.trackBarMfc2 = new System.Windows.Forms.TrackBar();
			this.buttonMfc2 = new System.Windows.Forms.Button();
			this.label7 = new System.Windows.Forms.Label();
			this.labelMfc2 = new System.Windows.Forms.Label();
			this.panel2 = new System.Windows.Forms.Panel();
			this.trackBarMfc1 = new System.Windows.Forms.TrackBar();
			this.buttonMfc1 = new System.Windows.Forms.Button();
			this.label5 = new System.Windows.Forms.Label();
			this.labelMfc1 = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.trackBarMfc0 = new System.Windows.Forms.TrackBar();
			this.buttonMfc0 = new System.Windows.Forms.Button();
			this.label4 = new System.Windows.Forms.Label();
			this.labelMfc0 = new System.Windows.Forms.Label();
			this.textBoxDown = new System.Windows.Forms.TextBox();
			this.textBoxUp = new System.Windows.Forms.TextBox();
			this.buttonTimerDown = new System.Windows.Forms.Button();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.buttonTimer = new System.Windows.Forms.Button();
			this.textBoxTickDown = new System.Windows.Forms.TextBox();
			this.textBoxTickUp = new System.Windows.Forms.TextBox();
			this.panel4 = new System.Windows.Forms.Panel();
			this.timerDown = new System.Windows.Forms.Timer(this.components);
			this.groupBox1.SuspendLayout();
			this.panel3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMfc2)).BeginInit();
			this.panel2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMfc1)).BeginInit();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMfc0)).BeginInit();
			this.panel4.SuspendLayout();
			this.SuspendLayout();
			// 
			// richTextBoxLog
			// 
			this.richTextBoxLog.Location = new System.Drawing.Point(7, 51);
			this.richTextBoxLog.Name = "richTextBoxLog";
			this.richTextBoxLog.Size = new System.Drawing.Size(340, 392);
			this.richTextBoxLog.TabIndex = 0;
			this.richTextBoxLog.Text = "";
			// 
			// textBoxSend
			// 
			this.textBoxSend.Location = new System.Drawing.Point(7, 8);
			this.textBoxSend.Name = "textBoxSend";
			this.textBoxSend.Size = new System.Drawing.Size(230, 21);
			this.textBoxSend.TabIndex = 12;
			this.textBoxSend.Text = "MFC,1,10";
			// 
			// timerUp
			// 
			this.timerUp.Interval = 1000;
			this.timerUp.Tick += new System.EventHandler(this.timer1_Tick);
			// 
			// buttonSend
			// 
			this.buttonSend.Location = new System.Drawing.Point(272, 9);
			this.buttonSend.Name = "buttonSend";
			this.buttonSend.Size = new System.Drawing.Size(75, 23);
			this.buttonSend.TabIndex = 18;
			this.buttonSend.Text = "Send";
			this.buttonSend.UseVisualStyleBackColor = true;
			this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(643, 11);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(13, 12);
			this.label11.TabIndex = 70;
			this.label11.Text = "V";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(602, 11);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(13, 12);
			this.label10.TabIndex = 69;
			this.label10.Text = "P";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(557, 11);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(22, 12);
			this.label9.TabIndex = 68;
			this.label9.Text = "M3";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(516, 11);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(22, 12);
			this.label8.TabIndex = 67;
			this.label8.Text = "M2";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(475, 12);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(22, 12);
			this.label6.TabIndex = 66;
			this.label6.Text = "M1";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(430, 12);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(19, 12);
			this.label3.TabIndex = 65;
			this.label3.Text = "V1";
			// 
			// textBoxV
			// 
			this.textBoxV.Location = new System.Drawing.Point(640, 27);
			this.textBoxV.Name = "textBoxV";
			this.textBoxV.Size = new System.Drawing.Size(20, 21);
			this.textBoxV.TabIndex = 63;
			// 
			// textBoxP
			// 
			this.textBoxP.Location = new System.Drawing.Point(598, 27);
			this.textBoxP.Name = "textBoxP";
			this.textBoxP.Size = new System.Drawing.Size(20, 21);
			this.textBoxP.TabIndex = 62;
			// 
			// textBoxM3
			// 
			this.textBoxM3.Location = new System.Drawing.Point(557, 27);
			this.textBoxM3.Name = "textBoxM3";
			this.textBoxM3.Size = new System.Drawing.Size(20, 21);
			this.textBoxM3.TabIndex = 64;
			// 
			// textBoxM2
			// 
			this.textBoxM2.Location = new System.Drawing.Point(517, 27);
			this.textBoxM2.Name = "textBoxM2";
			this.textBoxM2.Size = new System.Drawing.Size(20, 21);
			this.textBoxM2.TabIndex = 61;
			// 
			// textBoxM1
			// 
			this.textBoxM1.Location = new System.Drawing.Point(476, 27);
			this.textBoxM1.Name = "textBoxM1";
			this.textBoxM1.Size = new System.Drawing.Size(20, 21);
			this.textBoxM1.TabIndex = 60;
			// 
			// textBoxV1
			// 
			this.textBoxV1.Location = new System.Drawing.Point(430, 27);
			this.textBoxV1.Name = "textBoxV1";
			this.textBoxV1.Size = new System.Drawing.Size(20, 21);
			this.textBoxV1.TabIndex = 59;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.buttonValveCheckboxSend);
			this.groupBox1.Controls.Add(this.checkBoxValve5);
			this.groupBox1.Controls.Add(this.checkBoxValve4);
			this.groupBox1.Controls.Add(this.label17);
			this.groupBox1.Controls.Add(this.label16);
			this.groupBox1.Controls.Add(this.label15);
			this.groupBox1.Controls.Add(this.label14);
			this.groupBox1.Controls.Add(this.label13);
			this.groupBox1.Controls.Add(this.checkBoxValve3);
			this.groupBox1.Controls.Add(this.checkBoxValve2);
			this.groupBox1.Controls.Add(this.checkBoxValve1);
			this.groupBox1.Controls.Add(this.checkBoxOnOff);
			this.groupBox1.Controls.Add(this.comboBoxValve);
			this.groupBox1.Controls.Add(this.buttonValveSend);
			this.groupBox1.Location = new System.Drawing.Point(363, 246);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(389, 101);
			this.groupBox1.TabIndex = 58;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Valve";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(321, 66);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(63, 23);
			this.button1.TabIndex = 37;
			this.button1.Text = "All Off";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// buttonValveCheckboxSend
			// 
			this.buttonValveCheckboxSend.Location = new System.Drawing.Point(229, 66);
			this.buttonValveCheckboxSend.Name = "buttonValveCheckboxSend";
			this.buttonValveCheckboxSend.Size = new System.Drawing.Size(62, 23);
			this.buttonValveCheckboxSend.TabIndex = 5;
			this.buttonValveCheckboxSend.Text = "Send";
			this.buttonValveCheckboxSend.UseVisualStyleBackColor = true;
			this.buttonValveCheckboxSend.Click += new System.EventHandler(this.buttonValveCheckboxSend_Click);
			// 
			// checkBoxValve5
			// 
			this.checkBoxValve5.AutoSize = true;
			this.checkBoxValve5.Location = new System.Drawing.Point(172, 75);
			this.checkBoxValve5.Name = "checkBoxValve5";
			this.checkBoxValve5.Size = new System.Drawing.Size(15, 14);
			this.checkBoxValve5.TabIndex = 4;
			this.checkBoxValve5.UseVisualStyleBackColor = true;
			// 
			// checkBoxValve4
			// 
			this.checkBoxValve4.AutoSize = true;
			this.checkBoxValve4.Location = new System.Drawing.Point(132, 75);
			this.checkBoxValve4.Name = "checkBoxValve4";
			this.checkBoxValve4.Size = new System.Drawing.Size(15, 14);
			this.checkBoxValve4.TabIndex = 4;
			this.checkBoxValve4.UseVisualStyleBackColor = true;
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(173, 60);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(11, 12);
			this.label17.TabIndex = 36;
			this.label17.Text = "5";
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(133, 60);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(11, 12);
			this.label16.TabIndex = 36;
			this.label16.Text = "4";
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(93, 60);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(11, 12);
			this.label15.TabIndex = 36;
			this.label15.Text = "3";
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(54, 60);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(11, 12);
			this.label14.TabIndex = 36;
			this.label14.Text = "2";
			this.label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(13, 60);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(11, 12);
			this.label13.TabIndex = 36;
			this.label13.Text = "1";
			// 
			// checkBoxValve3
			// 
			this.checkBoxValve3.AutoSize = true;
			this.checkBoxValve3.Location = new System.Drawing.Point(93, 75);
			this.checkBoxValve3.Name = "checkBoxValve3";
			this.checkBoxValve3.Size = new System.Drawing.Size(15, 14);
			this.checkBoxValve3.TabIndex = 4;
			this.checkBoxValve3.UseVisualStyleBackColor = true;
			// 
			// checkBoxValve2
			// 
			this.checkBoxValve2.AutoSize = true;
			this.checkBoxValve2.Location = new System.Drawing.Point(53, 75);
			this.checkBoxValve2.Name = "checkBoxValve2";
			this.checkBoxValve2.Size = new System.Drawing.Size(15, 14);
			this.checkBoxValve2.TabIndex = 4;
			this.checkBoxValve2.UseVisualStyleBackColor = true;
			// 
			// checkBoxValve1
			// 
			this.checkBoxValve1.AutoSize = true;
			this.checkBoxValve1.Location = new System.Drawing.Point(12, 75);
			this.checkBoxValve1.Name = "checkBoxValve1";
			this.checkBoxValve1.Size = new System.Drawing.Size(15, 14);
			this.checkBoxValve1.TabIndex = 4;
			this.checkBoxValve1.UseVisualStyleBackColor = true;
			// 
			// checkBoxOnOff
			// 
			this.checkBoxOnOff.AutoSize = true;
			this.checkBoxOnOff.Location = new System.Drawing.Point(140, 22);
			this.checkBoxOnOff.Name = "checkBoxOnOff";
			this.checkBoxOnOff.Size = new System.Drawing.Size(71, 16);
			this.checkBoxOnOff.TabIndex = 3;
			this.checkBoxOnOff.Text = "ON/OFF";
			this.checkBoxOnOff.UseVisualStyleBackColor = true;
			// 
			// comboBoxValve
			// 
			this.comboBoxValve.FormattingEnabled = true;
			this.comboBoxValve.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5"});
			this.comboBoxValve.Location = new System.Drawing.Point(11, 20);
			this.comboBoxValve.Name = "comboBoxValve";
			this.comboBoxValve.Size = new System.Drawing.Size(97, 20);
			this.comboBoxValve.TabIndex = 2;
			// 
			// buttonValveSend
			// 
			this.buttonValveSend.Location = new System.Drawing.Point(229, 17);
			this.buttonValveSend.Name = "buttonValveSend";
			this.buttonValveSend.Size = new System.Drawing.Size(62, 23);
			this.buttonValveSend.TabIndex = 1;
			this.buttonValveSend.Text = "Send";
			this.buttonValveSend.UseVisualStyleBackColor = true;
			this.buttonValveSend.Click += new System.EventHandler(this.buttonValveSend_Click);
			// 
			// panel3
			// 
			this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel3.Controls.Add(this.trackBarMfc2);
			this.panel3.Controls.Add(this.buttonMfc2);
			this.panel3.Controls.Add(this.label7);
			this.panel3.Controls.Add(this.labelMfc2);
			this.panel3.Location = new System.Drawing.Point(363, 189);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(389, 51);
			this.panel3.TabIndex = 56;
			// 
			// trackBarMfc2
			// 
			this.trackBarMfc2.LargeChange = 50;
			this.trackBarMfc2.Location = new System.Drawing.Point(53, 5);
			this.trackBarMfc2.Maximum = 500;
			this.trackBarMfc2.Name = "trackBarMfc2";
			this.trackBarMfc2.Size = new System.Drawing.Size(260, 45);
			this.trackBarMfc2.SmallChange = 10;
			this.trackBarMfc2.TabIndex = 20;
			this.trackBarMfc2.TickFrequency = 5;
			this.trackBarMfc2.Scroll += new System.EventHandler(this.trackBarMfc2_Scroll);
			// 
			// buttonMfc2
			// 
			this.buttonMfc2.Location = new System.Drawing.Point(319, 8);
			this.buttonMfc2.Name = "buttonMfc2";
			this.buttonMfc2.Size = new System.Drawing.Size(63, 23);
			this.buttonMfc2.TabIndex = 37;
			this.buttonMfc2.Text = "Send";
			this.buttonMfc2.UseVisualStyleBackColor = true;
			this.buttonMfc2.Click += new System.EventHandler(this.buttonMfc2_Click);
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(7, 6);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(38, 12);
			this.label7.TabIndex = 36;
			this.label7.Text = "MFC3";
			// 
			// labelMfc2
			// 
			this.labelMfc2.AutoSize = true;
			this.labelMfc2.Location = new System.Drawing.Point(20, 24);
			this.labelMfc2.Name = "labelMfc2";
			this.labelMfc2.Size = new System.Drawing.Size(11, 12);
			this.labelMfc2.TabIndex = 36;
			this.labelMfc2.Text = "0";
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel2.Controls.Add(this.trackBarMfc1);
			this.panel2.Controls.Add(this.buttonMfc1);
			this.panel2.Controls.Add(this.label5);
			this.panel2.Controls.Add(this.labelMfc1);
			this.panel2.Location = new System.Drawing.Point(363, 132);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(389, 51);
			this.panel2.TabIndex = 55;
			// 
			// trackBarMfc1
			// 
			this.trackBarMfc1.LargeChange = 50;
			this.trackBarMfc1.Location = new System.Drawing.Point(53, 5);
			this.trackBarMfc1.Maximum = 500;
			this.trackBarMfc1.Name = "trackBarMfc1";
			this.trackBarMfc1.Size = new System.Drawing.Size(260, 45);
			this.trackBarMfc1.SmallChange = 10;
			this.trackBarMfc1.TabIndex = 20;
			this.trackBarMfc1.TickFrequency = 5;
			this.trackBarMfc1.Scroll += new System.EventHandler(this.trackBarMfc1_Scroll);
			// 
			// buttonMfc1
			// 
			this.buttonMfc1.Location = new System.Drawing.Point(319, 6);
			this.buttonMfc1.Name = "buttonMfc1";
			this.buttonMfc1.Size = new System.Drawing.Size(63, 23);
			this.buttonMfc1.TabIndex = 37;
			this.buttonMfc1.Text = "Send";
			this.buttonMfc1.UseVisualStyleBackColor = true;
			this.buttonMfc1.Click += new System.EventHandler(this.buttonMfc1_Click);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(7, 6);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(38, 12);
			this.label5.TabIndex = 36;
			this.label5.Text = "MFC2";
			// 
			// labelMfc1
			// 
			this.labelMfc1.AutoSize = true;
			this.labelMfc1.Location = new System.Drawing.Point(20, 24);
			this.labelMfc1.Name = "labelMfc1";
			this.labelMfc1.Size = new System.Drawing.Size(11, 12);
			this.labelMfc1.TabIndex = 36;
			this.labelMfc1.Text = "0";
			// 
			// panel1
			// 
			this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panel1.Controls.Add(this.trackBarMfc0);
			this.panel1.Controls.Add(this.buttonMfc0);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.labelMfc0);
			this.panel1.Location = new System.Drawing.Point(363, 74);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(389, 51);
			this.panel1.TabIndex = 54;
			// 
			// trackBarMfc0
			// 
			this.trackBarMfc0.LargeChange = 50;
			this.trackBarMfc0.Location = new System.Drawing.Point(53, 5);
			this.trackBarMfc0.Maximum = 500;
			this.trackBarMfc0.Name = "trackBarMfc0";
			this.trackBarMfc0.Size = new System.Drawing.Size(260, 45);
			this.trackBarMfc0.SmallChange = 10;
			this.trackBarMfc0.TabIndex = 20;
			this.trackBarMfc0.TickFrequency = 5;
			this.trackBarMfc0.Scroll += new System.EventHandler(this.trackBarMfc0_Scroll);
			// 
			// buttonMfc0
			// 
			this.buttonMfc0.Location = new System.Drawing.Point(319, 6);
			this.buttonMfc0.Name = "buttonMfc0";
			this.buttonMfc0.Size = new System.Drawing.Size(63, 23);
			this.buttonMfc0.TabIndex = 37;
			this.buttonMfc0.Text = "Send";
			this.buttonMfc0.UseVisualStyleBackColor = true;
			this.buttonMfc0.Click += new System.EventHandler(this.buttonMfc0_Click);
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(7, 6);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(38, 12);
			this.label4.TabIndex = 36;
			this.label4.Text = "MFC1";
			// 
			// labelMfc0
			// 
			this.labelMfc0.AutoSize = true;
			this.labelMfc0.Location = new System.Drawing.Point(20, 24);
			this.labelMfc0.Name = "labelMfc0";
			this.labelMfc0.Size = new System.Drawing.Size(11, 12);
			this.labelMfc0.TabIndex = 36;
			this.labelMfc0.Text = "0";
			// 
			// textBoxDown
			// 
			this.textBoxDown.Location = new System.Drawing.Point(37, 56);
			this.textBoxDown.Name = "textBoxDown";
			this.textBoxDown.Size = new System.Drawing.Size(43, 21);
			this.textBoxDown.TabIndex = 53;
			this.textBoxDown.Text = "60";
			// 
			// textBoxUp
			// 
			this.textBoxUp.Location = new System.Drawing.Point(38, 13);
			this.textBoxUp.Name = "textBoxUp";
			this.textBoxUp.Size = new System.Drawing.Size(43, 21);
			this.textBoxUp.TabIndex = 52;
			this.textBoxUp.Text = "0";
			// 
			// buttonTimerDown
			// 
			this.buttonTimerDown.Location = new System.Drawing.Point(87, 54);
			this.buttonTimerDown.Name = "buttonTimerDown";
			this.buttonTimerDown.Size = new System.Drawing.Size(87, 23);
			this.buttonTimerDown.TabIndex = 51;
			this.buttonTimerDown.Text = "Timer Down";
			this.buttonTimerDown.UseVisualStyleBackColor = true;
			this.buttonTimerDown.Click += new System.EventHandler(this.buttonTimerDown_Click);
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(255, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(20, 12);
			this.label2.TabIndex = 50;
			this.label2.Text = "Up";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(255, 64);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(37, 12);
			this.label1.TabIndex = 49;
			this.label1.Text = "Down";
			// 
			// buttonTimer
			// 
			this.buttonTimer.Location = new System.Drawing.Point(87, 11);
			this.buttonTimer.Name = "buttonTimer";
			this.buttonTimer.Size = new System.Drawing.Size(87, 23);
			this.buttonTimer.TabIndex = 48;
			this.buttonTimer.Text = "Timer Up";
			this.buttonTimer.UseVisualStyleBackColor = true;
			this.buttonTimer.Click += new System.EventHandler(this.buttonTimer_Click_1);
			// 
			// textBoxTickDown
			// 
			this.textBoxTickDown.Location = new System.Drawing.Point(194, 56);
			this.textBoxTickDown.Name = "textBoxTickDown";
			this.textBoxTickDown.Size = new System.Drawing.Size(53, 21);
			this.textBoxTickDown.TabIndex = 47;
			// 
			// textBoxTickUp
			// 
			this.textBoxTickUp.Location = new System.Drawing.Point(194, 13);
			this.textBoxTickUp.Name = "textBoxTickUp";
			this.textBoxTickUp.Size = new System.Drawing.Size(53, 21);
			this.textBoxTickUp.TabIndex = 46;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.textBoxUp);
			this.panel4.Controls.Add(this.textBoxTickUp);
			this.panel4.Controls.Add(this.textBoxTickDown);
			this.panel4.Controls.Add(this.buttonTimer);
			this.panel4.Controls.Add(this.label1);
			this.panel4.Controls.Add(this.label2);
			this.panel4.Controls.Add(this.buttonTimerDown);
			this.panel4.Controls.Add(this.textBoxDown);
			this.panel4.Location = new System.Drawing.Point(363, 353);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(389, 90);
			this.panel4.TabIndex = 77;
			// 
			// timerDown
			// 
			this.timerDown.Interval = 1000;
			// 
			// FormMfc
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(764, 448);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.label11);
			this.Controls.Add(this.label10);
			this.Controls.Add(this.label9);
			this.Controls.Add(this.label8);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.textBoxV);
			this.Controls.Add(this.textBoxP);
			this.Controls.Add(this.textBoxM3);
			this.Controls.Add(this.textBoxM2);
			this.Controls.Add(this.textBoxM1);
			this.Controls.Add(this.textBoxV1);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.buttonSend);
			this.Controls.Add(this.textBoxSend);
			this.Controls.Add(this.richTextBoxLog);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "FormMfc";
			this.Text = "MFC Control";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.panel3.ResumeLayout(false);
			this.panel3.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMfc2)).EndInit();
			this.panel2.ResumeLayout(false);
			this.panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMfc1)).EndInit();
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMfc0)).EndInit();
			this.panel4.ResumeLayout(false);
			this.panel4.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RichTextBox richTextBoxLog;
		private System.Windows.Forms.TextBox textBoxSend;
		private System.Windows.Forms.Timer timerUp;
		private System.Windows.Forms.Button buttonSend;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox textBoxV;
		private System.Windows.Forms.TextBox textBoxP;
		private System.Windows.Forms.TextBox textBoxM3;
		private System.Windows.Forms.TextBox textBoxM2;
		private System.Windows.Forms.TextBox textBoxM1;
		private System.Windows.Forms.TextBox textBoxV1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button buttonValveCheckboxSend;
		private System.Windows.Forms.CheckBox checkBoxValve5;
		private System.Windows.Forms.CheckBox checkBoxValve4;
		private System.Windows.Forms.Label label17;
		private System.Windows.Forms.Label label16;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.CheckBox checkBoxValve3;
		private System.Windows.Forms.CheckBox checkBoxValve2;
		private System.Windows.Forms.CheckBox checkBoxValve1;
		private System.Windows.Forms.CheckBox checkBoxOnOff;
		private System.Windows.Forms.ComboBox comboBoxValve;
		private System.Windows.Forms.Button buttonValveSend;
		private System.Windows.Forms.Panel panel3;
		private System.Windows.Forms.TrackBar trackBarMfc2;
		private System.Windows.Forms.Button buttonMfc2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label labelMfc2;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.TrackBar trackBarMfc1;
		private System.Windows.Forms.Button buttonMfc1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label labelMfc1;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TrackBar trackBarMfc0;
		private System.Windows.Forms.Button buttonMfc0;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label labelMfc0;
		private System.Windows.Forms.TextBox textBoxDown;
		private System.Windows.Forms.TextBox textBoxUp;
		private System.Windows.Forms.Button buttonTimerDown;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button buttonTimer;
		private System.Windows.Forms.TextBox textBoxTickDown;
		private System.Windows.Forms.TextBox textBoxTickUp;
		private System.Windows.Forms.Panel panel4;
		private System.Windows.Forms.Timer timerDown;
	}
}