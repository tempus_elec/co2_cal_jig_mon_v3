﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class FormMfc : Form
	{
		Form1 frmMfc;

		bool counterFlag = false;
		bool DownCounterFlag = false;

		public FormMfc()
		{
			InitializeComponent();
		}

		public FormMfc(Form1 _form)
		{
			InitializeComponent();
			frmMfc = _form;
			
			FormClosing += new FormClosingEventHandler(FormMfc_FormClosing);
			MfcStart(1000);
		}

		private void FormMfc_FormClosing(Object sender, FormClosingEventArgs e)
		{
			MfcClose();
		}

		public void LOG(string str)
		{
			now = DateTime.Now;
			str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
			if (richTextBoxLog.InvokeRequired)
			{
				richTextBoxLog.Invoke(new MethodInvoker(delegate ()
				{
					//richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
					richTextBoxLog.AppendText(str);
					richTextBoxLog.ScrollToCaret();
				}));
			}
			else
			{
				//richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
				richTextBoxLog.AppendText(str);
				richTextBoxLog.ScrollToCaret();
			}
		}

		public void ERR(string str)
		{
			now = DateTime.Now;
			str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
			if (richTextBoxLog.InvokeRequired)
			{
				richTextBoxLog.Invoke(new MethodInvoker(delegate ()
				{
					richTextBoxLog.SelectionColor = System.Drawing.Color.Red;
					richTextBoxLog.AppendText(str);
					richTextBoxLog.ScrollToCaret();
					richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
				}));
			}
			else
			{
				richTextBoxLog.SelectionColor = System.Drawing.Color.Red;
				richTextBoxLog.AppendText(str);
				richTextBoxLog.ScrollToCaret();
				richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
			}
		}

		public string CallerName([CallerMemberName] string callerName = "")
		{
			return callerName;
		}

		private void buttonExit_Click(object sender, EventArgs e)
		{
			MfcClose();
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			CounterTick();
		}

		private UInt32 timeLeft;

		public void SetCounter(UInt32 _timeLeft)
		{
			timeLeft = _timeLeft;
		}

		public UInt32 GetCounter()
		{
			return timeLeft;
		}

		public void CounterTick()
		{
			if (timeLeft > 0)
			{
				timeLeft++;
				UpdateCounter(timeLeft);
			}
		}

		public void UpdateCounter(UInt32 timeLeft)
		{
			
		}

		public void CounterStart()
		{
			timerUp.Start();
			timeLeft = 0;
		}

		public void CounterStop()
		{
			timerUp.Stop();
			timeLeft = 0;
		}

		private void buttonTimer_Click(object sender, EventArgs e)
		{
			if (counterFlag == false)
			{
				LOG("Timer Start\n");
				CounterStart();
				counterFlag = true;
			}
			else
			{
				LOG("Timer Stop\n");
				CounterStop();
				counterFlag = false;
			}
		}

		private void buttonSend_Click(object sender, EventArgs e)
		{
			ClientSend(textBoxSend.Text.ToString());
		}

		private void buttonTimer_Click_1(object sender, EventArgs e)
		{
			if (counterFlag == false)
			{
				CounterUpStart();
				counterFlag = true;
			}
			else
			{
				CounterUpStop();
				counterFlag = false;
			}
		}

		private void buttonTimerDown_Click(object sender, EventArgs e)
		{
			if (DownCounterFlag == false)
			{
				CounterDownStart();
				DownCounterFlag = true;
			}
			else
			{
				CounterDownStop();
				DownCounterFlag = false;
			}
		}

		private void buttonValveCheckboxSend_Click(object sender, EventArgs e)
		{
			string str = "SET," + labelMfc0.Text + "," + labelMfc1.Text + "," + labelMfc2.Text + ",0,";

			if (checkBoxValve1.Checked) str = str + "1";
			else str = str + "0";

			if (checkBoxValve2.Checked) str = str + "1";
			else str = str + "0";

			if (checkBoxValve3.Checked) str = str + "1";
			else str = str + "0";

			if (checkBoxValve4.Checked) str = str + "1";
			else str = str + "0";

			if (checkBoxValve5.Checked) str = str + "1";
			else str = str + "0";

			ClientSend(str);
		}

		private void button1_Click(object sender, EventArgs e)
		{
			ClientSend("CLR");
		}

		private void buttonMfc0_Click(object sender, EventArgs e)
		{
			ClientSend("MFC,1," + labelMfc0.Text);
		}

		private void buttonMfc1_Click(object sender, EventArgs e)
		{
			ClientSend("MFC,2," + labelMfc1.Text);
		}

		private void buttonMfc2_Click(object sender, EventArgs e)
		{
			ClientSend("MFC,3," + labelMfc2.Text);
		}

		private void buttonValveSend_Click(object sender, EventArgs e)
		{
			if (comboBoxValve.SelectedIndex == -1)
			{
				MessageBox.Show("Please choose valve number\n");
				return;
			}

			string str = "VLV," + comboBoxValve.SelectedItem.ToString();
			if (checkBoxOnOff.Checked)
				str = str + ",1";
			else
				str = str + ",0";

			ClientSend(str);
		}

		private void trackBarMfc0_Scroll(object sender, EventArgs e)
		{
			labelMfc0.Text = Convert.ToString(trackBarMfc0.Value);
		}

		private void trackBarMfc1_Scroll(object sender, EventArgs e)
		{
			labelMfc1.Text = Convert.ToString(trackBarMfc1.Value);
		}

		private void trackBarMfc2_Scroll(object sender, EventArgs e)
		{
			labelMfc2.Text = Convert.ToString(trackBarMfc2.Value);
		}
	}
}
