﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class FormMfc : Form
	{
		private UInt32 timeLeftUp;
		private UInt32 timeLeftDown;

		public void SetUpCounter(UInt32 _timeLeft)
		{
			timeLeftUp = _timeLeft;
		}

		public void SetDownCounter(UInt32 _timeLeft)
		{
			timeLeftDown = _timeLeft;
		}

		public UInt32 GetUpCounter()
		{
			return timeLeftUp;
		}

		public UInt32 GetDownCounter()
		{
			return timeLeftDown;
		}

		public void CounterUpTick()
		{
			if (!(timeLeftUp < 0))
			{
				timeLeftUp++;
				UpdateUpCounter(timeLeftUp);
			}
		}

		public void CounterDownTick()
		{
			if (timeLeftDown > 0)
			{
				timeLeftDown--;
				UpdateDownCounter(timeLeftDown);
				if (timeLeftDown == 0)
					CounterDownStop();
			}
		}

		public void UpdateUpCounter(UInt32 timeLeft)
		{
			textBoxTickUp.Text = timeLeft.ToString();
		}

		public void UpdateDownCounter(UInt32 timeLeft)
		{
			textBoxTickDown.Text = timeLeft.ToString();
		}

		public void CounterUpStart()
		{
			timerUp.Start();
			SetUpCounter(Convert.ToUInt32(textBoxUp.Text));
			LOG("Up Timer Start\n");
		}

		public void CounterDownStart()
		{
			timerDown.Start();
			SetDownCounter(Convert.ToUInt32(textBoxDown.Text));
			LOG("Down Timer Start\n");
		}

		public void CounterUpStop()
		{
			timerUp.Stop();
			timeLeftUp = 0;
			LOG("Up Timer Stop\n");
		}

		public void CounterDownStop()
		{
			timerDown.Stop();
			timeLeftDown = 0;
			LOG("Down Timer Stop\n");
		}
	}
}
