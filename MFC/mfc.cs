﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class FormMfc : Form
	{
		AutoResetEvent mfcEvent;

		public byte[] getByteClient = new byte[64];
		public byte[] setByteClient = new byte[64];

		const int TIMEOUT = 1000;
		string DEST_IP = "127.0.0.1";
		public const int sPort = 5000;

		Thread client = null;
		public Socket clientSock;

		bool clientAlive = true;

		UInt16 targetPPM = 0;

		DateTime now;

		public void MfcStart(UInt16 ppm)
		{
			targetPPM = ppm;

			LOG("Start ClientTask\n");
			client = new Thread(MfcTask);
			client.Start();
		}

		public void MfcTask()
		{
			string getstring = null;
			string[] spData;

			IPAddress serverIP = IPAddress.Parse(DEST_IP);
			IPEndPoint serverEndPoint = new IPEndPoint(serverIP, sPort);
			int getValueLength = 0;

			if (mfcEvent == null)
				mfcEvent = new AutoResetEvent(false);

			clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			clientSock.ReceiveTimeout = TIMEOUT;
			clientSock.SendTimeout = TIMEOUT;
			clientAlive = true;

			/**************************************************************
			 * Connect to server
			 *************************************************************/
			LOG("------------------------------\n");
			LOG(" Connecting to server...\n");
			LOG("------------------------------\n");

			while (clientAlive)
			{
				try
				{
					clientSock.Connect(serverEndPoint);
					if (clientSock.Connected)
					{
						LOG(serverIP + ", connected\n");
						break;
					}
				}
				catch (SocketException ex)
				{
					if (ex.SocketErrorCode == (SocketError)10061)   //ConnectionRefused
					{
						//LOG("Server is not ready\n");
						LOG(".");
					}
					else
					{
						LOG(ex.Message + "\n");
					}
					Thread.Sleep(2000);
				}
				catch (Exception ex)
				{
					LOG(ex.Message + "\n");
					Thread.Sleep(1000);
				}
			}

			/**************************************************************
			 * Receive data from server
			 *************************************************************/
			while (clientAlive)
			{
				try
				{
					clientSock.Receive(getByteClient, 0, getByteClient.Length, SocketFlags.None);
					getValueLength = byteArrayDefrag(getByteClient) + 1;
					getstring = Encoding.UTF7.GetString(getByteClient, 0, getValueLength);
					getstring = getstring.Replace("\n\r", "");
					LOG("Rx : " + getstring + ", Len : " + getValueLength + "\n");

					spData = getstring.Split(',');
					if(spData[0] == "VALUE")
					{
						byte[] valve = Encoding.UTF8.GetBytes(spData[5]);

						if (textBoxV1.InvokeRequired)
						{
							textBoxV1.Invoke(new MethodInvoker(delegate ()
							{
								if (valve[0] == 48)
									textBoxV1.BackColor = System.Drawing.Color.White;
								else
									textBoxV1.BackColor = System.Drawing.Color.Blue;
							}));
						}
						else
						{
							if (valve[0] == 48)
								textBoxV1.BackColor = System.Drawing.Color.White;
							else
								textBoxV1.BackColor = System.Drawing.Color.Blue;
						}

						if (textBoxM1.InvokeRequired)
						{
							textBoxM1.Invoke(new MethodInvoker(delegate ()
							{
								if (valve[1] == 48)
									textBoxM1.BackColor = System.Drawing.Color.White;
								else
									textBoxM1.BackColor = System.Drawing.Color.Blue;
							}));
						}
						else
						{
							if (valve[1] == 48)
								textBoxM1.BackColor = System.Drawing.Color.White;
							else
								textBoxM1.BackColor = System.Drawing.Color.Blue;
						}

						if (textBoxM2.InvokeRequired)
						{
							textBoxM2.Invoke(new MethodInvoker(delegate ()
							{
								if (valve[2] == 48)
									textBoxM2.BackColor = System.Drawing.Color.White;
								else
									textBoxM2.BackColor = System.Drawing.Color.Blue;
							}));
						}
						else
						{
							if (valve[2] == 48)
								textBoxM2.BackColor = System.Drawing.Color.White;
							else
								textBoxM2.BackColor = System.Drawing.Color.Blue;
						}
#if false
						if (textBoxM3.InvokeRequired)
						{
							textBoxM3.Invoke(new MethodInvoker(delegate ()
							{
								if (valve[3] == 48)
									textBoxM3.BackColor = System.Drawing.Color.White;
								else
									textBoxM3.BackColor = System.Drawing.Color.Blue;
							}));
						}
						else
						{
							if (valve[3] == 48)
								textBoxM3.BackColor = System.Drawing.Color.White;
							else
								textBoxM3.BackColor = System.Drawing.Color.Blue;
						}
#endif
						if (textBoxP.InvokeRequired)
						{
							textBoxP.Invoke(new MethodInvoker(delegate ()
							{
								if (valve[3] == 48)
									textBoxP.BackColor = System.Drawing.Color.White;
								else
									textBoxP.BackColor = System.Drawing.Color.Blue;
							}));
						}
						else
						{
							if (valve[3] == 48)
								textBoxP.BackColor = System.Drawing.Color.White;
							else
								textBoxP.BackColor = System.Drawing.Color.Blue;
						}
#if true
						if (textBoxV.InvokeRequired)
						{
							textBoxV.Invoke(new MethodInvoker(delegate ()
							{
								if (valve[4] == 48)
									textBoxV.BackColor = System.Drawing.Color.White;
								else
									textBoxV.BackColor = System.Drawing.Color.Blue;
							}));
						}
						else
						{
							if (valve[4] == 48)
								textBoxV.BackColor = System.Drawing.Color.White;
							else
								textBoxV.BackColor = System.Drawing.Color.Blue;
						}						
#endif
					}
				}
				catch (SocketException ex)
				{
					if (ex.SocketErrorCode == (SocketError)(10060)) // TimedOut
					{

					}
					else if (ex.SocketErrorCode == (SocketError)(10054)) // ConnectionReset
					{
						LOG("Connection has disconnected, try again...\n");
						//while (true)
						{
							try
							{
								clientSock.Connect(serverEndPoint);
								if (clientSock.Connected)
								{
									LOG(serverIP + "에 연결 되었습니다.\n");
									break;
								}
							}
							catch (SocketException x)
							{
								if (x.SocketErrorCode == (SocketError)10061)   //ConnectionRefused
								{
									LOG("Server is not ready\n");
								}
								else
								{
									LOG(x.Message + "\n");
								}
								Thread.Sleep(1000);
							}
							catch (Exception x)
							{
								LOG(x.Message + "\n");
								Thread.Sleep(1000);
							}
						}
					}
					else
					{
						LOG("Client Rcv error..." + ex.Message + ", ErrCode " + ex.SocketErrorCode + "\n");
					}
				}
			}
			//LOG("Client Exit\n");
		}

		public int byteArrayDefrag(byte[] sData)
		{
			int endLength = 0;

			for (int i = 0; i < sData.Length; i++)
			{
				if ((byte)sData[i] != (byte)0)
				{
					endLength = i;
				}
			}

			return endLength;
		}

		public byte[] StrToByteArray(string str)
		{
			Dictionary<string, byte> hexindex = new Dictionary<string, byte>();
			for (int i = 0; i <= 255; i++)
				hexindex.Add(i.ToString("X2"), (byte)i);

			List<byte> hexres = new List<byte>();
			for (int i = 0; i < str.Length; i += 2)
				hexres.Add(hexindex[str.Substring(i, 2)]);

			return hexres.ToArray();
		}

		public void MfcClose()
		{
			clientAlive = false;
			Thread.Sleep(3000);
			client = null;
		}

		public void ClientSend(string str)
		{
			byte[] sendBytes = Encoding.UTF7.GetBytes(str + "\r\n");
			if(clientSock.Connected == false)
			{
				ERR("Please connects socket\n");
				return;
			}
			clientSock.Send(sendBytes, sendBytes.Length, SocketFlags.None);
			Thread.Sleep(500);
			sendBytes = Encoding.UTF7.GetBytes("GETS\r\n");
			clientSock.Send(sendBytes, sendBytes.Length, SocketFlags.None);
		}
	}
}
