﻿namespace CO2_CAL_JIG_Mon_V3
{
	partial class Form_MFCMini
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
			this.buttonCo2Set = new System.Windows.Forms.Button();
			this.buttonCo2On = new System.Windows.Forms.Button();
			this.buttonVentOn = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.trackBarMfcMini = new System.Windows.Forms.TrackBar();
			this.labelMfcMini = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textBoxTimer = new System.Windows.Forms.TextBox();
			this.buttonMfcMiniOff = new System.Windows.Forms.Button();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.buttonCo2Off = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.timer2 = new System.Windows.Forms.Timer(this.components);
			this.buttonTmrOn = new System.Windows.Forms.Button();
			this.buttonTmrOff = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMfcMini)).BeginInit();
			this.SuspendLayout();
			// 
			// richTextBoxLog
			// 
			this.richTextBoxLog.Location = new System.Drawing.Point(12, 66);
			this.richTextBoxLog.Name = "richTextBoxLog";
			this.richTextBoxLog.Size = new System.Drawing.Size(331, 96);
			this.richTextBoxLog.TabIndex = 0;
			this.richTextBoxLog.Text = "";
			// 
			// buttonCo2Set
			// 
			this.buttonCo2Set.Location = new System.Drawing.Point(266, 8);
			this.buttonCo2Set.Name = "buttonCo2Set";
			this.buttonCo2Set.Size = new System.Drawing.Size(61, 23);
			this.buttonCo2Set.TabIndex = 2;
			this.buttonCo2Set.Text = "Set";
			this.buttonCo2Set.UseVisualStyleBackColor = true;
			this.buttonCo2Set.Click += new System.EventHandler(this.buttonCo2Set_Click);
			// 
			// buttonCo2On
			// 
			this.buttonCo2On.Location = new System.Drawing.Point(12, 168);
			this.buttonCo2On.Name = "buttonCo2On";
			this.buttonCo2On.Size = new System.Drawing.Size(75, 23);
			this.buttonCo2On.TabIndex = 10;
			this.buttonCo2On.Text = "Co2 On";
			this.buttonCo2On.Click += new System.EventHandler(this.buttonCo2On_Click);
			// 
			// buttonVentOn
			// 
			this.buttonVentOn.Location = new System.Drawing.Point(94, 168);
			this.buttonVentOn.Name = "buttonVentOn";
			this.buttonVentOn.Size = new System.Drawing.Size(75, 23);
			this.buttonVentOn.TabIndex = 4;
			this.buttonVentOn.Text = "VENT On";
			this.buttonVentOn.UseVisualStyleBackColor = true;
			this.buttonVentOn.Click += new System.EventHandler(this.buttonVentOn_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.trackBarMfcMini);
			this.panel1.Controls.Add(this.labelMfcMini);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.buttonCo2Set);
			this.panel1.Location = new System.Drawing.Point(13, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(330, 50);
			this.panel1.TabIndex = 5;
			// 
			// trackBarMfcMini
			// 
			this.trackBarMfcMini.LargeChange = 50;
			this.trackBarMfcMini.Location = new System.Drawing.Point(59, 3);
			this.trackBarMfcMini.Maximum = 500;
			this.trackBarMfcMini.Name = "trackBarMfcMini";
			this.trackBarMfcMini.Size = new System.Drawing.Size(201, 45);
			this.trackBarMfcMini.SmallChange = 10;
			this.trackBarMfcMini.TabIndex = 21;
			this.trackBarMfcMini.TickFrequency = 5;
			this.trackBarMfcMini.Value = 100;
			this.trackBarMfcMini.Scroll += new System.EventHandler(this.trackBarMfcMini_Scroll);
			// 
			// labelMfcMini
			// 
			this.labelMfcMini.AutoSize = true;
			this.labelMfcMini.Location = new System.Drawing.Point(19, 30);
			this.labelMfcMini.Name = "labelMfcMini";
			this.labelMfcMini.Size = new System.Drawing.Size(23, 12);
			this.labelMfcMini.TabIndex = 4;
			this.labelMfcMini.Text = "100";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(29, 12);
			this.label1.TabIndex = 3;
			this.label1.Text = "CO2";
			// 
			// textBoxTimer
			// 
			this.textBoxTimer.Location = new System.Drawing.Point(261, 197);
			this.textBoxTimer.Name = "textBoxTimer";
			this.textBoxTimer.Size = new System.Drawing.Size(79, 21);
			this.textBoxTimer.TabIndex = 6;
			this.textBoxTimer.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
			// 
			// buttonMfcMiniOff
			// 
			this.buttonMfcMiniOff.Location = new System.Drawing.Point(261, 168);
			this.buttonMfcMiniOff.Name = "buttonMfcMiniOff";
			this.buttonMfcMiniOff.Size = new System.Drawing.Size(79, 23);
			this.buttonMfcMiniOff.TabIndex = 7;
			this.buttonMfcMiniOff.Text = "ALL OFF";
			this.buttonMfcMiniOff.UseVisualStyleBackColor = true;
			this.buttonMfcMiniOff.Click += new System.EventHandler(this.buttonMfcMiniOff_Click);
			// 
			// timer1
			// 
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.timer1_tick);
			// 
			// buttonCo2Off
			// 
			this.buttonCo2Off.Location = new System.Drawing.Point(12, 197);
			this.buttonCo2Off.Name = "buttonCo2Off";
			this.buttonCo2Off.Size = new System.Drawing.Size(75, 23);
			this.buttonCo2Off.TabIndex = 8;
			this.buttonCo2Off.Text = "CO2 Off";
			this.buttonCo2Off.UseVisualStyleBackColor = true;
			this.buttonCo2Off.Click += new System.EventHandler(this.buttonCo2Off_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(94, 197);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(75, 23);
			this.button2.TabIndex = 9;
			this.button2.Text = "VENT Off";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// timer2
			// 
			this.timer2.Interval = 2000;
			this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
			// 
			// buttonTmrOn
			// 
			this.buttonTmrOn.Location = new System.Drawing.Point(180, 168);
			this.buttonTmrOn.Name = "buttonTmrOn";
			this.buttonTmrOn.Size = new System.Drawing.Size(75, 23);
			this.buttonTmrOn.TabIndex = 11;
			this.buttonTmrOn.Text = "TMR On";
			this.buttonTmrOn.UseVisualStyleBackColor = true;
			this.buttonTmrOn.Click += new System.EventHandler(this.buttonTmrOn_Click);
			// 
			// buttonTmrOff
			// 
			this.buttonTmrOff.Location = new System.Drawing.Point(180, 196);
			this.buttonTmrOff.Name = "buttonTmrOff";
			this.buttonTmrOff.Size = new System.Drawing.Size(75, 23);
			this.buttonTmrOff.TabIndex = 12;
			this.buttonTmrOff.Text = "TMR Off";
			this.buttonTmrOff.UseVisualStyleBackColor = true;
			this.buttonTmrOff.Click += new System.EventHandler(this.buttonTmrOff_Click);
			// 
			// Form_MFCMini
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(360, 224);
			this.Controls.Add(this.buttonTmrOff);
			this.Controls.Add(this.buttonTmrOn);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.buttonCo2Off);
			this.Controls.Add(this.buttonMfcMiniOff);
			this.Controls.Add(this.textBoxTimer);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.buttonVentOn);
			this.Controls.Add(this.buttonCo2On);
			this.Controls.Add(this.richTextBoxLog);
			this.Name = "Form_MFCMini";
			this.Text = "Form_MFCMini";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.trackBarMfcMini)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.RichTextBox richTextBoxLog;
		private System.Windows.Forms.Button buttonCo2Set;
		private System.Windows.Forms.Button buttonCo2On;
		private System.Windows.Forms.Button buttonVentOn;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TextBox textBoxTimer;
		private System.Windows.Forms.Label labelMfcMini;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TrackBar trackBarMfcMini;
		private System.Windows.Forms.Button buttonMfcMiniOff;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Button buttonCo2Off;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Timer timer2;
		private System.Windows.Forms.Button buttonTmrOn;
		private System.Windows.Forms.Button buttonTmrOff;
	}
}