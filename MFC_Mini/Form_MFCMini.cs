﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form_MFCMini : Form
	{
		Form1 frmMfcMini;
		UInt32 timerTick = 0;

		public Form_MFCMini()
		{
			InitializeComponent();
		}

		public Form_MFCMini(Form1 _form)
		{
			InitializeComponent();
			frmMfcMini = _form;

			FormClosing += new FormClosingEventHandler(FormMfcMini_FormClosing);
			//timer2.Start();
			MfcMiniStart(1000);
		}

		private void FormMfcMini_FormClosing(Object sender, FormClosingEventArgs e)
		{
			timer2.Stop();
			MfcMiniClose();
		}

		public void LOG(string str)
		{
			try
			{
				now = DateTime.Now;
				str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
				if (richTextBoxLog.InvokeRequired)
				{
					richTextBoxLog.Invoke(new MethodInvoker(delegate ()
					{
						//richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
						richTextBoxLog.AppendText(str);
						richTextBoxLog.ScrollToCaret();
					}));
				}
				else
				{
					//richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
					richTextBoxLog.AppendText(str);
					richTextBoxLog.ScrollToCaret();
				}
			}
			catch(Exception ex)
			{
                
			}
		}

		private void trackBarMfcMini_Scroll(object sender, EventArgs e)
		{
			labelMfcMini.Text = Convert.ToString(trackBarMfcMini.Value);
		}

		private void buttonCo2Set_Click(object sender, EventArgs e)
		{
			ClientMiniSend("MFC,2," + labelMfcMini.Text);
		}

		private void buttonMfcMiniOff_Click(object sender, EventArgs e)
		{
			ClientMiniSend("CLR");

			textBoxTimer.Text = "0 sec";
			timer1.Stop();
		}

		private void buttonCo2On_Click(object sender, EventArgs e)
		{
			string str = "SET,0," + labelMfcMini.Text + ",0,0,10010";
			ClientMiniSend(str);
			//ClientMiniSend("VLV, 4, 1");
			LOG("CO2 feeding...\n");

			timerTick = 0;
			timer1.Start();
		}
		
		private void buttonCo2Off_Click(object sender, EventArgs e)
		{
			string str = "SET,0,0,0,0,00000";
			ClientMiniSend(str);
			//ClientMiniSend("VLV, 4, 0");
			LOG("CO2 stopping...\n");
			
			//textBoxTimer.Text = "0 sec";
			timer1.Stop();
		}

		private void buttonCo2Stop_Click(object sender, EventArgs e)
		{
			string str = "SET,0,10,0,0,11000";
			ClientMiniSend(str);
			LOG("Venting...\n");
		}

		private void timer1_tick(object sender, EventArgs e)
		{
			timerTick++;
			textBoxTimer.Text = timerTick.ToString() + " sec";
		}

		private void button2_Click(object sender, EventArgs e)
		{
			string str = "SET,0,0,0,0,00000";
			ClientMiniSend(str);
			LOG("Vent Off...\n");

			//textBoxTimer.Text = "0 sec";
			timer1.Stop();
		}

		private void buttonVentOn_Click(object sender, EventArgs e)
		{
			string str = "SET,0,0,0,0,11001";
			ClientMiniSend(str);
			LOG("Venting...\n");

			timerTick = 0;
			timer1.Start();
		}

		private void timer2_Tick(object sender, EventArgs e)
		{
			frmMfcMini.request_ref();
		}

		private void buttonTmrOn_Click(object sender, EventArgs e)
		{
			timer2.Start();
		}

		private void buttonTmrOff_Click(object sender, EventArgs e)
		{
			timer2.Stop();
		}
	}
}
