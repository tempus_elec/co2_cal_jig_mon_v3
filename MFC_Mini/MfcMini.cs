﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form_MFCMini : Form
	{
		AutoResetEvent mfcMiniEvent;

		public byte[] getByteClient = new byte[64];
		public byte[] setByteClient = new byte[64];

		const int TIMEOUT = 1000;
		string DEST_IP = "127.0.0.1";
		public const int sPort = 5000;

		Thread client = null;
		public Socket clientSock;

		bool clientAlive = true;

		UInt16 targetPPM = 0;

		DateTime now;

		public void MfcMiniStart(UInt16 ppm)
		{
			targetPPM = ppm;

			LOG("Start ClientTask\n");
			client = new Thread(MfcMiniTask);
			client.Start();
		}

		public void MfcMiniTask()
		{
			string getstring = null;

			IPAddress serverIP = IPAddress.Parse(DEST_IP);
			IPEndPoint serverEndPoint = new IPEndPoint(serverIP, sPort);
			int getValueLength = 0;

			if (mfcMiniEvent == null)
				mfcMiniEvent = new AutoResetEvent(false);

			clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			clientSock.ReceiveTimeout = TIMEOUT;
			clientSock.SendTimeout = TIMEOUT;
			clientAlive = true;

			/**************************************************************
			 * Connect to server
			 *************************************************************/
			LOG("------------------------------\n");
			LOG(" Connecting to server...\n");
			LOG("------------------------------\n");

			while (clientAlive)
			{
				try
				{
					clientSock.Connect(serverEndPoint);
					if (clientSock.Connected)
					{
						LOG(serverIP + ", connected\n");
						break;
					}
					LOG("Connecting...\n");
				}
				catch (Exception ex)
				{
					LOG(ex.Message + "\n");
				}
				Thread.Sleep(1000);
			}

			/**************************************************************
			 * Receive data from server
			 *************************************************************/
			while (clientAlive)
			{
				try
				{
					clientSock.Receive(getByteClient, 0, getByteClient.Length, SocketFlags.None);
					getValueLength = byteArrayDefrag(getByteClient) + 1;
					getstring = Encoding.UTF7.GetString(getByteClient, 0, getValueLength);
					getstring = getstring.Replace("\n\r", "");
					LOG("Rx : " + getstring + ", Len : " + getValueLength + "\n");
#if false
					spData = getstring.Split(',');
					if (spData[0] == "VALUE")
					{
						byte[] valve = Encoding.UTF8.GetBytes(spData[5]);
						
					}
#endif
				}
				catch (SocketException ex)
				{
					if (ex.SocketErrorCode == (SocketError)(10060)) // TimedOut
					{

					}
					else if (ex.SocketErrorCode == (SocketError)(10054)) // ConnectionReset
					{
						LOG("Connection has disconnected, try again...\n");
						//while (true)
						{
							try
							{
								clientSock.Connect(serverEndPoint);
								if (clientSock.Connected)
								{
									LOG(serverIP + "에 연결 되었습니다.\n");
									break;
								}
							}
							catch (SocketException x)
							{
								if (x.SocketErrorCode == (SocketError)10061)   //ConnectionRefused
								{
									LOG("Server is not ready\n");
								}
								else
								{
									LOG(x.Message + "\n");
								}
								Thread.Sleep(1000);
							}
							catch (Exception x)
							{
								LOG(x.Message + "\n");
								Thread.Sleep(1000);
							}
						}
					}
					else
					{
						LOG("Client Rcv error..." + ex.Message + ", ErrCode " + ex.SocketErrorCode + "\n");
					}
				}
			}
		}

		public void MfcMiniClose()
		{
			ClientMiniSend("CLR");

			clientAlive = false;
			Thread.Sleep(3000);
			client = null;
		}

		public int byteArrayDefrag(byte[] sData)
		{
			int endLength = 0;

			for (int i = 0; i < sData.Length; i++)
			{
				if ((byte)sData[i] != (byte)0)
				{
					endLength = i;
				}
			}

			return endLength;
		}

		public byte[] StrToByteArray(string str)
		{
			Dictionary<string, byte> hexindex = new Dictionary<string, byte>();
			for (int i = 0; i <= 255; i++)
				hexindex.Add(i.ToString("X2"), (byte)i);

			List<byte> hexres = new List<byte>();
			for (int i = 0; i < str.Length; i += 2)
				hexres.Add(hexindex[str.Substring(i, 2)]);

			return hexres.ToArray();
		}

		public void ClientMiniSend(string str)
		{
			byte[] sendBytes = Encoding.UTF7.GetBytes(str + "\r\n");
			try
			{
				if (clientSock.Connected == false)
				{
					LOG("Please connects socket\n");
					return;
				}
				clientSock.Send(sendBytes, sendBytes.Length, SocketFlags.None);
				Thread.Sleep(500);
				sendBytes = Encoding.UTF7.GetBytes("GETS\r\n");
				clientSock.Send(sendBytes, sendBytes.Length, SocketFlags.None);
			}
			catch(Exception ex)
			{
				LOG(ex.Message);
			}
		}
	}
}
