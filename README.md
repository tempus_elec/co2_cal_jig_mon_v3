# README #

CO2 Calibration JIG용으로 작업하는 PC용 Calibration 및 test tool입니다.
Module 20개를 테스트 하려다 보니 IO가 많아져서 208pin F767 을 적용하게 되어서 버전이 3.0에서 시작한다.
(원래는 v2.0으로 10가 target으로 준비했었음)


### What is this repository for? ###

* CO2 Calibration을 위한 PC용 tool
* Visual Studio 2017에서 C#을 이용하여 작업 함
* F767 MCU가 들어가는 PCB와 CDC를 이용하여 통신을 한다. CAL_JIG PCB없이 standalone동작은 안된다


### How do I get set up? ###

* Target HW : Tempus CO2 module calibration JIG v3.0
* Build environment : Visual Studio Community 2017 C#
* Connect : USB CDC
* Target FW : starts from v3.0
