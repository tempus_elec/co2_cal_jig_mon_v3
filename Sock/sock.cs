﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		AutoResetEvent mfcMiniEvent;

		public byte[] getByteClient = new byte[64];
		public byte[] setByteClient = new byte[64];

		const int TIMEOUT = 1000;
		string DEST_IP = "127.0.0.1";
		public const int sPort = 5000;

		Thread client = null;
		public Socket clientSock;

		bool clientAlive = true;
		

		public void SocketStart()
		{
			SOCK_LOG("Start SocketTask\n");
			client = new Thread(SocketTask);
			client.Start();
		}

		public void SocketStop()
		{
			SOCK_LOG("Leaving Socket\n");
			SocketSend("CLR");

			clientAlive = false;
			Thread.Sleep(3000);
			client = null;
			SOCK_LOG("Bye~~\n");
		}

		public void SocketTask()
		{
			string getstring = null;

			IPAddress serverIP = IPAddress.Parse(DEST_IP);
			IPEndPoint serverEndPoint = new IPEndPoint(serverIP, sPort);
			int getValueLength = 0;

			if (mfcMiniEvent == null)
				mfcMiniEvent = new AutoResetEvent(false);

			clientSock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			clientSock.ReceiveTimeout = TIMEOUT;
			clientSock.SendTimeout = TIMEOUT;
			clientAlive = true;

			/**************************************************************
			 * Connect to server
			 *************************************************************/
			SOCK_LOG("------------------------------\n");
			SOCK_LOG(" Connecting to server...\n");
			SOCK_LOG("------------------------------\n");

			while (clientAlive)
			{
				try
				{
					clientSock.Connect(serverEndPoint);
					if (clientSock.Connected)
					{
						SOCK_LOG(serverIP + ", connected\n");
						break;
					}
					SOCK_LOG("Connecting...\n");
				}
				catch (Exception ex)
				{
					SOCK_LOG(ex.Message + "\n");
				}
				Thread.Sleep(1000);
			}

			/**************************************************************
			 * Receive data from server
			 *************************************************************/
			while (clientAlive)
			{
				try
				{
					clientSock.Receive(getByteClient, 0, getByteClient.Length, SocketFlags.None);
					getValueLength = byteArrayDefrag(getByteClient) + 1;
					getstring = Encoding.UTF7.GetString(getByteClient, 0, getValueLength);
					getstring = getstring.Replace("\n\r", "");
					SOCK_LOG("Rx : " + getstring + ", Len : " + getValueLength + "\n");
#if false
					spData = getstring.Split(',');
					if (spData[0] == "VALUE")
					{
						byte[] valve = Encoding.UTF8.GetBytes(spData[5]);
						
					}
#endif
				}
				catch (SocketException ex)
				{
					if (ex.SocketErrorCode == (SocketError)(10060)) // TimedOut
					{

					}
					else if (ex.SocketErrorCode == (SocketError)(10054)) // ConnectionReset
					{
						SOCK_LOG("Connection has disconnected, try again...\n");
						//while (true)
						{
							try
							{
								clientSock.Connect(serverEndPoint);
								if (clientSock.Connected)
								{
									SOCK_LOG(serverIP + "에 연결 되었습니다.\n");
									break;
								}
							}
							catch (SocketException x)
							{
								if (x.SocketErrorCode == (SocketError)10061)   //ConnectionRefused
								{
									SOCK_LOG("Server is not ready\n");
								}
								else
								{
									SOCK_LOG(x.Message + "\n");
								}
								Thread.Sleep(1000);
							}
							catch (Exception x)
							{
								SOCK_LOG(x.Message + "\n");
								Thread.Sleep(1000);
							}
						}
					}
					else
					{
						SOCK_LOG("Client Rcv error..." + ex.Message + ", ErrCode " + ex.SocketErrorCode + "\n");
					}
				}
			}
		}

		public int byteArrayDefrag(byte[] sData)
		{
			int endLength = 0;

			for (int i = 0; i < sData.Length; i++)
			{
				if ((byte)sData[i] != (byte)0)
				{
					endLength = i;
				}
			}

			return endLength;
		}

		public void SocketSend(string str)
		{
			byte[] sendBytes = Encoding.UTF7.GetBytes(str + "\r\n");
			try
			{
				if (clientSock.Connected == false)
				{
					SOCK_LOG("Please connects socket\n");
					return;
				}
				clientSock.Send(sendBytes, sendBytes.Length, SocketFlags.None);
				Thread.Sleep(500);
				sendBytes = Encoding.UTF7.GetBytes("GETS\r\n");
				clientSock.Send(sendBytes, sendBytes.Length, SocketFlags.None);
			}
			catch (Exception ex)
			{
				SOCK_LOG(ex.Message);
			}
		}
	}
}
