﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		// log 저장을 위해 path와 file 정의 /////////////////////////////////////////////////
		FileStream file_streamCal;
		StreamWriter psWriterCal;

		FileStream file_streamLog;
		StreamWriter psWriterLog;
		
		public FileStream Config_stream = null;
		public StreamWriter psWriterConfig = null;
		public StreamReader psReaderConfig = null;

		public string desktop_path;
		DateTime now;

		string cdcPort;
		////////////////////////////////////////////////////////////////////////////////////

		public void LogInit()
		{
			file_streamCal = null;
			psWriterCal = null;

			file_streamLog = null;
			psWriterLog = null;

			now = DateTime.Now;
			desktop_path = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
			desktop_path = desktop_path + "/CO2_CAL_JIG_Mon_V3_" + now.ToString("yyyy-MM-dd-H-mm-ss");
			DirectoryInfo di = new DirectoryInfo(desktop_path);
			if (di.Exists == false)
			{
				di.Create();
				OpenLogFiles();
			}
			else
			{
				MessageBox.Show("Log folder creation failed");
			}
		}

		public void OpenLogFiles()
		{
			string path;

			try
			{
				now = DateTime.Now;

				if (psWriterCal == null)
				{
					// folder에 log file 열기
					
					// CAL Log file
					path = desktop_path + "/calLog" + ".log";
					file_streamCal = new FileStream(path, FileMode.Create, FileAccess.Write);
					psWriterCal = new StreamWriter(file_streamCal, System.Text.Encoding.Default);

					psWriterCal.Write("CO2 Calibration Log File\n");

					// Default Log file
					path = desktop_path + "/defaultLog" + ".log";
					file_streamLog = new FileStream(path, FileMode.Create, FileAccess.Write);
					psWriterLog = new StreamWriter(file_streamLog, System.Text.Encoding.Default);
					psWriterLog.Write("CO2 Default Log File\n");
				}
				else
				{
					MessageBox.Show("Log file creation has problem...\n");
				}
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void CloseLogFiles()
		{
			try
			{
				if (psWriterCal == null)
				{
					LOG("psWriterCal Log file not opened\n");
				}
				else
				{
					psWriterCal.Flush();
					psWriterCal.Close();
					psWriterCal = null;
				}

				if (psWriterLog == null)
				{
					LOG("psWriterLog Log file not opened\n");
				}
				else
				{
					psWriterLog.Flush();
					psWriterLog.Close();
					psWriterLog = null;
				}
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void ConfigFileOpen()
		{
			StreamReader cfgReader = null;
			string str = null;

			try
			{
				cfgReader = new StreamReader("./tempus.cfg");
				str = cfgReader.ReadLine();
				if(str == "CDC Port")
				{
					cdcPort = cfgReader.ReadLine();
					LOG("CDC Port : " + cdcPort + "\n");
					UartOpen(cdcPort);
				}
				else
				{
					ERR("Config file error\n");
				}
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void ConfigFileSave(string name)
		{
			StreamWriter cfgWriter = null;

			try
			{
				cfgWriter = new StreamWriter("./tempus.cfg");
				cfgWriter.WriteLine("CDC Port");
				cfgWriter.WriteLine(name);
				//LOG(CallerName() + "tempus.cfg saved with " + name + "\n");

				cfgWriter.Flush();
				cfgWriter.Close();
				cfgWriter = null;
			}
			catch (Exception ex)  // CS0168
			{
				MessageBox.Show(CallerName() + " : " + ex.Message);
			}
		}

		public void _L(string str)
		{
			now = DateTime.Now;
			str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
			if (richTextBoxLog.InvokeRequired)
			{
				richTextBoxLog.Invoke(new MethodInvoker(delegate ()
				{
					richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
					richTextBoxLog.AppendText(str);
					richTextBoxLog.ScrollToCaret();
					psWriterLog.Write(str);
				}));
			}
			else
			{
				richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
				richTextBoxLog.AppendText(str);
				richTextBoxLog.ScrollToCaret();
				psWriterLog.Write(str);
			}
		}

		public void LOG(string str)
		{
			if(checkBoxLog.Checked == true)
				_L(str);
		}

		public void ERR(string str)
		{
			if(checkBoxErr.Checked == true)
			{
				now = DateTime.Now;
				str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
				if (richTextBoxLog.InvokeRequired)
				{
					richTextBoxLog.Invoke(new MethodInvoker(delegate ()
					{
						richTextBoxLog.SelectionColor = System.Drawing.Color.Red;
						richTextBoxLog.AppendText(str);
						richTextBoxLog.ScrollToCaret();
						psWriterLog.Write(str);
						richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
					}));
				}
				else
				{
					richTextBoxLog.SelectionColor = System.Drawing.Color.Red;
					richTextBoxLog.AppendText(str);
					richTextBoxLog.ScrollToCaret();
					psWriterLog.Write(str);
					richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
				}
			}
			
		}

		public void INFO(string str)
		{
			if (checkBoxInfo.Checked == true)
			{
				now = DateTime.Now;
				str = "[" + DateTime.Now.ToString("H:mm:ss") + "] " + str;
				if (richTextBoxLog.InvokeRequired)
				{
					richTextBoxLog.Invoke(new MethodInvoker(delegate ()
					{
						richTextBoxLog.SelectionColor = System.Drawing.Color.Blue;
						richTextBoxLog.AppendText(str);
						richTextBoxLog.ScrollToCaret();
						psWriterLog.Write(str);
						richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
					}));
				}
				else
				{
					richTextBoxLog.SelectionColor = System.Drawing.Color.Blue;
					richTextBoxLog.AppendText(str);
					richTextBoxLog.ScrollToCaret();
					psWriterLog.Write(str);
					richTextBoxLog.SelectionColor = System.Drawing.Color.Black;
				}
			}
		}
		
		public void CAL_LOG(string str)
		{
			if (richTextBoxCalLog.InvokeRequired)
			{
				richTextBoxCalLog.Invoke(new MethodInvoker(delegate ()
				{
					richTextBoxCalLog.AppendText("[" + DateTime.Now.ToString("H:mm:ss") + "] " + str);
					richTextBoxCalLog.ScrollToCaret();
					psWriterCal.Write(str);
				}));
			}
			else
			{
				richTextBoxCalLog.AppendText("[" + DateTime.Now.ToString("H:mm:ss") + "] " + str);
				richTextBoxCalLog.ScrollToCaret();
				psWriterCal.Write(str);
			}
		}

		public void SOCK_LOG(string str)
		{
			if (richTextBoxSockLog.InvokeRequired)
			{
				richTextBoxSockLog.Invoke(new MethodInvoker(delegate ()
				{
					richTextBoxSockLog.AppendText("[" + DateTime.Now.ToString("H:mm:ss") + "] " + str);
					richTextBoxSockLog.ScrollToCaret();
				}));
			}
			else
			{
				richTextBoxSockLog.AppendText("[" + DateTime.Now.ToString("H:mm:ss") + "] " + str);
				richTextBoxSockLog.ScrollToCaret();
			}
		}

		public void LOG_Clear()
		{
			richTextBoxLog.Clear();
		}

		public void CAL_LOG_Clear()
		{
			richTextBoxCalLog.Clear();
		}

		public void SOCK_LOG_Clear()
		{
			richTextBoxSockLog.Clear();
		}
	}
}
