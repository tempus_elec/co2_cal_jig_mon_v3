﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	class QueBuffer<T>
	{
		int Count;
		int Capacity;
		T[] buf;

		public QueBuffer(int cap)
		{
			buf = new T[cap];
			Count = 0;
			Capacity = cap;
		}

		public void EnQue(T q)
		{
			if (Count >= Capacity)
			{
				Count = 0;
			}
			buf[Count++] = q;
		}

		public T DeQue(int offset)
		{
			T q;

			if (offset > (Capacity - 1))
			{
				//return offset;
			}
			q = buf[offset];

			return q;
		}

		public void Clear()
		{
			Count = 0;
		}

		public int GetMax()
		{
			return Capacity;
		}

		public int GetCount()
		{
			return Count;
		}
	}

	struct TpsHandler
	{
		public QueBuffer<UInt32> dC_Q;
		public QueBuffer<UInt32> sC_Q;
		public QueBuffer<UInt32> dR_Q;
		public QueBuffer<UInt32> sR_Q;
		public QueBuffer<byte> TS_Q;
		public QueBuffer<byte> Temp_Q;
		public QueBuffer<UInt16> ppm_Q;
		public QueBuffer<int> flag_Q;
	}

	struct Module
	{
		public bool done;
	}
}
