﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CO2_CAL_JIG_Mon_V3
{
	public partial class Form1 : Form
	{
		public DateTime Delay(int MS)
		{
			DateTime ThisMoment = DateTime.Now;
			TimeSpan duration = new TimeSpan(0, 0, 0, 0, MS);
			DateTime AfterWards = ThisMoment.Add(duration);

			while (AfterWards >= ThisMoment)
			{
				System.Windows.Forms.Application.DoEvents();
				ThisMoment = DateTime.Now;
			}

			return DateTime.Now;
		}

		public string Str2Hex(string strData)
		{
			string resultHex = string.Empty;
			byte[] arr_byteStr = Encoding.Default.GetBytes(strData);

			foreach (byte byteStr in arr_byteStr)
				resultHex += string.Format("{0:X2}", byteStr);

			return resultHex;
		}

		public string ByteArrayToString(byte[] ba)
		{
			StringBuilder hex = new StringBuilder(ba.Length * 2);
			foreach (byte b in ba)
				hex.AppendFormat("{0:x2}", b);
			return hex.ToString();
		}

		public byte[] StrToByteArray(string str)
		{
			Dictionary<string, byte> hexindex = new Dictionary<string, byte>();
			for (int i = 0; i <= 255; i++)
				hexindex.Add(i.ToString("X2"), (byte)i);

			List<byte> hexres = new List<byte>();
			for (int i = 0; i < str.Length; i += 2)
				hexres.Add(hexindex[str.Substring(i, 2)]);

			return hexres.ToArray();
		}

		public UInt16 GetCRC16(byte[] temp, byte length)
		{
			UInt16 CRC16 = 0xffff;
			UInt16 POLY = 0xa001;
			byte MessageLength = length;
			UInt32 shift = 0;
			UInt16 k = 0;
			UInt16 code;
			UInt16 crcTemp;
			for (k = 0; k < MessageLength; k++)
			{
				code = (UInt16)temp[k];
				CRC16 = (UInt16)(CRC16 ^ code);
				shift = 0;
				while (shift <= 7)
				{
					crcTemp = (UInt16)(CRC16 & 0x01);
					if (crcTemp != 0)
					{
						CRC16 = (UInt16)(CRC16 >> 1);
						CRC16 = (UInt16)(CRC16 ^ POLY);

					}
					else
					{
						CRC16 = (UInt16)(CRC16 >> 1);
					}
					shift++;
				}
			}
			return CRC16;
		}

		public string ConvertStringArrayToString(string[] array)
		{
			//
			// Concatenate all the elements into a StringBuilder.
			//
			StringBuilder strinbuilder = new StringBuilder();
			foreach (string value in array)
			{
				strinbuilder.Append(value);
				strinbuilder.Append(' ');
			}
			return strinbuilder.ToString();
		}

		// 바이트 배열을 String으로 변환 
		private string ByteToString(byte[] strByte)
		{
			string str = Encoding.Default.GetString(strByte);
			return str;
		}

		// String을 바이트 배열로 변환 
		private byte[] StringToByte(string str)
		{
			byte[] StrByte = Encoding.UTF8.GetBytes(str); return StrByte;
		}

		private byte get1ByteFromBytesArray(byte[] arr, UInt16 offset)
		{
			byte value = arr[offset];

			return value;
		}

		private UInt16 get2ByteFromBytesArray(byte[] arr, UInt16 offset)
		{
			UInt16 value = (UInt16)(arr[offset + 1] << 8 | arr[offset]);

			return value;
		}

		private UInt32 get4ByteFromBytesArray(byte[] arr, UInt16 offset)
		{
			UInt32 value = (UInt32)(arr[offset + 3] << 24 | arr[offset + 2] << 16 | arr[offset + 1] << 8 | arr[offset]);

			return value;
		}

		private float getFloatFromBytesArray(byte[] arr, UInt16 offset)
		{
			float value = (float)(arr[offset + 3] << 24 | arr[offset + 2] << 16 | arr[offset + 1] << 8 | arr[offset]);

			return value;
		}

		public string CallerName([CallerMemberName] string callerName = "")
		{
			return callerName;
		}
	}
}
